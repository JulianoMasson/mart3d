#include "MeshIO.h"

#include <Windows.h>

#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>

#include <vtkAbstractPolyDataReader.h>
#include <vtkPLYReader.h>
#include <vtkOBJReader.h>
#include <vtkOBJImporter.h>
#include <vtkVRMLImporter.h>
#include <vtkOBJExporter.h>

#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtksys/SystemTools.hxx>

#include <wx/log.h>

#include "Utils.h"
#include "Mesh.h"
#include "OutputErrorWindow.h"
#include "PLYReader.h"
#include "PLYWriter.h"

bool MeshIO::getActorsFromFile(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>>& actors, std::vector<std::string>& textureNames)
{
	if (!Utils::exists(filePath))
	{
		return 0;
	}
	std::string extension = Utils::getFileExtension(filePath);
	if (extension == "ply" || extension == "PLY")
	{
		return getActorsFromPLY(filePath, actors);
	}
	else if (extension == "obj" || extension == "OBJ")
	{
		std::string filenameMTL = getMTLFilenameFromOBJ(filePath);
		if (filenameMTL != "")
		{
			filenameMTL = Utils::getPath(filePath) + filenameMTL;
			if (!Utils::exists(filenameMTL))
			{
				return getActorsFromOBJ(filePath, actors);
			}
			else
			{
				textureNames = getTextureNamesFromMTL(filenameMTL);
				if (textureNames.size() == 0)
				{
					wxLogError(wxString("No texture inside " + filenameMTL + " loading obj without texture"));
					return getActorsFromOBJ(filePath, actors);
				}
				return getActorsFromTexturedOBJ(filePath, filenameMTL, actors);
			}
		}
		else
		{
			return getActorsFromOBJ(filePath, actors);
		}
	}
	else if (extension == "wrl" || extension == "WRL")
	{
		return getActorsFromWRL(filePath, actors);
	}
	else
	{
		wxLogError(wxString("File extension " + extension + " not supported"));
	}
	return 0;
}

bool MeshIO::exportMesh(const std::string& filePath, Mesh * mesh)
{
	if (mesh->GetHasTexture())
	{
		return saveOBJ(filePath, mesh);
	}
	else
	{
		return savePLY(filePath, mesh);
	}
}

bool MeshIO::getActorsFromPLY(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>>& actors)
{
	PLYReader plyReader;
	vtkSmartPointer<vtkPolyData> polyData = plyReader.read(filePath);
	if (!polyData)
	{
		return 0;
	}
	vtkNew<vtkPolyDataMapper> mapper;
	if (polyData->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
	{
		vtkNew<vtkCellArray> vertices;
		vertices->InsertNextCell(polyData->GetPoints()->GetNumberOfPoints());
		unsigned int numberOfPoints = polyData->GetPoints()->GetNumberOfPoints();
		for (vtkIdType i = 0; i < numberOfPoints; i++)
		{
			vertices->InsertCellPoint(i);
		}
		polyData->SetVerts(vertices);
		mapper->ScalarVisibilityOn();
		mapper->SetScalarModeToUsePointData();
	}
	mapper->SetInputData(polyData);
	vtkNew<vtkActor> actor;
	actor->SetMapper(mapper);
	actors.reserve(1);
	actors.emplace_back(actor);
	return 1;
}

bool MeshIO::getActorsFromOBJ(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>>& actors)
{
	vtkNew<vtkOBJReader> reader;
	reader->SetFileName(filePath.c_str());
	reader->Update();
	if (!OutputErrorWindow::checkOutputErrorWindow())
	{
		return 0;
	}
	vtkSmartPointer<vtkPolyData> polyData = reader->GetOutput();
	if (!polyData)
	{
		return 0;
	}
	vtkNew<vtkPolyDataMapper> mapper;
	if (polyData->GetPolys()->GetNumberOfCells() == 0)//it is a point cloud
	{
		vtkNew<vtkCellArray> vertices;
		vertices->InsertNextCell(polyData->GetPoints()->GetNumberOfPoints());
		unsigned int numberOfPoints = polyData->GetPoints()->GetNumberOfPoints();
		for (vtkIdType i = 0; i < numberOfPoints; i++)
		{
			vertices->InsertCellPoint(i);
		}
		polyData->SetVerts(vertices);
		mapper->ScalarVisibilityOn();
		mapper->SetScalarModeToUsePointData();
	}
	mapper->SetInputData(polyData);
	vtkNew<vtkActor> actor;
	actor->SetMapper(mapper);
	actors.reserve(1);
	actors.emplace_back(actor);
	return 1;
}

bool MeshIO::getActorsFromTexturedOBJ(const std::string& filePathOBJ, const std::string& filePathMTL, std::vector<vtkSmartPointer<vtkActor>>& actors)
{
	//Create the temp render window
	vtkNew<vtkRenderWindow> renderWindow;
	vtkNew<vtkOBJImporter> objImporter;
	objImporter->SetFileName(filePathOBJ.c_str());
	objImporter->SetFileNameMTL(filePathMTL.c_str());
	objImporter->SetTexturePath(Utils::getPath(filePathOBJ, false).c_str());
	objImporter->SetRenderWindow(renderWindow);
	objImporter->Update();
	if (!OutputErrorWindow::checkOutputErrorWindow())
	{
		return 0;
	}
	vtkSmartPointer<vtkRenderer> renderer = renderWindow->GetRenderers()->GetFirstRenderer();
	vtkSmartPointer<vtkActorCollection> actorCollection = renderer->GetActors();
	actorCollection->InitTraversal();
	const unsigned int numberOfItems = actorCollection->GetNumberOfItems();
	actors.reserve(numberOfItems);
	for (vtkIdType i = 0; i < numberOfItems; i++)
	{
		vtkNew<vtkActor> actor;
		actor->ShallowCopy(actorCollection->GetNextActor());
		actors.emplace_back(actor);
	}
	actorCollection->InitTraversal();
	for (vtkIdType i = 0; i < numberOfItems; i++)
	{
		renderer->RemoveActor(actorCollection->GetNextActor());
	}
	return 1;
}

std::string MeshIO::getMTLFilenameFromOBJ(const std::string& filename)
{
	std::ifstream myfile(filename);
	std::string line;
	std::string mtlFile = "";
	if (myfile.is_open())
	{
		while (getline(myfile, line, '\n'))
		{
			if (line.find("mtllib ", 0) != std::string::npos)
			{
				mtlFile = vtksys::SystemTools::GetFilenameName(line.substr(line.find(' ', 0) + 1));
				break;
			}
			else if (line[0] == 'v')//the vertex list started, there is no mtllib
			{
				break;
			}
		}
	}
	myfile.close();
	return mtlFile;
}

std::vector<std::string> MeshIO::getTextureNamesFromMTL(const std::string& filename)
{
	std::ifstream mtlFile(filename);
	std::string line;
	std::vector<std::string> textureNames;
	if (mtlFile.is_open())
	{
		while (getline(mtlFile, line, '\n'))
		{
			if (line.find("map_Kd ", 0) != std::string::npos)
			{
				textureNames.push_back(line.substr(line.find(' ', 0) + 1));
			}
		}
	}
	mtlFile.close();
	return textureNames;
}

bool MeshIO::getActorsFromWRL(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>>& actors)
{
	//Create the temp render window
	vtkNew<vtkRenderWindow> renderWindow;
	vtkNew<vtkVRMLImporter> vrlmImporter;
	vrlmImporter->SetFileName(filePath.c_str());
	vrlmImporter->SetRenderWindow(renderWindow);
	vrlmImporter->Update();
	if (!OutputErrorWindow::checkOutputErrorWindow())
	{
		return 0;
	}
	vtkSmartPointer<vtkRenderer> renderer = renderWindow->GetRenderers()->GetFirstRenderer();
	vtkSmartPointer<vtkActorCollection> actorCollection = renderer->GetActors();
	actorCollection->InitTraversal();
	const unsigned int numberOfItems = actorCollection->GetNumberOfItems();
	actors.reserve(numberOfItems);
	for (vtkIdType i = 0; i < numberOfItems; i++)
	{
		vtkNew<vtkActor> actor;
		actor->ShallowCopy(actorCollection->GetNextActor());
		actors.emplace_back(actor);
	}
	actorCollection->InitTraversal();
	for (vtkIdType i = 0; i < numberOfItems; i++)
	{
		renderer->RemoveActor(actorCollection->GetNextActor());
	}
	return 1;
}

bool MeshIO::savePLY(const std::string& filename, Mesh * mesh)
{
	vtkSmartPointer<vtkPolyData> polyData = mesh->GetPolyData();
	if (!polyData)
	{
		return 0;
	}
	return PLYWriter::Write(filename, polyData, !mesh->GetIsPointCloud());
}

bool MeshIO::saveOBJ(const std::string& filename, Mesh * mesh)
{
	vtkNew<vtkRenderer> rendererAux;
	vtkNew<vtkRenderWindow> renWinAux;
	renWinAux->AddRenderer(rendererAux);
	for (auto actor : mesh->actors)
	{
		rendererAux->AddActor(actor);
	}
	vtkNew<vtkOBJExporter> objExport;
	objExport->SetRenderWindow(renWinAux);
	std::string baseFilename = filename.substr(0, filename.size() - 4);
	objExport->SetFilePrefix(baseFilename.c_str());
	objExport->Write();
	for (auto actor : mesh->actors)
	{
		rendererAux->RemoveActor(actor);
	}
	if (!Utils::exists(baseFilename + ".mtl"))
	{
		wxLogError("No mtl generated");
		return 0;
	}
	AdjustTextureNamesMTL(baseFilename + ".mtl");
	return 1;
}

void MeshIO::AdjustTextureNamesMTL(const std::string& mtlFilename)
{
	std::ifstream mtlFile(mtlFilename);
	std::string line;
	std::string newFile;
	if (mtlFile.is_open())
	{
		while (getline(mtlFile, line, '\n'))
		{
			if (line.find("map_Kd", 0) != std::string::npos)
			{
				const auto fileName = Utils::getFileName(line.substr(line.find_first_of(' ')));
				newFile += "map_Kd " + fileName + "\n";
			}
			else
			{
				newFile += line + "\n";
			}
		}
		mtlFile.close();
		std::ofstream newMtlFile(mtlFilename);
		if (newMtlFile.is_open())
		{
			newMtlFile << newFile;
		}
		newMtlFile.close();
	}
}
