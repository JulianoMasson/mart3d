#include "GPSCoordinatesDialog.h"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>
#include <wx/button.h>

GPSCoordinatesDialog::GPSCoordinatesDialog(wxWindow * parent, wxWindowID id, const wxString & title,
	const wxPoint & pos, const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	auto bSizer = new wxBoxSizer(wxVERTICAL);

	auto fgSizer = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizer->SetFlexibleDirection(wxBOTH);
	fgSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Latitude"), 0, wxALL, 5);
	spinLatitude = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, -90.0, 90.0, 0.0);
	fgSizer->Add(spinLatitude, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Longitude"), 0, wxALL, 5);
	spinLongitude = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, -180.0, 180.0, 0.0);
	fgSizer->Add(spinLongitude, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Altitude"), 0, wxALL, 5);
	spinAltitude = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, std::numeric_limits<double>::min(), std::numeric_limits<double>::max(), 0.0);
	fgSizer->Add(spinAltitude, 0, wxALL, 5);

	bSizer->Add(fgSizer, 0, wxEXPAND, 5);

	auto bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL, 5);

	bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL, 5);

	bSizer->Add(bSizerBts, 0, wxALIGN_RIGHT, 5);
	
	this->SetSizer(bSizer);
	this->Layout();

	this->Centre(wxBOTH);
}

GPSCoordinatesDialog::~GPSCoordinatesDialog()
{
}

double GPSCoordinatesDialog::GetLatitude()
{
	return spinLatitude->GetValue();
}

double GPSCoordinatesDialog::GetLongitude()
{
	return spinLongitude->GetValue();
}

double GPSCoordinatesDialog::GetAltitude()
{
	return spinAltitude->GetValue();;
}
