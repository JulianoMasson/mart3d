#pragma once
#include <wx/dialog.h>

#include "SSDRecon.h"

class wxChoice;

class SSDDialog : public wxDialog
{
public:
	SSDDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "SSD", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(300, 140), long style = wxDEFAULT_DIALOG_STYLE);
	~SSDDialog();

	SSDRecon::Options getOptions() const;

private:
	DECLARE_EVENT_TABLE()

	void OnBtDefault(wxCommandEvent& WXUNUSED(event));
	
	wxSpinCtrl* spinDepth;
	wxChoice* choiceBoundary;
};
enum EnumSSDDialog
{
	idBtDefaultSSDDialog
};