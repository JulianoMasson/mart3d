#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

class vtkPropPicker;
class ImplicitPlaneWidget;
class vtkActor;
class vtkPoints;
class vtkPolyData;
class vtkPlaneCollection;
class LineWidget;
class wxTreeListCtrl;
class Project;
class Mesh;

class VolumeTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static VolumeTool *New();

	vtkTypeMacro(VolumeTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}


	void setMesh(Mesh* mesh){ this->mesh = mesh; };
	void setProject(Project* project) { this->project = project; };

	void enterKeyPressed();

protected:
	VolumeTool();
	~VolumeTool();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	// Controlling ivars
	void UpdateRepresentation();

	void OnKeyPress();
	void OnRightButton();

	// Create cutting planes from a sequence of points that represent a polygon
	// - points, the polygon points (they will be modified)
	// - vector, the vector that describe the direction and height that the volume should be created
	static vtkSmartPointer<vtkPlaneCollection> createCuttingPlanes(vtkSmartPointer<vtkPoints> points, const double vector[3]);
	// Create a cutting volume from a sequence of points that represent a polygon
	// - pointsIn, the polygon points
	// - vector, the vector that describe the direction and height that the volume should be created
	vtkSmartPointer<vtkPolyData> createCuttingVolume(vtkSmartPointer<vtkPoints> pointsIn, const double vector[3]);
	// Get the triangles that make the base of the selection
	vtkSmartPointer<vtkPolyData> getBaseTriangles();
	// Get the normal used to define the direction the cutting volume should grow
	void getNormal(double normal[3]);
	// Mesh used to compute the volume
	vtkSmartPointer<vtkPolyData> meshPolyData = nullptr;
	void computeVolume();

	// Plane widget
	void createPlaneWidget();
	vtkSmartPointer<ImplicitPlaneWidget> planeWidget = nullptr;
	vtkSmartPointer<vtkActor> planeActor = nullptr;

	vtkSmartPointer<vtkPropPicker> propPicker = nullptr;
	vtkSmartPointer<vtkActor> cuttingVolumeActor = nullptr;
	double polygonHeight = 1.0;
	double polygonHeightIncrement = 0.1;

	// Line Widget
	void createLineWidget();
	vtkSmartPointer<LineWidget> lineWidget = nullptr;

	Mesh* mesh = nullptr;
	Project* project = nullptr;

	bool updatePolygon = false;
	bool invertSelection = false;

private:
	VolumeTool(const VolumeTool&) VTK_DELETE_FUNCTION;
	void operator=(const VolumeTool&) VTK_DELETE_FUNCTION;
};
