#include "Reconstruction.h"

#include <wx/filename.h>
#include <wx/log.h>

#include "HelperCOLMAP.h"
#include "HelperMeshRecon.h"
#include "HelperTexRecon.h"
#include "HelperAVTexturing.h"
#include "ReconstructionLog.h"
#include "ConfigurationDialog.h"
#include "ImageIO.h"
#include "Utils.h"

bool Reconstruction::Reconstruct(const std::string & projectFolder, bool generateTexture)
{
	const auto imagesFolder = projectFolder + "\\images";
	//Creating directories
	const auto tempDir = projectFolder + "\\temp";
	const auto reconstructionDir = projectFolder + "\\3DData";
	if (!Utils::CreateDir(tempDir) || !Utils::CreateDir(reconstructionDir))
	{
		return 0;
	}
	//Files
	const auto pointCloudPath = reconstructionDir + "\\PointCloud.ply";
	const auto surfacePath = reconstructionDir + "\\Surface.ply";
	//Start processing
	//Log
	ReconstructionLog log(projectFolder + "\\log.txt");
	auto bool2String = [](bool flag) { if (flag) { return "Yes"; } return "No"; };
	log.write("Generate mesh - Yes");
	log.write("Generate texture - " + ((std::string)bool2String(generateTexture)));
	log.addSeparator();
	//SFM
	std::string nvmPath = tempDir + "\\cameras.nvm";
	if (!Reconstruction::SFM(imagesFolder, nvmPath, log))
	{
		wxLogError("Error during SFM");
		return 0;
	}
	//Copy the nvm file to the project folder
	if (!wxCopyFile(nvmPath, projectFolder + "\\cameras.nvm"))
	{
		wxLogError("Error moving the cameras file");
		return 0;
	}
	//Meshing
	if (!Reconstruction::Meshing(tempDir, imagesFolder, nvmPath, pointCloudPath, surfacePath, log))
	{
		wxLogError("Error during meshing");
		return 0;
	}
	if (generateTexture)
	{
		const auto texturizationDir = reconstructionDir + "\\TexturedSurface";
		if (!Utils::CreateDir(texturizationDir))
		{
			return 0;
		}
		const auto texturedSurfacePath = texturizationDir + "\\TexturedSurface.obj";
		if (!Reconstruction::Texturization(surfacePath, nvmPath, texturedSurfacePath, log))
		{
			wxLogError("Error during texturization");
			return 0;
		}
	}
	//Remove the temp dir
	//wxRmdir(tempDir, wxPATH_RMDIR_RECURSIVE);
	return 1;
}

bool Reconstruction::SFM(const std::string & imagesPath, const std::string & nvmPath, ReconstructionLog & log)
{
	log.write("Started SFM", true, true);
	if (!HelperCOLMAP::executeSparse(imagesPath, nvmPath))
	{
		log.write("Error during SFM", true, true);
		return 0;
	}
	log.write("Finished SFM", true, true);
	log.addSeparator();
	return 1;
}

bool Reconstruction::Meshing(const std::string & tempDir, const std::string & imagesPath, const std::string & nvmPath,
	const std::string & pointCloudPath, const std::string & meshPath, ReconstructionLog & log)
{
	//MeshRecon
	if (ImageIO::GetNumberOfCameras(nvmPath) < 100 && !ConfigurationDialog::getForceDenseCOLMAP())
	{
		log.write("Started NVM2SFM", true, true);
		std::string sfmPath = nvmPath.substr(0, nvmPath.size() - 4) + ".sfm";
		if (!HelperMeshRecon::executeNVM2SFM(nvmPath, sfmPath, ConfigurationDialog::getBoundingBox()))
		{
			log.write("Error during NVM2SFM", true, true);
			return 0;
		}
		log.write("Finished NVM2SFM", true, true);
		log.addSeparator();
		log.write("Started MeshRecon", true, true);
		if (!HelperMeshRecon::executeMeshRecon(tempDir, sfmPath, meshPath, ConfigurationDialog::getLevelOfDetails()))
		{
			log.write("Error during MeshRecon", true, true);
			return 0;
		}
		log.write("Finished MeshRecon", true, true);
		log.addSeparator();
	}
	else //COLMAP
	{
		log.write("Started COLMAP dense reconstruction", true, true);
		if (!HelperCOLMAP::executeDense(imagesPath, nvmPath))
		{
			log.write("Error during COLMAP dense reconstruction", true, true);
			return 0;
		}
		log.write("Finished COLMAP dense reconstruction", true, true);
		log.addSeparator();
		//Move the files
		std::string originalPointCloud = wxPathOnly(nvmPath) + "/dense/0/fused.ply";
		if (!wxCopyFile(originalPointCloud, pointCloudPath))
		{
			log.write("Error coping file " + originalPointCloud, true, true);
			return 0;
		}
		if (!wxRemoveFile(originalPointCloud))
		{
			log.write("Error removing file " + originalPointCloud, true, true);
			return 0;
		}
		std::string originalPLY = wxPathOnly(nvmPath) + "/dense/0/meshed-" + ConfigurationDialog::getMesher() + ".ply";
		if (!wxCopyFile(originalPLY, meshPath))
		{
			log.write("Error coping file " + originalPLY, true, true);
			return 0;
		}
		if (!wxRemoveFile(originalPLY))
		{
			log.write("Error removing file " + originalPLY, true, true);
			return 0;
		}
	}
	return 1;
}

bool Reconstruction::Texturization(const std::string & meshPath, const std::string & camerasPath, const std::string & outputPath, ReconstructionLog & log)
{
	//TexRecon
	if (ConfigurationDialog::getForceTexReconTexture())
	{
		log.write("Started TexRecon", true, true);
		if (!HelperTexRecon::executeTexRecon(camerasPath, meshPath, outputPath, ConfigurationDialog::getTexReconOptions()))
		{
			log.write("Error during TexRecon", true, true);
			return 0;
		}
		log.write("Finished TexRecon", true, true);
	}
	else//AVTexturing
	{
		wxBeginBusyCursor();
		log.write("Started AVTexturing", true, true);
		if (!HelperAVTexturing::computeAVTexturization(meshPath, camerasPath, outputPath))
		{
			log.write("Error during AVTexturing", true, true);
			wxEndBusyCursor();
			return 0;
		}
		log.write("Finished AVTexturing", true, true);
		wxEndBusyCursor();
	}
	log.addSeparator();
	return 1;
}
