#pragma once
#include <wx/dialog.h>
#include <vtkSmartPointer.h>

class vtkRenderer;
class vtkTransform;
class wxSpinCtrlDouble;
class wxCheckBox;
class wxButton;
class wxTreeListCtrl;
class Project;
class Mesh;

class ICPDialog : public wxDialog
{
public:
	ICPDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "ICP",
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(355, 320), long style = wxDEFAULT_DIALOG_STYLE);
	~ICPDialog();

	void SetSourceData(Project* project, Mesh* mesh) 
	{ 
		projectSource = project;
		meshSource = mesh;
	};
	void SetTargetData(Project* project, Mesh* mesh) 
	{ 
		projectTarget = project;
		meshTarget = mesh; 
	};
	void SetRenderer(vtkSmartPointer<vtkRenderer> renderer) { this->renderer = renderer; };
	void SetTree(wxTreeListCtrl* treeMesh) { this->treeMesh = treeMesh; };
	vtkSmartPointer<vtkTransform> GetTransform() const { return T; };

private:
	DECLARE_EVENT_TABLE()

	void OnCkUseMeanDistance(wxCommandEvent& WXUNUSED(event));
	void OnApplyTransform(wxCommandEvent& WXUNUSED(event));
	void OnRevertTransform(wxCommandEvent& WXUNUSED(event));
	void OnBtStartICP(wxCommandEvent& WXUNUSED(event));
	void OnBtChangeMeshOrder(wxCommandEvent& WXUNUSED(event));
	void OnClose(wxCloseEvent& WXUNUSED(event));
	
	wxSpinCtrl* spinMaxNumberOfIterations;
	wxSpinCtrl* spinMaxNumberOfLandmarks;
	wxSpinCtrlDouble* spinMaxMeanDistance;
	wxSpinCtrlDouble* spinClosestPointMaxMeanDistance;
	wxCheckBox* ckUseMeanDistance;
	wxCheckBox* ckStartMatchingCentroids;
	wxCheckBox* ckDelimitByMinimumBoundingBox;

	wxButton* btApplyTransform;
	wxButton* btRevertTransform;

	vtkSmartPointer<vtkRenderer> renderer = nullptr;
	vtkSmartPointer<vtkTransform> T = nullptr;

	Mesh* meshSource = nullptr;
	Mesh* meshTarget = nullptr;
	Project* projectSource = nullptr;
	Project* projectTarget = nullptr;
	wxTreeListCtrl* treeMesh = nullptr;
};

enum EnumICPDialog
{
	idBtApplyTransform,
	idBtRevertTransform,
	idBtStartICP,
	idBtChangeMeshOrder,
	idCkUseMeanDistance
};