#include "Project.h"

#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkMath.h>
#include <vtkRegularPolygonSource.h>
#include <vtkPolyDataMapper.h>

#include <wx/log.h>
#include <wx/filefn.h> 
#include <wx/msgdlg.h>
#include <wx/dirdlg.h>
#include <wx/dir.h>

#include "Calibration.h"
#include "Camera.h"
#include "Volume.h"
#include "Draw.h"
#include "Utils.h"
#include "Mesh.h"
#include "ImageIO.h"

Project::Project(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkRenderer> renderer360, wxTreeListCtrl * tree, const std::string & filePath)
	: renderer(renderer), renderer360(renderer360), tree(tree), isLoaded(false), is360(false), projectPath(filePath), calibration(new Calibration()), visibility(true), modified(false)
{
	if (!Utils::exists(filePath))
	{
		return;
	}
	std::ifstream projectFile(filePath);
	std::string line;
	if (projectFile.is_open())
	{
		while (std::getline(projectFile, line, '\n'))
		{
			if (line.substr(0, line.find_first_of('=')) == "is360")
			{
				is360 = std::stoi(line.substr(line.find_first_of('=') + 1, line.size()));
			}
		}
		projectFile.close();
	}
	else
	{
		wxLogError("Unable to open the project file");
		return;
	}
	// Create the project tree item
	this->SetTreeItem(Utils::addItemToTree(tree, tree->GetRootItem(), Utils::getFileName(filePath, false)));
	projectFolder = wxPathOnly(filePath);
	// Load calibration
	calibration->load(projectFolder + "\\calibration.calib");
	// Load meshes
	LoadMeshes();
	// Load cameras
	LoadCameras();
	isLoaded = true;
}

Project::~Project()
{
	for (auto mesh : meshes)
	{
		delete mesh;
	}
	meshes.clear();
	meshNames.clear();
	DestructCameras();
	DestructVolumes();
	if (treeItem && tree)
	{
		tree->DeleteItem(treeItem);
		treeItem = nullptr;
	}
}

void Project::SetVisibility(bool visibility)
{
	for (auto mesh : meshes)
	{
		mesh->SetVisibility(visibility);
		tree->CheckItem(mesh->GetTreeItem(), (wxCheckBoxState)visibility);
	}
	SetCamerasVisibility(visibility);
	SetVolumesVisibility(visibility);
	this->visibility = visibility;
}

std::string Project::GetName() const
{
	return Utils::getFileName(projectPath, false);
}

std::string Project::GetPath() const
{
	return projectPath;
}

void Project::SetModified(bool modified)
{
	this->modified = modified;
	if (tree)
	{
		if (modified)
		{
			tree->SetItemText(GetTreeItem(), Utils::getFileName(projectPath, false) + "*");
		}
		else
		{
			tree->SetItemText(GetTreeItem(), Utils::getFileName(projectPath, false));
		}
	}
}

bool Project::GetModified() const
{
	for (const auto& mesh : meshes)
	{
		if (mesh->GetModified())
		{
			return true;
		}
	}
	return modified;
}

void Project::SetRepresentation(int representation)
{
	for (auto mesh : meshes)
	{
		for (auto actor : mesh->actors)
		{
			actor->GetProperty()->SetRepresentation(representation);
		}
	}
}

void Project::SetCalibration(Calibration * calibration)
{
	if (this->calibration)
	{
		delete this->calibration;
	}
	this->calibration = calibration;
};
void Project::DestructCameras()
{
	for (int i = 0; i < cameras.size(); i++)
	{
		cameras.at(i)->destruct(renderer, renderer360, tree);
		delete cameras.at(i);
	}
	cameras.clear();
	if (treeItemCameras)
	{
		tree->DeleteItem(treeItemCameras);
		treeItemCameras = nullptr;
	}
}

void Project::DeleteCamera(int cameraIndex)
{
	cameras.at(cameraIndex)->destruct(renderer, renderer360, tree);
	delete cameras.at(cameraIndex);
	cameras.erase(cameras.begin() + cameraIndex);
}

void Project::SetCamerasVisibility(bool visibility)
{
	if (GetTreeItemCameras())
	{
		tree->CheckItem(GetTreeItemCameras(), (wxCheckBoxState)visibility);
	}
	for (auto camera : cameras)
	{
		camera->setVisibility(visibility);
		if (camera->getListItemCamera())
		{
			tree->CheckItem(camera->getListItemCamera(), (wxCheckBoxState)visibility);
		}
	}
}

bool Project::GetCamerasVisibility() const
{
	for (const auto &camera : cameras)
	{
		if (camera->getVisibility())
		{
			return true;
		}
	}
	return false;
}

void Project::DestructVolumes()
{
	for (int i = 0; i < volumes.size(); i++)
	{
		volumes.at(i)->destruct(renderer, tree);
		delete volumes.at(i);
	}
	volumes.clear();
	if (treeItemVolumes)
	{
		tree->DeleteItem(treeItemVolumes);
		treeItemVolumes = nullptr;
	}
}

void Project::DeleteVolume(int volumeIndex)
{
	volumes.at(volumeIndex)->destruct(renderer, tree);
	delete volumes.at(volumeIndex);
	volumes.erase(volumes.begin() + volumeIndex);
}

void Project::AddVolume(vtkSmartPointer<vtkActor> actor,
	vtkSmartPointer<vtkCaptionActor2D> textActor, vtkSmartPointer<vtkPolyData> holePolyData)
{
	// Create the volumes tree item, if it does not exist
	if (!GetTreeItemVolumes())
	{
		SetTreeItemVolumes(tree->AppendItem(GetTreeItem(), "Volumes"));
	}
	volumes.emplace_back(new Volume(actor, textActor, qtdVolumes,
		calibration->getScaleFactor(),
		calibration->getMeasureUnit(),
		holePolyData));
	qtdVolumes++;
	// Create the volume tree item
	wxString name;
	name << "#" << qtdVolumes;
	volumes.back()->setListItem(tree->AppendItem(GetTreeItemVolumes(), name));
	tree->CheckItem(GetTreeItemVolumes(), wxCHK_CHECKED);
	tree->CheckItem(volumes.back()->getListItem(), wxCHK_CHECKED);
}

void Project::SetVolumesVisibility(bool visible)
{
	if (GetTreeItemVolumes())
	{
		tree->CheckItem(GetTreeItemVolumes(), (wxCheckBoxState)visibility);
	}
	for (auto volume : volumes)
	{
		volume->setVisibility(visible);
		if (volume->getListItem())
		{
			tree->CheckItem(volume->getListItem(), (wxCheckBoxState)visibility);
		}
		
	}
}

bool Project::GetVolumesVisibility() const
{
	for (auto const &volume : volumes)
	{
		if (volume->getVisibility())
		{
			return true;
		}
	}
	return false;
}

void Project::Update360ClickPoints(Camera * newCam)
{
	for (auto cam : cameras)
	{
		renderer360->RemoveActor(cam->image360ClickPoint);
		cam->image360ClickPoint = nullptr;
	}
	if (!newCam)
	{
		return;
	}
	double c[3];
	vtkMath::Subtract(newCam->cameraPoints[0], newCam->cameraPoints[5], c);
	double radius = vtkMath::Norm(c);
	for (auto cam : cameras)
	{
		if (cam == newCam)// && newCam->getDistanceBetweenCameraCenters(cam) < 20)
		{
			continue;
		}
		double point[3];
		Utils::createDoubleVector(cam->cameraPoints[0], point);
		point[1] = 1.3;
		// Create a circle
		vtkNew<vtkRegularPolygonSource> polygonSource;
		polygonSource->SetNumberOfSides(50);
		polygonSource->SetRadius(0.2);
		polygonSource->SetCenter(point);
		if (newCam->getDistanceBetweenCameraCenters(cam) > 10.0)
		{
			double normal[3];
			vtkMath::Subtract(point, newCam->cameraPoints[0], normal);
			vtkMath::Normalize(normal);
			polygonSource->SetNormal(normal);
		}
		else
		{
			polygonSource->SetNormal(0, 1, 0);
		}
		vtkNew<vtkPolyDataMapper> mapper;
		mapper->SetInputConnection(polygonSource->GetOutputPort());
		vtkNew<vtkActor> actor;
		actor->SetMapper(mapper);
		actor->GetProperty()->EdgeVisibilityOn();
		actor->GetProperty()->SetLineWidth(1.0);
		actor->GetProperty()->SetLighting(false);
		renderer360->AddActor(actor);
		cam->image360ClickPoint = actor;
	}
}

void Project::LoadMeshes()
{
	wxArrayString files;
	if (wxDir::GetAllFiles(projectFolder + "\\3DData", &files) == 0)
	{
		return;
	}
	for (const auto& filePath : files)
	{
		const auto path = filePath.ToStdString();
		const auto extension = Utils::toUpper(Utils::getFileExtension(path));
		if (extension == "PLY" || extension == "OBJ")
		{
			auto mesh = new Mesh(renderer, tree, path);
			if (!mesh->GetIsLoaded())
			{
				delete mesh;
				continue;
			}
			meshNames.insert(std::make_pair(Utils::getFileName(path, false), 0));
			mesh->SetTreeItem(Utils::addItemToTree(tree, GetTreeItem(), Utils::getFileName(path, false), wxCHK_UNCHECKED));
			if (mesh->GetHasColor() || mesh->GetHasTexture())
			{
				mesh->SetTreeItemTexture(Utils::addItemToTree(tree, mesh->GetTreeItem(), "Texture", wxCHK_CHECKED));
			}
			mesh->SetVisibility(false);
			meshes.emplace_back(mesh);
		}
	}
}

void Project::LoadCameras()
{
	auto path = projectFolder + "\\cameras.sfm";
	if (!Utils::exists(path))
	{
		path = projectFolder + "\\cameras.nvm";
		if (!Utils::exists(path))
		{
			return;
		}
	}
	std::vector<std::string> imagePaths;
	if (!ImageIO::getCamerasFileImagePaths(path, imagePaths))
	{
		return;
	}
	//Test to see if the paths inside the SFM/NVM are correct
	if (!ImageIO::getImagePathsExist(imagePaths))
	{
		// Try the images folder, if the project was moved
		std::string newImageDir = projectFolder + "\\images";
		if (!ImageIO::getImagePathsExist(imagePaths, newImageDir))
		{
			wxMessageBox("Please select the correct image folder", "Error", wxICON_ERROR);
			wxDirDialog imageDialog(nullptr, "Choose the image folder", "", wxDD_DEFAULT_STYLE);
			//Just stop when all the images are in the selected folder
			bool insist = 0;
			do
			{
				if (insist)
				{
					wxMessageBox("The folder still wrong, please select the correct image folder", "Warning", wxICON_WARNING);
				}
				insist = 1;
				if (imageDialog.ShowModal() == wxID_OK)
				{
					newImageDir = imageDialog.GetPath();
				}
				else
				{
					wxLogError(wxString("Error loading " + Utils::getFileName(path)));
					return;
				}
			} while (!ImageIO::getImagePathsExist(imagePaths, newImageDir));
		}
		ImageIO::replaceCamerasFileImageDir(path, newImageDir);
	}
	//Now the SFM/NVM is correct, lets load the cameras
	if (!ImageIO::loadCameraParameters(path, cameras))
	{
		wxLogError(wxString("Error loading " + Utils::getFileName(path)));
		cameras.clear();
		return;
	}
	//Add cameras tree item in the mesh
	if (!GetTreeItemCameras())
	{
		SetTreeItemCameras(Utils::addItemToTree(tree, GetTreeItem(), "Cameras", 0));
	}
	SetCamerasVisibility(false);
	//Create the frustum and tree itens
	for (const auto& camera : cameras)
	{
		camera->createActorFrustrum(renderer);
		camera->setListItemCamera(Utils::addItemToTree(tree, GetTreeItemCameras(), Utils::getFileName(camera->filePath), 0));
		camera->setVisibility(false);
	}
}

void Project::SaveMeshes(bool pathModified)
{
	const auto reconstructionDir = projectFolder + "\\3DData\\";
	if (pathModified)
	{
		if (!Utils::CreateDir(reconstructionDir))
		{
			wxMessageBox("Error creating default directories.", "Error", wxICON_ERROR);
			return;
		}
	}
	for (auto mesh : meshes)
	{
		if (!pathModified)
		{
			mesh->Save(reconstructionDir);
		}
		else
		{
			if (mesh->GetHasTexture())
			{
				const auto texturedSurfaceDir = reconstructionDir + mesh->GetFilename();
				if (!Utils::CreateDir(texturedSurfaceDir))
				{
					wxMessageBox("Error creating textured surface directory.", "Error", wxICON_ERROR);
				}
				mesh->Save(texturedSurfaceDir, true);
			}
			else
			{
				mesh->Save(reconstructionDir, true);
			}
		}
	}
}

void Project::SaveCameras(bool pathModified)
{
	if (cameras.size() == 0)
	{
		return;
	}
	const auto imagesDir = projectFolder + "\\images";
	if (pathModified)
	{
		if (!Utils::CreateDir(imagesDir))
		{
			wxMessageBox("Error creating images directory.", "Error", wxICON_ERROR);
			return;
		}
		// Copy the images
		for (const auto& camera : cameras)
		{
			const auto newPath = imagesDir + "\\" + Utils::getFileName(camera->filePath);
			if (!wxCopyFile(camera->filePath, newPath))
			{
				wxLogError("Error copying file " + wxFileNameFromPath(camera->filePath));
			}
		}
	}
	const auto camerasPath = projectFolder + "\\cameras.sfm";
	ImageIO::saveCameras(camerasPath, cameras);
	if (pathModified)
	{
		ImageIO::replaceCamerasFileImageDir(camerasPath, imagesDir);
	}
}

void Project::AddMesh(Mesh * mesh)
{
	// Avoid adding duplicated meshes
	if (meshNames.find(mesh->GetFilename()) != meshNames.end())
	{
		auto newFilename = mesh->GetFilename();
		do
		{
			meshNames[mesh->GetFilename()]++;
			newFilename = mesh->GetFilename() + " (" + std::to_string(meshNames[mesh->GetFilename()]) + ")";
		} while (meshNames.find(newFilename) != meshNames.end());
		mesh->SetFilename(newFilename);
	}
	else
	{
		meshNames.insert(std::make_pair(mesh->GetFilename(), 0));
	}
	mesh->SetTreeItem(Utils::addItemToTree(tree, GetTreeItem(), mesh->GetFilename()));
	if (mesh->GetHasColor() || mesh->GetHasTexture())
	{
		mesh->SetTreeItemTexture(Utils::addItemToTree(tree, mesh->GetTreeItem(), "Texture"));
	}
	meshes.emplace_back(mesh);
	// We need to update the tree item and save it on the project folder
	mesh->SetModified(true);
	this->SetModified(true);
}

void Project::AddMesh(std::string& filename, vtkSmartPointer<vtkPolyData> polyData)
{
	auto mesh = new Mesh(renderer, tree, filename, polyData);
	if (!mesh->GetIsLoaded())
	{
		delete mesh;
		return;
	}
	AddMesh(mesh);
}

void Project::ImportMesh(const std::string & filePath)
{
	auto mesh = new Mesh(renderer, tree, filePath);
	if (!mesh->GetIsLoaded())
	{
		delete mesh;
		return;
	}
	AddMesh(mesh);
}

void Project::DeleteMesh(int meshIndex)
{
	auto mesh = meshes.at(meshIndex);
	if (mesh->GetOnDisk())
	{
		if (wxMessageBox("Do you want to delete the files on disk?", "", wxYES_NO | wxICON_QUESTION) == wxYES)
		{
			if (mesh->GetHasTexture())
			{
				// OBJ
				const auto texturedSurfaceDir = projectFolder + "\\3DData\\" + mesh->GetFilename();
				if (wxDirExists(texturedSurfaceDir))
				{
					wxArrayString files;
					wxDir::GetAllFiles(texturedSurfaceDir, &files);
					for (const auto& path : files)
					{
						Utils::RemoveFile(path.ToStdString());
					}
					wxRmDir(texturedSurfaceDir);
				}
			}
			else
			{
				Utils::RemoveFile(projectFolder + "\\3DData\\" + mesh->GetFilename() + ".ply");
			}
		}
	}
	delete meshes.at(meshIndex);
	meshes.erase(meshes.begin() + meshIndex);
}

void Project::Transform(vtkSmartPointer<vtkTransform> transform)
{
	SetModified(true);
	for (auto& mesh : meshes)
	{
		mesh->Transform(transform);
	}
	for (auto& camera : cameras)
	{
		camera->transform(renderer, transform);
	}
	for (auto& volume : volumes)
	{
		volume->transform(transform);
	}
}

void Project::Save(const std::string & newFilePath)
{
	auto pathModified = false;
	if (newFilePath != "")
	{
		projectPath = newFilePath;
		projectFolder = wxPathOnly(projectPath);
		tree->SetItemText(this->GetTreeItem(), Utils::getFileName(newFilePath, false));
		if (!Utils::CreateDir(projectFolder))
		{
			wxMessageBox("Error creating project directory.", "Error", wxICON_ERROR);
			return;
		}
		pathModified = true;
	}
	std::ofstream projectFile(projectPath);
	if (!projectFile.is_open())
	{
		wxLogError("Unable to save the project file");
		return;
	}
	projectFile << "is360=" << (int)is360 << "\n";
	projectFile.close();
	// Additional data
	calibration->save(projectFolder + "\\calibration.calib");
	SaveMeshes(pathModified);
	SaveCameras(pathModified);
	SetModified(false);
}
