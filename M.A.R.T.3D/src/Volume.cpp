#include "Volume.h"

#include <vtkRenderer.h>
#include <vtkCaptionActor2D.h>
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkMultiObjectMassProperties.h>
#include <vtkMapper.h>

#include <wx/msgdlg.h>

double Volume::computeVolume(vtkSmartPointer<vtkPolyData> polyData, double scalarFactor)
{
	//Compute the volume
	vtkNew<vtkMultiObjectMassProperties> massProperties;
	massProperties->SkipValidityCheckOn();
	if (scalarFactor != 1.0)
	{
		vtkNew<vtkTransform> transform;
		transform->Scale(scalarFactor, scalarFactor, scalarFactor);
		vtkNew<vtkTransformFilter> transformFilter;
		transformFilter->SetInputData(polyData);
		transformFilter->SetTransform(transform);
		transformFilter->Update();
		massProperties->SetInputConnection(transformFilter->GetOutputPort());
	}
	else
	{
		massProperties->SetInputData(polyData);
	}
	massProperties->Update();
	return massProperties->GetTotalVolume();
}

Volume::Volume(vtkSmartPointer<vtkActor> actor, vtkSmartPointer<vtkCaptionActor2D> textActor,
	unsigned int index, double scaleFactor, const std::string & measureUnit, vtkSmartPointer<vtkPolyData> holePolyData) :
	actor(actor), textActor(textActor), visible(true), index(index), holePolyData(holePolyData)
{
	updateCalibration(scaleFactor, measureUnit);
}

void Volume::destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree)
{
	if (textActor)
	{
		renderer->RemoveActor2D(textActor);
		textActor = nullptr;
	}
	if (actor)
	{
		renderer->RemoveActor(actor);
		actor = nullptr;
	}
	if (listItem)
	{
		tree->DeleteItem(listItem);
		listItem = nullptr;
	}
	holePolyData = nullptr;
}

void Volume::setVisibility(bool visibility)
{
	if (actor && textActor)
	{
		actor->SetVisibility(visibility);
		textActor->SetVisibility(visibility);
		this->visible = visibility;
	}
}

void Volume::updateText(const std::string& measureUnit)
{
	if (textActor)
	{
		wxString ss;
		ss << "#" << index << " - " << volume << measureUnit << "\u00B3";
		textActor->SetCaption(ss.utf8_str());
	}
}

void Volume::updateCalibration(double scalarFactor, const std::string& measureUnit)
{
	if (!actor)
	{
		return;
	}
	this->volume = computeVolume(vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput()), scalarFactor);
	// We have a hole
	if (holePolyData)
	{
		this->volume -= computeVolume(holePolyData, scalarFactor);
	}
	updateText(measureUnit);
}

void Volume::transform(vtkSmartPointer<vtkTransform> T)
{
	if (actor)
	{
		vtkNew<vtkTransformFilter> transformFilter;
		transformFilter->SetTransform(T);
		transformFilter->SetInputData(actor->GetMapper()->GetInput());
		transformFilter->Update();
		actor->GetMapper()->SetInputConnection(transformFilter->GetOutputPort());
		// Update text position
		if (textActor)
		{
			textActor->SetAttachmentPoint(actor->GetCenter());
		}
	}
}

double * Volume::getActorBounds()
{
	if (actor)
	{
		return actor->GetBounds();
	}
	return nullptr;
}
