#include "HelperFastQuadricSimplification.h"

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>

#include <FastQuadricSimplification.h>

vtkSmartPointer<vtkPolyData> HelperFastQuadricSimplification::computeQuadricDecimation(vtkSmartPointer<vtkPolyData> polyData, const float reduction)
{
	// Get PolyData data
	auto points = polyData->GetPoints();
	const auto numberOfPoints = points->GetNumberOfPoints();
	const auto colors = vtkUnsignedCharArray::SafeDownCast(polyData->GetPointData()->GetScalars());
	auto numberOfColorChannels = 0;
	if (colors)
	{
		numberOfColorChannels = colors->GetNumberOfComponents();
	}
	const auto normals = vtkFloatArray::SafeDownCast(polyData->GetPointData()->GetNormals());
	const auto tCoords = vtkFloatArray::SafeDownCast(polyData->GetPointData()->GetTCoords());

	// Add the data inside the FastQuadricSimplification mesh
	Simplify::FastQuadricSimplification meshSimplification;
	meshSimplification.vertices.resize(numberOfPoints);
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		Simplify::FastQuadricSimplification::Vertex v;
		double p[3];
		points->GetPoint(i, p);
		v.p.x = p[0];
		v.p.y = p[1];
		v.p.z = p[2];
		if (colors)
		{
			double* c = new double[numberOfColorChannels];
			colors->GetTuple(i, c);
			v.color.x = c[0];
			v.color.y = c[1];
			v.color.z = c[2];
			delete c;
		}
		if (normals)
		{
			double n[3];
			normals->GetTuple(i, n);
			v.n.x = n[0];
			v.n.y = n[1];
			v.n.z = n[2];
		}
		if (tCoords)
		{
			double tCoord[2];
			tCoords->GetTuple(i, tCoord);
			v.tCoord.x = tCoord[0];
			v.tCoord.y = tCoord[1];
			v.tCoord.z = 0;
		}
		meshSimplification.vertices[i] = v;
	}

	auto offsetArray = polyData->GetPolys()->GetOffsetsArray();
	auto idsArray = polyData->GetPolys()->GetConnectivityArray();
	const auto numberOfCells = polyData->GetPolys()->GetNumberOfCells();
	meshSimplification.triangles.resize(numberOfCells);
#pragma omp parallel for
	for (int i = 0; i < numberOfCells; i++)
	{
		Simplify::FastQuadricSimplification::Triangle triangle;
		double initialOffset[1], vertexIndex[1];
		offsetArray->GetTuple(i, initialOffset);
		for (size_t j = 0; j < 3; j++)
		{
			idsArray->GetTuple(initialOffset[0] + j, vertexIndex);
			triangle.v[j] = vertexIndex[0];
		}
		meshSimplification.triangles[i] = triangle;
	}
	// Simplify
	meshSimplification.simplify_mesh(reduction * polyData->GetNumberOfCells());
	// Get the result
	const auto newNumberOfPoints = meshSimplification.vertices.size();
	vtkNew<vtkPoints> newPoints;
	newPoints->SetNumberOfPoints(newNumberOfPoints);
	vtkNew<vtkUnsignedCharArray> newColors;
	if (colors)
	{
		newColors->SetNumberOfComponents(3);
		newColors->SetNumberOfTuples(newNumberOfPoints);
		newColors->SetName("RGB");
	}
	vtkNew<vtkFloatArray> newNormals;
	if (normals)
	{
		newNormals->SetNumberOfComponents(3);
		newNormals->SetNumberOfTuples(newNumberOfPoints);
		newNormals->SetName("Normals");
	}
	vtkNew<vtkFloatArray> newtCoords;
	if (tCoords)
	{
		newtCoords->SetNumberOfComponents(2);
		newtCoords->SetNumberOfTuples(newNumberOfPoints);
		newtCoords->SetName("tCoords");
	}
#pragma omp parallel for
	for (int i = 0; i < newNumberOfPoints; i++)
	{
		const auto vertex = meshSimplification.vertices[i];
		newPoints->InsertPoint(i, vertex.p.x, vertex.p.y, vertex.p.z);
		if (colors)
		{
			newColors->InsertTuple3(i, vertex.color.x, vertex.color.y, vertex.color.z);
		}
		if (normals)
		{
			newNormals->InsertTuple3(i, vertex.n.x, vertex.n.y, vertex.n.z);
		}
		if (tCoords)
		{
			newtCoords->InsertTuple2(i, vertex.tCoord.x, vertex.tCoord.y);
		}
	}
	vtkNew<vtkIdTypeArray> newOffsets;
	vtkNew<vtkIdTypeArray> newConnectivity;
	const auto newNumberOfTriangles = meshSimplification.triangles.size();
	newOffsets->SetNumberOfComponents(1);
	// The last position should be the lenght of the connectivity array
	newOffsets->SetNumberOfTuples(newNumberOfTriangles + 1);
	newConnectivity->SetNumberOfComponents(1);
	newConnectivity->SetNumberOfTuples(newNumberOfTriangles * 3);
#pragma omp parallel for
	for (int i = 0; i < newNumberOfTriangles; i++)
	{
		const auto triangle = meshSimplification.triangles[i];
		newOffsets->InsertTuple1(i, i * 3);
		for (int j = 0; j < 3; j++)
		{
			newConnectivity->InsertTuple1((i * 3) + j, triangle.v[j]);
		}
	}
	newOffsets->InsertTuple1(newNumberOfTriangles, newConnectivity->GetNumberOfTuples());
	// Create the resulting mesh
	vtkNew<vtkPolyData> newMesh;
	newMesh->SetPoints(newPoints);
	if (colors)
	{
		newMesh->GetPointData()->SetScalars(newColors);
	}
	if (normals)
	{
		newMesh->GetPointData()->SetNormals(newNormals);
	}
	if (tCoords)
	{
		newMesh->GetPointData()->SetTCoords(newtCoords);
	}
	vtkNew<vtkCellArray> triangles;
	triangles->SetData(newOffsets, newConnectivity);
	newMesh->SetPolys(triangles);
	return newMesh;
}
