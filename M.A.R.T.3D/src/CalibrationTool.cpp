#include "CalibrationTool.h"

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkMath.h>
#include <vtkPoints.h>
#include <vtkLandmarkTransform.h>
#include <vtkTransform.h>
#include <vtkCaptionActor2D.h>

#include <wx/textdlg.h>
#include <wx/msgdlg.h>

#include "LineWidget.h"
#include "Draw.h"
#include "Utils.h"
#include "Calibration.h"
#include "Mesh.h"
#include "Project.h"
#include "GPSCoordinatesDialog.h"
#include "GPSData.h"
#include "Camera.h"

vtkStandardNewMacro(CalibrationTool);

//----------------------------------------------------------------------------
CalibrationTool::CalibrationTool() : 
	vtk3DWidget(), lineWidget(nullptr), textActor(nullptr), mesh(nullptr), project(nullptr), window(nullptr), oldNumberOfPoints(0), useGPS(false)
{
	this->EventCallbackCommand->SetCallback(CalibrationTool::ProcessEvents);
}

//----------------------------------------------------------------------------
CalibrationTool::~CalibrationTool()
{
	for (const auto& pair : gpsData)
	{
		CurrentRenderer->RemoveActor(pair.first);
	}
	gpsData.clear();
	if (textActor)
	{
		CurrentRenderer->RemoveActor(textActor);
		textActor = nullptr;
	}
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
		lineWidget = nullptr;
	}
	mesh = nullptr;
	project = nullptr;
}

//----------------------------------------------------------------------------
void CalibrationTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh || !project || !window) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		if (!lineWidget)
		{
			lineWidget = vtkSmartPointer<LineWidget>::New();
			lineWidget->SetInteractor(this->Interactor);
			if (useGPS)
			{
				lineWidget->setCloseLoopOnFirstNode(true);
			}
			else
			{
				lineWidget->setMaxNumberOfNodes(2);
			}
			lineWidget->CreateRepresentationFromMesh(mesh);
		}
		lineWidget->EnabledOn();
		lineWidget->AddObserver(vtkCommand::StartInteractionEvent, this->EventCallbackCommand, this->Priority);
		lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
		lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		for (const auto& pair : gpsData)
		{
			CurrentRenderer->RemoveActor(pair.first);
		}
		gpsData.clear();
		if (textActor)
		{
			CurrentRenderer->RemoveActor(textActor);
			textActor = nullptr;
		}
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
		}
		mesh = nullptr;
		project = nullptr;
		oldNumberOfPoints = 0;
		useGPS = false;

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void CalibrationTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	CalibrationTool* self =
		reinterpret_cast<CalibrationTool *>(clientdata);

	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::StartInteractionEvent:
	case vtkCommand::InteractionEvent:
	case vtkCommand::EndInteractionEvent:
		self->UpdateRepresentation();
		break;
	}
}

//----------------------------------------------------------------------------
void CalibrationTool::UpdateRepresentation()
{
	if (!lineWidget)
	{
		return;
	}
	vtkSmartPointer<vtkPoints> points = lineWidget->GetRepresentation()->getPoints();
	if (useGPS)
	{
		if (!points)
		{
			for (const auto& pair : gpsData)
			{
				CurrentRenderer->RemoveActor(pair.first);
			}
			gpsData.clear();
			oldNumberOfPoints = 0;
			return;
		}
		const auto numberOfPoints = points->GetNumberOfPoints() - 1;
		if (lineWidget->GetRepresentation()->isLoopClosed())
		{
			for (vtkIdType i = 0; i < numberOfPoints; i++)
			{
				gpsData[i].first->SetAttachmentPoint(points->GetPoint(i));
			}
			return;
		}
		if (oldNumberOfPoints > numberOfPoints)
		{
			CurrentRenderer->RemoveActor(gpsData.back().first);
			gpsData.pop_back();
			oldNumberOfPoints = numberOfPoints;
			return;
		}
		if (oldNumberOfPoints == numberOfPoints)
		{
			return;
		}
		GPSCoordinatesDialog dialog(window);
		if (dialog.ShowModal() == wxID_OK)
		{
			wxString label;
			label << dialog.GetLatitude() << " \u00B0, " << dialog.GetLongitude() << " \u00B0, " << dialog.GetAltitude() << " m";
			gpsData.emplace_back(std::make_pair(
				Draw::createNumericIndicator(
					CurrentRenderer, points->GetPoint(numberOfPoints - 1),
					label.ToUTF8().data(),
					24, 1.0, 1.0, 1.0),
				GPSData(dialog.GetLatitude(), dialog.GetLongitude(), dialog.GetAltitude())
			));
		}
		else
		{
			lineWidget->GetRepresentation()->removeLastNode();
		}
		oldNumberOfPoints = numberOfPoints;
		// Invoke the mouse event to enable the MouseEvent inside LineWidget
		this->Interactor->SetEventInformation(this->Interactor->GetLastEventPosition()[0],
			this->Interactor->GetLastEventPosition()[1]);
		this->Interactor->InvokeEvent(vtkCommand::MouseMoveEvent);
	}
	else
	{
		if (!points)
		{
			if (textActor)
			{
				this->CurrentRenderer->RemoveActor2D(textActor);
				textActor = nullptr;
				this->Interactor->Render();
			}
			return;
		}
		if (points->GetNumberOfPoints() != 2)
		{
			return;
		}
		double point0[3];
		double point1[3];
		points->GetPoint(0, point0);
		points->GetPoint(1, point1);
		double attachmentPoint[3];
		Utils::getMidpoint(point0, point1, attachmentPoint);
		if (!textActor)
		{
			textActor = Draw::createText(this->CurrentRenderer, attachmentPoint, "", 24, 1.0, 1.0, 1.0);
		}
		textActor->SetAttachmentPoint(attachmentPoint);
		textActor->SetCaption(std::to_string(std::sqrt(vtkMath::Distance2BetweenPoints(point0, point1))).c_str());
	}
}

bool CalibrationTool::OnEnterKeyPressed()
{
	if (!lineWidget)
	{
		return false;
	}
	if (useGPS)
	{
		if (!lineWidget->GetRepresentation()->isLoopClosed())
		{
			return false;
		}
		std::vector< std::pair<double*, GPSData>> newGPSData;
		for (const auto& pair : gpsData)
		{
			newGPSData.emplace_back(std::make_pair(pair.first->GetAttachmentPoint(), pair.second));
		}
		Calibrate(newGPSData);
		return true;
	}
	auto distance = std::stof(textActor->GetCaption());
	auto realMeasureTxt = wxGetTextFromUser("Please insert the real object measure with the measure unit", "Calibration", "Ex.30cm");
	double realMeasure;
	realMeasureTxt.ToDouble(&realMeasure);
	if (realMeasure <= 0)
	{
		wxMessageBox("You need a number greater than 0.", "Error", wxICON_ERROR);
		return false;
	}
	wxString measureUnit;
	for (const auto& c : realMeasureTxt)
	{
		if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		{
			measureUnit << c;
		}
	}
	project->SetCalibration(new Calibration(realMeasure / distance, measureUnit.ToStdString()));
	return true;
}

void CalibrationTool::Calibrate(std::vector< std::pair<double*, GPSData>> gpsData)
{
	// We have cameras with gpsdata
	vtkNew<vtkPoints> sourcePoints, targetPoints;
	sourcePoints->SetDataTypeToDouble();
	targetPoints->SetDataTypeToDouble();
	// Lat, long and alt used as reference
	wxMessageBox("Please set the GPS reference point.", "Information", wxICON_INFORMATION, window);
	double gpsReference[3];
	GPSCoordinatesDialog dialog(window);
	if (dialog.ShowModal() == wxID_OK)
	{
		gpsReference[0] = dialog.GetLatitude();
		gpsReference[1] = dialog.GetLongitude();
		gpsReference[2] = dialog.GetAltitude();
	}
	else
	{
		return;
	}
	for (const auto& gps : gpsData)
	{
		sourcePoints->InsertNextPoint(gps.first);
		double ecef[3];
		Utils::lla_to_ecef(gps.second.getLatitude(),
			gps.second.getLongitude(),
			gps.second.getAltitude(),
			ecef);
		double enu[3];
		Utils::ecef_to_enu(ecef, gpsReference[0], gpsReference[1], gpsReference[2], enu);
		targetPoints->InsertNextPoint(enu);
	}
	//Landmark transform
	vtkNew<vtkLandmarkTransform> landmarkTransform;
	landmarkTransform->SetSourceLandmarks(sourcePoints);
	landmarkTransform->SetTargetLandmarks(targetPoints);
	vtkNew<vtkTransform> T;
	T->SetMatrix(landmarkTransform->GetMatrix());
	// Print error
	double sum = 0;
	for (size_t i = 0; i < gpsData.size(); i++)
	{
		double* point = T->TransformPoint(gpsData[i].first);
		sum += std::sqrt(vtkMath::Distance2BetweenPoints(point, targetPoints->GetPoint(i)));
	}
	std::stringstream ss;
	ss << std::fixed << std::setprecision(5) << sum / project->cameras.size();
	wxMessageBox("Mean error " + ss.str());
	wxBeginBusyCursor();
	project->Transform(T);
	project->SetCalibration(new Calibration(1.0, "m"));
	project->GetCalibration()->SetGPSReferencePoint(gpsReference);
	CurrentRenderer->ResetCamera();
	CurrentRenderer->ResetCameraClippingRange();
	CurrentRenderer->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
