#pragma once
#include <sstream>
#include <iomanip>
#include <string>

class Calibration
{
private:
	double scaleFactor = 1.0;
	std::string measureUnit = "";

	//GPS
	double gpsReferencePoint[3] = {-1.0, -1.0, -1.0};

public:
	Calibration() {};
	Calibration(double scaleFactor, const std::string& measureUnit) : measureUnit(measureUnit)
	{
		if (scaleFactor > 0.0)
		{
			this->scaleFactor = scaleFactor;
		}
	};
	~Calibration() {};

	// Load the calibration from a .calib file
	bool load(const std::string& calibrationPath);
	// Save the calibration to a .calib file
	bool save(const std::string& calibrationPath);

	//Return the distance with the right scale
	double getCalibratedDistance(double dist) const { return dist * scaleFactor; };
	//Return a text with the right distance and the measure unit
	std::string getCalibratedText(double dist) const
	{
		std::stringstream s;
		s << std::fixed << std::setprecision(3) << dist * scaleFactor << measureUnit;
		return s.str();
	};
	//Return a text with the right distance and the measure unit
	std::string getCalibratedText(double dist, double deltaX, double deltaY, double deltaZ) const
	{
		std::stringstream s;
		s << std::fixed << std::setprecision(3) << dist * scaleFactor << measureUnit
			<< " (" << deltaX * scaleFactor
			<< ", " << deltaY * scaleFactor
			<< ", " << deltaZ * scaleFactor << ")";
		return s.str();
	};
	//Return the scale factor
	double getScaleFactor() const { return scaleFactor; };
	//Return the measure unit
	std::string getMeasureUnit() const { return measureUnit; };

	void SetGPSReferencePoint(double gpsReferencePoint[3]) 
	{
		for (size_t i = 0; i < 3; i++)
		{
			this->gpsReferencePoint[i] = gpsReferencePoint[i];
		}
	};

	void GetGPSReferencePoint(double gpsReferencePoint[3]) const
	{
		for (size_t i = 0; i < 3; i++)
		{
			gpsReferencePoint[i] = this->gpsReferencePoint[i];
		}
	}
};