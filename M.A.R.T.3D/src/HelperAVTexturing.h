#pragma once
#include <vector>
#include <string>

#include <vtkSmartPointer.h>

#include <AVTexturing.h>

class vtkPolyData;
class Camera;

class HelperAVTexturing
{
public:
	HelperAVTexturing() {};
	~HelperAVTexturing() {};

	static bool computeAVTexturization(const std::string& meshPath, const std::string& camerasPath, const std::string& outputPath);

private:
	static vtkSmartPointer<vtkPolyData> loadMeshToTexturize(const std::string& meshPath, AVTexturing::MeshAux& outMesh);
	static void createPointVisibilityData(AVTexturing::MeshAux& mesh, vtkSmartPointer<vtkPolyData> meshPolyData, std::vector<Camera*> cameras);
};


