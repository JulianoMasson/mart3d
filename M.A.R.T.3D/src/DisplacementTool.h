#pragma once
#include <vector>
#include <vtkSmartPointer.h>

class vtkPoints;
class vtkPolyData;
class Mesh;

class DisplacementTool
{
public:
	DisplacementTool() : pointCloud(nullptr), simplifiedMeshPointCloud(nullptr), simplifiedMesh(nullptr), datFile("") {};
	DisplacementTool(vtkSmartPointer<vtkPoints> pointCloud, vtkSmartPointer<vtkPoints> simplifiedMeshPointCloud, Mesh* simplifiedMesh,
		const std::string& datFilePath) : pointCloud(pointCloud), simplifiedMeshPointCloud(simplifiedMeshPointCloud),
		simplifiedMesh(simplifiedMesh), datFile(datFilePath) {};
	~DisplacementTool()
	{
		pointCloud = nullptr;
		simplifiedMeshPointCloud = nullptr;
		simplifiedMesh = nullptr;
	};

	//Inputs
	void SetPointCloud(vtkSmartPointer<vtkPoints> points) { pointCloud = points; };
	void SetSimplifiedMeshPointCloud(vtkSmartPointer<vtkPoints> points) { simplifiedMeshPointCloud = points; };
	void SetSimplifiedMesh(Mesh* simplifiedMesh) { this->simplifiedMesh = simplifiedMesh; };
	void SetDatFilePath(const std::string& filePath) { datFile = filePath; };

	//Process
	vtkSmartPointer<vtkPolyData> Compute();

	//Output
	Mesh* GetSimplifiedMesh() const { return simplifiedMesh; };

private:
	vtkSmartPointer<vtkPoints> pointCloud;
	// The point cloud that was used to create the simplified mesh
	vtkSmartPointer<vtkPoints> simplifiedMeshPointCloud;
	Mesh* simplifiedMesh;
	std::string datFile;

	bool WriteDatFile(const std::string& filename, const std::vector<double*>& axisDistances, bool computePP);
	bool ExecuteCPFL(const std::string& inputDatFile, const std::string& outputDatFile);
	//Load the .dat result from stress3D_CPFL.exe
	vtkSmartPointer<vtkPolyData> LoadDatFile(const std::string& datFile);

};