#pragma once
#include <wx/dialog.h>
#include <vtkSmartPointer.h>

class vtkRenderer;
class vtkActor;
class vtkPolyData;
class wxSpinCtrl;
class wxSpinCtrlDouble;
class wxCheckBox;
class Mesh;

class SmoothMeshDialog : public wxDialog
{
public:
	SmoothMeshDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Smooth mesh",
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(355, 350), long style = wxDEFAULT_DIALOG_STYLE);
	~SmoothMeshDialog() 
	{ 
		mesh = nullptr;
		renderer = nullptr;
	};

	void setMesh(Mesh* mesh) { this->mesh = mesh; };
	void SetRenderer(vtkSmartPointer<vtkRenderer> renderer) { this->renderer = renderer; };

private:
	DECLARE_EVENT_TABLE()

	void OnOK(wxCommandEvent& WXUNUSED(event));
	void OnCancel(wxCommandEvent& WXUNUSED(event));
	void OnClose(wxCloseEvent& WXUNUSED(event));

	void OnCheckBoxUpdate(wxCommandEvent& WXUNUSED(event));
	void OnSpinCtrlUpdate(wxSpinEvent& WXUNUSED(event));
	void update();
	vtkSmartPointer<vtkPolyData> smoothMesh();
	
	wxSpinCtrl* spinMaxNumberOfIterations;
	wxSpinCtrlDouble* spinPassBand;
	wxSpinCtrlDouble* spinFeatureAngle;
	wxSpinCtrlDouble* spinEdgeAngle;
	wxCheckBox* ckFeatureEdgeSmoothing;
	wxCheckBox* ckBoundarySmoothing;
	wxCheckBox* ckNonManifoldSmoothing;
	wxCheckBox* ckShowErrors;
	wxCheckBox* ckPreview;

	vtkSmartPointer<vtkRenderer> renderer = nullptr;
	vtkSmartPointer<vtkActor> actor = nullptr;

	Mesh* mesh = nullptr;
};

enum EnumSmoothPolyDataDialog
{
	idUpdate
};