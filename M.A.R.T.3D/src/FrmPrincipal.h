#pragma once
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);

#define vtkRenderingCore_AUTOINIT 3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL2)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL2)

#include <set>

#include <vtkSmartPointer.h>

#include <wx/frame.h>
#include <wx/treelist.h>
//VTK
class vtkRenderer;
class wxVTKRenderWindowInteractor;
//Tools
class DistanceTool;
class AngleTool;
class AreaTool;
class ElevationTool;
class ViewTool;
class AlignWithAxisTool;
class VolumeTool;
class ScalarViewerTool;
class CheckCamTool;
class DeleteTool;
class UpdateCameraTool;
class AlignTool;
class ICPDialog;
class vtkOrientationMarkerWidget;
class SmoothMeshDialog;
class ColorTool;
class CalibrationTool;
//Wx
class wxHtmlHelpController;
class wxSplitterEvent;
class wxSplitterWindow;
class wxToolBarToolBase;
class wxMouseEvent;
//
class InteractorStyle;
class OutputErrorWindow;
class Project;
class Mesh;
class Camera;

class FrmPrincipal : public wxFrame
{
private:
	DECLARE_EVENT_TABLE()

	//VTK
	vtkSmartPointer<vtkRenderer> renderer = nullptr;
	vtkSmartPointer<vtkRenderer> renderer360 = nullptr;

	//Help
	wxHtmlHelpController*  helpController = nullptr;

	//Axis
	vtkSmartPointer<vtkOrientationMarkerWidget> axisWidget = nullptr;

	//Light tool
	bool isLightOn = true;

	//Tools
	vtkSmartPointer<DistanceTool> distanceTool = nullptr;
	vtkSmartPointer<AngleTool> angleTool = nullptr;
	vtkSmartPointer<AreaTool> areaTool = nullptr;
	vtkSmartPointer<ElevationTool> elevationTool = nullptr;
	vtkSmartPointer<ViewTool> viewTool = nullptr;
	vtkSmartPointer<AlignWithAxisTool> aligWithAxisTool = nullptr;
	vtkSmartPointer<VolumeTool> volumeTool = nullptr;
	vtkSmartPointer<ScalarViewerTool> scalarViewerTool = nullptr;
	vtkSmartPointer<CheckCamTool> checkCamTool = nullptr;
	vtkSmartPointer<DeleteTool> deleteTool = nullptr;
	vtkSmartPointer<UpdateCameraTool> updateCameraTool = nullptr;
	vtkSmartPointer<AlignTool> alignTool = nullptr;
	vtkSmartPointer<ColorTool> colorTool = nullptr;
	vtkSmartPointer<CalibrationTool> calibrationTool = nullptr;
	std::unique_ptr<ICPDialog> icpDialog = nullptr;
	std::unique_ptr<SmoothMeshDialog> smoothMeshDialog = nullptr; 

	void disableTools();
	//Disable all tools but the tool in the parameter
	/*
	Distance - 0
	Angle - 1
	Elevation - 2
	CheckCam - 3
	Volume - 4
	Delete points and faces - 5
	Update camera - 6
	Align - 7
	ScalarViewerTool - 8
	ColorTool - 9
	AreaTool - 10
	CalibrationTool - 11
	*/
	void disableMeasureTools(int toolToAvoid);


	vtkSmartPointer<InteractorStyle> iterStyle = nullptr;

	//Tree
	wxTreeListItem addItemToTree(const wxTreeListItem& parentItem, const std::string& itemName, wxCheckBoxState state);
	//Get the selected Project from the tree
	Project* GetProjectFromTree();
	//Get the Project from the tree using its tree item or one of its children tree item
	Project* GetProjectFromTree(wxTreeListItem item);
	//Get the selected Mesh from the tree
	Mesh* getMeshFromTree();
	//Get the Mesh from the tree using its tree item or one of its children tree item
	Mesh* getMeshFromTree(wxTreeListItem item);
	//Get multiple selected Meshs from the tree, it obligates the user to select numberOfMeshs meshs on the tree
	std::set<Mesh*> getMeshsFromTree(unsigned int numberOfMeshs);
	//Get the selected Camera from the tree
	Camera* getCameraFromTree();

	//Splitter
	bool isTreeVisible;
	void setTreeVisibility(bool visibility);

	//Menu
	void OnMenuCreateProject(wxCommandEvent& event);
	void OnMenuOpenProject(wxCommandEvent& event);
	void OnMenuSaveProject(wxCommandEvent& event);
	void OnMenuSaveProjectAs(wxCommandEvent& event);
	void OnMenuImportMesh(wxCommandEvent& event);
	void OnMenuExportMesh(wxCommandEvent& event);
	void OnMenuTransform(wxCommandEvent& event);
	void OnMenuUpdateCamera(wxCommandEvent& event);
	void OnMenuSnapshot(wxCommandEvent& event);
	void OnMenuShowAxis(wxCommandEvent& event);
	void OnMenuShowViews(wxCommandEvent& event);
	void OnMenuFlyToPoint(wxCommandEvent& event);
	void OnMenuPoisson(wxCommandEvent& event);
	void OnMenuSSDRecon(wxCommandEvent& event);
	void OnMenuICP(wxCommandEvent& event);
	void OnMenuAlignTool(wxCommandEvent& event);
	void OnMenuSurfaceTrimmer(wxCommandEvent& event);
	void OnMenuFastQuadricSimplification(wxCommandEvent& event);
	void OnMenuSmoothMesh(wxCommandEvent& event);
	void OnMenuRemeshing(wxCommandEvent& event);
	void OnMenuColorTool(wxCommandEvent& event);
	void OnMenuAreaTool(wxCommandEvent& event);
	void OnMenuComputeDisplacement(wxCommandEvent& event);
	void OnMenuScalarViewer(wxCommandEvent& event);
	void OnMenuHelp(wxCommandEvent& event);
	void OnMenuAbout(wxCommandEvent& event);
	//Tools
	void OnToolDelete(wxCommandEvent& event);
	void OnToolDebug(wxCommandEvent& event);
	void OnToolPoints(wxCommandEvent& event);
	void OnToolWireframe(wxCommandEvent& event);
	void OnToolSurface(wxCommandEvent& event);
	void OnToolMeasure(wxCommandEvent& event);
	void OnToolAngle(wxCommandEvent& event);
	void OnToolVolume(wxCommandEvent& event);
	void OnToolElevation(wxCommandEvent& event);
	void OnToolCalibration(wxCommandEvent& event);
	void OnToolCheckCamVisibility(wxCommandEvent& event);
	void OnToolStartReconstruction(wxCommandEvent& event);
	void OnToolSnapshot(wxCommandEvent& event);
	void OnToolLight(wxCommandEvent& event);
	void OnToolDeletePointsFaces(wxCommandEvent& event);
	void OnToolVR(wxCommandEvent& event);
	/*
	Select checkbox tree
	*/
	void OnTreeChoice(wxTreeListEvent& event);
	/*
	Double Click tree
	*/
	void OnTreeItemActivated(wxTreeListEvent& event);
	void OnClose(wxCloseEvent& event);
	void OnSplitterDClick(wxSplitterEvent& event);
	void OnSashPosChanged(wxSplitterEvent& event);
	void OnSize(wxSizeEvent& event);
	void OnKeyPress(wxKeyEvent& event);
	void OnDropFiles(wxDropFilesEvent& event);

protected:
	//VTK
	vtkSmartPointer<OutputErrorWindow> outputErrorWindow = nullptr;
	std::vector<Project*> projects;

	//WX
	wxSplitterWindow* splitterWind;
	vtkSmartPointer<wxVTKRenderWindowInteractor> vtk_panel = nullptr;
	//Menu
	wxMenuBar* menuBar;
	wxMenu* menuFile;
	wxMenuItem* menuItemUpdateCamera;
	wxMenuItem* menuItemShowAxis;
	wxMenuItem* menuItemShowViews;
	wxMenuItem* menuItemFlyToPoint;
	wxMenuItem* menuItemAlignTool;
	wxMenu* menuEdit;
	wxMenu* menuRepresentation;
	wxMenuItem* menuItemToolDeletePoints;
	wxMenu* menuFilters;
	wxMenuItem* menuItemColorTool;
	wxMenu* menuView;
	wxMenuItem* menuItemToolCheckCamVisibility;
	wxMenuItem* menuItemToolScalarViewer;
	wxMenu* menuTools;
	wxMenuItem* menuItemToolMeasure;
	wxMenuItem* menuItemToolAngle;
	wxMenuItem* menuItemToolArea;
	wxMenuItem* menuItemToolVolume;
	wxMenuItem* menuItemToolElevation;
	wxMenu* menuHelp;
	//ToolBar
	wxToolBar* toolBar;
	//Just the tools that use bitmap On and Off need an object
	wxToolBarToolBase* toolMeasure;
	wxToolBarToolBase* toolAngle;
	wxToolBarToolBase* toolCalcArea;
	wxToolBarToolBase* toolCalcVolume;
	wxToolBarToolBase* toolElevation;
	wxToolBarToolBase* toolCalibration;
	wxToolBarToolBase* toolCheckCamVisibility;
	wxToolBarToolBase* toolDeletePoints;
	wxToolBarToolBase* toolVR;
	//Bitmaps
	wxBitmap bmpToolMeasureOFF;
	wxBitmap bmpToolMeasureON;
	wxBitmap bmpToolAngleOFF;
	wxBitmap bmpToolAngleON;
	wxBitmap bmpToolCalcAreaOFF;
	wxBitmap bmpToolCalcAreaON;
	wxBitmap bmpToolCalcVolumeOFF;
	wxBitmap bmpToolCalcVolumeON;
	wxBitmap bmpToolElevationOFF;
	wxBitmap bmpToolElevationON;
	wxBitmap bmpToolCalibrationOFF;
	wxBitmap bmpToolCalibrationON;
	wxBitmap bmpToolCheckCamVisibilityOFF;
	wxBitmap bmpToolCheckCamVisibilityON;
	wxBitmap bmpToolDeletePointsFacesOFF;
	wxBitmap bmpToolDeletePointsFacesON;
	wxBitmap bmpToolVROFF;
	wxBitmap bmpToolVRON;

	//Tree
	wxTreeListCtrl* treeMesh = nullptr;
	wxBitmap bmpTreeCheckCamera;
public:
	FrmPrincipal(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(500, 300), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~FrmPrincipal();

	//Help
	wxHtmlHelpController* GetHelpController() const { return helpController; };

	//Welcome dialog
	void ShowWelcomeDialog();

	//Load
	void LoadProject(const std::string& projectPath);

	void OnLeftDClick(wxMouseEvent& event);

};
enum {
	idMenuCreateProject,
	idErrorEnter,//Do not use this ID, it will trigger when you press Enter
	idMenuOpenProject,
	idMenuSaveProject,
	idMenuSaveProjectAs,
	idMenuImportMesh,
	idMenuExportMesh,
	idMenuTransform,
	idMenuUpdateCamera,
	idMenuSnapshot,
	idMenuShowAxis,
	idMenuShowViews,
	idMenuFlyToPoint,
	idMenuICP,
	idMenuAlignTool,
	idMenuSurfaceTrimmer,
	idMenuFastQuadricSimplification,
	idMenuSmoothMesh,
	idMenuRemeshing,
	idMenuColorTool,
	idMenuAreaTool,
	idMenuComputeDisplacement,
	idMenuPoisson,
	idMenuSSDRecon,
	idMenuScalarViewer,
	idMenuAbout,
	idMenuHelp,
	idToolDelete,
	idToolDebug,
	idToolPoints,
	idToolWireframe,
	idToolSurface,
	idToolMeasure,
	idToolAngle,
	idToolVolume,
	idToolElevation,
	idToolCalibration,
	idToolCheckCamVisibility,
	idToolStartReconstruction,
	idToolSnapshot,
	idToolLight,
	idToolDeletePointsFaces,
	idToolVR,
	idSplitterWindow
};