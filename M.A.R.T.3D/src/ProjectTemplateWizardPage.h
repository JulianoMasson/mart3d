#pragma once
#include <wx/wizard.h>

class wxChoice;
class wxCheckBox;

class ProjectTemplateWizardPage : public wxWizardPageSimple
{
public:
	ProjectTemplateWizardPage(wxWizard *parent, wxWizardPage *prev = nullptr, wxWizardPage *next = nullptr, const wxBitmap &bitmap = wxNullBitmap);
	~ProjectTemplateWizardPage() {};

	int GetProjectType();
	int GetProjectQuality();
	bool GetGenerateTexture();

private:
	wxChoice* chType;
	wxChoice* chQuality;
	wxCheckBox* ckGenerateTexture;

	void OnChoiceType(wxCommandEvent& event);
	void OnChoiceQuality(wxCommandEvent& event);
	void OnBtQuality(wxCommandEvent& event);
};