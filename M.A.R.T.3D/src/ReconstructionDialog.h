#pragma once
#include <wx\dialog.h>

class wxFileDirPickerEvent;
struct MeshAux;
struct CameraParameters;
class Camera;
class wxCheckBox;
class wxStaticText;
class wxDirPickerCtrl;
class wxFilePickerCtrl;
class ReconstructionLog;

struct BatchItem
{
	BatchItem(bool generateMesh, bool generateTexture, const wxString& imagePath,
		const wxString& meshPath, const wxString& camerasPath) :
		generateMesh(generateMesh), generateTexture(generateTexture), imagePath(imagePath), meshPath(meshPath),
		camerasPath(camerasPath) {};
	const bool generateMesh;
	const bool generateTexture;
	const wxString imagePath;
	const wxString meshPath;
	const wxString camerasPath;
	wxString outputPath;
};

class ReconstructionDialog : public wxDialog
{
public:
  ReconstructionDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Reconstruction/Texturization",
	  const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
  ~ReconstructionDialog();

  wxString getOutputPath();

  //SFM
  bool runSFM(const std::string& imagesPath, const std::string& nvmPath, ReconstructionLog & log);
  //Meshing
  bool runMeshing(const std::string& imagesPath, const std::string& nvmPath, ReconstructionLog & log);
  //Texturization
  bool runTexturization(const std::string& meshPath, const std::string& camerasPath, const std::string& outputPath, ReconstructionLog & log);

  //Method used to texturize a mesh without opening the dialog, it will use the last set parameters in the config dialog
  void texturizeMesh(std::string meshPath, std::string nvmPath, std::string outputPath);

private:
  DECLARE_EVENT_TABLE()

  void OnOK(wxCommandEvent& WXUNUSED(event));
  void OnBtConfig(wxCommandEvent& WXUNUSED(event));
  void OnBtBatchAdd(wxCommandEvent& WXUNUSED(event));
  void OnBtBatchStart(wxCommandEvent& WXUNUSED(event));
  void OnCheckBoxs(wxCommandEvent& WXUNUSED(event));
  void OnDirPickerImages(wxFileDirPickerEvent& WXUNUSED(event));
  void OnDirPickerMesh(wxFileDirPickerEvent& WXUNUSED(event));
  void OnDirPickerCameras(wxFileDirPickerEvent& WXUNUSED(event));
  void OnDirPickerOutput(wxFileDirPickerEvent& WXUNUSED(event));

  //Mesh
  //True - OK
  //False - wrong
  bool testImageFolder(wxString imagePath);
  int widthDefault = -1;
  int heightDefault = -1;

  //Batch reconstruction
  std::vector<BatchItem> batchItems;
  //Disable the EndModal in the OnOk method, this is used in the batch reconstruction
  bool disableEndModal = false;

  wxCheckBox* cbGenerateMesh;
  wxCheckBox* cbTexturize;

  //Mesh
  wxStaticText* textImages;
  wxDirPickerCtrl* pickerImages;
  wxStaticText* textOutputMesh;
  wxFilePickerCtrl* pickerOutputMesh;
  
  //Texture
  wxStaticText* textMesh;
  wxFilePickerCtrl* pickerMesh;
  wxStaticText* textCameras;
  wxFilePickerCtrl* pickerCameras;
  wxStaticText* textOutputTexture;
  wxFilePickerCtrl* pickerOutputTexture;

  //Batch reconstruction
  wxStaticText* textQueueSize;

  //Reconstruction
  // Check if all fields are correctly filled and return the output path, generateMesh and generateTexture
  std::string checkFields(bool& generateMesh, bool& generateTexture);
  //Test if we have memory to use MeshRecon
  bool useMeshRecon(const std::string& nvmPath, ReconstructionLog & log);

};

enum {
  idCBMesh,
  idCBTexture,
  idBtConfig,
  idBtBatchAdd,
  idBtBatchStart,
  idDirPickerImages,
  idDirPickerMesh,
  idDirPickerCameras,
  idDirPickerOutput
};
