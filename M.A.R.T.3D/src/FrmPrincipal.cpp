#include "FrmPrincipal.h"

#include <sstream>
//VTK
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkOutputWindow.h>
#include <vtkAxesActor.h>
#include <vtkLandmarkTransform.h>
#include <vtkTransform.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPLYWriter.h>
#include <vtkPLYReader.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkMapper.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkCleanPolyData.h>
#include <vtkFeatureEdges.h>
#include <vtkAssemblyPath.h>
#include <vtkPropPicker.h>
#include <vtkNew.h>
//WX
#include <wx/sizer.h>
#include <wx/menu.h>
#include <wx/toolbar.h>
#include <wx/splitter.h>
#include <wx/html/helpctrl.h>
#include <wx/dir.h>
#include <wx/progdlg.h>
#include <wx/numdlg.h> 
#include <wx/choicdlg.h>
#include <wx/stdpaths.h>
//Tools
#include "DistanceTool.h"
#include "AngleTool.h"
#include "AreaTool.h"
#include "ElevationTool.h"
#include "ViewTool.h"
#include "AlignWithAxisTool.h"
#include "VolumeTool.h"
#include "CheckCamTool.h"
#include "DeleteTool.h"
#include "UpdateCameraTool.h"
#include "AlignTool.h"
#include "SnapshotTool.h"
#include <vtkOrientationMarkerWidget.h>
#include "ScalarViewerTool.h"
#include "ColorTool.h"
#include "DisplacementTool.h"
#include "CalibrationTool.h"
//Dialogs
#include "ICPDialog.h"
#include "TransformDialog.h"
#include "ReconstructionDialog.h"
#include "PoissonDialog.h"
#include "SSDDialog.h"
#include "AboutDialog.h"
#include "VRDialog.h"
#include "SmoothMeshDialog.h"
#include "WelcomeDialog.h"
#include "GetDoubleDialog.h"

#include "HelperAdaptativeSolvers.h"
#include "HelperFastQuadricSimplification.h"
#include "InteractorStyle.h"
#include "wxVTKRenderWindowInteractor.h"
#include "OutputErrorWindow.h"
#include "GPSData.h"
#include "Calibration.h"
#include "Volume.h"
#include "Draw.h"
#include "Utils.h"
#include "Mesh.h"
#include "Camera.h"
#include "MeshIO.h"
#include "ImageIO.h"
#include "Project.h"

//#define DEBUG_MART

BEGIN_EVENT_TABLE(FrmPrincipal, wxFrame)
EVT_MENU(idMenuCreateProject, FrmPrincipal::OnMenuCreateProject)
EVT_MENU(idMenuOpenProject, FrmPrincipal::OnMenuOpenProject)
EVT_MENU(idMenuSaveProject, FrmPrincipal::OnMenuSaveProject)
EVT_MENU(idMenuSaveProjectAs, FrmPrincipal::OnMenuSaveProjectAs)
EVT_MENU(idMenuImportMesh, FrmPrincipal::OnMenuImportMesh)
EVT_MENU(idMenuExportMesh, FrmPrincipal::OnMenuExportMesh)
EVT_MENU(idMenuTransform, FrmPrincipal::OnMenuTransform)
EVT_MENU(idMenuUpdateCamera, FrmPrincipal::OnMenuUpdateCamera)
EVT_MENU(idMenuSnapshot, FrmPrincipal::OnMenuSnapshot)
EVT_MENU(idMenuShowAxis, FrmPrincipal::OnMenuShowAxis)
EVT_MENU(idMenuShowViews, FrmPrincipal::OnMenuShowViews)
EVT_MENU(idMenuFlyToPoint, FrmPrincipal::OnMenuFlyToPoint)
EVT_MENU(idMenuICP, FrmPrincipal::OnMenuICP)
EVT_MENU(idMenuAlignTool, FrmPrincipal::OnMenuAlignTool)
EVT_MENU(idMenuSurfaceTrimmer, FrmPrincipal::OnMenuSurfaceTrimmer)
EVT_MENU(idMenuFastQuadricSimplification, FrmPrincipal::OnMenuFastQuadricSimplification)
EVT_MENU(idMenuSmoothMesh, FrmPrincipal::OnMenuSmoothMesh)
EVT_MENU(idMenuRemeshing, FrmPrincipal::OnMenuRemeshing)
EVT_MENU(idMenuColorTool, FrmPrincipal::OnMenuColorTool)
EVT_MENU(idMenuAreaTool, FrmPrincipal::OnMenuAreaTool)
EVT_MENU(idMenuComputeDisplacement, FrmPrincipal::OnMenuComputeDisplacement)
EVT_MENU(idMenuPoisson, FrmPrincipal::OnMenuPoisson)
EVT_MENU(idMenuSSDRecon, FrmPrincipal::OnMenuSSDRecon)
EVT_MENU(idMenuScalarViewer, FrmPrincipal::OnMenuScalarViewer)
EVT_MENU(idMenuHelp, FrmPrincipal::OnMenuHelp)
EVT_MENU(idMenuAbout, FrmPrincipal::OnMenuAbout)
EVT_TOOL(idToolDelete, FrmPrincipal::OnToolDelete)
EVT_TOOL(idToolDebug, FrmPrincipal::OnToolDebug)
EVT_TOOL(idToolPoints, FrmPrincipal::OnToolPoints)
EVT_TOOL(idToolWireframe, FrmPrincipal::OnToolWireframe)
EVT_TOOL(idToolSurface, FrmPrincipal::OnToolSurface)
EVT_TOOL(idToolMeasure, FrmPrincipal::OnToolMeasure)
EVT_TOOL(idToolAngle, FrmPrincipal::OnToolAngle)
EVT_TOOL(idToolVolume, FrmPrincipal::OnToolVolume)
EVT_TOOL(idToolElevation, FrmPrincipal::OnToolElevation)
EVT_TOOL(idToolCalibration, FrmPrincipal::OnToolCalibration)
EVT_TOOL(idToolCheckCamVisibility, FrmPrincipal::OnToolCheckCamVisibility)
EVT_TOOL(idToolStartReconstruction, FrmPrincipal::OnToolStartReconstruction)
EVT_TOOL(idToolSnapshot, FrmPrincipal::OnToolSnapshot)
EVT_TOOL(idToolLight, FrmPrincipal::OnToolLight)
EVT_TOOL(idToolDeletePointsFaces, FrmPrincipal::OnToolDeletePointsFaces)
EVT_TOOL(idToolVR, FrmPrincipal::OnToolVR)
EVT_CLOSE(FrmPrincipal::OnClose)
EVT_SIZE(FrmPrincipal::OnSize)
EVT_SPLITTER_DCLICK(idSplitterWindow, FrmPrincipal::OnSplitterDClick)
EVT_SPLITTER_SASH_POS_CHANGED(idSplitterWindow, FrmPrincipal::OnSashPosChanged)
EVT_CHAR_HOOK(FrmPrincipal::OnKeyPress)
EVT_DROP_FILES(FrmPrincipal::OnDropFiles)
END_EVENT_TABLE()

FrmPrincipal::FrmPrincipal(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
	this->SetIcon(wxICON(AAAAAPROGRAM));
	//Help
	helpController = new wxHtmlHelpController(this, wxHF_TOOLBAR | wxHF_CONTENTS | wxHF_INDEX | wxHF_SEARCH | wxHF_BOOKMARKS | wxHF_PRINT);
	//VTK
	outputErrorWindow = vtkSmartPointer<OutputErrorWindow>::New();
	vtkOutputWindow::SetInstance(outputErrorWindow);
	//WX
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	//Menu
	menuBar = new wxMenuBar(0);
	//File
	menuFile = new wxMenu();
	menuFile->Append(new wxMenuItem(menuFile, idMenuCreateProject, "Create project..."));
	menuFile->Append(new wxMenuItem(menuFile, idMenuOpenProject, "Open project..."));
	menuFile->AppendSeparator();
	menuFile->Append(new wxMenuItem(menuFile, idMenuSaveProject, "Save project"));
	menuFile->Append(new wxMenuItem(menuFile, idMenuSaveProjectAs, "Save project As..."));
	menuFile->AppendSeparator();
	menuFile->Append(new wxMenuItem(menuFile, idMenuImportMesh, "Import mesh..."));
	menuFile->Append(new wxMenuItem(menuFile, idMenuExportMesh, "Export mesh..."));
	//menuFile->Append(new wxMenuItem(menuFile, idMenuExportCameras, "Export cameras..."));
	menuFile->AppendSeparator();
	//menuFile->Append(new wxMenuItem(menuFile, idMenuTransform, "Tranform..."));
	//menuItemUpdateCamera = menuFile->AppendCheckItem(idMenuUpdateCamera, "Update camera...");
	//menuFile->AppendSeparator();
	menuItemShowAxis = menuFile->AppendCheckItem(idMenuShowAxis, "Show Axis");
	menuItemShowViews =menuFile->AppendCheckItem(idMenuShowViews, "Show Views");
	menuItemFlyToPoint = menuFile->AppendCheckItem(idMenuFlyToPoint, "Fly to point");
	//Edit
	menuEdit = new wxMenu();
	menuRepresentation = new wxMenu();
	menuRepresentation->AppendRadioItem(idToolPoints, "Points");
	menuRepresentation->AppendRadioItem(idToolWireframe, "Wireframe");
	menuRepresentation->AppendRadioItem(idToolSurface, "Surface");
	menuRepresentation->Check(idToolSurface, true);
	menuEdit->AppendSubMenu(menuRepresentation, "Representations");
	menuEdit->AppendSeparator();
	menuEdit->Append(idToolDelete, "Delete");
	menuItemToolDeletePoints = menuEdit->AppendCheckItem(idToolDeletePointsFaces, "Delete points/faces");
	menuItemColorTool = menuEdit->AppendCheckItem(idMenuColorTool, "Delete points by color");
	//Filters
	menuFilters = new wxMenu();
	wxMenu* menuRegistration = new wxMenu();
	menuRegistration->Append(new wxMenuItem(menuRegistration, idMenuICP, "ICP..."));
	menuItemAlignTool = menuRegistration->AppendCheckItem(idMenuAlignTool, "Align meshs");
	menuFilters->AppendSubMenu(menuRegistration, "Registration");
	wxMenu* menuSimplification = new wxMenu();
	menuSimplification->Append(new wxMenuItem(menuSimplification, idMenuSurfaceTrimmer, "Surface trimmer..."));
	menuSimplification->Append(new wxMenuItem(menuSimplification, idMenuFastQuadricSimplification, "Fast quadric simplification..."));
	menuSimplification->Append(new wxMenuItem(menuSimplification, idMenuSmoothMesh, "Smooth mesh..."));
	menuSimplification->Append(new wxMenuItem(menuSimplification, idMenuRemeshing, "Remeshing..."));
	menuFilters->AppendSubMenu(menuSimplification, "Simplification");
	wxMenu* menuReconstruction = new wxMenu();
	menuReconstruction->Append(new wxMenuItem(menuReconstruction, idMenuPoisson, "Poisson..."));
	menuReconstruction->Append(new wxMenuItem(menuReconstruction, idMenuSSDRecon, "SSD..."));
	menuFilters->AppendSubMenu(menuReconstruction, "Reconstruction");
	//Views
	menuView = new wxMenu();
	menuItemToolCheckCamVisibility = menuView->AppendCheckItem(idToolCheckCamVisibility, "Check camera visibility");
	menuItemToolScalarViewer = menuView->AppendCheckItem(idMenuScalarViewer, "Scalar viewer tool...");
	menuView->AppendSeparator();
	menuView->Append(idToolLight, "Light");
	//Tools
	menuTools = new wxMenu();
	menuItemToolMeasure = menuTools->AppendCheckItem(idToolMeasure, "Distance");
	menuItemToolAngle = menuTools->AppendCheckItem(idToolAngle, "Angle");
	menuItemToolArea = menuTools->AppendCheckItem(idMenuAreaTool, "Compute area");
	menuItemToolVolume = menuTools->AppendCheckItem(idToolVolume, "Compute volume");
	menuTools->Append(idMenuComputeDisplacement, "Compute displacement...");
	menuItemToolElevation = menuTools->AppendCheckItem(idToolElevation, "Elevation");
	menuTools->AppendSeparator();
	menuTools->Append(new wxMenuItem(menuTools, idMenuSnapshot, "Snapshot..."));
	//Help
	menuHelp = new wxMenu();
	menuHelp->Append(new wxMenuItem(menuHelp, idMenuHelp, "Help..."));
	menuHelp->AppendSeparator();
	menuHelp->Append(new wxMenuItem(menuHelp, idMenuAbout, "About..."));

	menuBar->Append(menuFile, "File");
	menuBar->Append(menuEdit, "Edit");
	menuBar->Append(menuFilters, "Filters");
	menuBar->Append(menuTools, "Tools");
	menuBar->Append(menuView, "View");
	menuBar->Append(menuHelp, "Help");

	//Bitmpas
	bmpToolMeasureOFF = wxICON(ICON_MEASURE_OFF);
	bmpToolMeasureON = wxICON(ICON_MEASURE_ON);
	bmpToolAngleOFF = wxICON(ICON_ANGLE_OFF);
	bmpToolAngleON = wxICON(ICON_ANGLE_ON);
	bmpToolCalcAreaOFF = wxICON(ICON_CALC_AREA_OFF);
	bmpToolCalcAreaON = wxICON(ICON_CALC_AREA_ON);
	bmpToolCalcVolumeOFF = wxICON(ICON_CALC_VOLUME_OFF);
	bmpToolCalcVolumeON = wxICON(ICON_CALC_VOLUME_ON);
	bmpToolElevationOFF = wxICON(ICON_ELEVATION_OFF);
	bmpToolElevationON = wxICON(ICON_ELEVATION_ON);
	bmpToolCalibrationOFF = wxICON(ICON_CALIBRATION_OFF);
	bmpToolCalibrationON = wxICON(ICON_CALIBRATION_ON);
	bmpToolCheckCamVisibilityOFF = wxICON(ICON_CHECKCAM_OFF);
	bmpToolCheckCamVisibilityON = wxICON(ICON_CHECKCAM_ON);
	bmpToolDeletePointsFacesOFF = wxICON(ICON_DELETE_POINTS_OFF);
	bmpToolDeletePointsFacesON = wxICON(ICON_DELETE_POINTS_ON);
	bmpToolVROFF = wxICON(ICON_VR_OFF);
	bmpToolVRON = wxICON(ICON_VR_ON);

	//ToolBar
	toolBar = this->CreateToolBar(wxTB_HORIZONTAL, wxID_ANY);
	toolBar->AddTool(idMenuOpenProject, "ToolOpen", wxICON(ICON_OPEN), wxNullBitmap, wxITEM_NORMAL, "Open project...");
	toolBar->AddTool(idToolDelete, "ToolDelete", wxICON(ICON_DELETE), wxNullBitmap, wxITEM_NORMAL, "Delete");
	toolBar->AddSeparator();
#ifdef DEBUG_MART
	toolBar->AddTool(idToolDebug, "ToolDebug", wxICON(ICON_DEBUG), wxNullBitmap, wxITEM_NORMAL, "Debug");
	toolBar->AddSeparator();
#endif // DEBUG_MART
	toolBar->AddTool(idToolPoints, "ToolPoints", wxICON(ICON_POINTS), wxNullBitmap, wxITEM_NORMAL, "Points");
	toolBar->AddTool(idToolWireframe, "ToolWireframe", wxICON(ICON_WIREFRAME), wxNullBitmap, wxITEM_NORMAL, "Wireframe");
	toolBar->AddTool(idToolSurface, "ToolSurface", wxICON(ICON_SURFACE), wxNullBitmap, wxITEM_NORMAL, "Surface");
	toolBar->AddSeparator();
	toolMeasure = toolBar->AddTool(idToolMeasure, "ToolMeasure", bmpToolMeasureOFF, wxNullBitmap, wxITEM_NORMAL, "Distance");
	toolAngle = toolBar->AddTool(idToolAngle, "ToolAngle", bmpToolAngleOFF, wxNullBitmap, wxITEM_NORMAL, "Angle");
	toolCalcArea = toolBar->AddTool(idMenuAreaTool, "ToolCalcArea", bmpToolCalcAreaOFF, wxNullBitmap, wxITEM_NORMAL, "Compute area");
	toolCalcVolume = toolBar->AddTool(idToolVolume, "ToolCalcVolume", bmpToolCalcVolumeOFF, wxNullBitmap, wxITEM_NORMAL, "Compute volume");
	toolElevation = toolBar->AddTool(idToolElevation, "ToolElevation", bmpToolElevationOFF, wxNullBitmap, wxITEM_NORMAL, "Elevation");
	toolCalibration = toolBar->AddTool(idToolCalibration, "ToolCalibration", bmpToolCalibrationOFF, wxNullBitmap, wxITEM_NORMAL, "Calibration...");
	toolCheckCamVisibility = toolBar->AddTool(idToolCheckCamVisibility, "ToolCheckCamVisibility", bmpToolCheckCamVisibilityOFF, wxNullBitmap, wxITEM_NORMAL, "Check camera visibility");
	toolBar->AddSeparator();
	//toolBar->AddTool(idToolStartReconstruction, "ToolStartReconstruction", wxICON(ICON_START_RECONS), wxNullBitmap, wxITEM_NORMAL, "Start reconstruction...");
	toolBar->AddTool(idToolSnapshot, "ToolSnapshot", wxICON(ICON_SNAPSHOT), wxNullBitmap, wxITEM_NORMAL, "Take a snapshot...");

	toolBar->AddTool(idToolLight, "ToolLight", wxICON(ICON_LIGHT), wxNullBitmap, wxITEM_NORMAL, "Light");
	toolDeletePoints = toolBar->AddTool(idToolDeletePointsFaces, "DeletePointsFaces", bmpToolDeletePointsFacesOFF, wxNullBitmap, wxITEM_NORMAL, "Delete points/faces");
	//toolVR = toolBar->AddTool(idToolVR, "VR", bmpToolVROFF, wxNullBitmap, wxITEM_NORMAL, "Send to VR...");

	//InteractorStyle
	iterStyle = vtkSmartPointer<InteractorStyle>::New();

	//Splitter
	splitterWind = new wxSplitterWindow(this, idSplitterWindow, wxDefaultPosition, wxDefaultSize, wxSP_3D, "Splitter");
	splitterWind->SetMinimumPaneSize(2);//Needed otherwise it will unslip in the double click

	//TreeListCtrl
	treeMesh = new wxTreeListCtrl(splitterWind, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTL_MULTIPLE | wxTL_NO_HEADER | wxTL_CHECKBOX);
	treeMesh->AppendColumn("Meshs", wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, 1);
	//Load image
	bmpTreeCheckCamera = wxICON(ICON_TREE_CHECKCAM);
	wxImage imgTreeCheckCamera = bmpTreeCheckCamera.ConvertToImage();
	imgTreeCheckCamera.Rescale(16, 16);
	//Define the image that will be shown if the camera is seeing the point on checkCamVisibility
	wxImageList *treeMeshImageList = new wxImageList(imgTreeCheckCamera.GetWidth(), imgTreeCheckCamera.GetHeight(), true);
	treeMeshImageList->Add(imgTreeCheckCamera);
	treeMesh->AssignImageList(treeMeshImageList);

	//VTK panel
	vtk_panel = new wxVTKRenderWindowInteractor(splitterWind, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, "VTK");

	renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer360 = vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renWin;

	renWin = vtk_panel->GetRenderWindow();
	renWin->AddRenderer(renderer);
	iterStyle->SetInteractor(vtk_panel);
	iterStyle->SetDefaultRenderer(renderer);
	renWin->GetInteractor()->SetInteractorStyle(iterStyle);

	// 360
	renWin->SetNumberOfLayers(2);
	renderer360->SetActiveCamera(renderer->GetActiveCamera());
	renderer360->SetLayer(1);
	renWin->AddRenderer(renderer360);
	iterStyle->SetRenderer360(renderer360);

	splitterWind->SplitVertically(vtk_panel, treeMesh, 0);
	setTreeVisibility(false);

	//IterStyle
	iterStyle->projects = &projects;
	iterStyle->treeMesh = treeMesh;

	//Tools
	distanceTool = vtkSmartPointer<DistanceTool>::New();
	distanceTool->SetInteractor(vtk_panel);
	angleTool = vtkSmartPointer<AngleTool>::New();
	angleTool->SetInteractor(vtk_panel);
	areaTool = vtkSmartPointer<AreaTool>::New();
	areaTool->SetInteractor(vtk_panel);
	elevationTool = vtkSmartPointer<ElevationTool>::New();
	elevationTool->SetInteractor(vtk_panel);
	elevationTool->setMeshTree(treeMesh);
	viewTool = vtkSmartPointer<ViewTool>::New();
	viewTool->setProjects(&projects);
	viewTool->setTree(treeMesh);
	viewTool->SetInteractor(vtk_panel);
	aligWithAxisTool = vtkSmartPointer<AlignWithAxisTool>::New();
	aligWithAxisTool->SetInteractor(vtk_panel);
	volumeTool = vtkSmartPointer<VolumeTool>::New();
	volumeTool->SetInteractor(vtk_panel);
	checkCamTool = vtkSmartPointer<CheckCamTool>::New();
	checkCamTool->SetInteractor(vtk_panel);
	checkCamTool->setTreeMesh(treeMesh);
	scalarViewerTool = vtkSmartPointer<ScalarViewerTool>::New();
	scalarViewerTool->SetInteractor(vtk_panel);
	scalarViewerTool->SetTree(treeMesh);
	deleteTool = vtkSmartPointer<DeleteTool>::New();
	deleteTool->SetInteractor(vtk_panel);
	updateCameraTool = vtkSmartPointer<UpdateCameraTool>::New();
	updateCameraTool->SetInteractor(vtk_panel);
	alignTool = vtkSmartPointer<AlignTool>::New();
	alignTool->SetInteractor(vtk_panel);
	alignTool->SetTree(treeMesh);
	colorTool = vtkSmartPointer<ColorTool>::New();
	colorTool->SetInteractor(vtk_panel);
	calibrationTool = vtkSmartPointer<CalibrationTool>::New();
	calibrationTool->SetInteractor(vtk_panel);
	calibrationTool->SetWindow(this);

	//Axis
	vtkNew<vtkAxesActor> axisActor;
	axisWidget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	axisWidget->SetOutlineColor(0.9300, 0.5700, 0.1300);
	axisWidget->SetOrientationMarker(axisActor);
	axisWidget->SetInteractor(vtk_panel->GetRenderWindow()->GetInteractor());
	axisWidget->SetViewport(0.0, 0.0, 0.3, 0.3);
	axisWidget->SetEnabled(true);
	axisWidget->InteractiveOff();
	axisWidget->SetEnabled(false);

	//Sizer
	wxBoxSizer* boxSizer = new wxBoxSizer(wxHORIZONTAL);
	boxSizer->Add(splitterWind, 1, wxEXPAND | wxALL, 0);

	this->SetMenuBar(menuBar);
	toolBar->Realize();

	this->SetSizer(boxSizer);
	this->Layout();

	this->Centre(wxBOTH);

	Connect(wxEVT_TREELIST_ITEM_CHECKED, wxTreeListEventHandler(FrmPrincipal::OnTreeChoice), NULL, this);
	Connect(wxEVT_TREELIST_ITEM_ACTIVATED, wxTreeListEventHandler(FrmPrincipal::OnTreeItemActivated), NULL, this);
}

FrmPrincipal::~FrmPrincipal()
{
}

//Menu
void FrmPrincipal::OnMenuCreateProject(wxCommandEvent & event)
{
	WelcomeDialog welcomeDialog(this);
	welcomeDialog.ShowNewProjectWizard();
	const auto projectPath = welcomeDialog.GetProjectPath();
	if (projectPath != "")
	{
		wxBeginBusyCursor();
		LoadProject(projectPath);
		wxEndBusyCursor();
	}
}
void FrmPrincipal::OnMenuOpenProject(wxCommandEvent & event)
{
	wxFileDialog openDialog(this, "Find some cool project", "", "",
		"PROJECT file (*.project)|*.project", wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_OK)
	{
		wxBeginBusyCursor();
		LoadProject(openDialog.GetPath().ToStdString());
		setTreeVisibility(true);
		wxEndBusyCursor();
	}
}
void FrmPrincipal::OnMenuSaveProject(wxCommandEvent & event)
{
	auto project = GetProjectFromTree();
	if (project)
	{
		wxBeginBusyCursor();
		project->Save();
		wxEndBusyCursor();
	}
}
void FrmPrincipal::OnMenuSaveProjectAs(wxCommandEvent & event)
{
	auto project = GetProjectFromTree();
	if (!project)
	{
		return;
	}
	wxFileDialog saveDialog(this, "Save project As", "", "",
		"PROJECT file (*.project)|*.project", wxFD_SAVE);
	bool insist = true;
	wxString path;
	while (insist)
	{
		if (saveDialog.ShowModal() == wxID_OK)
		{
			path = saveDialog.GetPath();
			path = path.substr(0, path.find_last_of('.'));
			if (!wxDirExists(path))
			{
				insist = false;
			}
			else
			{
				wxMessageBox("This project name was already taken.", "Error", wxICON_ERROR);
			}
		}
		else
		{
			return;
		}
	}
	wxBeginBusyCursor();
	project->Save((path + "\\" + wxFileNameFromPath(saveDialog.GetPath())).ToStdString());
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuImportMesh(wxCommandEvent & event)
{
	auto project = GetProjectFromTree();
	if (!project)
	{
		return;
	}
	wxFileDialog openDialog(this, "Find some cool mesh", "", "",
		"OBJ and PLY file (*.obj;*.ply)|*.obj;*.ply", wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() != wxID_OK)
	{
		return;
	}
	wxBeginBusyCursor();
	project->ImportMesh(openDialog.GetPath().ToStdString());
	renderer->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuExportMesh(wxCommandEvent & event)
{
	auto mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	if (mesh->GetHasTexture())
	{
		wxFileDialog saveDialog(this, "Save the mesh", "", "mesh.obj", "OBJ files (*.obj)|*.obj", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (saveDialog.ShowModal() == wxID_OK)
		{
			wxBeginBusyCursor();
			if (!mesh->Export(saveDialog.GetPath().ToStdString()))
			{
				wxLogError("Error while exporting mesh");
			}
			wxEndBusyCursor();
		}
	}
	else
	{
		wxFileDialog saveDialog(this, "Save the mesh", "", "mesh.ply", "PLY files (*.ply)|*.ply", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (saveDialog.ShowModal() == wxID_OK)
		{
			wxBeginBusyCursor();
			if (!mesh->Export(saveDialog.GetPath().ToStdString()))
			{
				wxLogError("Error while exporting mesh");
			}
			wxEndBusyCursor();
		}
	}
}
void FrmPrincipal::OnMenuSnapshot(wxCommandEvent & event)
{
	SnapshotTool::takeSnapshot(vtk_panel->GetRenderWindow(), true);
}
void FrmPrincipal::OnMenuTransform(wxCommandEvent & event)
{
	auto mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	auto project = GetProjectFromTree(mesh->GetTreeItem());
	if (!project)
	{
		return;
	}
	if (aligWithAxisTool->GetEnabled())
	{
		aligWithAxisTool->SetEnabled(false);
		return;
	}
	disableMeasureTools(-1);
	TransformDialog transformDialog(this);
	int id = transformDialog.ShowModal();
	if (id == wxID_OK)
	{
		wxBeginBusyCursor();
		project->Transform(transformDialog.transform);
		renderer->ResetCamera(mesh->GetPolyData()->GetBounds());
		renderer->ResetCameraClippingRange();
		vtk_panel->GetRenderWindow()->Render();
		wxEndBusyCursor();
	}
	else if (id == TransformDialog::idAlignWithTool)
	{
		aligWithAxisTool->setMesh(mesh);
		aligWithAxisTool->SetEnabled(true);
	}
}
void FrmPrincipal::OnMenuUpdateCamera(wxCommandEvent & event)
{
	disableMeasureTools(6);
	if (!updateCameraTool->GetEnabled())
	{
		Camera* cam = getCameraFromTree();
		if (cam == nullptr)
		{
			menuItemUpdateCamera->Check(false);
			return;
		}
		cam->setVisibility(true);
		treeMesh->CheckItem(cam->getListItemCamera(), wxCHK_CHECKED);
		Utils::updateCamera(renderer, cam);
		cam->createImageActor(renderer);
		updateCameraTool->setCamera(cam);
		updateCameraTool->SetEnabled(true);
		menuItemUpdateCamera->Check();
	}
	else
	{
		updateCameraTool->SetEnabled(false);
		menuItemUpdateCamera->Check(false);
	}
	vtk_panel->SetFocus();
}
void FrmPrincipal::OnMenuShowAxis(wxCommandEvent & event)
{
	axisWidget->SetEnabled(menuItemShowAxis->IsChecked());
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnMenuShowViews(wxCommandEvent & event)
{
	viewTool->SetEnabled(menuItemShowViews->IsChecked());
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnMenuFlyToPoint(wxCommandEvent & event)
{
	this->iterStyle->setFlyToPoint(!this->iterStyle->getFlyToPoint());
}
void FrmPrincipal::OnMenuPoisson(wxCommandEvent & event)
{
	auto project = GetProjectFromTree();
	if (!project)
	{
		return;
	}
	auto mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	PoissonDialog* poissonDialog = new PoissonDialog(this);
	if (poissonDialog->ShowModal() != wxID_OK)
	{
		delete poissonDialog;
		return;
	}
	wxBeginBusyCursor();
	vtkSmartPointer<vtkPolyData> polyData = mesh->GetPolyData();
	if (!polyData->GetPointData()->GetNormals())
	{
		//Just generate mesh for cells, not points
		vtkNew<vtkPolyDataNormals> normalGenerator;
		normalGenerator->SetInputData(polyData);
		normalGenerator->Update();
		polyData = normalGenerator->GetOutput();
		if (!polyData->GetPointData()->GetNormals())//It is a point cloud 
		{
			wxMessageBox("The point cloud has no normals", "Error", wxICON_ERROR);
			wxEndBusyCursor();
			delete poissonDialog;
			return;
		}
	}
	polyData = HelperAdaptativeSolvers::computePoisson(polyData, poissonDialog->getOptions());
	const auto depth = poissonDialog->getOptions().depth;
	delete poissonDialog;
	if (!polyData)
	{
		wxMessageBox("It was not possible to reconstruct", "Error", wxICON_ERROR);
		wxEndBusyCursor();
		return;
	}
	std::string filename = "Poisson_Depth_" + std::to_string(depth) + ".ply";
	project->AddMesh(filename, polyData);
	vtk_panel->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuSSDRecon(wxCommandEvent & event)
{
	auto project = GetProjectFromTree();
	if (!project)
	{
		return;
	}
	auto mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	SSDDialog* ssdDialog = new SSDDialog(this);
	if (ssdDialog->ShowModal() != wxID_OK)
	{
		delete ssdDialog;
		return;
	}
	wxBeginBusyCursor();
	vtkSmartPointer<vtkPolyData> polyData = mesh->GetPolyData();
	if (!polyData->GetPointData()->GetNormals())
	{
		//Just generate mesh for cells, not points
		vtkNew<vtkPolyDataNormals> normalGenerator;
		normalGenerator->SetInputData(polyData);
		normalGenerator->Update();
		polyData = normalGenerator->GetOutput();
		if (!polyData->GetPointData()->GetNormals())//It is a point cloud 
		{
			wxMessageBox("The point cloud has no normals", "Error", wxICON_ERROR);
			wxEndBusyCursor();
			delete ssdDialog;
			return;
		}
	}
	polyData = HelperAdaptativeSolvers::computeSSD(polyData, ssdDialog->getOptions());
	const auto depth = ssdDialog->getOptions().depth;
	delete ssdDialog;;
	if (!polyData)
	{
		wxMessageBox("It was not possible to reconstruct", "Error", wxICON_ERROR);
		wxEndBusyCursor();
		return;
	}
	std::string filename = "SSD_Depth_" + std::to_string(depth) + ".ply";
	project->AddMesh(filename, polyData);
	vtk_panel->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuICP(wxCommandEvent & event)
{
	std::set<Mesh*> meshs = getMeshsFromTree(2);
	if (meshs.size() != 2)
	{
		return;
	}
	if (!icpDialog)
	{
		icpDialog = std::make_unique<ICPDialog>(this);
		icpDialog->SetRenderer(renderer);
		icpDialog->SetTree(treeMesh);
	}
	auto meshSource = *std::next(meshs.begin(), 0);
	auto meshTarget = *std::next(meshs.begin(), 1);
	icpDialog->SetSourceData(GetProjectFromTree(meshSource->GetTreeItem()), meshSource);
	icpDialog->SetTargetData(GetProjectFromTree(meshTarget->GetTreeItem()), meshTarget);
	treeMesh->SetItemText(meshSource->GetTreeItem(), "Source - ICP tool");
	treeMesh->SetItemText(meshTarget->GetTreeItem(), "Target - ICP tool");
	icpDialog->Show();
}
void FrmPrincipal::OnMenuAlignTool(wxCommandEvent & event)
{
	menuItemAlignTool->Check(false);
	disableMeasureTools(7);
	if (alignTool->GetEnabled())
	{
		alignTool->SetEnabled(false);
		return;
	}
	std::set<Mesh*> meshs = getMeshsFromTree(2);
	if (meshs.size() != 2)
	{
		return;
	}
	auto meshSource = *std::next(meshs.begin(), 0);
	auto meshTarget = *std::next(meshs.begin(), 1);
	alignTool->SetSourceData(GetProjectFromTree(meshSource->GetTreeItem()), meshSource);
	alignTool->SetTargetData(GetProjectFromTree(meshTarget->GetTreeItem()), meshTarget);
	alignTool->SetEnabled(true);
	menuItemAlignTool->Check(true);
}
void FrmPrincipal::OnMenuSurfaceTrimmer(wxCommandEvent & event)
{
	Mesh* mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	if (mesh->GetIsPointCloud())
	{
		wxMessageBox("This filter only works with meshs", "Error", wxICON_ERROR);
		return;
	}
	auto polyData = mesh->GetPolyData();
	if (polyData->GetPointData())
	{
		if (!polyData->GetPointData()->GetArray("Values"))
		{
			wxMessageBox("The mesh has no value field", "Error", wxICON_ERROR);
			return;
		}
	}
	else
	{
		wxMessageBox("The mesh has no value field", "Error", wxICON_ERROR);
		return;
	}
	int trimValue = wxGetNumberFromUser("Choose the trim value", "Trim", "Surface trimmer", 7, 1, 100, this);
	if (trimValue == -1)
	{
		return;
	}
	wxBeginBusyCursor();
	SurfaceTrimmer::Options options;
	options.trim_value = trimValue;
	polyData = HelperAdaptativeSolvers::computeSurfaceTrimmer(polyData, options);
	if (!polyData)
	{
		wxMessageBox("It was not possible to trim", "Error", wxICON_ERROR);
		wxEndBusyCursor();
		return;
	}
	mesh->UpdateData(polyData);
	vtk_panel->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuFastQuadricSimplification(wxCommandEvent & event)
{
	Mesh* mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	if (mesh->GetIsPointCloud())
	{
		wxLogError("This method does not work with point clouds");
		return;
	}
	const auto reduction = wxGetNumberFromUser("Choose the percentage of reduction", "Reduction (%)", "Fast quadric simplification", 50, 1, 100, nullptr);
	if (reduction != -1)
	{
		wxBeginBusyCursor();
		const auto reductionPercent = (100 - reduction) / 100.0f;
		auto count = 0;
		for (const auto& actor : mesh->actors)
		{
			auto result = HelperFastQuadricSimplification::computeQuadricDecimation(Utils::getActorPolyData(actor), reductionPercent);
			if (result)
			{
				mesh->UpdateData(result, count);
			}
			count++;
		}
		renderer->GetRenderWindow()->Render();
		wxEndBusyCursor();
	}
}
void FrmPrincipal::OnMenuSmoothMesh(wxCommandEvent & event)
{
	Mesh* mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	if (mesh->GetIsPointCloud())
	{
		wxMessageBox("This tool does not work with point clouds", "Error", wxICON_ERROR, this);
		return;
	}
	if (mesh->GetHasTexture())
	{
		wxMessageBox("This tool does not work with textured meshes", "Error", wxICON_ERROR, this);
		return;
	}
	if (!smoothMeshDialog)
	{
		smoothMeshDialog = std::make_unique<SmoothMeshDialog>(this);
		smoothMeshDialog->SetRenderer(renderer);
	}
	smoothMeshDialog->setMesh(mesh);
	smoothMeshDialog->Show();
}
void FrmPrincipal::OnMenuRemeshing(wxCommandEvent & event)
{
	auto mesh = getMeshFromTree();
	if (!mesh)
	{
		return;
	}
	auto project = GetProjectFromTree(mesh->GetTreeItem());
	if (!project)
	{
		return;
	}
	if (mesh->GetIsPointCloud())
	{
		wxMessageBox("This tool does not work with point clouds", "Error", wxICON_ERROR, this);
		return;
	}
	if (mesh->GetHasTexture())
	{
		wxMessageBox("This tool does not work with textured meshes", "Error", wxICON_ERROR, this);
		return;
	}
	GetDoubleDialog doubleDialog("Set the remeshing parameters", "Target edge length", "Remeshing", 0.04, 0.00001, 100.0);
	if (doubleDialog.ShowModal() != wxOK)
	{
		return;
	}
	wxBeginBusyCursor();
	const auto polyData = Utils::Remeshing(mesh->GetPolyData(), doubleDialog.GetValue());
	std::string filename = "Remeshing_Edge_Length_" + std::to_string(doubleDialog.GetValue()) + ".ply";
	project->AddMesh(filename, polyData);
	vtk_panel->GetRenderWindow()->Render();
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuColorTool(wxCommandEvent & event)
{
	menuItemColorTool->Check(false);
	disableMeasureTools(9);
	if (!colorTool->GetEnabled())
	{
		Mesh* mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		if (!mesh->GetIsPointCloud())
		{
			wxMessageBox("This tool only works with point clouds.", "Error", wxICON_ERROR);
			return;
		}
		if (!mesh->GetHasColor())
		{
			wxMessageBox("This tool only works with colorful point clouds.", "Error", wxICON_ERROR);
			return;
		}
		colorTool->setMesh(mesh);
		colorTool->SetEnabled(true);
		menuItemColorTool->Check(true);
	}
	else
	{
		colorTool->SetEnabled(false);
	}
}
void FrmPrincipal::OnMenuAreaTool(wxCommandEvent & event)
{
	disableMeasureTools(10);
	menuItemToolArea->Check(false);
	toolCalcArea->SetNormalBitmap(bmpToolCalcAreaOFF);
	if (!areaTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		auto project = GetProjectFromTree();
		if (!mesh || !project)
		{
			return;
		}
		areaTool->setMesh(mesh);
		areaTool->setCalibration(project->GetCalibration());
		areaTool->SetEnabled(true);
		menuItemToolArea->Check(true);
		toolCalcArea->SetNormalBitmap(bmpToolCalcAreaON);
	}
	else
	{
		areaTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnMenuComputeDisplacement(wxCommandEvent & event)
{
	auto meshs = getMeshsFromTree(3);
	if (meshs.size() != 3)
	{
		return;
	}
	Mesh* pointCloud = nullptr;
	Mesh* simplifiedPointCloud = nullptr;
	Mesh* simplifiedMesh = nullptr;
	for (const auto& mesh : meshs)
	{
		if (mesh->GetIsPointCloud())
		{
			if (!pointCloud)
			{
				pointCloud = mesh;
			}
			else
			{
				simplifiedPointCloud = mesh;
			}
		}
		else
		{
			simplifiedMesh = mesh;
		}
	}
	if (!pointCloud || !simplifiedPointCloud || !simplifiedMesh)
	{
		wxMessageBox("You need to select a surface and two point clouds.", "Error", wxICON_ERROR);
		return;
	}
	if (treeMesh->GetItemParent(simplifiedPointCloud->GetTreeItem()) != treeMesh->GetItemParent(simplifiedMesh->GetTreeItem()))
	{
		auto temp = simplifiedPointCloud;
		simplifiedPointCloud = pointCloud;
		pointCloud = temp;
	}
	const auto datFile = wxStandardPaths::Get().GetTempDir() + "\\input.dat";
	DisplacementTool displacement(pointCloud->GetPolyData()->GetPoints(),
		simplifiedPointCloud->GetPolyData()->GetPoints(), simplifiedMesh, datFile.ToStdString());
	wxBeginBusyCursor();
	auto polyData = displacement.Compute();
	if (!polyData)
	{
		wxMessageBox("Could not compute the displacement.", "Error", wxICON_ERROR);
	}
	else
	{
		auto project = GetProjectFromTree(displacement.GetSimplifiedMesh()->GetTreeItem());
		std::string fileName = "Displacement.ply";
		project->AddMesh(fileName, polyData);
	}
	wxEndBusyCursor();
}
void FrmPrincipal::OnMenuScalarViewer(wxCommandEvent & event)
{
	disableMeasureTools(8);
	menuItemToolScalarViewer->Check(false);
	if (!scalarViewerTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		const auto numberOfArrays = mesh->GetPolyData()->GetPointData()->GetNumberOfArrays();
		if (numberOfArrays == 0)
		{
			wxMessageBox("This mesh/point cloud does not have arrays", "Error", wxICON_ERROR);
			return;
		}
		wxArrayString choices;
		for (size_t i = 0; i < numberOfArrays; i++)
		{
			choices.Add(mesh->GetPolyData()->GetPointData()->GetAbstractArray(i)->GetName());
		}
		const int selection = wxGetSingleChoiceIndex("Select the scalar you want to inspect", "Scalar selection", choices);
		if (selection == -1)
		{
			return;
		}
		scalarViewerTool->SetScalar(mesh->GetPolyData()->GetPointData()->GetArray(selection));
		scalarViewerTool->SetMesh(mesh);
		scalarViewerTool->SetCalibration(project->GetCalibration());
		scalarViewerTool->SetEnabled(true);
		menuItemToolScalarViewer->Check(true);
	}
	else
	{
		scalarViewerTool->SetEnabled(false);
	}
}
void FrmPrincipal::OnMenuHelp(wxCommandEvent & event)
{
	helpController->DisplayContents();
}
void FrmPrincipal::OnMenuAbout(wxCommandEvent & event)
{
	AboutDialog* about = new AboutDialog(this);
	about->ShowModal();
	delete about;
}
//ToolBar
void FrmPrincipal::OnToolDelete(wxCommandEvent & event)
{
	wxTreeListItems itens;
	if (!treeMesh->GetSelections(itens))
	{
		wxMessageBox("Please select something on the tree", "Error", wxICON_ERROR, this);
		return;
	}
	disableTools();
	for (const auto& item : itens)
	{
		if (!item.IsOk())
		{
			continue;
		}
		for (int i = 0; i < projects.size(); i++)
		{
			if (projects[i]->GetTreeItem() == item)
			{
				if (projects[i]->GetModified())
				{
					if (wxMessageBox("The project " + treeMesh->GetItemText(item) +
						" was modified, do you want to save it?", "", wxYES_NO | wxICON_QUESTION, this) == wxYES)
					{
						projects[i]->Save();
					}
				}
				delete projects[i];
				projects.erase(projects.begin() + i);
				if (projects.size() == 0)
				{
					setTreeVisibility(false);
					this->Layout();
				}
				continue;
			}
			else if (projects[i]->GetTreeItemCameras() == item)
			{
				projects[i]->DestructCameras();
				continue;
			}
			else if (projects[i]->GetTreeItemVolumes() == item)
			{
				projects[i]->DestructVolumes();
				continue;
			}
			for (int j = 0; j < projects[i]->meshes.size(); j++)
			{
				if (projects[i]->meshes[j]->GetTreeItem() == item)
				{
					projects[i]->DeleteMesh(j);
					continue;
				}
			}
			for (int j = 0; j < projects[i]->cameras.size(); j++)
			{
				if (projects[i]->cameras[j]->getListItemCamera() == item)
				{
					projects[i]->DeleteCamera(j);
					continue;
				}
			}
			for (int j = 0; j < projects[i]->volumes.size(); j++)
			{
				if (projects[i]->volumes[j]->getListItem() == item)
				{
					projects[i]->DeleteVolume(j);
					continue;
				}
			}
		}
	}
	renderer->GetRenderWindow()->Render();
}
//DEBUG
#include <vtkTableBasedClipDataSet.h>
#include <vtkTriangleFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkGeometryFilter.h>
#include <vtkConnectivityFilter.h>
#include <vtkPlane.h>
#include <vtkStripper.h>
#include <vtkIdFilter.h>
void FrmPrincipal::OnToolDebug(wxCommandEvent & event)
{
	//auto mesh = getMeshFromTree();
	//if (!mesh)
	//{
	//	return;
	//}
	//auto polyData = mesh->GetPolyData();
	//auto scalars = polyData->GetPointData()->GetScalars();
	//const auto numberOfPoints = scalars->GetNumberOfTuples();
	//vtkNew<vtkUnsignedCharArray> newScalars;
	//newScalars->SetName("RGB");
	//newScalars->SetNumberOfComponents(3);
	//newScalars->SetNumberOfTuples(numberOfPoints);
	//for (size_t i = 0; i < numberOfPoints; i++)
	//{
	//	double tuple[4];
	//	scalars->GetTuple(i, tuple);
	//	newScalars->InsertTuple3(i, tuple[0], tuple[1], tuple[2]);
	//}
	//polyData->GetPointData()->SetScalars(newScalars);
	//mesh->UpdateData(polyData);
	//return;
	//auto polyData = mesh->GetPolyData();
	//vtkNew<vtkPlane> plane;
	//plane->SetOrigin(polyData->GetCenter());
	//double normal[3] = {0.0, 1.0, 0.0};
	//plane->SetNormal(normal);
	//vtkNew<vtkTableBasedClipDataSet> clipPolyData;
	//clipPolyData->SetInputData(polyData);
	//clipPolyData->SetClipFunction(plane);
	//clipPolyData->Update();
	//vtkNew<vtkGeometryFilter> geometryFilter;
	//geometryFilter->SetInputData(clipPolyData->GetOutput());
	//geometryFilter->MergingOff();
	//geometryFilter->Update();
	//auto geometryFilterOutput = geometryFilter->GetOutput();
	//vtkNew<vtkIdFilter> idFilter;
	//idFilter->SetInputConnection(geometryFilter->GetOutputPort());
	//idFilter->SetPointIdsArrayName("OriginalPointIds");
	//idFilter->Update();
	////
	//vtkNew<vtkFeatureEdges> featureEdges;
	//featureEdges->ExtractAllEdgeTypesOff();
	//featureEdges->BoundaryEdgesOn();
	//featureEdges->SetInputData(idFilter->GetOutput());
	//featureEdges->Update();

	//vtkNew<vtkConnectivityFilter> connectivityFilter;
	//connectivityFilter->SetInputData(featureEdges->GetOutput());
	//connectivityFilter->SetExtractionModeToAllRegions();
	//connectivityFilter->Update();
	//const auto numberOfRegions = connectivityFilter->GetNumberOfExtractedRegions();
	//for (vtkIdType i = 0; i < numberOfRegions; i++)
	//{
	//	vtkNew<vtkConnectivityFilter> connectivityFilterAux;
	//	connectivityFilterAux->SetInputData(connectivityFilter->GetOutput());
	//	connectivityFilterAux->SetExtractionModeToSpecifiedRegions();
	//	connectivityFilterAux->AddSpecifiedRegion(i);
	//	connectivityFilterAux->Update();
	//	vtkNew<vtkStripper> stripper;
	//	stripper->SetInputData(connectivityFilterAux->GetOutput());
	//	stripper->SetMaximumLength(VTK_INT_MAX);
	//	stripper->Update();
	//	vtkNew<vtkCleanPolyData> cleanPolyData;
	//	cleanPolyData->SetInputData(stripper->GetOutput());
	//	cleanPolyData->PointMergingOff();
	//	cleanPolyData->Update();
	//	const auto originalPointIds = cleanPolyData->GetOutput()->GetPointData()->GetArray("OriginalPointIds");
	//	const auto triangles = Utils::closePolyline(cleanPolyData->GetOutput()->GetPoints());
	//	for (const auto& triangle : triangles)
	//	{
	//		vtkNew<vtkIdList> pts;
	//		for (const auto& id : triangle)
	//		{
	//			pts->InsertNextId(originalPointIds->GetTuple1(id));
	//		}
	//		geometryFilterOutput->InsertNextCell(VTK_TRIANGLE, pts);
	//	}
	//	Draw::createDataSet(renderer, geometryFilterOutput, 1.0, 0.0, 0.0);
	//	//vtkNew<vtkPoints> points;
	//	//for (size_t k = 0; k < cleanPolyData->GetOutput()->GetNumberOfPoints(); k++)
	//	//{
	//	//	points->InsertNextPoint(cleanPolyData->GetOutput()->GetPoint(k));
	//	//}
	//	//Draw::create3DLine(renderer, points);
	//}
	//
	//mesh->UpdateData(Utils::fillHoles(mesh->GetPolyData()));
}
void FrmPrincipal::OnToolPoints(wxCommandEvent& event)
{
	menuRepresentation->Check(idToolPoints, true);
	for (auto project: projects)
	{
		project->SetRepresentation(0);
	}
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolWireframe(wxCommandEvent& event)
{
	menuRepresentation->Check(idToolWireframe, true);
	for (auto project : projects)
	{
		project->SetRepresentation(1);
	}
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolSurface(wxCommandEvent& event)
{
	menuRepresentation->Check(idToolSurface, true);
	for (auto project : projects)
	{
		project->SetRepresentation(2);
	}
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolMeasure(wxCommandEvent& event)
{
	disableMeasureTools(0);
	menuItemToolMeasure->Check(false);
	toolMeasure->SetNormalBitmap(bmpToolMeasureOFF);
	if (!distanceTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		distanceTool->setProject(project);
		distanceTool->setMesh(mesh);
		distanceTool->SetEnabled(true);
		menuItemToolMeasure->Check(true);
		toolMeasure->SetNormalBitmap(bmpToolMeasureON);
	}
	else
	{
		distanceTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolAngle(wxCommandEvent& event)
{
	disableMeasureTools(1);
	menuItemToolAngle->Check(false);
	toolAngle->SetNormalBitmap(bmpToolAngleOFF);
	if (!angleTool->GetEnabled())
	{
		Mesh* mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		angleTool->setMesh(mesh);
		angleTool->SetEnabled(true);
		menuItemToolAngle->Check(true);
		toolAngle->SetNormalBitmap(bmpToolAngleON);
	}
	else
	{
		angleTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolVolume(wxCommandEvent & event)
{
	disableMeasureTools(4);
	menuItemToolVolume->Check(false);
	toolCalcVolume->SetNormalBitmap(bmpToolCalcVolumeOFF);
	if (!volumeTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		wxBeginBusyCursor();
		//Test if the mesh is watertight
		vtkSmartPointer<vtkPolyData> polyData = mesh->GetPolyData();
		if (!polyData)
		{
			wxMessageBox("It was not possible to calculate the volume", "Error", wxICON_ERROR);
			wxEndBusyCursor();
			return;
		}
		if (!mesh->GetIsPointCloud())//Mesh
		{
			vtkNew<vtkFeatureEdges> featureEdges;
			featureEdges->SetInputData(polyData);
			featureEdges->ExtractAllEdgeTypesOff();
			featureEdges->BoundaryEdgesOn();
			featureEdges->Update();
			if (featureEdges->GetOutput()->GetPoints()->GetNumberOfPoints() == 0)//Closed mesh
			{
				wxEndBusyCursor();
			}
			else
			{
				wxMessageBox("You need a closed mesh to compute the volume", "Error", wxICON_ERROR);
				wxEndBusyCursor();
				return;
			}
		}
		else
		{
			wxMessageBox("You need a closed mesh to compute the volume", "Error", wxICON_ERROR);
			wxEndBusyCursor();
			return;
		}
		volumeTool->setMesh(mesh);
		volumeTool->setProject(project);
		volumeTool->SetEnabled(true);
		menuItemToolVolume->Check(true);
		toolCalcVolume->SetNormalBitmap(bmpToolCalcVolumeON);
	}
	else
	{
		volumeTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolElevation(wxCommandEvent & event)
{
	disableMeasureTools(2);
	menuItemToolElevation->Check(false);
	toolElevation->SetNormalBitmap(bmpToolElevationOFF);
	if (!elevationTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		wxBeginBusyCursor();
		elevationTool->setMesh(mesh);
		elevationTool->setCalibration(project->GetCalibration());
		elevationTool->SetEnabled(true);
		menuItemToolElevation->Check(true);
		toolElevation->SetNormalBitmap(bmpToolElevationON);
		wxEndBusyCursor();
	}
	else
	{
		elevationTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolCalibration(wxCommandEvent& event)
{
	disableMeasureTools(11);
	toolCalibration->SetNormalBitmap(bmpToolCalibrationOFF);
	if (!calibrationTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		bool useGPS = false;
		if (wxMessageBox("Do you want to calibrate using GPS data?", "", wxYES_NO | wxICON_QUESTION, this) == wxYES)
		{
			useGPS = true;
			if (project->cameras.size() > 0)
			{
				if (project->cameras[0]->gpsData != nullptr)
				{
					if (wxMessageBox("Do you want to use the camera's GPS data?", "", wxYES_NO | wxICON_QUESTION, this) == wxYES)
					{
						std::vector< std::pair<double*, GPSData>> gpsData;
						for (const auto& camera : project->cameras)
						{
							if (camera->cameraPoints.size() > 0 && camera->gpsData)
							{
								gpsData.emplace_back(std::make_pair(camera->cameraPoints[0], *camera->gpsData));
							}
						}
						if (gpsData.size() < 4)
						{
							wxMessageBox("Not enough GPS points.", "Error", wxICON_ERROR, this);
							return;
						}
						calibrationTool->SetMesh(mesh);
						calibrationTool->SetProject(project);
						calibrationTool->SetEnabled(true);
						calibrationTool->Calibrate(gpsData);
						calibrationTool->SetEnabled(false);
						return;
					}
				}
			}
		}
		calibrationTool->SetMesh(mesh);
		calibrationTool->SetProject(project);
		calibrationTool->SetUseGPS(useGPS);
		calibrationTool->SetEnabled(true);
		toolCalibration->SetNormalBitmap(bmpToolCalibrationON);
	}
	else
	{
		calibrationTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolCheckCamVisibility(wxCommandEvent & event)
{
	menuItemToolCheckCamVisibility->Check(false);
	toolCheckCamVisibility->SetNormalBitmap(bmpToolCheckCamVisibilityOFF);
	if (!checkCamTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		auto project = GetProjectFromTree(mesh->GetTreeItem());
		if (!project)
		{
			return;
		}
		if (project->cameras.size() > 0)
		{
			if (project->cameras.back()->getIs360())
			{
				wxMessageBox("This tool does not work with 360 images", "Error", wxICON_ERROR, this);
				return;
			}
		}
		if (project->cameras.size() == 0)
		{
			wxMessageBox("This mesh doesn't have images", "Error", wxICON_ERROR, this);
			return;
		}
		checkCamTool->setProject(project);
		checkCamTool->setMesh(mesh);
		checkCamTool->SetEnabled(true);
		menuItemToolCheckCamVisibility->Check(true);
		toolCheckCamVisibility->SetNormalBitmap(bmpToolCheckCamVisibilityON);
	}
	else
	{
		checkCamTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolStartReconstruction(wxCommandEvent & event)
{
	ReconstructionDialog* recons = new ReconstructionDialog(this);
	if (recons->ShowModal() == wxID_OK)
	{
		//if (wxMessageBox("The reconstruction was a success! Do you wanna see it?", "", wxYES_NO | wxICON_QUESTION, this) == wxYES)
		//{
		//	wxArrayString paths;
		//	paths.push_back(recons->getOutputPath());
		//	loadMesh(paths);
		//}
	}
	delete recons;
}
void FrmPrincipal::OnToolSnapshot(wxCommandEvent & event)
{
	SnapshotTool::takeSnapshot(vtk_panel->GetRenderWindow());
}
void FrmPrincipal::OnToolLight(wxCommandEvent & event)
{
	isLightOn = !isLightOn;
	for (const auto& project : projects)
	{
		for (const auto& mesh : project->meshes)
		{
			for (const auto& actor : mesh->actors)
			{
				actor->GetProperty()->SetLighting(isLightOn);
			}
		}
	}
	vtk_panel->GetRenderWindow()->Render();
}
void FrmPrincipal::OnToolDeletePointsFaces(wxCommandEvent & event)
{
	disableMeasureTools(5);
	menuItemToolDeletePoints->Check(false);
	toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
	if (!deleteTool->GetEnabled())
	{
		auto mesh = getMeshFromTree();
		if (!mesh)
		{
			return;
		}
		wxBeginBusyCursor();
		deleteTool->setMesh(mesh);
		deleteTool->SetEnabled(true);
		menuItemToolDeletePoints->Check(true);
		toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesON);
		wxEndBusyCursor();
	}
	else
	{
		deleteTool->SetEnabled(false);
	}
	toolBar->Realize();
}
void FrmPrincipal::OnToolVR(wxCommandEvent & event)
{
	Mesh* mesh = getMeshFromTree();
	if (mesh == NULL)
	{
		return;
	}
	auto project = GetProjectFromTree();
	VRDialog* vrDialog = new VRDialog(this, mesh, project->GetCalibration());
	vrDialog->setCamera(renderer->GetActiveCamera());
	toolVR->SetNormalBitmap(bmpToolVRON);
	toolBar->Realize();
	vrDialog->ShowModal();
	delete vrDialog;
	toolVR->SetNormalBitmap(bmpToolVROFF);
	toolBar->Realize();
}
// CheckBox
void FrmPrincipal::OnTreeChoice(wxTreeListEvent& event)
{
	wxTreeListItem item = event.GetItem();
	if (!item.IsOk())
	{
		return;
	}
	for (auto project : projects)
	{
		if (project->GetTreeItem() == item)
		{
			project->SetVisibility(!project->GetVisibility());
			break;
		}
		else if (project->GetTreeItemCameras() == item)
		{
			//Here i just need to copy the checkstate
			project->SetCamerasVisibility(treeMesh->GetCheckedState(item));
			treeMesh->CheckItemRecursively(item, (wxCheckBoxState)project->GetCamerasVisibility());
			break;
		}
		else if (project->GetTreeItemVolumes() == item)
		{
			//Here i just need to copy the checkstate
			project->SetVolumesVisibility(treeMesh->GetCheckedState(item));
			treeMesh->CheckItemRecursively(item, (wxCheckBoxState)project->GetVolumesVisibility());
			break;
		}
		for (auto mesh : project->meshes)
		{
			if (mesh->GetTreeItem() == item)
			{
				mesh->SetVisibility(!mesh->GetVisibility());
				break;
			}
			else if (mesh->GetTreeItemTexture() == item)
			{
				mesh->SetTextureVisibility(!mesh->GetTextureVisibility());
				break;
			}
		}
		for (auto camera : project->cameras)
		{
			if (camera->getListItemCamera() == item)
			{
				camera->setVisibility(!camera->getVisibility());
				break;
			}
		}
		for (auto volume : project->volumes)
		{
			if (volume->getListItem() == item)
			{
				volume->setVisibility(!volume->getVisibility());
				break;
			}
		}
	}
	vtk_panel->SetFocus();
	//disableMeasureTools(3);
	renderer->ResetCameraClippingRange();
	vtk_panel->GetRenderWindow()->Render();
}
// Double click
void FrmPrincipal::OnTreeItemActivated(wxTreeListEvent & event)
{
	iterStyle->setMode360Image(false);
	wxTreeListItem item = event.GetItem();
	if (!item.IsOk())
	{
		return;
	}
	vtk_panel->SetFocus();
	for (auto project : projects)
	{
		project->Update360ClickPoints(nullptr);
		if (project->GetTreeItemCameras() == item)
		{
			return;
		}
		else if (project->GetTreeItemVolumes() == item)
		{
			return;
		}
		for (auto mesh : project->meshes)
		{
			if (mesh->GetTreeItem() == item)
			{
				mesh->SetVisibility(true);
				treeMesh->CheckItem(item, wxCHK_CHECKED);
				renderer->ResetCamera(mesh->GetPolyData()->GetBounds());
				renderer->ResetCameraClippingRange();
				vtk_panel->GetRenderWindow()->Render();
				return;
			}
		}
		for (auto camera : project->cameras)
		{
			if (camera->getListItemCamera() == item)
			{
				camera->setVisibility(true);
				treeMesh->CheckItem(item, wxCHK_CHECKED);
				Utils::updateCamera(renderer, camera);
				camera->createImageActor(renderer);
				if (camera->getIs360() && camera->image360Actor)
				{
					iterStyle->setMode360Image(true);
					project->Update360ClickPoints(camera);
				}
				vtk_panel->GetRenderWindow()->Render();
				return;
			}
		}
		for (auto volume : project->volumes)
		{
			if (volume->getListItem() == item)
			{
				volume->setVisibility(true);
				treeMesh->CheckItem(item, wxCHK_CHECKED);
				renderer->ResetCamera(volume->getActorBounds());
				renderer->ResetCameraClippingRange();
				vtk_panel->GetRenderWindow()->Render();
				return;
			}
		}
	}
}
wxTreeListItem FrmPrincipal::addItemToTree(const wxTreeListItem& parentItem, const std::string& itemName, wxCheckBoxState state)
{
	wxTreeListItem newTreeItem = treeMesh->AppendItem(parentItem, wxString(itemName));
	treeMesh->CheckItem(newTreeItem, state);
	if (!isTreeVisible)
	{
		setTreeVisibility(true);
	}
	return newTreeItem;
}
Project * FrmPrincipal::GetProjectFromTree()
{
	wxTreeListItem item;
	if (projects.size() == 1)//Avoid annoying the user if there is just one mesh on the tree
	{
		item = projects[0]->GetTreeItem();
	}
	else
	{
		wxTreeListItems itens;
		if (treeMesh->GetSelections(itens))
		{
			item = itens[0];
		}
	}
	if (!item.IsOk())
	{
		wxLogWarning("Please select some project on the tree");
		return nullptr;
	}
	return GetProjectFromTree(item);
}
Project * FrmPrincipal::GetProjectFromTree(wxTreeListItem item)
{
	if (!item.IsOk())
	{
		return nullptr;
	}
	//If the user selects a project's children, find the project
	while (treeMesh->GetItemParent(item) != treeMesh->GetRootItem())
	{
		item = treeMesh->GetItemParent(item);
	}
	for (const auto& project : projects)
	{
		if (item == project->GetTreeItem())
		{
			treeMesh->CheckItem(item, wxCHK_CHECKED);
			return project;
		}
	}
	return nullptr;
}
Mesh* FrmPrincipal::getMeshFromTree()
{
	wxTreeListItem item;
	wxTreeListItems itens;
	if (treeMesh->GetSelections(itens))
	{
		item = itens[0];
	}
	if (!item.IsOk())
	{
		wxLogWarning("Please select some mesh on the tree");
		return nullptr;
	}
	return getMeshFromTree(item);
}
Mesh * FrmPrincipal::getMeshFromTree(wxTreeListItem item)
{
	if (!item.IsOk())
	{
		return nullptr;
	}
	for (const auto& project : projects)
	{
		for (auto mesh : project->meshes)
		{
			if (item == mesh->GetTreeItem() || item == mesh->GetTreeItemTexture())
			{
				treeMesh->CheckItem(item, wxCHK_CHECKED);
				mesh->SetVisibility(true);
				vtk_panel->GetRenderWindow()->Render();
				return mesh;
			}
		}
	}
	wxLogWarning("Please select some mesh on the tree");
	return nullptr;
}
std::set<Mesh*> FrmPrincipal::getMeshsFromTree(unsigned int numberOfMeshs)
{
	std::set<Mesh*> meshs;
	wxTreeListItems itens;
	if (treeMesh->GetSelections(itens))
	{
		if (itens.size() != numberOfMeshs)
		{
			wxLogWarning(wxString("Please select " + std::to_string(numberOfMeshs)  + " meshs on the tree"));
			return meshs;
		}
		for (auto item : itens)
		{
			if (item.IsOk())
			{
				Mesh* mesh = getMeshFromTree(item);
				if (mesh)
				{
					meshs.insert(mesh);
				}
			}
		}
		if (meshs.size() != numberOfMeshs)
		{
			meshs.clear();
			wxLogWarning(wxString("Please select " + std::to_string(numberOfMeshs) + " meshs on the tree"));
		}
	}
	else
	{
		wxLogWarning(wxString("Please select " + std::to_string(numberOfMeshs) + " meshs on the tree"));
	}
	return meshs;
}
Camera* FrmPrincipal::getCameraFromTree()
{
	wxTreeListItem item;
	wxTreeListItems itens;
	if (treeMesh->GetSelections(itens))
	{
		item = itens[0];
	}
	if (!item.IsOk())
	{
		wxLogWarning("Please select some camera on the tree");
		return nullptr;
	}
	for (const auto& project : projects)
	{
		for (const auto& camera : project->cameras)
		{
			if (camera->getListItemCamera() == item)
			{
				return camera;
			}
		}
	}
	wxLogWarning("Please select some camera on the tree");
	return nullptr;
}

void FrmPrincipal::LoadProject(const std::string& projectPath)
{
	if (!Utils::exists(projectPath) || Utils::toUpper(Utils::getFileExtension(projectPath)) != "PROJECT")
	{
		return;
	}
	// Check if we already have this project loaded
	for (const auto& project : projects)
	{
		if (project->GetPath() == projectPath)
		{
			wxMessageBox("This project was already loaded.", "Error", wxICON_ERROR);
			return;
		}
	}
	auto project = new Project(renderer, renderer360, treeMesh, projectPath);
	if (!project->GetIsLoaded())
	{
		delete project;
		return;
	}
	if (project->GetIs360() && project->cameras.size() > 0)
	{
		for (const auto& cam : project->cameras)
		{
			cam->setIs360(renderer, true);
		}
		Camera* cam = project->cameras.front();
		if (Utils::exists(cam->filePath))
		{
			iterStyle->setMode360Image(true);
			cam->setVisibility(true);
			treeMesh->CheckItem(cam->getListItemCamera(), wxCHK_CHECKED);
			Utils::updateCamera(renderer, cam);
			cam->createImageActor(renderer);
			project->Update360ClickPoints(cam);
			treeMesh->CheckItem(project->GetTreeItemCameras(), wxCHK_CHECKED);
		}
	}
	else
	{
		renderer->ResetCamera();
	}
	projects.emplace_back(project);
	setTreeVisibility(true);
	renderer->GetRenderWindow()->Render();
}

void FrmPrincipal::setTreeVisibility(bool visibility)
{
	int x, y;
	this->GetSize(&x, &y);
	if (x >= 0)
	{
		if (visibility)//If we want to show, we need to calc the correct size
		{
			if ((x - x * .7) > 250)//More than 250 is too much
			{
				x = x - 250;
			}
			else
			{
				x = x * .7;//Tree get 30% of the screen
			}
		}
		splitterWind->SetSashPosition(x, true);
		isTreeVisible = visibility;
	}
}

void FrmPrincipal::OnClose(wxCloseEvent& event)
{
	axisWidget->SetEnabled(false);
	viewTool->SetEnabled(false);
	disableTools();
	for (int i = 0; i < projects.size(); i++)
	{
		if (projects[i]->GetModified())
		{
			if (wxMessageBox("The project " + projects[i]->GetName() +
				" was modified, do you want to save it?", "", wxYES_NO | wxICON_QUESTION, this) == wxYES)
			{
				projects[i]->Save();
			}
		}
		delete projects[i];
	}
	projects.clear();
	vtk_panel->Delete();
	event.Skip();
}

void FrmPrincipal::OnSplitterDClick(wxSplitterEvent & event)
{
	setTreeVisibility(!isTreeVisible);
	event.Skip();
}

void FrmPrincipal::OnSashPosChanged(wxSplitterEvent & event)
{
	int x, y;
	this->GetSize(&x, &y);
	if (x >= 0)
	{
		if (splitterWind->GetSashPosition() < x*.90)
		{
			isTreeVisible = true;
		}
		else
		{
			isTreeVisible = false;
		}
	}
	event.Skip();
}

void FrmPrincipal::OnSize(wxSizeEvent & event)
{
	if (splitterWind != NULL)
	{
		setTreeVisibility(isTreeVisible);
	}
	wxSize* size = &this->GetSize();
	if (size != NULL && vtk_panel != NULL)
	{
		vtk_panel->UpdateSize(size->x, size->y);
	}
	event.Skip();
}

void FrmPrincipal::OnKeyPress(wxKeyEvent & event)
{
	char key = (char)event.GetKeyCode();
	if (event.ControlDown())
	{
		if (event.ShiftDown() && key == 'S')
		{
			OnMenuSaveProjectAs((wxCommandEvent)NULL);
		}
		else if (key == 'S')
		{
			OnMenuSaveProject((wxCommandEvent)NULL);
		}
		else if (key == 'R')
		{
			renderer->ResetCamera();
			renderer->GetRenderWindow()->Render();
		}
		else if (key == 'V')
		{
			wxTreeListItems treeItens;
			treeMesh->GetSelections(treeItens);
			if (!treeItens[0].IsOk())
			{
				return;
			}
			auto project = GetProjectFromTree();
			for (auto camera : project->cameras)
			{
				if (camera->getListItemCamera() == treeItens[0])
				{
					if (camera->changeViewUp())
					{
						Utils::updateCamera(renderer, camera);
						vtk_panel->GetRenderWindow()->Render();
						return;
					}
				}
			}
		}
		else if (key == 'U')
		{
			//Representation models
			//VTK_POINTS    0
			//VTK_WIREFRAME 1
			//VTK_SURFACE   2
			int oldRepresentation = 2;
			for (auto project : projects)
			{
				for (auto mesh : project->meshes)
				{
					for (auto actor : mesh->actors)
					{
						oldRepresentation = actor->GetProperty()->GetRepresentation();
						if (oldRepresentation < 2)
						{
							actor->GetProperty()->SetRepresentationToSurface();
						}
					}
				}
			}
			//now we can do the pick
			double* focalDisplay = iterStyle->getDisplayPosition(renderer->GetActiveCamera()->GetFocalPoint());
			double clickPos[3];
			vtkNew<vtkPropPicker> picker;
			picker->Pick(focalDisplay[0], focalDisplay[1], 0, renderer);
			picker->GetPickPosition(clickPos);
			//Let's back to the old representation
			if (oldRepresentation != 2)
			{
				for (auto project : projects)
				{
					for (auto mesh : project->meshes)
					{
						for (auto actor : mesh->actors)
						{
							actor->GetProperty()->SetRepresentation(oldRepresentation);
						}
					}
				}
			}
			//We put it down here to avoid spending time rendering the change in the representation
			if (picker->GetActor())
			{
				renderer->GetActiveCamera()->SetFocalPoint(clickPos);
				renderer->ResetCameraClippingRange();
				renderer->GetRenderWindow()->Render();
			}
			//props.clear();
			delete focalDisplay;
		}
		else if (key == 'F')
		{
			this->ShowFullScreen(!this->IsFullScreen());
		}
	}
	if (key == WXK_ESCAPE)
	{
		disableTools();
	}
	else if (key == WXK_DELETE)
	{
		if (deleteTool->GetEnabled())
		{
			if (deleteTool->enterKeyPressed())
			{
				toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
				toolBar->Realize();
			}
		}
		else
		{
			OnToolDelete((wxCommandEvent)NULL);
		}
	}
	else if (key == 'r' || key == WXK_RETURN)//Necessary to be here because VTK does not support ENTER Key // 'r' is the NUMPAD_ENTER the WXK_NUMPAD_ENTER DOES NOT WORK
	{
		if (deleteTool->GetEnabled())
		{
			if (deleteTool->enterKeyPressed())
			{
				menuItemToolDeletePoints->Check(false);
				toolDeletePoints->SetNormalBitmap(bmpToolDeletePointsFacesOFF);
				toolBar->Realize();
			}
		}
		if (alignTool->GetEnabled())
		{
			if (alignTool->HasFinished())
			{
				//Show the user the transformation matrix and ask to Save it in a txt file
				Utils::dialogSaveTransform(alignTool->GetTransform());
				OnMenuAlignTool((wxCommandEvent)NULL);
			}
			else
			{
				alignTool->EnterKeyPressed();
			}
		}
		if (volumeTool->GetEnabled())
		{
			wxBeginBusyCursor();
			volumeTool->enterKeyPressed();
			wxEndBusyCursor();
		}
		if (colorTool->GetEnabled())
		{
			if (colorTool->enterKeyPressed())
			{
				menuItemColorTool->Check(false);
			}
		}
		if (calibrationTool->GetEnabled())
		{
			if (calibrationTool->OnEnterKeyPressed())
			{
				OnToolCalibration((wxCommandEvent)NULL);
			}
		}
	}
	else if (event.GetKeyCode() == WXK_F1)
	{
		OnMenuHelp((wxCommandEvent)NULL);
	}
	event.Skip();
}

void FrmPrincipal::OnDropFiles(wxDropFilesEvent& event)
{
	if (event.GetNumberOfFiles() != 0)
	{
		std::string path = event.GetFiles()[0];
		std::string extension = Utils::toUpper(Utils::getFileExtension(path));
		if (extension == "PROJECT")
		{
			LoadProject(path);
		}
	}
	event.Skip();
}

void FrmPrincipal::OnLeftDClick(wxMouseEvent & event)
{
	int x, y;
	int w, h;
	vtk_panel->GetLastEventPosition(x, y);
	vtk_panel->GetClientSize(&w, &h);
	wxPoint p = event.GetPosition();
	if (iterStyle != NULL && x == p.x && (h - (y + 1)) == p.y)
	{
		iterStyle->doubleClick();
	}
	//.Skip is used in the ProjetoMeshApp::OnLeftDClick
}

void FrmPrincipal::ShowWelcomeDialog()
{
	WelcomeDialog welcomeDialog(this);
	if (welcomeDialog.ShowModal() == wxOK)
	{
		wxBeginBusyCursor();
		LoadProject(welcomeDialog.GetProjectPath());
		wxEndBusyCursor();
	}
}

void FrmPrincipal::disableTools()
{
	this->ShowFullScreen(false);
	disableMeasureTools(-1);
}

void FrmPrincipal::disableMeasureTools(int toolToAvoid)
{
	if (distanceTool->GetEnabled() && toolToAvoid != 0)
	{
		OnToolMeasure((wxCommandEvent)NULL);
	}
	if (angleTool->GetEnabled() && toolToAvoid != 1)
	{
		OnToolAngle((wxCommandEvent)NULL);
	}
	if (elevationTool->GetEnabled() && toolToAvoid != 2)
	{
		OnToolElevation((wxCommandEvent)NULL);
	}
	if (checkCamTool->GetEnabled() && toolToAvoid != 3)
	{
		OnToolCheckCamVisibility((wxCommandEvent)NULL);
	}
	if (volumeTool->GetEnabled() && toolToAvoid != 4)
	{
		OnToolVolume((wxCommandEvent)NULL);
	}
	if (deleteTool->GetEnabled() && toolToAvoid != 5)
	{
		OnToolDeletePointsFaces((wxCommandEvent)NULL);
	}
	if (updateCameraTool->GetEnabled() && toolToAvoid != 6)
	{
		OnMenuUpdateCamera((wxCommandEvent)NULL);
	}
	if (alignTool->GetEnabled() && toolToAvoid != 7)
	{
		OnMenuAlignTool((wxCommandEvent)NULL);
	}
	if (scalarViewerTool->GetEnabled() && toolToAvoid != 8)
	{
		OnMenuScalarViewer((wxCommandEvent)NULL);
	}
	if (colorTool->GetEnabled() && toolToAvoid != 9)
	{
		OnMenuColorTool((wxCommandEvent)NULL);
	}
	if (areaTool->GetEnabled() && toolToAvoid != 10)
	{
		OnMenuAreaTool((wxCommandEvent)NULL);
	}
	if (calibrationTool->GetEnabled() && toolToAvoid != 11)
	{
		OnToolCalibration((wxCommandEvent)NULL);
	}
}