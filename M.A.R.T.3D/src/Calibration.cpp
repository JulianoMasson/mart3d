#include "Calibration.h"

#include "Utils.h"

bool Calibration::load(const std::string & calibrationPath)
{
	if (!Utils::exists(calibrationPath))
	{
		return false;
	}
	std::ifstream calibrationFile(calibrationPath);
	if (!calibrationFile.is_open())
	{
		return false;
	}
	std::string line;
	while (std::getline(calibrationFile, line, '\n'))
	{
		if (line.substr(0, line.find_first_of('=')) == "scaleFactor")
		{
			scaleFactor = std::stod(line.substr(line.find_first_of('=') + 1, line.size()));
		}
		else if (line.substr(0, line.find_first_of('=')) == "measureUnit")
		{
			measureUnit = line.substr(line.find_first_of('=') + 1, line.size());
		}
		else if (line.substr(0, line.find_first_of('=')) == "gpsReferencePoint")
		{
			std::stringstream referencePoint(line.substr(line.find_first_of('=') + 1, line.size()));
			std::string token;
			unsigned int count = 0;
			while (std::getline(referencePoint, token,';'))
			{
				if (count >= 3)
				{
					break;
				}
				gpsReferencePoint[count] = std::stod(token);
				count++;
			}
		}
	}
	calibrationFile.close();
	return true;
}

bool Calibration::save(const std::string & calibrationPath)
{
	std::ofstream calibrationFile(calibrationPath);
	if (!calibrationFile.is_open())
	{
		return false;
	}
	calibrationFile << "scalarFactor=" << scaleFactor << "\n";
	calibrationFile << "measureUnit=" << measureUnit << "\n";
	calibrationFile << "gpsReferencePoint=" << std::setprecision(15) << gpsReferencePoint[0] << ";" << gpsReferencePoint[1] << ";" << gpsReferencePoint[2] << "\n";
	calibrationFile.close();
	return true;
}
