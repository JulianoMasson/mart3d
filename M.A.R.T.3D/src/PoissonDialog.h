#pragma once
#include <wx/dialog.h>

#include "PoissonRecon.h"

class wxChoice;

class PoissonDialog : public wxDialog
{
public:
	PoissonDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "Poisson", const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(300, 140), long style = wxDEFAULT_DIALOG_STYLE);
	~PoissonDialog();

	PoissonRecon::Options getOptions() const;

private:
	DECLARE_EVENT_TABLE()

	void OnBtDefault(wxCommandEvent& WXUNUSED(event));
	
	wxSpinCtrl* spinDepth;
	wxChoice* choiceBoundary;
};
enum EnumPoissonDialog
{
	idBtDefaultPoissonDialog
};