#pragma once
#include <vector>
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

class wxWindow;
class vtkCaptionActor2D;
class LineWidget;
class Project;
class Mesh;
class GPSData;

class CalibrationTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static CalibrationTool *New();

	vtkTypeMacro(CalibrationTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}

	//Set the mesh that is going to be used
	void SetMesh(Mesh* mesh) { this->mesh = mesh; };
	void SetProject(Project* project) { this->project = project; };
	void SetWindow(wxWindow* window) { this->window = window; };
	void SetUseGPS(bool useGPS) { this->useGPS = useGPS; };
	void Calibrate(std::vector<std::pair<double*, GPSData>> gpsData);
	bool OnEnterKeyPressed();

protected:
	CalibrationTool();
	~CalibrationTool();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	void UpdateRepresentation();

	vtkSmartPointer<LineWidget> lineWidget;
	std::vector< std::pair<vtkSmartPointer<vtkCaptionActor2D>, GPSData> > gpsData;
	vtkSmartPointer<vtkCaptionActor2D> textActor;

	Mesh* mesh;
	Project* project;
	wxWindow* window;
	// Used to control when we add a point
	unsigned int oldNumberOfPoints;
	bool useGPS;

private:
	CalibrationTool(const CalibrationTool&) VTK_DELETE_FUNCTION;
	void operator=(const CalibrationTool&) VTK_DELETE_FUNCTION;
};
