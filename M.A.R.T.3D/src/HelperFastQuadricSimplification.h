#pragma once
#include <vtkSmartPointer.h>

class vtkPolyData;

class HelperFastQuadricSimplification
{
public:
	HelperFastQuadricSimplification() {};
	~HelperFastQuadricSimplification() {};

	// Return a decimated mesh (reduction is in % of the original mesh polygon count), only holds normals, color and texture coordinates
	static vtkSmartPointer<vtkPolyData> computeQuadricDecimation(vtkSmartPointer<vtkPolyData> polyData, const float reduction);
};


