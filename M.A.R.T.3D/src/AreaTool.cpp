#include "AreaTool.h"

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkCaptionActor2D.h>
#include <vtkMath.h>
#include <vtkPoints.h>
#include <vtkPolygon.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkPolyData.h>

#include <wx/string.h>

#include "LineWidget.h"
#include "Draw.h"
#include "Utils.h"
#include "Calibration.h"
#include "Mesh.h"

vtkStandardNewMacro(AreaTool);

//----------------------------------------------------------------------------
AreaTool::AreaTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(AreaTool::ProcessEvents);
}

//----------------------------------------------------------------------------
AreaTool::~AreaTool()
{
	if (textActor)
	{
		this->CurrentRenderer->RemoveActor2D(textActor);
		textActor = nullptr;
	}
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
	}
	mesh = nullptr;
}

//----------------------------------------------------------------------------
void AreaTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh || !calibration) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		if (!lineWidget)
		{
			lineWidget = vtkSmartPointer<LineWidget>::New();
			lineWidget->SetInteractor(this->Interactor);
			lineWidget->setCloseLoopOnFirstNode(true);
			lineWidget->CreateRepresentationFromMesh(mesh);
		}
		lineWidget->EnabledOn();
		lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
		lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		// turn off the various actors
		if (textActor)
		{
			this->CurrentRenderer->RemoveActor2D(textActor);
			textActor = nullptr;
		}
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
		}
		mesh = nullptr;
		calibration = nullptr;

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void AreaTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	AreaTool* self =
		reinterpret_cast<AreaTool *>(clientdata);

	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::InteractionEvent:// InteractionEvent to get the Ctrl + Z
	case vtkCommand::EndInteractionEvent:
		self->UpdateRepresentation();
		break;
	}
}

//----------------------------------------------------------------------------
void AreaTool::UpdateRepresentation()
{
	if (!lineWidget)
	{
		return;
	}
	if (!lineWidget->GetRepresentation()->isLoopClosed())
	{
		if (textActor)
		{
			this->CurrentRenderer->RemoveActor2D(textActor);
			textActor = nullptr;
			this->Interactor->Render();
		}
		return;
	}
	vtkSmartPointer<vtkPoints> points = lineWidget->GetRepresentation()->getPoints();
	if (!points)
	{
		return;
	}
	// Adjust using the calibration
	const auto scaleFactor = calibration->getScaleFactor();
	vtkNew<vtkTransform> T;
	T->Scale(scaleFactor, scaleFactor, scaleFactor);
	vtkNew<vtkTransformFilter> transformFilter;
	transformFilter->SetTransform(T);
	vtkNew<vtkPolyData> polyDataTemp;
	polyDataTemp->SetPoints(points);
	transformFilter->SetInputData(polyDataTemp);
	transformFilter->Update();
	auto pointsTransformed = transformFilter->GetOutput()->GetPoints();
	// Compute area and perimeter
	const auto numberOfPoints = pointsTransformed->GetNumberOfPoints();
	double normal[3];
	// Remove the last, since it is equal to the first
	const auto area = vtkPolygon::ComputeArea(pointsTransformed, numberOfPoints - 1, nullptr, normal);
	auto perimeter = 0.0;
	for (vtkIdType i = 0; i < numberOfPoints-1; i++)
	{
		double point0[3], point1[3];
		pointsTransformed->GetPoint(i, point0);
		pointsTransformed->GetPoint(i + 1, point1);
		perimeter += std::sqrt(vtkMath::Distance2BetweenPoints(
			point0,
			point1));
	}
	if (!textActor)
	{
		textActor = Draw::createText(this->CurrentRenderer, points->GetPoint(0), "", 24, 1.0, 1.0, 1.0);
	}
	textActor->SetAttachmentPoint(points->GetPoint(0));

	std::stringstream s;
	s << std::fixed << std::setprecision(3) << area << calibration->getMeasureUnit() << "\u00B2 / " << perimeter << calibration->getMeasureUnit();
	wxString stringTemp(s.str());
	textActor->SetCaption(stringTemp.ToUTF8());

}
