#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

#include <vector>

class vtkActor;
class vtkSliderWidget;
class vtkPointPicker;
class vtkDataArray;
class vtkScalarBarActor;
class wxTreeListCtrl;
class vtkCaptionActor2D;
class Calibration;
class Mesh;

class ScalarViewerTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static ScalarViewerTool *New();

	vtkTypeMacro(ScalarViewerTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}

	void SetScalar(vtkSmartPointer<vtkDataArray> scalar) { this->scalars = scalar; };
	void SetMesh(Mesh* mesh) { this->mesh = mesh; };
	void SetCalibration(Calibration* calibration) { this->calibration = calibration; };
	void SetTree(wxTreeListCtrl* treeMesh) { this->treeMesh = treeMesh; };

protected:
	ScalarViewerTool();
	~ScalarViewerTool();

	//Delete what is necessary to properly disable/kill this class
	void Destruct();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	// ProcessEvents() dispatches to these methods.
	void OnMiddleButtonDown();
	void OnKeyPress();

	//Actors
	vtkSmartPointer<vtkCaptionActor2D> textActor = nullptr;
	vtkSmartPointer<vtkScalarBarActor> scalarBarActor = nullptr;
	vtkSmartPointer<vtkActor> scalarActor = nullptr;

	vtkSmartPointer<vtkDataArray> scalars = nullptr;

	bool CreateActors();

	//Slider
	vtkSmartPointer<vtkSliderWidget> sliderWidget = nullptr;
	void CreateSlider();
	void UpdateSliderLimits(double minValue, double maxValue);
	void OnSliderChanged();
	// Vector used to control the slider limits
	std::vector< std::pair<double, double> > historyOfSliderLimits;

	// Do the picking
	int GetMousePosition(double * point);
	vtkSmartPointer<vtkPointPicker> pointPicker = nullptr;

	Mesh* mesh = nullptr;
	Calibration* calibration = nullptr;
	wxTreeListCtrl * treeMesh = nullptr;

private:
	ScalarViewerTool(const ScalarViewerTool&) VTK_DELETE_FUNCTION;
	void operator=(const ScalarViewerTool&) VTK_DELETE_FUNCTION;
};

