#include "Utils.h"

#include <vector>
#include <sstream>
#include <Windows.h>

#include <vtkMath.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTransformFilter.h>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkTexture.h>
#include <vtkTransform.h>
#include <vtkImageImport.h>
#include <vtkImageData.h>
#include <vtkWindowToImageFilter.h>
#include <vtkImageReader2.h>
#include <vtkJPEGReader.h>
#include <vtkBMPReader.h>
#include <vtkTIFFReader.h>
#include <vtkPNGReader.h>
#include <vtkPNGWriter.h>
#include <vtkInteractorObserver.h>
#include <vtkAbstractPicker.h>
#include <vtkMatrix4x4.h>
#include <vtksys/SystemTools.hxx>
#include <vtkCleanPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkCenterOfMass.h>
#include <vtkFeatureEdges.h>
#include <vtkConnectivityFilter.h>

#include <wx/log.h>
#include <wx/image.h>
#include <wx/stdpaths.h>
#include <wx/msgdlg.h>
#include <wx/filedlg.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polygon_mesh_processing/triangulate_hole.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/polygon_mesh_to_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>

#include "Camera.h"

Utils::Utils()
{
}

Utils::~Utils()
{
}

void Utils::updateCamera(vtkSmartPointer<vtkRenderer> renderer, const Camera* cam)
{
	if (cam->viewUp)
	{
		//Updating camera
		if (cam->getIs360())
		{
			renderer->GetActiveCamera()->SetViewAngle(50);
		}
		else
		{
			if (cam->getWidth() > cam->getHeight())//I want all the image, so get the bigger side
			{
				renderer->GetActiveCamera()->SetViewAngle(vtkMath::DegreesFromRadians(2.0 * atan(cam->getWidth() / (2.0*cam->getFocalX()))));
			}
			else
			{
				renderer->GetActiveCamera()->SetViewAngle(vtkMath::DegreesFromRadians(2.0 * atan(cam->getHeight() / (2.0*cam->getFocalY()))));
			}
		}
		renderer->GetActiveCamera()->SetPosition(cam->cameraPoints.at(0));
		renderer->GetActiveCamera()->SetFocalPoint(cam->cameraPoints.at(5));
		renderer->GetActiveCamera()->SetViewUp(cam->viewUp);
		//Necessary to make all actors visible
		renderer->ResetCameraClippingRange();
	}
}

void Utils::takeSnapshot(const std::string& path, int magnification, bool getAlpha, vtkRenderWindow* renderWindow)
{
	vtkNew<vtkWindowToImageFilter> windowToImageFilter;
	windowToImageFilter->SetInput(renderWindow);
	windowToImageFilter->SetScale(magnification);
	if (getAlpha)
	{
		windowToImageFilter->SetInputBufferTypeToRGBA();
	}
	else
	{
		windowToImageFilter->SetInputBufferTypeToRGB();
	}
	windowToImageFilter->ReadFrontBufferOff(); // read from the back buffer
	windowToImageFilter->Update();

	vtkNew<vtkPNGWriter> writer;
	writer->SetFileName(path.c_str());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();
	renderWindow->Render();
}

void Utils::transformPoint(double point[3], vtkSmartPointer<vtkMatrix4x4> matrixRT)
{
	double x = (matrixRT->Element[0][0] * point[0] + matrixRT->Element[0][1] * point[1] + matrixRT->Element[0][2] * point[2] + matrixRT->Element[0][3]);
	double y = (matrixRT->Element[1][0] * point[0] + matrixRT->Element[1][1] * point[1] + matrixRT->Element[1][2] * point[2] + matrixRT->Element[1][3]);
	double z = (matrixRT->Element[2][0] * point[0] + matrixRT->Element[2][1] * point[1] + matrixRT->Element[2][2] * point[2] + matrixRT->Element[2][3]);
	point[0] = x; 
	point[1] = y; 
	point[2] = z;
}

void Utils::getNormal(const double pointA[3], const double pointB[3], const double pointC[3], double n[3])
{
	double v1[3];
	double v2[3];
	vtkMath::Subtract(pointB, pointA, v1);
	vtkMath::Subtract(pointB, pointC, v2);
	vtkMath::Normalize(v1);
	vtkMath::Normalize(v2);
	vtkMath::Cross(v1, v2, n);
	vtkMath::Normalize(n);
}

void Utils::getNormal(const double pointA[3], const double pointB[3], const double pointC[3], const double pointTest[3], double n[3])
{
	double v1[3];
	double v2[3];
	vtkMath::Subtract(pointB, pointA, v1);
	vtkMath::Subtract(pointB, pointC, v2);
	vtkMath::Normalize(v1);
	vtkMath::Normalize(v2);
	vtkMath::Cross(v1, v2, n);
	vtkMath::Normalize(n);
	double n2[3] = {n[0], n[1], n[2]};
	vtkMath::MultiplyScalar(n2, -1);
	vtkMath::Add(n, pointB, v1);
	vtkMath::Add(n2, pointB, v2);
	if (vtkMath::Distance2BetweenPoints(v1, pointTest) > vtkMath::Distance2BetweenPoints(v2, pointTest))
	{
		n[0] = n2[0];
		n[1] = n2[1];
		n[2] = n2[2];
	}
}

vtkSmartPointer<vtkImageData> Utils::wxImage2ImageData(const wxImage& img)
{
	vtkNew<vtkImageImport> importer;
	vtkNew<vtkImageData> imageData;
	importer->SetOutput(imageData);
	importer->SetDataSpacing(1, 1, 1);
	importer->SetDataOrigin(0, 0, 0);
	importer->SetWholeExtent(0, img.GetWidth() - 1, 0, img.GetHeight() - 1, 0, 0);
	importer->SetDataExtentToWholeExtent();
	importer->SetDataScalarTypeToUnsignedChar();
	importer->SetNumberOfScalarComponents(3);
	importer->SetImportVoidPointer(img.GetData());
	importer->Update();
	return imageData;
}

void Utils::deletePoint(vtkSmartPointer<vtkPoints> points, vtkIdType id)
{
	vtkNew<vtkPoints> newPoints;
	const unsigned int numberOfPoints = points->GetNumberOfPoints();
	for (unsigned int i = 0; i < numberOfPoints; i++)
	{
		if (i != id)
		{
			double p[3];
			points->GetPoint(i, p);
			newPoints->InsertNextPoint(p);
		}
	}
	points->ShallowCopy(newPoints);
}

void Utils::getSkewSym(const double v[3], vtkSmartPointer<vtkMatrix4x4> result)
{
	result->SetElement(0, 0, 0);
	result->SetElement(0, 1, -v[2]);
	result->SetElement(0, 2, v[1]);
	result->SetElement(0, 3, 0);

	result->SetElement(1, 0, v[2]);
	result->SetElement(1, 1, 0);
	result->SetElement(1, 2, -v[0]);
	result->SetElement(1, 3, 0);

	result->SetElement(2, 0, -v[1]);
	result->SetElement(2, 1, v[0]);
	result->SetElement(2, 2, 0);
	result->SetElement(2, 3, 0);

	result->SetElement(3, 0, 0);
	result->SetElement(3, 1, 0);
	result->SetElement(3, 2, 0);
	result->SetElement(3, 3, 1);
}

void Utils::multiplyMatrix4x4ByScalar(vtkSmartPointer<vtkMatrix4x4> mat, double scalar, vtkSmartPointer<vtkMatrix4x4> result)
{
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			result->SetElement(i, j, mat->GetElement(i, j)*scalar);
		}
	}
}

void Utils::sumMatrix4x4(vtkSmartPointer<vtkMatrix4x4> mat1, vtkSmartPointer<vtkMatrix4x4> mat2, vtkSmartPointer<vtkMatrix4x4> result)
{
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			result->SetElement(i, j, mat1->GetElement(i, j) + mat2->GetElement(i, j));
		}
	}
}

void Utils::getDisplayPosition(vtkSmartPointer<vtkRenderer> renderer, const double point[3], double  displayPosition[3])
{
	vtkInteractorObserver::ComputeWorldToDisplay(renderer, point[0], point[1], point[2], displayPosition);
	displayPosition[2] = 0;
}

bool Utils::pickPosition(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkAbstractPicker> picker, const double displayPosition[3], double point[3])
{
	if (picker->Pick(displayPosition[0], displayPosition[1], 0, renderer))
	{
		picker->GetPickPosition(point);
		return true;
	}
	return false;
}

vtkSmartPointer<vtkActor> Utils::duplicateActor(vtkSmartPointer<vtkActor> actor)
{
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
	vtkNew<vtkTransformFilter> transformFilter;
	vtkNew<vtkTransform> T;
	T->Identity();
	if (actor->GetUserMatrix())
	{
		T->SetMatrix(actor->GetUserMatrix());
	}
	else if (actor->GetMatrix())
	{
		T->SetMatrix(actor->GetMatrix());
	}
	transformFilter->SetTransform(T);
	transformFilter->SetInputData(polydata);
	transformFilter->Update();
	polydata = transformFilter->GetPolyDataOutput();
	vtkNew<vtkPolyData> polydataCopy;
	polydataCopy->DeepCopy(polydata);

	vtkNew<vtkPolyDataMapper> mapper;
	mapper->ScalarVisibilityOn();
	mapper->SetScalarModeToUsePointData();

	mapper->SetInputData(polydataCopy);

	vtkNew<vtkActor> newActor;
	newActor->SetMapper(mapper);
	if (actor->GetTexture())
	{
		vtkNew<vtkTexture> texture;
		vtkNew<vtkImageData> imgCopy;
		imgCopy->DeepCopy(actor->GetTexture()->GetInput());
		texture->SetInputData(imgCopy);
		newActor->SetTexture(texture);
		newActor->GetProperty()->SetInterpolationToPhong();
		newActor->GetProperty()->SetLighting(false);
	}
	return newActor;
}

double Utils::getDistanceBetweenGeographicCoordinate(double lat1, double long1, double alt1, double lat2, double long2, double alt2)
{
	double R = 6378.137;

	double dLat = vtkMath::RadiansFromDegrees(lat2) - vtkMath::RadiansFromDegrees(lat1);
	double dLon = vtkMath::RadiansFromDegrees(long2) - vtkMath::RadiansFromDegrees(long1);

	double a = sin(dLat / 2.0f) * sin(dLat / 2.0f) + cos(vtkMath::RadiansFromDegrees(lat1)) * cos(vtkMath::RadiansFromDegrees(lat2)) * sin(dLon / 2.0f) * sin(dLon / 2.0f);

	double c = 2 * atan2(sqrt(a), sqrt(1 - a));

	double d_xy = R * c * 1000;

	return sqrt(pow(d_xy, 2) + pow(alt2 - alt1, 2));
}

vtkSmartPointer<vtkTransform> Utils::getTransformToAlignVectors(double a[3], double b[3])
{
	vtkMath::Normalize(a);
	vtkMath::Normalize(b);

	double rot_angle = acos(vtkMath::Dot(a, b));

	double cVector[3];
	vtkMath::Cross(a, b, cVector);

	vtkMath::Normalize(cVector);

	double x = cVector[0];
	double y = cVector[1];
	double z = cVector[2];

	double c = cos(rot_angle);

	double s = sin(rot_angle);

	vtkNew<vtkMatrix4x4> mat;

	mat->SetElement(0, 0, x*x*(1 - c) + c);
	mat->SetElement(0, 1, x*y*(1 - c) - z * s);
	mat->SetElement(0, 2, x*z*(1 - c) + y * s);
	mat->SetElement(0, 3, 0);
	mat->SetElement(1, 0, y*x*(1 - c) + z * s);
	mat->SetElement(1, 1, y*y*(1 - c) + c);
	mat->SetElement(1, 2, y*z*(1 - c) - x * s);
	mat->SetElement(1, 3, 0);
	mat->SetElement(2, 0, x*z*(1 - c) - y * s);
	mat->SetElement(2, 1, y*z*(1 - c) + x * s);
	mat->SetElement(2, 2, z*z*(1 - c) + c);
	mat->SetElement(2, 3, 0);
	mat->SetElement(3, 0, 0);
	mat->SetElement(3, 1, 0);
	mat->SetElement(3, 2, 0);
	mat->SetElement(3, 3, 1);

	vtkNew<vtkTransform> T;
	T->SetMatrix(mat);

	return T;
}

vtkSmartPointer<vtkPolyData> Utils::getActorPolyData(vtkSmartPointer<vtkActor> actor)
{
	if (!actor)
	{
		return nullptr;
	}
	vtkSmartPointer<vtkPolyData> meshPolyData = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
	vtkNew<vtkTransformFilter> transformFilter;
	vtkNew<vtkTransform> T;
	if (actor->GetUserMatrix())
	{
		T->SetMatrix(actor->GetUserMatrix());
	}
	else if (actor->GetMatrix())
	{
		T->SetMatrix(actor->GetMatrix());
	}
	else
	{
		return meshPolyData;
	}
	transformFilter->SetTransform(T);
	transformFilter->SetInputData(meshPolyData);
	transformFilter->Update();
	meshPolyData = transformFilter->GetPolyDataOutput();
	return meshPolyData;
}

std::wstring Utils::s2ws(const std::string & s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

int Utils::startProcess(const std::string& path_with_command)
{
	std::wstring stemp = s2ws(path_with_command);
	LPWSTR path_command = const_cast<LPWSTR>(stemp.c_str());

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		path_command,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		wxLogError("CreateProcess failed (%d).\n", GetLastError());
		return 0;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return 1;
}

int Utils::startProcess(const std::string& path_with_command, const std::string& workingDirectory)
{
	std::wstring stemp = s2ws(path_with_command);
	LPWSTR path_command = const_cast<LPWSTR>(stemp.c_str());

	std::wstring stemp2 = s2ws(workingDirectory);
	LPCWSTR working_dir = stemp2.c_str();

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process. 
	if (!CreateProcess(NULL,   // No module name (use command line)
		path_command,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		working_dir,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		wxLogError("CreateProcess failed (%d).\n", GetLastError());
		return 0;
	}

	// Wait until child process exits.
	WaitForSingleObject(pi.hProcess, INFINITE);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return 1;
}

int Utils::startProcess(const std::string& path_exec, const std::string& parameters, const std::string& workingDirectory)
{
	std::wstring stemp = s2ws(path_exec);
	LPWSTR path_exe = const_cast<LPWSTR>(stemp.c_str());

	std::wstring stemp2 = s2ws(workingDirectory);
	LPCWSTR working_dir = stemp2.c_str();

	std::wstring stemp3 = s2ws(parameters);
	LPCWSTR param = stemp3.c_str();

	SHELLEXECUTEINFO shExInfo = { 0 };
	shExInfo.cbSize = sizeof(shExInfo);
	shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExInfo.hwnd = 0;
	shExInfo.lpVerb = _T("runas");                // Operation to perform
	shExInfo.lpFile = path_exe;       // Application to start    
	shExInfo.lpParameters = param;                  // Additional parameters
	shExInfo.lpDirectory = working_dir;
	shExInfo.nShow = SW_SHOW;
	shExInfo.hInstApp = 0;

	if (ShellExecuteEx(&shExInfo))
	{
		WaitForSingleObject(shExInfo.hProcess, INFINITE);
		CloseHandle(shExInfo.hProcess);
	}
	else
	{
		return 0;
	}
	return 1;
}

std::string Utils::getExecutionPath()
{
	return vtksys::SystemTools::GetFilenamePath(wxStandardPaths::Get().GetExecutablePath().ToStdString());
}

void Utils::removeExtraModelsFromNVM(const std::string& filename)
{
	std::ifstream nvmFile(filename);
	std::stringstream out;
	std::string line;
	if (nvmFile.is_open())
	{
		//NVM_V3
		for (unsigned int i = 0; i < 3; i++)
		{
			getline(nvmFile, line, '\n');
			out << line << "\n";
		}
		int numberOfCameras = std::atoi(line.c_str());
		for (int i = 0; i < numberOfCameras + 2; i++)
		{
			getline(nvmFile, line, '\n');
			out << line << "\n";
		}
		int numberOfFeatures = std::atoi(line.c_str());
		for (int i = 0; i < numberOfFeatures; i++)
		{
			getline(nvmFile, line, '\n');
			out << line << "\n";
		}
	}
	nvmFile.close();
	//Overwrite the file
	std::ofstream outFile(filename.c_str());
	if (!outFile.good())
	{
		return;
	}
	outFile << out.rdbuf();
	outFile.close();
}

vtkSmartPointer<vtkImageData> Utils::loadImage(const std::string& filePath)
{
	if (!exists(filePath))
	{
		return nullptr;
	}
	std::string extension = filePath.substr(filePath.find_last_of("."));
	vtkSmartPointer<vtkImageReader2> reader;
	if (extension == ".jpg" || extension == ".JPG" || extension == ".jpeg")
	{
		reader = vtkSmartPointer<vtkJPEGReader>::New();
	}
	else if (extension == ".png" || extension == ".PNG")
	{
		reader = vtkSmartPointer<vtkPNGReader>::New();
	}
	else if (extension == ".bmp" || extension == ".BMP")
	{
		reader = vtkSmartPointer<vtkBMPReader>::New();
	}
	else if (extension == ".tiff" || extension == ".TIFF")
	{
		reader = vtkSmartPointer<vtkTIFFReader>::New();
	}
	//else if (extension == ".pnm" || extension == ".PNM")
	//{
	   // reader = vtkSmartPointer<vtkPNMReader>::New();
	//}
	else
	{
		return nullptr;
	}
	reader->SetFileName(filePath.c_str());
	reader->Update();
	if (!reader->GetOutput())
	{
		return nullptr;
	}
	return reader->GetOutput();
}

bool Utils::intersectLineSphere(const double linePoint0[3], const double linePoint1[3],
	const double sphereCenter[3], double sphereRadius, double intersectionPoint[3])
{
	//http://www.codeproject.com/Articles/19799/Simple-Ray-Tracing-in-C-Part-II-Triangles-Intersec

	double cx = sphereCenter[0];
	double cy = sphereCenter[1];
	double cz = sphereCenter[2];

	double px = linePoint0[0];
	double py = linePoint0[1];
	double pz = linePoint0[2];

	double vx = linePoint1[0] - px;
	double vy = linePoint1[1] - py;
	double vz = linePoint1[2] - pz;

	double A = vx * vx + vy * vy + vz * vz;
	double B = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
	double C = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy +
		pz * pz - 2 * pz * cz + cz * cz - sphereRadius * sphereRadius;

	// discriminant
	double D = B * B - 4 * A * C;

	if (D < 0)
	{
		return 0;
	}

	double t1 = (-B - std::sqrt(D)) / (2.0 * A);

	double solution1[3];
	Utils::createDoubleVector(linePoint0[0] * (1 - t1) + t1 * linePoint1[0],
		linePoint0[1] * (1 - t1) + t1 * linePoint1[1],
		linePoint0[2] * (1 - t1) + t1 * linePoint1[2],
		solution1);
	if (D == 0)
	{
		intersectionPoint[0] = solution1[0];
		intersectionPoint[1] = solution1[1];
		intersectionPoint[2] = solution1[2];
		return 1;
	}

	double t2 = (-B + std::sqrt(D)) / (2.0 * A);
	double solution2[3];
	Utils::createDoubleVector(linePoint0[0] * (1 - t2) + t2 * linePoint1[0],
		linePoint0[1] * (1 - t2) + t2 * linePoint1[1],
		linePoint0[2] * (1 - t2) + t2 * linePoint1[2],
		solution2);

	// prefer a solution that is closer to the second sphere (linePoint1)
	if (vtkMath::Distance2BetweenPoints(solution1, linePoint1) < vtkMath::Distance2BetweenPoints(solution2, linePoint1))
	{
		intersectionPoint[0] = solution1[0];
		intersectionPoint[1] = solution1[1];
		intersectionPoint[2] = solution1[2];
		return 1;
	}
	else
	{
		intersectionPoint[0] = solution2[0];
		intersectionPoint[1] = solution2[1];
		intersectionPoint[2] = solution2[2];
		return 1;
	}
}

void Utils::lla_to_ecef(double lat, double lon, double alt, double output[3])
{
	const double clat = std::cos(vtkMath::RadiansFromDegrees(lat));
	const double slat = std::sin(vtkMath::RadiansFromDegrees(lat));
	const double clon = std::cos(vtkMath::RadiansFromDegrees(lon));
	const double slon = std::sin(vtkMath::RadiansFromDegrees(lon));

	const double a2 = std::pow(6378137.0, 2);
	const double b2 = std::pow(6356752.314245, 2);

	const double L = 1.0 / std::sqrt(a2 * std::pow(clat, 2) + b2 * std::pow(slat, 2));
	const double x = (a2 * L + alt) * clat * clon;
	const double y = (a2 * L + alt) * clat * slon;
	const double z = (b2 * L + alt) * slat;

	output[0] = x;
	output[1] = y;
	output[2] = z;
}

void Utils::ecef_to_enu(const double ecef[3], const double latRef, const double lonRef, const double altRef, double enu[3])
{
	auto cosLatRef = std::cos(vtkMath::RadiansFromDegrees(latRef));
	auto sinLatRef = std::sin(vtkMath::RadiansFromDegrees(latRef));

	auto cosLongRef = std::cos(vtkMath::RadiansFromDegrees(lonRef));
	auto sinLongRef = std::sin(vtkMath::RadiansFromDegrees(lonRef));

	auto R = 6378137;
	auto f = 1.0 / 298.257224;
	auto e2 = 1 - (1 - f) * (1 - f);

	auto cRef = 1 / std::sqrt(cosLatRef * cosLatRef + (1 - f) * (1 - f) * sinLatRef * sinLatRef);

	auto x0 = (R*cRef + altRef) * cosLatRef * cosLongRef;
	auto y0 = (R*cRef + altRef) * cosLatRef * sinLongRef;
	auto z0 = (R*cRef*(1 - e2) + altRef) * sinLatRef;

	enu[0] = (-(ecef[0] - x0) * sinLongRef) + ((ecef[1] - y0)*cosLongRef);

	enu[1] = (-cosLongRef * sinLatRef*(ecef[0] - x0)) - (sinLatRef*sinLongRef*(ecef[1] - y0)) + (cosLatRef*(ecef[2] - z0));

	enu[2] = (cosLatRef*cosLongRef*(ecef[0] - x0)) + (cosLatRef*sinLongRef*(ecef[1] - y0)) + (sinLatRef*(ecef[2] - z0));
}

bool Utils::exists(const std::string & name)
{
	return wxFileExists(name);
}

std::string Utils::preparePath(std::string path)
{
	std::replace(path.begin(), path.end(), '\\', '/');
	return "\"" + path + "\"";
}

std::string Utils::getFileExtension(const std::string & filePath)
{
	if (filePath.find_last_of('.') != std::string::npos)
	{
		return filePath.substr(filePath.find_last_of('.') + 1, filePath.size());
	}
	return "";
}

std::string Utils::getFileName(const std::string & filePath, bool withExtension)
{
	std::string fileName = filePath;
	if (fileName.find_last_of('/') != std::string::npos)
	{
		fileName = fileName.substr(fileName.find_last_of('/') + 1, fileName.size());
	}
	else if (filePath.find_last_of('\\') != std::string::npos)
	{
		fileName = fileName.substr(fileName.find_last_of('\\') + 1, fileName.size());
	}
	if (fileName != "" && !withExtension)
	{
		return fileName.substr(0, fileName.find_last_of('.'));
	}
	return fileName;
}

std::string Utils::getPath(const std::string & filePath, bool returnWithLastSlash)
{
	if (filePath.find_last_of('/') != std::string::npos)
	{
		if (returnWithLastSlash)
		{
			return filePath.substr(0, filePath.find_last_of('/') + 1);
		}
		else
		{
			return filePath.substr(0, filePath.find_last_of('/'));
		}
	}
	else if (filePath.find_last_of('\\') != std::string::npos)
	{
		if (returnWithLastSlash)
		{
			return filePath.substr(0, filePath.find_last_of('\\') + 1);
		}
		else
		{
			return filePath.substr(0, filePath.find_last_of('\\'));
		}
	}
	return "";
}

std::string Utils::toUpper(const std::string & str)
{
	std::string copy = str;
	for (auto & c : copy)
	{
		c = toupper(c);
	}
	return copy;
}

vtkSmartPointer<vtkPoints> Utils::getPointsInsideBB(vtkSmartPointer<vtkPoints> points, const double boudingBox[6])
{
	int numberOfPoints = points->GetNumberOfPoints();
	std::vector<bool> ids;
	ids.resize(numberOfPoints, false);
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		double x[3];
		points->GetPoint(i, x);
		if (x[0] > boudingBox[0] && x[0] < boudingBox[1] &&
			x[1] > boudingBox[2] && x[1] < boudingBox[3] &&
			x[2] > boudingBox[4] && x[2] < boudingBox[5])
		{
			ids[i] = true;
		}
	}
	vtkNew<vtkPoints> pointsOut;
	for (int i = 0; i < numberOfPoints; i++)
	{
		if (ids[i])
		{
			pointsOut->InsertNextPoint(points->GetPoint(i));
		}
	}
	return pointsOut;
}

void Utils::dialogSaveTransform(vtkSmartPointer<vtkTransform> transform)
{
	vtkSmartPointer<vtkMatrix4x4> mat = transform->GetMatrix();
	std::stringstream s;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			s << std::fixed << std::setprecision(7) << mat->GetElement(i, j) << " ";
		}
		s << "\n";
	}
	s << "\nDo you want to save the matrix?\n";
	if (wxMessageBox(s.str(), "Transformation matrix", wxYES_NO | wxICON_QUESTION) == 2)
	{
		wxFileDialog saveDialog(nullptr, "Save the transformation matrix",
			"", "transform.txt", "Txt files (*.txt)|*.txt", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
		if (saveDialog.ShowModal() == wxID_OK)
		{
			if (!saveMatrix4x4(saveDialog.GetPath().ToStdString(), mat))
			{
				wxMessageBox("Could not save the matrix", "Error", wxICON_ERROR);
			}
		}
	}
}

bool Utils::saveMatrix4x4(const std::string & filename, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	std::ofstream matrixFile;
	matrixFile.open(filename);
	if (matrixFile.is_open())
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				matrixFile << std::fixed << std::setprecision(7) << matrix->GetElement(i, j) << " ";
			}
			matrixFile << "\n";
		}
	}
	else
	{
		return 0;
	}
	matrixFile.close();
	return 1;
}

bool Utils::loadMatrix4x4(const std::string & filename, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	if (!Utils::exists(filename))
	{
		return 0;
	}
	std::ifstream in(filename.c_str());
	if (!in.is_open())
	{
		return 0;
	}
	if (!matrix)
	{
		matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	}
	double temp;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			in >> temp;
			matrix->SetElement(i, j, temp);
		}
		in.eof();
	}
	in.close();
	return 1;
}

void Utils::RGB2HSV(const double RGB[3], double & H, double & S, double & V)
{
	float rgb[3] = { RGB[0], RGB[1], RGB[2] };
	vtkMath::MultiplyScalar(rgb, 1 / 255.f);
	// V
	const auto cMax = std::max(std::max(rgb[0], rgb[1]), rgb[2]);
	const auto cMin = std::min(std::min(rgb[0], rgb[1]), rgb[2]);
	const auto deltaC = cMax - cMin;
	V = cMax;
	// S
	if (cMax == 0) 
	{
		S = 0.0; 
	}
	else
	{
		S = deltaC / cMax;
	}
	// H
	if (deltaC == 0)
	{
		H = 0.0;
	}
	else if (cMax == rgb[0]) // Cmax == R'
	{
		if (rgb[1] >= rgb[2]) // G' > B'
		{
			H = 60.0 * ((rgb[1] - rgb[2]) / deltaC);
		}
		else
		{
			H = 60.0 * ((rgb[1] - rgb[2]) / deltaC) + 360.0;
		}
	}
	else if (cMax == rgb[1]) // Cmax == G'
	{
		H = 60.0 * ((rgb[2] - rgb[0]) / deltaC) + 120.0;
	}
	else if (cMax == rgb[2]) // Cmax == B'
	{
		H = 60.0 * ((rgb[0] - rgb[1]) / deltaC) + 240.0;
	}
}

std::vector<std::unordered_set<vtkIdType>> Utils::findComponents(vtkSmartPointer<vtkPolyData> inputPolyData)
{
	const auto numberOfPoints = inputPolyData->GetNumberOfPoints();
	const auto numberOfCells = inputPolyData->GetNumberOfCells();

	std::vector< std::vector <vtkIdType> > pointIdsWithCellIds;
	pointIdsWithCellIds.resize(numberOfPoints);

	vtkIdType npts;
	const vtkIdType* pts;
	for (vtkIdType i = 0; i < numberOfCells; i++)
	{
		inputPolyData->GetCellPoints(i, npts, pts);
		for (vtkIdType j = 0; j < npts; j++)
		{
			pointIdsWithCellIds[pts[j]].emplace_back(i);
		}
	}
	std::vector<bool> usedPoints;
	usedPoints.resize(numberOfPoints, false);
	std::vector<bool> usedCells;
	usedCells.resize(numberOfCells, false);

	std::vector< std::unordered_set<vtkIdType> > components;

	for (vtkIdType i = 0; i < numberOfCells; i++)
	{
		// If the cell was already added in a component
		if (usedCells[i])
		{
			continue;
		}
		// New cell
		usedCells[i] = true;
		std::unordered_set<vtkIdType> component;
		component.emplace(i);
		//Get the points of cell i
		std::vector<vtkIdType> pointsToLook;
		inputPolyData->GetCellPoints(i, npts, pts);
		for (vtkIdType j = 0; j < npts; j++)
		{
			if (usedPoints[pts[j]])
			{
				continue;
			}
			usedPoints[pts[j]] = true;
			pointsToLook.emplace_back(pts[j]);
		}
		while (!pointsToLook.empty())
		{
			vtkIdType searchPoint = pointsToLook.back();
			pointsToLook.pop_back();
			for (const auto& cellId : pointIdsWithCellIds[searchPoint])
			{
				if (usedCells[cellId])
				{
					continue;
				}
				component.emplace(cellId);
				usedCells[cellId] = true;
				inputPolyData->GetCellPoints(cellId, npts, pts);
				for (vtkIdType j = 0; j < npts; j++)
				{
					if (usedPoints[pts[j]])
					{
						continue;
					}
					usedPoints[pts[j]] = true;
					pointsToLook.emplace_back(pts[j]);
				}
			}
		}
		components.emplace_back(component);
	}
	return components;
}

double Utils::getAngle(const double point1[3], const double point2[3], const double point3[3])
{
	double v1[3];
	double v2[3];
	vtkMath::Subtract(point1, point2, v1);
	vtkMath::Subtract(point3, point2, v2);
	return vtkMath::DegreesFromRadians(vtkMath::AngleBetweenVectors(v1, v2));
}

// CGAL

void polyData2PolygonSoup(vtkSmartPointer<vtkPolyData> polyData,
	std::vector<CGAL::Simple_cartesian<double>::Point_3>& pointsSoup,
	std::vector<std::vector<std::size_t> >& polygonsSoup,
	bool orientSoup = false)
{
	typedef CGAL::Simple_cartesian<double> K;

	// Vertices
	auto points = polyData->GetPoints();
	const auto numberOfPoints = points->GetNumberOfPoints();
	pointsSoup.resize(numberOfPoints);
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		double point[3];
		points->GetPoint(i, point);
		pointsSoup[i] = K::Point_3(point[0], point[1], point[2]);
	}
	// Faces
	auto offsetArray = polyData->GetPolys()->GetOffsetsArray();
	auto idsArray = polyData->GetPolys()->GetConnectivityArray();
	const auto numberOfCells = polyData->GetPolys()->GetNumberOfCells();
	polygonsSoup.resize(numberOfCells);
#pragma omp parallel for
	for (int i = 0; i < numberOfCells; i++)
	{
		double initialOffset[1], vertexIndex[1];
		offsetArray->GetTuple(i, initialOffset);
		polygonsSoup[i].reserve(3);
		for (size_t j = 0; j < 3; j++)
		{
			idsArray->GetTuple(initialOffset[0] + j, vertexIndex);
			polygonsSoup[i].emplace_back(vertexIndex[0]);
		}
	}
	if (orientSoup)
	{
		CGAL::Polygon_mesh_processing::orient_polygon_soup(pointsSoup, polygonsSoup);
	}
}

vtkSmartPointer<vtkPolyData> polygonSoup2PolyData(const std::vector<CGAL::Simple_cartesian<double>::Point_3>& pointsSoup,
	const std::vector<std::vector<std::size_t> >& polygonsSoup)
{
	typedef CGAL::Simple_cartesian<double> K;

	const auto newNumberOfPoints = pointsSoup.size();
	vtkNew<vtkPoints> pointsOut;
	pointsOut->SetNumberOfPoints(newNumberOfPoints);
#pragma omp parallel for
	for (int i = 0; i < newNumberOfPoints; i++)
	{
		K::Point_3 pt = pointsSoup[i];
		pointsOut->InsertPoint(i, (float)pt.x(), (float)pt.y(), (float)pt.z());
	}

	//Write faces
	const auto newNumberOfTriangles = polygonsSoup.size();
	vtkNew<vtkIdTypeArray> newOffsets;
	vtkNew<vtkIdTypeArray> newConnectivity;
	newOffsets->SetNumberOfComponents(1);
	// The last position should be the lenght of the connectivity array
	newOffsets->SetNumberOfTuples(newNumberOfTriangles + 1);
	newConnectivity->SetNumberOfComponents(1);
	newConnectivity->SetNumberOfTuples(newNumberOfTriangles * 3);
#pragma omp parallel for
	for (int i = 0; i < newNumberOfTriangles; i++)
	{
		newOffsets->InsertTuple1(i, i * 3);
		const auto numberOfIndexes = polygonsSoup[i].size();
		for (size_t j = 0; j < numberOfIndexes; j++)
		{
			newConnectivity->InsertTuple1((i * 3) + j, polygonsSoup[i][j]);
		}
	}
	newOffsets->InsertTuple1(newNumberOfTriangles, newConnectivity->GetNumberOfTuples());

	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(pointsOut);
	vtkNew<vtkCellArray> trianglesVTK;
	trianglesVTK->SetData(newOffsets, newConnectivity);
	polyData->SetPolys(trianglesVTK);
	return polyData;
}

vtkSmartPointer<vtkPolyData> Utils::fillHoles(vtkSmartPointer<vtkPolyData> inputPolyData)
{
	typedef CGAL::Simple_cartesian<double> K;
	typedef CGAL::Surface_mesh<K::Point_3> Mesh;
	typedef Mesh::Vertex_index vertex_descriptor;
	typedef Mesh::Face_index face_descriptor;
	typedef CGAL::Polyhedron_3<K>     Polyhedron;
	typedef Polyhedron::Halfedge_handle    Halfedge_handle;
	typedef Polyhedron::Facet_handle       Facet_handle;
	typedef Polyhedron::Vertex_handle      Vertex_handle;

	std::vector<K::Point_3> pointsSoup;
	std::vector<std::vector<size_t>> polygonsSoup;
	polyData2PolygonSoup(inputPolyData, pointsSoup, polygonsSoup);
	Polyhedron poly;
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(pointsSoup, polygonsSoup, poly);

	// Incrementally fill the holes
	for (Halfedge_handle h : halfedges(poly))
	{
		if (h->is_border())
		{
			std::vector<Facet_handle>  patch_facets;
			//std::vector<Vertex_handle> patch_vertices;
			CGAL::Polygon_mesh_processing::triangulate_hole(
				poly,
				h,
				std::back_inserter(patch_facets),
				//std::back_inserter(patch_vertices),
				CGAL::Polygon_mesh_processing::parameters::vertex_point_map(
					CGAL::get(CGAL::vertex_point, poly)).geom_traits(K()).use_delaunay_triangulation(true)
			);
		}
	}
	// Get the output
	std::vector<K::Point_3> pointsSoupOut;
	std::vector<std::vector<size_t>> polygonsSoupOut;
	CGAL::Polygon_mesh_processing::polygon_mesh_to_polygon_soup(poly, pointsSoupOut, polygonsSoupOut);
	return polygonSoup2PolyData(pointsSoupOut, polygonsSoupOut);
}

std::vector<std::vector<vtkIdType>> Utils::closePolyline(vtkSmartPointer<vtkPoints> inputPoints)
{
	typedef CGAL::Simple_cartesian<double> K;

	const auto numberOfPoints = inputPoints->GetNumberOfPoints();

	std::vector<K::Point_3> points;
	points.resize(numberOfPoints);
#pragma omp parallel for
	for (vtkIdType i = 0; i < numberOfPoints; i++)
	{
		double point[3];
		inputPoints->GetPoint(i, point);
		points[i] = K::Point_3(point[0], point[1], point[2]);
	}
	// any type, having Type(int, int, int) constructor available, can be used to hold output triangles
	typedef CGAL::Triple<int, int, int> Triangle_int;
	std::vector<Triangle_int> patch;
	patch.reserve(points.size() - 2); // there will be exactly n-2 triangles in the patch
	CGAL::Polygon_mesh_processing::triangulate_hole_polyline(points, std::back_inserter(patch));

	const auto numberOfTriangles = patch.size();

	std::vector<std::vector<vtkIdType>> triangles;
	triangles.resize(numberOfTriangles);
#pragma omp parallel for
	for (int i = 0; i < numberOfTriangles; i++)
	{
		triangles[i].reserve(3);
		triangles[i].emplace_back(patch[i].first);
		triangles[i].emplace_back(patch[i].second);
		triangles[i].emplace_back(patch[i].third);
	}
	return triangles;
}

vtkSmartPointer<vtkPolyData> Utils::computeIntersection(vtkSmartPointer<vtkPolyData> inputPolyData1, vtkSmartPointer<vtkPolyData> inputPolyData2)
{
	typedef CGAL::Simple_cartesian<double> K;
	typedef CGAL::Surface_mesh<K::Point_3> Mesh;
	typedef Mesh::Vertex_index vertex_descriptor;
	typedef Mesh::Face_index face_descriptor;

	std::vector<K::Point_3> pointsSoup;
	std::vector<std::vector<size_t>> polygonsSoup;

	Mesh mesh1;
	polyData2PolygonSoup(inputPolyData1, pointsSoup, polygonsSoup);
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(pointsSoup, polygonsSoup, mesh1);
	pointsSoup.clear();
	polygonsSoup.clear();

	Mesh mesh2;
	polyData2PolygonSoup(inputPolyData2, pointsSoup, polygonsSoup, true);
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(pointsSoup, polygonsSoup, mesh2);
	CGAL::Polygon_mesh_processing::orient_to_bound_a_volume(mesh2);
	pointsSoup.clear();
	polygonsSoup.clear();

	Mesh mesh_out;
	bool valid_operation = CGAL::Polygon_mesh_processing::corefine_and_compute_intersection(mesh1, mesh2, mesh_out);
	if (!valid_operation)
	{
		return nullptr;
	}
	// Get the output
	CGAL::Polygon_mesh_processing::polygon_mesh_to_polygon_soup(mesh_out, pointsSoup, polygonsSoup);
	return polygonSoup2PolyData(pointsSoup, polygonsSoup);
}

vtkSmartPointer<vtkPolyData> Utils::Remeshing(vtkSmartPointer<vtkPolyData> inputPolyData, const double targetEdgeLength)
{
	typedef CGAL::Simple_cartesian<double> K;
	typedef CGAL::Surface_mesh<K::Point_3> Mesh;
	typedef boost::graph_traits<Mesh>::halfedge_descriptor halfedge_descriptor;
	typedef boost::graph_traits<Mesh>::edge_descriptor     edge_descriptor;

	struct halfedge2edge
	{
		halfedge2edge(const Mesh& m, std::vector<edge_descriptor>& edges)
			: m_mesh(m), m_edges(edges)
		{}
		void operator()(const halfedge_descriptor& h) const
		{
			m_edges.push_back(edge(h, m_mesh));
		}
		const Mesh& m_mesh;
		std::vector<edge_descriptor>& m_edges;
	};

	std::vector<K::Point_3> pointsSoup;
	std::vector<std::vector<size_t>> polygonsSoup;

	Mesh mesh;
	polyData2PolygonSoup(inputPolyData, pointsSoup, polygonsSoup);
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(pointsSoup, polygonsSoup, mesh);
	pointsSoup.clear();
	polygonsSoup.clear();

	unsigned int nb_iter = 3;
	std::vector<edge_descriptor> border;
	CGAL::Polygon_mesh_processing::border_halfedges(faces(mesh),
		mesh,
		boost::make_function_output_iterator(halfedge2edge(mesh, border)));
	CGAL::Polygon_mesh_processing::split_long_edges(border, targetEdgeLength, mesh);
	CGAL::Polygon_mesh_processing::isotropic_remeshing(
		faces(mesh),
		targetEdgeLength,
		mesh,
		CGAL::Polygon_mesh_processing::parameters::number_of_iterations(nb_iter)
		.protect_constraints(true)//i.e. protect border, here
	);

	// Get the output
	CGAL::Polygon_mesh_processing::polygon_mesh_to_polygon_soup(mesh, pointsSoup, polygonsSoup);
	return polygonSoup2PolyData(pointsSoup, polygonsSoup);
}

wxTreeListItem Utils::addItemToTree(wxTreeListCtrl*  tree, const wxTreeListItem & parentItem, const std::string & itemName, const int state)
{
	wxTreeListItem newTreeItem = tree->AppendItem(parentItem, wxString(itemName));
	tree->CheckItem(newTreeItem, (wxCheckBoxState)state);
	return newTreeItem;
}

bool Utils::CreateDir(const std::string & dirName)
{
	if (!wxMkdir(dirName))
	{
		wxLogError(wxString("Error creating the directory " + dirName));
		return 0;
	}
	return 1;
}

std::string Utils::GetLastDirName(const std::string & dirPath)
{
	if (dirPath.find_last_of('\\') != std::string::npos)
	{
		return dirPath.substr(dirPath.find_last_of('\\'));
	}
	return dirPath.substr(dirPath.find_last_of('/'));
}

bool Utils::RemoveFile(const std::string & path)
{
	if (exists(path))
	{
		return wxRemoveFile(path);
	}
	else
	{
		return false;
	}
}
