#include "SliderRepresentation2D.h"

#include <vtkObjectFactory.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkRegularPolygonSource.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkCubeSource.h>
#include <vtkActor2D.h>
#include <vtkProperty2D.h>
#include <vtkRenderer.h>
#include <vtkWindow.h>

#include "Calibration.h"

vtkStandardNewMacro(SliderRepresentation2D);

//----------------------------------------------------------------------
SliderRepresentation2D::SliderRepresentation2D()
{
	//Text Property
	textProperty = vtkSmartPointer<vtkTextProperty>::New();
	textProperty->SetBold(1);
	textProperty->SetShadow(1);
	textProperty->SetFontFamilyToArial();
	textProperty->SetJustificationToCentered();
	textProperty->SetVerticalJustificationToCentered();

	//Limits
	maxTextActor = vtkSmartPointer<vtkTextActor>::New();
	maxTextActor->SetInput("");
	maxTextActor->SetTextProperty(textProperty);

	minTextActor = vtkSmartPointer<vtkTextActor>::New();
	minTextActor->SetInput("");
	minTextActor->SetTextProperty(textProperty);

	//Title
	titleTextActor = vtkSmartPointer<vtkTextActor>::New();
	titleTextActor->SetInput("");
	titleTextActor->SetTextProperty(textProperty);

	//Value
	sliderMinValueTextActor = vtkSmartPointer<vtkTextActor>::New();
	sliderMinValueTextActor->SetInput("");
	sliderMinValueTextActor->SetTextProperty(textProperty);

	sliderMaxValueTextActor = vtkSmartPointer<vtkTextActor>::New();
	sliderMaxValueTextActor->SetInput("");
	sliderMaxValueTextActor->SetTextProperty(textProperty);


	//Slider
	vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
	T->Identity();

	vtkSmartPointer<vtkRegularPolygonSource> polygonSource = vtkSmartPointer<vtkRegularPolygonSource>::New();
	polygonSource->SetNumberOfSides(50);
	polygonSource->SetRadius(sliderRadius);
	polygonSource->SetCenter(0, 0, 0);

	sliderTransFilter = vtkSmartPointer<vtkTransformFilter>::New();
	sliderTransFilter->SetTransform(T);
	sliderTransFilter->SetInputConnection(polygonSource->GetOutputPort());
	sliderTransFilter->Update();

	vtkSmartPointer<vtkPolyDataMapper2D> sliderMinMapper = vtkSmartPointer<vtkPolyDataMapper2D>::New();
	sliderMinMapper->SetInputConnection(sliderTransFilter->GetOutputPort());

	sliderMinActor = vtkSmartPointer<vtkActor2D>::New();
	sliderMinActor->GetProperty()->SetColor(0, 0, 1);
	sliderMinActor->SetMapper(sliderMinMapper);

	vtkSmartPointer<vtkPolyDataMapper2D> sliderMaxMapper = vtkSmartPointer<vtkPolyDataMapper2D>::New();
	sliderMaxMapper->SetInputConnection(sliderTransFilter->GetOutputPort());

	sliderMaxActor = vtkSmartPointer<vtkActor2D>::New();
	sliderMaxActor->GetProperty()->SetColor(0, 0, 1);
	sliderMaxActor->SetMapper(sliderMaxMapper);


	vtkSmartPointer<vtkCubeSource> cubeSource = vtkSmartPointer<vtkCubeSource>::New();
	cubeSource->SetCenter(0, 0, 0);
	cubeSource->SetXLength(xLenghtTube * 2);
	cubeSource->SetYLength(yLenghtTube * 2);
	cubeSource->SetZLength(1);

	sliderTubeTransFilter = vtkSmartPointer<vtkTransformFilter>::New();
	sliderTubeTransFilter->SetTransform(T);
	sliderTubeTransFilter->SetInputConnection(cubeSource->GetOutputPort());
	sliderTubeTransFilter->Update();

	vtkSmartPointer<vtkPolyDataMapper2D> sliderTubeMapper = vtkSmartPointer<vtkPolyDataMapper2D>::New();
	sliderTubeMapper->SetInputConnection(sliderTubeTransFilter->GetOutputPort());

	sliderTubeActor = vtkSmartPointer<vtkActor2D>::New();
	sliderTubeActor->GetProperty()->SetColor(1, 1, 1);
	sliderTubeActor->SetMapper(sliderTubeMapper);
}

SliderRepresentation2D::~SliderRepresentation2D()
{
	if (titleTextActor)
	{
		this->GetRenderer()->RemoveActor(titleTextActor);
		titleTextActor = nullptr;
	}
	if (minTextActor)
	{
		this->GetRenderer()->RemoveActor(minTextActor);
		minTextActor = nullptr;
	}
	if (maxTextActor)
	{
		this->GetRenderer()->RemoveActor(maxTextActor);
		maxTextActor = nullptr;
	}
	if (sliderMinValueTextActor)
	{
		this->GetRenderer()->RemoveActor(sliderMinValueTextActor);
		sliderMinValueTextActor = nullptr;
	}
	if (sliderMaxValueTextActor)
	{
		this->GetRenderer()->RemoveActor(sliderMaxValueTextActor);
		sliderMaxValueTextActor = nullptr;
	}
	if (sliderTubeActor)
	{
		this->GetRenderer()->RemoveActor2D(sliderTubeActor);
		sliderTubeActor = nullptr;
	}
	if (sliderMinActor)
	{
		this->GetRenderer()->RemoveActor2D(sliderMinActor);
		sliderMinActor = nullptr;
	}
	if (sliderMaxActor)
	{
		this->GetRenderer()->RemoveActor2D(sliderMaxActor);
		sliderMaxActor = nullptr;
	}
}

//----------------------------------------------------------------------
void SliderRepresentation2D::SetTitleText(const char* label)
{
	titleTextActor->SetInput(label);
}

//----------------------------------------------------------------------
const char* SliderRepresentation2D::GetTitleText()
{
	return titleTextActor->GetInput();
}

//----------------------------------------------------------------------
void SliderRepresentation2D::StartWidgetInteraction(double eventPos[2])
{
	if (eventPos[0] > xMinSliderMin && eventPos[0] < xMaxSliderMin &&
		eventPos[1] > yMinSliderMin && eventPos[1] < yMaxSliderMin)
	{
		this->InteractionState = vtkSliderRepresentation::Slider;
		selectedSlider = 0;
		return;
	}
	else if (eventPos[0] > xMinSliderMax && eventPos[0] < xMaxSliderMax &&
		eventPos[1] > yMinSliderMax && eventPos[1] < yMaxSliderMax)
	{
		this->InteractionState = vtkSliderRepresentation::Slider;
		selectedSlider = 1;
		return;
	}
	else if (eventPos[0] > xMinTube && eventPos[0] < xMaxTube &&
		eventPos[1] > yMinTube && eventPos[1] < yMaxTube)
	{
		if (!useMaxSlider)
		{
			this->InteractionState = vtkSliderRepresentation::Tube;
			this->ComputePickPosition(eventPos);
			selectedSlider = 0;
			return;
		}
	}
	this->InteractionState = vtkSliderRepresentation::Outside;
	selectedSlider = -1;
}

//----------------------------------------------------------------------
void SliderRepresentation2D::WidgetInteraction(double eventPos[2])
{
	if (selectedSlider == -1)
	{
		return;
	}
	double t = this->ComputePickPosition(eventPos);
	if (selectedSlider == 0)
	{
		this->Value = this->MinimumValue + t * (this->MaximumValue - this->MinimumValue);
		if (useMaxSlider && this->Value >= sliderMaxValue)
		{
			this->Value = this->GetMinimumValue();
		}
		this->Modified();
	}
	else if (selectedSlider == 1)
	{
		sliderMaxValue = this->MinimumValue + t * (this->MaximumValue - this->MinimumValue);
		if (sliderMaxValue <= this->Value)
		{
			sliderMaxValue = this->GetMaximumValue();
		}
		this->Modified();
	}
	this->BuildRepresentation();
}

void SliderRepresentation2D::SetMaxLimitText(const char* text)
{
	maxTextActor->SetInput(text);
}

void SliderRepresentation2D::SetMinLimitText(const char* text)
{
	minTextActor->SetInput(text);
}

//----------------------------------------------------------------------
void SliderRepresentation2D::PlaceWidget(double *vtkNotUsed(bds[6]))
{
	// Position the handles at the end of the lines
	this->BuildRepresentation();
}

//----------------------------------------------------------------------
double SliderRepresentation2D::ComputePickPosition(double eventPos[2])
{
	if (xMinTube == -1)
	{
		return 0.0;
	}
	this->PickedT = (eventPos[0] - xMinTube) / (xMaxTube - xMinTube);
	this->PickedT = (this->PickedT < 0 ? 0.0 :
		(this->PickedT > 1.0 ? 1.0 : this->PickedT));

	return this->PickedT;
}

//----------------------------------------------------------------------
void SliderRepresentation2D::Highlight(int highlight)
{
	if (selectedSlider == 0)
	{
		if (highlight)
		{
			sliderMinActor->GetProperty()->SetColor(1, 0, 0);
		}
		else
		{
			sliderMinActor->GetProperty()->SetColor(0, 0, 1);
		}
	}
	else if (selectedSlider == 1)
	{
		if (highlight)
		{
			sliderMaxActor->GetProperty()->SetColor(1, 0, 0);
		}
		else
		{
			sliderMaxActor->GetProperty()->SetColor(0, 0, 1);
		}
	}
}


//----------------------------------------------------------------------
void SliderRepresentation2D::BuildRepresentation()
{
	if (this->GetMTime() > this->BuildTime || (this->Renderer && this->Renderer->GetVTKWindow() && this->Renderer->GetVTKWindow()->GetMTime() > this->BuildTime))
	{
		int *size = this->Renderer->GetSize();
		if (0 == size[0] || 0 == size[1])
		{
			// Renderer has no size yet: wait until the next
			// BuildRepresentation...
			return;
		}
		double scale = size[0] / 500.f;
		if (scale*xLenghtTube > 150)
		{
			scale = 150.f / xLenghtTube;
		}
		if (scale*xLenghtTube < 70)
		{
			scale = 70.f / xLenghtTube;
		}
		//Sliders
		vtkSmartPointer<vtkTransform> T = vtkSmartPointer<vtkTransform>::New();
		T->Scale(scale, scale, 1);
		sliderTubeTransFilter->SetTransform(T);
		sliderTubeTransFilter->Update();
		sliderTransFilter->SetTransform(T);
		sliderTransFilter->Update();

		double xCenterTube = (xLenghtTube + 30 + xCenterOffset) * scale;
		double yCenterTube = (yLenghtTube + 35 + yCenterOffset) * scale;
		double xHalfTube = xLenghtTube * scale;
		double yHalfTube = (yLenghtTube + 5) * scale;// + 5 to make easier to select 
		xMinTube = xCenterTube - xHalfTube;
		xMaxTube = xCenterTube + xHalfTube;
		yMinTube = yCenterTube - yHalfTube;
		yMaxTube = yCenterTube + yHalfTube;
		sliderTubeActor->SetPosition(xCenterTube, yCenterTube);

		double t = (this->Value - this->MinimumValue) / (this->MaximumValue - this->MinimumValue);
		double xSliderMin = (xMaxTube - xMinTube) * t;
		double halfSlider = sliderRadius * scale;
		xMinSliderMin = xMinTube + xSliderMin - halfSlider;
		xMaxSliderMin = xMinTube + xSliderMin + halfSlider;
		yMinSliderMin = yCenterTube - halfSlider;
		yMaxSliderMin = yCenterTube + halfSlider;
		sliderMinActor->SetPosition(xMinTube + xSliderMin, yCenterTube);

		if (useMaxSlider)
		{
			double tMax = (sliderMaxValue - this->MinimumValue) / (this->MaximumValue - this->MinimumValue);
			double xSliderMax = (xMaxTube - xMinTube) * tMax;
			double halfSliderMax = sliderRadius * scale;
			xMinSliderMax = xMinTube + xSliderMax - halfSliderMax;
			xMaxSliderMax = xMinTube + xSliderMax + halfSliderMax;
			yMinSliderMax = yCenterTube - halfSliderMax;
			yMaxSliderMax = yCenterTube + halfSliderMax;
			sliderMaxActor->SetPosition(xMinTube + xSliderMax, yCenterTube);
			char labelMax[256];
			if (calib)
			{
				snprintf(labelMax, sizeof(labelMax), this->LabelFormat, this->sliderMaxValue * calib->getScaleFactor());
			}
			else
			{
				snprintf(labelMax, sizeof(labelMax), this->LabelFormat, this->sliderMaxValue);
			}
			sliderMaxValueTextActor->SetInput(labelMax);
			sliderMaxValueTextActor->SetPosition(xMinTube + xSliderMax, yCenterTube + 20 * scale);
		}

		//Texts
		titleTextActor->SetPosition(xCenterTube, yCenterTube - 20 * scale);
		char labelMin[256];
		if (calib)
		{
			snprintf(labelMin, sizeof(labelMin), this->LabelFormat, this->Value * calib->getScaleFactor());
		}
		else
		{
			snprintf(labelMin, sizeof(labelMin), this->LabelFormat, this->Value);
		}
		sliderMinValueTextActor->SetInput(labelMin);
		sliderMinValueTextActor->SetPosition(xMinTube + xSliderMin, yCenterTube + 20 * scale);
		minTextActor->SetPosition(xMinTube, yCenterTube - 20 * scale);
		maxTextActor->SetPosition(xMaxTube, yCenterTube - 20 * scale);
		textProperty->SetFontSize(15 * scale);

		this->BuildTime.Modified();
	}
}

//----------------------------------------------------------------------
void SliderRepresentation2D::GetActors2D(vtkPropCollection *pc)
{
	pc->AddItem(maxTextActor);
	pc->AddItem(minTextActor);
	pc->AddItem(titleTextActor);
	pc->AddItem(sliderMinValueTextActor);
	pc->AddItem(sliderTubeActor);
	pc->AddItem(sliderMinActor);
	if (useMaxSlider)
	{
		pc->AddItem(sliderMaxValueTextActor);
		pc->AddItem(sliderMaxActor);
	}
}

//----------------------------------------------------------------------
void SliderRepresentation2D::ReleaseGraphicsResources(vtkWindow *w)
{
	maxTextActor->ReleaseGraphicsResources(w);
	minTextActor->ReleaseGraphicsResources(w);
	titleTextActor->ReleaseGraphicsResources(w);
	sliderMinValueTextActor->ReleaseGraphicsResources(w);
	sliderTubeActor->ReleaseGraphicsResources(w);
	sliderMinActor->ReleaseGraphicsResources(w);
	if (useMaxSlider)
	{
		sliderMaxValueTextActor->ReleaseGraphicsResources(w);
		sliderMaxActor->ReleaseGraphicsResources(w);
	}
}

//----------------------------------------------------------------------
int SliderRepresentation2D::RenderOpaqueGeometry(vtkViewport *viewport)
{
	this->BuildRepresentation();
	int count = sliderTubeActor->RenderOpaqueGeometry(viewport);
	count += sliderMinActor->RenderOpaqueGeometry(viewport);
	count += maxTextActor->RenderOpaqueGeometry(viewport);
	count += minTextActor->RenderOpaqueGeometry(viewport);
	count += titleTextActor->RenderOpaqueGeometry(viewport);
	count += sliderMinValueTextActor->RenderOpaqueGeometry(viewport);
	if (useMaxSlider)
	{
		count += sliderMaxActor->RenderOpaqueGeometry(viewport);
		count += sliderMaxValueTextActor->RenderOpaqueGeometry(viewport);
	}
	return count;
}

//----------------------------------------------------------------------
int SliderRepresentation2D::RenderOverlay(vtkViewport *viewport)
{
	this->BuildRepresentation();
	int count = sliderTubeActor->RenderOverlay(viewport);
	count += sliderMinActor->RenderOverlay(viewport);
	count += maxTextActor->RenderOverlay(viewport);
	count += minTextActor->RenderOverlay(viewport);
	count += titleTextActor->RenderOverlay(viewport);
	count += sliderMinValueTextActor->RenderOverlay(viewport);
	if (useMaxSlider)
	{
		count += sliderMaxActor->RenderOverlay(viewport);
		count += sliderMaxValueTextActor->RenderOverlay(viewport);
	}
	return count;
}
