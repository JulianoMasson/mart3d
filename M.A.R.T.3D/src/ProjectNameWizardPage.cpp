#include "ProjectNameWizardPage.h"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/filepicker.h>
#include <wx/stdpaths.h>

ProjectNameWizardPage::ProjectNameWizardPage(wxWizard * parent, wxWizardPage * prev, wxWizardPage * next, const wxBitmap & bitmap) :
	wxWizardPageSimple(parent, prev, next, bitmap)
{
	auto bSizer = new wxBoxSizer(wxVERTICAL);

	bSizer->Add(new wxStaticText(this, wxID_ANY, "Select the project name and where it should be created."));

	bSizer->AddSpacer(15);

	auto fgSizer = new wxFlexGridSizer(0, 2, wxSize(10, 10));
	fgSizer->AddGrowableCol(1);
	fgSizer->SetFlexibleDirection(wxBOTH);
	fgSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Name"));

	projectName = new wxTextCtrl(this, wxID_ANY);

	fgSizer->Add(projectName, 0, wxEXPAND, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Create in"));

	wxStandardPaths stdPaths = wxStandardPaths::Get();
	const auto smartPath = stdPaths.GetDocumentsDir() + "\\" + "SMART3D";
	if (!wxDirExists(smartPath))
	{
		wxMkdir(smartPath);
	}
	projectDir = new wxDirPickerCtrl(this, wxID_ANY, smartPath);

	fgSizer->Add(projectDir, 0, wxEXPAND, 5);

	bSizer->Add(fgSizer, 0, wxEXPAND, 5);

	usedNameWarning = new wxStaticText(this, wxID_ANY, "");
	usedNameWarning->SetForegroundColour(wxColour(255, 0, 0));

	fgSizer->Add(usedNameWarning);

	this->SetSizer(bSizer);

	projectName->Connect(wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler(ProjectNameWizardPage::OnTextCtrl), nullptr, this);
	projectDir->Connect(wxEVT_DIRPICKER_CHANGED, wxCommandEventHandler(ProjectNameWizardPage::OnDirPickerCtrl), nullptr, this);
	this->Connect(wxEVT_WIZARD_PAGE_SHOWN, wxCommandEventHandler(ProjectNameWizardPage::OnDirPickerCtrl), nullptr, this);
	this->Layout();
}

std::string ProjectNameWizardPage::GetProjectPath()
{
	return std::string(projectDir->GetPath() + "\\" + projectName->GetValue());
}

void ProjectNameWizardPage::OnTextCtrl(wxCommandEvent& event)
{
	if (projectDir->GetPath() != "")
	{
		const auto projectPath = projectDir->GetPath() + "\\" +  projectName->GetValue();
		auto foward = this->FindWindowById(wxID_FORWARD);
		if (wxDirExists(projectPath))
		{
			usedNameWarning->SetLabel("Name already used");
			if (foward)
			{
				foward->Disable();
			}
		}
		else
		{
			usedNameWarning->SetLabel("");
			if (foward)
			{
				foward->Enable();
			}
		}
	}
}

void ProjectNameWizardPage::OnDirPickerCtrl(wxCommandEvent& event)
{
	auto foward = this->FindWindowById(wxID_FORWARD);
	if (foward)
	{
		foward->Disable();
	}
	if (projectName->GetValue() != "")
	{
		const auto projectPath = projectDir->GetPath() + "\\" + projectName->GetValue();
		
		if (wxDirExists(projectPath))
		{
			usedNameWarning->SetLabel("Name already used");
		}
		else
		{
			usedNameWarning->SetLabel("");
			if (foward)
			{
				foward->Enable();
			}
		}
	}
}
