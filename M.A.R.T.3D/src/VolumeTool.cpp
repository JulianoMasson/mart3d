#include "VolumeTool.h"

#include <sstream>

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkCommand.h>
#include <vtkPropPicker.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkDelaunay3D.h>
#include <vtkPolygon.h>
#include <vtkPlaneCollection.h>
#include <vtkPlane.h>
#include <vtkUnstructuredGrid.h>
#include <vtkMapper.h>
#include <vtkClipClosedSurface.h>
#include <vtkTriangleFilter.h>
#include <vtkClipPolyData.h>
#include <vtkMultiObjectMassProperties.h>
#include <vtkCenterOfMass.h>
#include <vtkFeatureEdges.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkTransform.h>
#include <vtkAppendPolyData.h>
#include <vtkCleanPolyData.h>

#include <wx/treelist.h>
#include <wx/msgdlg.h>

#include "ImplicitPlaneWidget.h"
#include "LineWidget.h"
#include "LineWidgetRepresentation.h"
#include "Draw.h"
#include "Utils.h"
#include "Volume.h"
#include "Project.h"
#include "Mesh.h"

vtkStandardNewMacro(VolumeTool);

//----------------------------------------------------------------------------
VolumeTool::VolumeTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(VolumeTool::ProcessEvents);
}

//----------------------------------------------------------------------------
VolumeTool::~VolumeTool()
{
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
		lineWidget = nullptr;
	}
	if (planeWidget)
	{
		planeWidget->EnabledOff();
		planeWidget = nullptr;
	}
	if (cuttingVolumeActor)
	{
		CurrentRenderer->RemoveActor(cuttingVolumeActor);
		cuttingVolumeActor = nullptr;
	}
	if (planeActor)
	{
		CurrentRenderer->RemoveActor(planeActor);
		planeActor = nullptr;
	}
	mesh = nullptr;
	project = nullptr;
	meshPolyData = nullptr;
	propPicker = nullptr;
}

//----------------------------------------------------------------------------
void VolumeTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh || !project) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		// listen for the following events
		vtkRenderWindowInteractor *i = this->Interactor;
		i->AddObserver(vtkCommand::KeyPressEvent,
			this->EventCallbackCommand, this->Priority);
		i->AddObserver(vtkCommand::RightButtonPressEvent,
			this->EventCallbackCommand, this->Priority);

		createLineWidget();

		//Mesh to be tested
		meshPolyData = mesh->GetPolyData();
		vtkNew<vtkCleanPolyData> cleanPolyData;
		cleanPolyData->SetInputData(meshPolyData);
		cleanPolyData->PointMergingOff();
		cleanPolyData->Update();
		meshPolyData = cleanPolyData->GetOutput();

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		mesh = nullptr;
		project = nullptr;
		meshPolyData = nullptr;
		propPicker = nullptr;
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
		}
		if (planeWidget)
		{
			planeWidget->EnabledOff();
			planeWidget = nullptr;
		}
		if (cuttingVolumeActor)
		{
			CurrentRenderer->RemoveActor(cuttingVolumeActor);
			cuttingVolumeActor = nullptr;
		}
		if (planeActor)
		{
			CurrentRenderer->RemoveActor(planeActor);
			planeActor = nullptr;
		}
		updatePolygon = false;

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void VolumeTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	VolumeTool* self =
		reinterpret_cast<VolumeTool*>(clientdata);

	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::StartInteractionEvent:
	case vtkCommand::InteractionEvent:
	case vtkCommand::EndInteractionEvent:
		self->updatePolygon = true;
		self->UpdateRepresentation();
		break;
	case vtkCommand::KeyPressEvent:
		self->OnKeyPress();
		break;
	case vtkCommand::RightButtonPressEvent:
		self->OnRightButton();
		break;
	}
}

void VolumeTool::enterKeyPressed()
{
	if (planeWidget)
	{
		if (planeWidget->GetEnabled())
		{
			//Get plane actor
			if (!planeActor)
			{
				planeActor = vtkSmartPointer<vtkActor>::New();
			}
			planeActor = planeWidget->GetPlaneActor();
			planeActor->GetProperty()->SetColor(0.0, 0.0, 0.0);
			planeWidget->Off();
			CurrentRenderer->AddActor(planeActor);
			CurrentRenderer->GetRenderWindow()->Render();
			//Start the line widget
			createLineWidget();
		}
	}
	if (lineWidget)
	{
		if (lineWidget->GetRepresentation()->isLoopClosed())
		{
			computeVolume();
			if (planeActor)
			{
				CurrentRenderer->RemoveActor(planeActor);
				planeActor = nullptr;
				CurrentRenderer->GetRenderWindow()->Render();
			}
			// Reset the line widget
			createLineWidget();
		}
	}
}

void VolumeTool::OnKeyPress()
{
	char key = this->Interactor->GetKeyCode();
	if (this->Interactor->GetControlKey() && key == 'P')
	{
		createPlaneWidget();
	}
	else if (key == '=')
	{
		polygonHeight += polygonHeightIncrement;
		updatePolygon = true;
		this->UpdateRepresentation();
	}
	else if (key == '-')
	{
		polygonHeight -= polygonHeightIncrement;
		updatePolygon = true;
		this->UpdateRepresentation();
	}
	else if (key == 'I')
	{
		invertSelection = true;
		this->UpdateRepresentation();
	}
}

void VolumeTool::OnRightButton()
{
	if (propPicker && !planeWidget->GetEnabled() && !planeActor)
	{
		if (propPicker->Pick(this->GetInteractor()->GetEventPosition()[0], this->GetInteractor()->GetEventPosition()[1], 0, CurrentRenderer))
		{
			double clickPos[3];
			propPicker->GetPickPosition(clickPos);
			planeWidget->SetOrigin(clickPos);
			planeWidget->SetNormal(0, 0, 1);
			planeWidget->UpdatePlacement();
			planeWidget->On();
		}
	}
}

//----------------------------------------------------------------------------
void VolumeTool::UpdateRepresentation()
{
	if (lineWidget && updatePolygon)
	{
		vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
		if (rep->isLoopClosed())
		{
			updatePolygon = false;
			auto repPoints = rep->getPoints();
			vtkNew<vtkPoints> pointsCuttingvolume;
			const auto numberOfPoints = repPoints->GetNumberOfPoints() - 1;
			double sumLineSegmentsDistance = 0.0;
			for (unsigned int i = 0; i < numberOfPoints; i++)
			{
				double point[3];
				repPoints->GetPoint(i, point);
				pointsCuttingvolume->InsertNextPoint(point);
				sumLineSegmentsDistance += std::sqrt(vtkMath::Distance2BetweenPoints(point, repPoints->GetPoint(i + 1)));
			}
			if (!cuttingVolumeActor)
			{
				polygonHeight = sumLineSegmentsDistance / numberOfPoints;
				polygonHeightIncrement = polygonHeight / 100.0;
			}
			//To change the height of the polygon you should multiply the n vector
			double normal[3];
			getNormal(normal);
			vtkMath::MultiplyScalar(normal, polygonHeight);
			if (cuttingVolumeActor)
			{
				CurrentRenderer->RemoveActor(cuttingVolumeActor);
			}
			cuttingVolumeActor = Draw::createPolyData(CurrentRenderer,
				createCuttingVolume(pointsCuttingvolume, normal),
				1.0, 0.0, 0.0);
			cuttingVolumeActor->GetProperty()->SetOpacity(0.3);
			cuttingVolumeActor->SetPickable(false);
			CurrentRenderer->AddActor(cuttingVolumeActor);
			this->Interactor->Render();
		}
		else
		{
			if (cuttingVolumeActor)
			{
				CurrentRenderer->RemoveActor(cuttingVolumeActor);
				this->Interactor->Render();
			}
		}
	}
}

vtkSmartPointer<vtkPlaneCollection> VolumeTool::createCuttingPlanes(vtkSmartPointer<vtkPoints> points, const double vector[3])
{
	// Compute the top points
	const auto numberOfPoints = points->GetNumberOfPoints();
	double addResult[3];
	for (unsigned int i = 0; i < numberOfPoints; i++)
	{
		vtkMath::Add(vector, points->GetPoint(i), addResult);
		points->InsertNextPoint(addResult);
	}
	//Compute centroid
	vtkNew<vtkPolyData> polyDataCentroid;
	polyDataCentroid->SetPoints(points);
	vtkNew<vtkCenterOfMass> centroidFilter;
	centroidFilter->SetInputData(polyDataCentroid);
	centroidFilter->Update();
	double centroid[3];
	centroidFilter->GetCenter(centroid);
	// Create the cutting planes
	vtkNew<vtkPlaneCollection> planes;
	for (int i = 0; i < 2; i++)//The top and bottom planes
	{
		double normal[3];
		double pointA[3];
		double pointB[3];
		double pointC[3];
		points->GetPoint(numberOfPoints - 1 + (numberOfPoints * i), pointA);
		points->GetPoint(numberOfPoints * i, pointB);
		points->GetPoint(1 + (numberOfPoints * i), pointC);
		Utils::getNormal(pointA, pointB, pointC, centroid, normal);
		vtkNew<vtkPlane> plane;
		plane->SetOrigin(pointB);
		plane->SetNormal(normal);
		planes->AddItem(plane);
	}
	for (int i = 0; i < numberOfPoints; i++)
	{
		double normal[3];
		double pointA[3];
		double pointB[3];
		double pointC[3];
		points->GetPoint(i + numberOfPoints, pointA);
		points->GetPoint(i, pointB);
		if ((i + 1) == numberOfPoints)//Last plane
		{
			points->GetPoint(0, pointC);
		}
		else//Planes in the side
		{
			points->GetPoint(i + 1, pointC);
		}
		Utils::getNormal(pointA, pointB, pointC, centroid, normal);
		vtkNew<vtkPlane> plane;
		plane->SetOrigin(pointB);
		plane->SetNormal(normal);
		planes->AddItem(plane);
	}
	return planes;
}

vtkSmartPointer<vtkPolyData> VolumeTool::createCuttingVolume(vtkSmartPointer<vtkPoints> pointsIn, const double vector[3])
{
	vtkNew<vtkPoints> points;
	points->DeepCopy(pointsIn);
	// Compute the top points
	const auto numberOfPoints = points->GetNumberOfPoints();
	double addResult[3];
	for (unsigned int i = 0; i < numberOfPoints; i++)
	{
		vtkMath::Add(vector, points->GetPoint(i), addResult);
		points->InsertNextPoint(addResult);
	}
	// Create the polygons
	vtkNew<vtkCellArray> cellArray;
	for (vtkIdType i = 0; i < 2; i++)//The top and bottom planes
	{
		vtkNew<vtkPolygon> polygon;
		polygon->GetPointIds()->SetNumberOfIds(numberOfPoints);
		for (vtkIdType j = 0; j < numberOfPoints; j++)
		{
			polygon->GetPointIds()->SetId(j, j + (numberOfPoints * i));
		}
		cellArray->InsertNextCell(polygon);
	}
	for (vtkIdType i = 0; i < numberOfPoints; i++)
	{
		vtkNew<vtkPolygon> polygon;
		polygon->GetPointIds()->SetNumberOfIds(4);
		polygon->GetPointIds()->SetId(0, i);
		if ((i + 1) == numberOfPoints)//Last plane
		{
			polygon->GetPointIds()->SetId(1, 0);
			polygon->GetPointIds()->SetId(2, numberOfPoints);
		}
		else//Planes in the side
		{
			polygon->GetPointIds()->SetId(1, i + 1);
			polygon->GetPointIds()->SetId(2, i + 1 + numberOfPoints);
		}
		polygon->GetPointIds()->SetId(3, i + numberOfPoints);
		cellArray->InsertNextCell(polygon);
	}
	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(points);
	polyData->SetPolys(cellArray);
	vtkNew<vtkTriangleFilter> triangleFilter;
	triangleFilter->SetInputData(polyData);
	triangleFilter->Update();
	return triangleFilter->GetOutput();
}

vtkSmartPointer<vtkPolyData> VolumeTool::getBaseTriangles()
{
	auto pointsIn = lineWidget->GetRepresentation()->getPoints();
	const auto numberOfPoints = pointsIn->GetNumberOfPoints() - 1;
	// Add the points
	vtkNew<vtkPoints> points;
	vtkNew<vtkPolygon> polygon;
	polygon->GetPointIds()->SetNumberOfIds(numberOfPoints);
	for (unsigned int i = 0; i < numberOfPoints; i++)
	{
		points->InsertNextPoint(pointsIn->GetPoint(i));
		polygon->GetPointIds()->SetId(i, i);
	}
	// Create the PolyData
	vtkNew<vtkCellArray> cellArray;
	cellArray->InsertNextCell(polygon);
	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(points);
	polyData->SetPolys(cellArray);
	vtkNew<vtkTriangleFilter> triangleFilter;
	triangleFilter->SetInputData(polyData);
	triangleFilter->Update();
	return triangleFilter->GetOutput();
}

void VolumeTool::getNormal(double normal[3])
{
	if (planeWidget)
	{
		planeWidget->GetNormal(normal);
	}
	else
	{
		vtkPolygon::ComputeNormal(lineWidget->GetRepresentation()->getPoints(), normal);
	}
	double z[3] = {0.0, 0.0, 1.0};
	// Force the normal to be in the Z+ direction
	if (vtkMath::Dot(normal, z) < 0)
	{
		vtkMath::MultiplyScalar(normal, -1.0);
	}
}

void VolumeTool::computeVolume()
{
	double normal[3];
	getNormal(normal);
	vtkMath::MultiplyScalar(normal, polygonHeight);
	// Get the triangles in the base of the selection
	auto triangles = getBaseTriangles();
	// Compute the closed surface for each triangle
	const auto numberOfCells = triangles->GetNumberOfCells();
	vtkNew<vtkAppendPolyData> appendPolyData;
	appendPolyData->UserManagedInputsOn();
	appendPolyData->SetNumberOfInputs(numberOfCells);
#pragma omp parallel for
	for (int i = 0; i < numberOfCells; i++)
	{
		vtkIdType npts;
		const vtkIdType* pts;
		triangles->GetCellPoints(i, npts, pts);
		vtkNew<vtkPoints> trianglePoints;
		for (vtkIdType j = 0; j < npts; j++)
		{
			double point[3];
			triangles->GetPoint(pts[j], point);
			trianglePoints->InsertNextPoint(point);
		}
		vtkNew<vtkClipClosedSurface> clipperClosed;
		clipperClosed->SetInputData(meshPolyData);
		clipperClosed->SetClippingPlanes(createCuttingPlanes(trianglePoints, normal));
		clipperClosed->Update();
		if (clipperClosed->GetOutput()->GetPolys()->GetNumberOfCells() != 0)
		{
			//Pass to triangles
			vtkNew<vtkTriangleFilter> triangleFilter;
			triangleFilter->SetInputConnection(clipperClosed->GetOutputPort());
			triangleFilter->Update();
			//Test if we have holes
			vtkNew<vtkFeatureEdges> featureEdgesFilter;
			featureEdgesFilter->SetInputData(triangleFilter->GetOutput());
			featureEdgesFilter->ExtractAllEdgeTypesOff();
			featureEdgesFilter->BoundaryEdgesOn();
			featureEdgesFilter->Update();
			if (featureEdgesFilter->GetOutput()->GetPoints()->GetNumberOfPoints() != 0)
			{
				appendPolyData->SetInputDataByNumber(i, Utils::fillHoles(triangleFilter->GetOutput()));
			}
			else
			{
				appendPolyData->SetInputDataByNumber(i, triangleFilter->GetOutput());
			}
		}
		else
		{
			appendPolyData->SetInputDataByNumber(i, clipperClosed->GetOutput());
		}
		//DEBUG
		//for (int k = 0; k < planes->GetNumberOfItems(); k++)
		//{
		//	Draw::createPlane(CurrentRenderer, planes->GetItem(k)->GetOrigin(), planes->GetItem(k)->GetNormal(), 255, 0, 0);
		//}
	}
	appendPolyData->Update();
	auto result = appendPolyData->GetOutput();
	//
	if (result->GetPolys()->GetNumberOfCells() == 0)
	{
		wxMessageBox("Nothing was selected", "Error", wxICON_ERROR);
		return;
	}
	auto textActor = Draw::createText(CurrentRenderer, result->GetCenter(), "", 24, 1.0, 1.0, 1.0);
	if (polygonHeight > 0)
	{
		project->AddVolume(Draw::createPolyData(CurrentRenderer, result, 1.0, 1.0, 0.0), textActor);
	}
	else// We want the volume of a hole
	{
		auto volumeActor = Draw::createPolyData(CurrentRenderer,
			vtkPolyData::SafeDownCast(cuttingVolumeActor->GetMapper()->GetInput()), 1.0, 1.0, 0.0);
		volumeActor->GetProperty()->SetOpacity(0.3);
		project->AddVolume(volumeActor, textActor, result);
	}
}

void VolumeTool::createPlaneWidget()
{
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
		lineWidget = nullptr;
	}
	if (!propPicker)
	{
		propPicker = vtkSmartPointer<vtkPropPicker>::New();
		propPicker->PickFromListOn();
	}
	if (!planeWidget)
	{
		planeWidget = vtkSmartPointer<ImplicitPlaneWidget>::New();
	}
	planeWidget->SetInteractor(this->GetInteractor());
	planeWidget->GetPlaneProperty()->SetColor(1.0, 1.0, 0.0);
	planeWidget->GetSelectedPlaneProperty()->SetColor(0.0, 1.0, 0.0);
	planeWidget->GetSelectedPlaneProperty()->SetOpacity(1.0);
	planeWidget->TubingOff();
	planeWidget->OutlineTranslationOff();
	planeWidget->OriginTranslationOff();
	planeWidget->PickingManagedOff();
	planeWidget->SetPlaceFactor(1.25);
	planeWidget->SetDiagonalRatio(0.05);
	planeWidget->PlaceWidget(mesh->GetPolyData()->GetBounds());
	for (auto actor : mesh->actors)
	{
		propPicker->AddPickList(actor);
	}
}

void VolumeTool::createLineWidget()
{
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
	}
	lineWidget = vtkSmartPointer<LineWidget>::New();
	lineWidget->SetInteractor(this->Interactor);
	lineWidget->setCloseLoopOnFirstNode(true);
	if (planeActor)
	{
		vtkNew<LineWidgetRepresentation> rep;
		rep->addProp(planeActor);
		lineWidget->SetRepresentation(rep);
	}
	else
	{
		lineWidget->CreateRepresentationFromMesh(mesh);
	}
	lineWidget->EnabledOn();
	lineWidget->AddObserver(vtkCommand::StartInteractionEvent, this->EventCallbackCommand, this->Priority);
	lineWidget->AddObserver(vtkCommand::InteractionEvent, this->EventCallbackCommand, this->Priority);
	lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
}
