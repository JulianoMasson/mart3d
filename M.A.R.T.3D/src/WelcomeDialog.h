#pragma once
#include <wx\dialog.h>

class wxDataViewListCtrl;
class wxDataViewEvent;
class wxButton;

class WelcomeDialog : public wxDialog
{
public:
	WelcomeDialog(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = "S.M.A.R.T.3D",
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(700, 200), long style = wxDEFAULT_DIALOG_STYLE);
	~WelcomeDialog() {};

	std::string GetProjectPath() const { return projectPath; };

	void ShowNewProjectWizard();

private:
	DECLARE_EVENT_TABLE()

	wxArrayString projectList;
	std::string projectPath;

	wxDataViewListCtrl* dataView;
	wxButton* btCreateNewProject;
	wxButton* btLoadProject;
	wxButton* btImportProject;

	void OnCreateNewProject(wxCommandEvent& WXUNUSED(event));
	void OnLoadProject(wxCommandEvent& WXUNUSED(event));
	void OnImportProject(wxCommandEvent& WXUNUSED(event));
	void OnDataViewItemActivated(wxDataViewEvent& WXUNUSED(event));

	enum {
		idDataView,
		idCreateNewProject,
		idLoadProject,
		idImportProject
	};
};