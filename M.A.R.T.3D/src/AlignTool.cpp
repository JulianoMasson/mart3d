#include "AlignTool.h"

#include <vtkObjectFactory.h>
#include <vtkPoints.h>
#include <vtkTransform.h>
#include <vtkRenderer.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkLandmarkTransform.h>

#include <wx/msgdlg.h>
#include "Utils.h"
#include "Project.h"
#include "Mesh.h"
#include "LineWidget.h"

vtkStandardNewMacro(AlignTool);

//----------------------------------------------------------------------------
AlignTool::AlignTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(AlignTool::ProcessEvents);
}

//----------------------------------------------------------------------------
AlignTool::~AlignTool()
{
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget = nullptr;
	}
	meshSource = nullptr;
	meshTarget = nullptr;
	projectSource = nullptr;
	projectTarget = nullptr;
	pointsSource = nullptr;
	pointsTarget = nullptr;
	transform = nullptr;
	treeMesh = nullptr;
}

//----------------------------------------------------------------------------
void AlignTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !meshSource || !meshTarget || !projectSource || !projectTarget) //already enabled, just return
		{
			return;
		}
		treeMesh->SetItemText(meshSource->GetTreeItem(), "Source - align tool");
		treeMesh->SetItemText(meshTarget->GetTreeItem(), "Target - align tool");
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		// listen for the following events
		this->Interactor->AddObserver(vtkCommand::KeyPressEvent,
			this->EventCallbackCommand, this->Priority);

		this->SetLineWidgetMesh(meshSource);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget = nullptr;
		}
		treeMesh->SetItemText(meshSource->GetTreeItem(), Utils::getFileName(meshSource->GetFilename()));
		treeMesh->SetItemText(meshTarget->GetTreeItem(), Utils::getFileName(meshTarget->GetFilename()));
		meshSource = nullptr;
		meshTarget = nullptr;
		projectSource = nullptr;
		projectTarget = nullptr;
		pointsSource = nullptr;
		pointsTarget = nullptr;
		transform = nullptr;
		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void AlignTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	AlignTool* self =
		reinterpret_cast<AlignTool *>(clientdata);

	//okay, let's do the right thing
	if (event == vtkCommand::KeyPressEvent)
	{
		const char key = self->Interactor->GetKeyCode();
		if (key == 'I')
		{
			self->pointsSource = nullptr;
			self->pointsTarget = nullptr;
			self->transform = nullptr;
			auto tempMesh = self->meshSource;
			self->meshSource = self->meshTarget;
			self->meshTarget = tempMesh;
			auto tempProject = self->projectSource;
			self->projectSource = self->projectTarget;
			self->projectTarget = tempProject;
			self->SetLineWidgetMesh(self->meshSource);
			self->treeMesh->SetItemText(self->meshSource->GetTreeItem(), "Source - align tool");
			self->treeMesh->SetItemText(self->meshTarget->GetTreeItem(), "Target - align tool");
		}
		else if (self->Interactor->GetControlKey())
		{
			if (key == 'Z')
			{
				if (self->transform)
				{
					self->transform->Inverse();
					self->projectSource->Transform(self->transform);
					self->pointsSource = nullptr;
					self->pointsTarget = nullptr;
					self->transform = nullptr;
					self->SetLineWidgetMesh(self->meshSource);
					self->Interactor->Render();
				}
			}
		}
	}
}

void AlignTool::SetLineWidgetMesh(Mesh * mesh)
{
	if (!mesh)
	{
		return;
	}
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget = nullptr;
	}
	lineWidget = vtkSmartPointer<LineWidget>::New();
	lineWidget->SetInteractor(this->Interactor);
	lineWidget->setCloseLoopOnFirstNode(true);
	lineWidget->CreateRepresentationFromMesh(mesh);
	lineWidget->EnabledOn();
}

void AlignTool::EnterKeyPressed()
{
	if (lineWidget)
	{
		if (!lineWidget->GetRepresentation()->isLoopClosed())
		{
			wxMessageBox("You need to close the loop", "Warning", wxICON_WARNING);
			return;
		}
		//Last point equal to the first
		const unsigned int numberOfPoints = lineWidget->GetRepresentation()->getPoints()->GetNumberOfPoints() - 1;
		vtkSmartPointer<vtkPoints> lineWidgetPoints = lineWidget->GetRepresentation()->getPoints();
		if (!pointsSource)
		{
			if (numberOfPoints < 4)
			{
				wxMessageBox("You need to select at least 4 points!", "Error", wxICON_ERROR);
				return;
			}
			pointsSource = vtkSmartPointer<vtkPoints>::New();
			for (size_t i = 0; i < numberOfPoints; i++)
			{
				pointsSource->InsertNextPoint(lineWidgetPoints->GetPoint(i));
			}
			this->SetLineWidgetMesh(meshTarget);
			wxMessageBox("Select " + std::to_string(numberOfPoints) + " points in the Target mesh/point cloud",
				"Information", wxICON_INFORMATION);
		}
		else
		{
			if (!pointsTarget)
			{
				if (numberOfPoints < pointsSource->GetNumberOfPoints() || numberOfPoints > pointsSource->GetNumberOfPoints())
				{
					wxMessageBox("You should select " + std::to_string(pointsSource->GetNumberOfPoints()) + " points!", "Error", wxICON_ERROR);
					if (lineWidget->GetRepresentation()->isLoopClosed())
					{
						SetLineWidgetMesh(meshTarget);
						this->Interactor->Render();
					}
					return;
				}
				pointsTarget = vtkSmartPointer<vtkPoints>::New();
				for (size_t i = 0; i < numberOfPoints; i++)
				{
					pointsTarget->InsertNextPoint(lineWidgetPoints->GetPoint(i));
				}
				if (lineWidget)
				{
					lineWidget->EnabledOff();
					lineWidget->RemoveObserver(this->EventCallbackCommand);
					lineWidget = nullptr;
				}
				// Do the matching
				wxBeginBusyCursor();
				vtkNew<vtkLandmarkTransform> landmarkTransform;
				landmarkTransform->SetSourceLandmarks(pointsSource);
				landmarkTransform->SetTargetLandmarks(pointsTarget);
				transform = vtkSmartPointer<vtkTransform>::New();
				transform->SetMatrix(landmarkTransform->GetMatrix());
				projectSource->Transform(transform);
				wxEndBusyCursor();
				//Show all the meshs
				treeMesh->CheckItem(meshSource->GetTreeItem());
				meshSource->SetVisibility(true);
				treeMesh->CheckItem(meshTarget->GetTreeItem());
				meshTarget->SetVisibility(true);
				this->Interactor->Render();
			}
		}
	}
}