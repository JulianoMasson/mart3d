#pragma once
#include <wx\dialog.h>

class wxSpinCtrlDouble;

class GetDoubleDialog : public wxDialog
{
public:
	GetDoubleDialog(const wxString& message = wxEmptyString, const wxString& prompt = wxEmptyString,
		const wxString& caption = wxEmptyString, const double value = 0.0, const double minValue = 0.0, const double maxValue = 100.0,
		wxWindow* parent = nullptr, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
	~GetDoubleDialog() {};

	double GetValue();

private:
	DECLARE_EVENT_TABLE()

	void OnOK(wxCommandEvent& WXUNUSED(event));

	wxSpinCtrlDouble* spinDouble;
};