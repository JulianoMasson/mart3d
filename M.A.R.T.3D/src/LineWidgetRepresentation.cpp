#include "LineWidgetRepresentation.h"

#include <windows.h>
#include <gl\GL.h>

#include <vtkObjectFactory.h>
#include <vtkCellLocator.h>
#include <vtkPoints.h>
#include <vtkAbstractPicker.h>
#include <vtkPropPicker.h>
#include <vtkCellPicker.h>
#include <vtkPointPicker.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkActor2D.h>
#include <vtkProperty2D.h>
#include <vtkLineSource.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataMapper2D.h>
#include <vtkInteractorObserver.h>
#include <vtkTransform.h>
#include <vtkPolyLine.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkWindow.h>
#include <vtkCamera.h>

#include "Utils.h"

vtkStandardNewMacro(LineWidgetRepresentation);

LineWidgetRepresentation::LineWidgetRepresentation()
{
	this->InteractionState = LineWidgetRepresentation::Outside;
	propPicker = vtkSmartPointer<vtkPropPicker>::New();
	propPicker->PickFromListOn();
	pointPicker = vtkSmartPointer<vtkPointPicker>::New();
	pointPicker->PickFromListOn();
}

LineWidgetRepresentation::~LineWidgetRepresentation()
{
	this->InteractionState = LineWidgetRepresentation::Outside;
	indexActiveNode = -1;
	nodesActors.clear();
	nodesTransformFilter.clear();
	nodePoints = nullptr;
	lineActor = nullptr;
	lineActor2D = nullptr;
	lineSource = nullptr;
}

bool LineWidgetRepresentation::isOverFirstNode(int x, int y)
{
	if (pickNode(x, y))
	{
		return indexActiveNode == 0;
	}
	return false;
}

void LineWidgetRepresentation::closeLoop()
{
	nodePoints->SetPoint(nodePoints->GetNumberOfPoints() - 1, nodePoints->GetPoint(0));
	createNode(nodePoints->GetPoint(0));
	loopClosed = true;
}

bool LineWidgetRepresentation::isLoopClosed()
{
	return loopClosed;
}

int LineWidgetRepresentation::getNumberOfNodes() const
{
	return static_cast<int>(nodesActors.size());
}

void LineWidgetRepresentation::set2DRepresentation(bool is2D)
{
	this->representation2D = is2D;
}

void LineWidgetRepresentation::addProp(vtkSmartPointer<vtkProp> prop)
{
	if (!propPicker)
	{
		propPicker = vtkSmartPointer<vtkPropPicker>::New();
		propPicker->PickFromListOn();
	}
	propPicker->AddPickList(prop);
	if (!pointPicker)
	{
		pointPicker = vtkSmartPointer<vtkPointPicker>::New();
		pointPicker->PickFromListOn();
	}
	pointPicker->AddPickList(prop);
}

void LineWidgetRepresentation::createNode(const double position[3])
{
	vtkNew<vtkSphereSource> sphereSource;
	sphereSource->SetRadius(0.1);
	vtkNew<vtkTransform> T;
	T->Identity();
	vtkNew<vtkTransformPolyDataFilter> transformFilter;
	transformFilter->SetTransform(T);
	transformFilter->SetInputConnection(sphereSource->GetOutputPort());
	transformFilter->Update();
	nodesTransformFilter.emplace_back(transformFilter);
	//Create a mapper and actor
	vtkNew<vtkPolyDataMapper> mapper;
	mapper->SetInputConnection(transformFilter->GetOutputPort());
	vtkNew<vtkActor> actorSphere;
	actorSphere->SetMapper(mapper);
	actorSphere->GetProperty()->SetColor(sphereColor);
	actorSphere->SetPosition(position[0], position[1], position[2]);
	nodesActors.emplace_back(actorSphere);
}

void LineWidgetRepresentation::addNode(int x, int y)
{
	double pickedPos[3];
	if (pickFinalPosition(x, y, pickedPos))
	{
		if (!nodePoints)
		{
			nodePoints = vtkSmartPointer<vtkPoints>::New();
			nodePoints->InsertNextPoint(pickedPos);
		}
		else
		{
			nodePoints->SetPoint(nodePoints->GetNumberOfPoints() - 1, pickedPos);
		}
		nodePoints->InsertNextPoint(pickedPos);
		createNode(pickedPos);
	}
}

bool LineWidgetRepresentation::addFinalNode(int x, int y)
{
	double pickedPos[3];
	if (pickFinalPosition(x, y, pickedPos))
	{
		nodePoints->SetPoint(nodePoints->GetNumberOfPoints() - 1, pickedPos);
		createNode(pickedPos);
		return true;
	}
	return false;
}

void LineWidgetRepresentation::deleteNode(int id)
{
	if (nodesActors.size() == 0)
	{
		return;
	}
	this->InteractionState = LineWidgetRepresentation::Outside;
	indexActiveNode = -1;
	if (nodesActors.size() == 1)
	{
		nodesActors.clear();
		nodesTransformFilter.clear();
		nodePoints = nullptr;
		lineActor = nullptr;
		lineActor2D = nullptr;
		lineSource = nullptr;
	}
	else if (nodesActors.size() < nodePoints->GetNumberOfPoints())//We are still adding points, remove the last point
	{
		nodesActors.pop_back();
		nodesTransformFilter.pop_back();
		Utils::deletePoint(nodePoints, nodePoints->GetNumberOfPoints() - 1);
	}
	else if (loopClosed)
	{
		if (id == 0 || id == nodesActors.size() - 1)
		{
			nodesActors.pop_back();
			nodesTransformFilter.pop_back();
			Utils::deletePoint(nodePoints, nodePoints->GetNumberOfPoints() - 1);
			nodePoints->InsertNextPoint(nodesActors.back()->GetPosition());
			loopClosed = false;
		}
		else
		{
			nodesActors.erase(nodesActors.begin() + id);
			nodesTransformFilter.erase(nodesTransformFilter.begin() + id);
			Utils::deletePoint(nodePoints, id);
		}
		if (loopClosed && nodesActors.size() == 3)//we just have a line, destroy the last.
		{
			nodesActors.pop_back();
			nodesTransformFilter.pop_back();
			Utils::deletePoint(nodePoints, nodePoints->GetNumberOfPoints() - 1);
			nodePoints->InsertNextPoint(nodesActors.back()->GetPosition());
			loopClosed = false;
		}
	}
	else
	{
		nodesActors.erase(nodesActors.begin() + id);
		nodesTransformFilter.erase(nodesTransformFilter.begin() + id);
		Utils::deletePoint(nodePoints, id);
		//Add a point because the user need to add more points to match the maxNumberOfPoints
		nodePoints->InsertNextPoint(nodesActors.back()->GetPosition());
	}
}

void LineWidgetRepresentation::removeLastNode()
{
	deleteNode(-1);
}

void LineWidgetRepresentation::removeActiveNode()
{
	if (indexActiveNode != -1)
	{
		deleteNode(indexActiveNode);
	}
}

void LineWidgetRepresentation::resetPickers()
{
	if (!propPicker)
	{
		propPicker = vtkSmartPointer<vtkPropPicker>::New();
	}
	propPicker->PickFromListOn();
	propPicker->InitializePickList();
	if (!pointPicker)
	{
		pointPicker = vtkSmartPointer<vtkPointPicker>::New();
	}
	pointPicker->PickFromListOn();
	pointPicker->InitializePickList();
}

void LineWidgetRepresentation::reset()
{
	this->InteractionState = LineWidgetRepresentation::Outside;
	indexActiveNode = -1;
	nodesActors.clear();
	nodesTransformFilter.clear();
	nodePoints = nullptr;
	lineActor = nullptr;
	lineActor2D = nullptr;
	lineSource = nullptr;
	loopClosed = false;
}

vtkSmartPointer<vtkPoints> LineWidgetRepresentation::getPoints()
{
	return nodePoints;
}

void LineWidgetRepresentation::updateActiveNode(int x, int y)
{
	if (indexActiveNode == -1)
	{
		return;
	}
	double pickedPos[3];
	if (pickPosition(propPicker, x, y, pickedPos))
	{
		if (loopClosed && (indexActiveNode == 0 || indexActiveNode == nodesActors.size() - 1))
		{
			nodesActors.at(0)->SetPosition(pickedPos);
			nodesActors.at(nodesActors.size() - 1)->SetPosition(pickedPos);
			nodePoints->SetPoint(0, pickedPos);
			nodePoints->SetPoint(nodesActors.size() - 1, pickedPos);
		}
		else
		{
			nodesActors.at(indexActiveNode)->SetPosition(pickedPos);
			nodePoints->SetPoint(indexActiveNode, pickedPos);
		}
		this->BuildRepresentation();
	}
}

void LineWidgetRepresentation::finishUpdateActiveNode(int x, int y)
{
	if (indexActiveNode == -1)
	{
		return;
	}
	double pickedPos[3];
	if (pickFinalPosition(x, y, pickedPos))
	{
		nodesActors.at(indexActiveNode)->SetPosition(pickedPos);
		nodePoints->SetPoint(indexActiveNode, pickedPos);
		this->BuildRepresentation();
		indexActiveNode = -1;
		this->InteractionState = LineWidgetRepresentation::Outside;
	}
}

void LineWidgetRepresentation::updateLine(int x, int y)
{
	if (lineActor || lineActor2D)
	{
		double pickedPos[3];
		if (pickPosition(propPicker, x, y, pickedPos))
		{
			nodePoints->SetPoint(nodePoints->GetNumberOfPoints() - 1, pickedPos);
		}
	}
}

bool LineWidgetRepresentation::pickNode(int x, int y)
{
	for (int i = 0; i < nodesActors.size(); i++)
	{
		double p[3];
		getDisplayPosition(nodesActors.at(i)->GetPosition(), p);
		if (x < p[0] + pixelTolerance && x > p[0] - pixelTolerance &&
			y < p[1] + pixelTolerance && y > p[1] - pixelTolerance)
		{
			indexActiveNode = i;
			return true;
		}
	}
	indexActiveNode = -1;
	return false;
}

bool LineWidgetRepresentation::pickPosition(vtkSmartPointer<vtkAbstractPicker> picker, int x, int y, double point[3])
{
	if (representation2D)
	{
		double worldPoint[4];
		vtkInteractorObserver::ComputeDisplayToWorld(Renderer, x, y, 1, worldPoint);//1 to avoid giant nodes
		point[0] = worldPoint[0];
		point[1] = worldPoint[1];
		point[2] = worldPoint[2];
		return true;
	}
	if (picker->Pick(x, y, 0, Renderer))
	{
		picker->GetPickPosition(point);
		return true;
	}
	return false;
}

bool LineWidgetRepresentation::pickFinalPosition(int x, int y, double point[3])
{
	if (isPointCloud)
	{
		return pickPosition(pointPicker, x, y, point);
	}
	return pickPosition(propPicker, x, y, point);
}

void LineWidgetRepresentation::StartWidgetInteraction(double eventPos[2])
{
	this->StartEventPosition[0] = eventPos[0];
	this->StartEventPosition[1] = eventPos[1];
}

void LineWidgetRepresentation::WidgetInteraction(double eventPos[2])
{
	this->Modified();
	this->BuildRepresentation();
}

int LineWidgetRepresentation::ComputeInteractionState(int X, int Y, int vtkNotUsed(modify))
{
	if (X != 0 && Y != 0)
	{
		if (pickNode(X, Y))
		{
			this->InteractionState = LineWidgetRepresentation::OverNode;
			return this->InteractionState;
		}
	}
	this->InteractionState = LineWidgetRepresentation::Outside;
	return this->InteractionState;
}

vtkMTimeType LineWidgetRepresentation::GetMTime()
{
	vtkMTimeType mTime = this->Superclass::GetMTime();
	if (nodesTransformFilter.size() != 0)
	{
		mTime = std::max(mTime, this->nodesTransformFilter.at(0)->GetMTime());
	}
	return mTime;
}

void LineWidgetRepresentation::BuildRepresentation()
{
	if (this->Renderer && (this->GetMTime() > this->BuildTime || (this->Renderer->GetVTKWindow() && this->Renderer->GetVTKWindow()->GetMTime() > this->BuildTime)))
	{
		if (drawLine)
		{
			if (representation2D)
			{
				if (lineActor2D)
				{
					this->Renderer->RemoveActor2D(lineActor2D);
					lineActor2D = nullptr;
				}
				if (nodePoints)
				{
					vtkNew<vtkPolyLine> polyLine;
					polyLine->GetPointIds()->SetNumberOfIds(nodePoints->GetNumberOfPoints());
					for (unsigned int i = 0; i < nodePoints->GetNumberOfPoints(); i++)
					{
						polyLine->GetPointIds()->SetId(i, i);
					}
					// Create a cell array to store the lines in and add the lines to it
					vtkNew<vtkCellArray> cells;
					cells->InsertNextCell(polyLine);

					// Create a polydata to store everything in
					vtkNew<vtkPolyData> polyData2D;
					polyData2D->Initialize();
					polyData2D->SetPoints(nodePoints);
					polyData2D->SetLines(cells);
					polyData2D->Modified();
					vtkNew<vtkCoordinate> coordinateLine2D;
					coordinateLine2D->SetCoordinateSystemToWorld();
					vtkNew<vtkPolyDataMapper2D> mapperLine2D;
					mapperLine2D->SetInputData(polyData2D);
					mapperLine2D->SetTransformCoordinate(coordinateLine2D);
					mapperLine2D->ScalarVisibilityOn();
					mapperLine2D->SetScalarModeToUsePointData();
					mapperLine2D->Update();
					lineActor2D = vtkSmartPointer<vtkActor2D>::New();
					lineActor2D->SetMapper(mapperLine2D);
					lineActor2D->GetProperty()->SetLineWidth(3);
					lineActor2D->GetProperty()->SetColor(255, 0, 0);
				}
			}
			else
			{
				if (!lineActor && nodePoints)
				{
					lineSource = vtkSmartPointer<vtkLineSource>::New();
					lineSource->SetPoints(nodePoints);
					//Create a mapper and actor
					vtkNew<vtkPolyDataMapper> mapperLine;
					mapperLine->SetInputConnection(lineSource->GetOutputPort());
					lineActor = vtkSmartPointer<vtkActor>::New();
					lineActor->GetProperty()->SetColor(255, 0, 0);
					lineActor->GetProperty()->SetLineWidth(3);
					lineActor->SetMapper(mapperLine);
				}
				else if (lineActor)
				{
					lineSource->Modified();
				}
			}
		}

		double p1[4], p2[4];
		this->Renderer->GetActiveCamera()->GetFocalPoint(p1);
		p1[3] = 1.0;
		this->Renderer->SetWorldPoint(p1);
		this->Renderer->WorldToView();
		this->Renderer->GetViewPoint(p1);

		double depth = p1[2];
		double aspect[2];
		this->Renderer->ComputeAspect();
		this->Renderer->GetAspect(aspect);

		p1[0] = -aspect[0];
		p1[1] = -aspect[1];
		this->Renderer->SetViewPoint(p1);
		this->Renderer->ViewToWorld();
		this->Renderer->GetWorldPoint(p1);

		p2[0] = aspect[0];
		p2[1] = aspect[1];
		p2[2] = depth;
		p2[3] = 1.0;
		this->Renderer->SetViewPoint(p2);
		this->Renderer->ViewToWorld();
		this->Renderer->GetWorldPoint(p2);

		double distance = std::sqrt(vtkMath::Distance2BetweenPoints(p1, p2));

		int size[2];
		size[0] = this->Renderer->GetRenderWindow()->GetSize()[0];
		size[1] = this->Renderer->GetRenderWindow()->GetSize()[1];
		double viewport[4];
		this->Renderer->GetViewport(viewport);

		double x, y, scale;

		x = size[0] * (viewport[2] - viewport[0]);
		y = size[1] * (viewport[3] - viewport[1]);

		scale = std::sqrt(x * x + y * y);


		distance = 1000 * distance / scale;
		scale = distance * this->HandleSize;
		vtkNew<vtkTransform> T;
		T->Scale(scale, scale, scale);
		for (int i = 0; i < nodesTransformFilter.size(); i++)
		{
			if (i == indexActiveNode)
			{
				vtkNew<vtkTransform> T2;
				T2->Scale(4 * scale, 4 * scale, 4 * scale);
				nodesTransformFilter.at(i)->SetTransform(T2);
				nodesTransformFilter.at(i)->Update();
			}
			else
			{
				nodesTransformFilter.at(i)->SetTransform(T);
				nodesTransformFilter.at(i)->Update();
			}
		}

		// Set things up
		this->BuildTime.Modified();
	}
}

void LineWidgetRepresentation::getDisplayPosition(const double pointIn[3], double pointOut[3])
{
	vtkInteractorObserver::ComputeWorldToDisplay(this->Renderer, pointIn[0], pointIn[1], pointIn[2], pointOut);
}

void LineWidgetRepresentation::GetActors(vtkPropCollection *pc)
{
	if (lineActor)
	{
		lineActor->GetActors(pc);
	}
	for (auto nodeActor : nodesActors)
	{
		nodeActor->GetActors(pc);
	}
}

void LineWidgetRepresentation::GetActors2D(vtkPropCollection *pc)
{
	if (lineActor2D)
	{
		lineActor2D->GetActors(pc);
	}
}

void LineWidgetRepresentation::ReleaseGraphicsResources(vtkWindow *w)
{
	if (lineActor)
	{
		lineActor->ReleaseGraphicsResources(w);
	}
	if (lineActor2D)
	{
		lineActor2D->ReleaseGraphicsResources(w);
	}
	for (auto nodeActor : nodesActors)
	{
		nodeActor->ReleaseGraphicsResources(w);
	}
}

int LineWidgetRepresentation::RenderOverlay(vtkViewport *w)
{
	this->BuildRepresentation();
	int count = 0;
	if (lineActor)
	{
		count += lineActor->RenderOverlay(w);
	}
	if (lineActor2D)
	{
		count += lineActor2D->RenderOverlay(w);
	}
	for (auto nodeActor : nodesActors)
	{
		count += nodeActor->RenderOverlay(w);
	}
	return count;
}

int LineWidgetRepresentation::RenderOpaqueGeometry(vtkViewport *w)
{
	this->BuildRepresentation();

	if (lineActor && alwaysOnTop)
	{
		GLboolean flag = GL_FALSE;
		if (lineActor->GetVisibility())
		{
			glGetBooleanv(GL_DEPTH_TEST, &flag);
			if (flag)
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
	}

	int count = 0;
	if (lineActor)
	{
		count += lineActor->RenderOpaqueGeometry(w);
	}
	if (lineActor2D)
	{
		count += lineActor2D->RenderOpaqueGeometry(w);
	}
	for (auto nodeActor : nodesActors)
	{
		count += nodeActor->RenderOpaqueGeometry(w);
	}
	if (lineActor && alwaysOnTop)
	{
		if (lineActor->GetVisibility() && alwaysOnTop)
		{
			glEnable(GL_DEPTH_TEST);
		}
	}
	return count;
}

int LineWidgetRepresentation::RenderTranslucentPolygonalGeometry(vtkViewport *w)
{
	this->BuildRepresentation();
	int count = 0;
	if (lineActor)
	{
		count += lineActor->RenderTranslucentPolygonalGeometry(w);
	}
	if (lineActor2D)
	{
		count += lineActor2D->RenderTranslucentPolygonalGeometry(w);
	}
	for (auto nodeActor : nodesActors)
	{
		count += nodeActor->RenderTranslucentPolygonalGeometry(w);
	}
	return count;
}

// Description:
// Does this prop have some translucent polygonal geometry?
int LineWidgetRepresentation::HasTranslucentPolygonalGeometry()
{
	this->BuildRepresentation();
	int count = 0;
	if (lineActor)
	{
		count += lineActor->HasTranslucentPolygonalGeometry();
	}
	if (lineActor2D)
	{
		count += lineActor2D->HasTranslucentPolygonalGeometry();
	}
	for (auto nodeActor : nodesActors)
	{
		count += nodeActor->HasTranslucentPolygonalGeometry();
	}
	return count;
}

void LineWidgetRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}