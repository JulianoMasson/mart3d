#pragma once

#include <vector>
#include <map>

#include <vtkSmartPointer.h>

#include <wx/treelist.h>

class vtkRenderer;
class vtkActor;
class vtkCaptionActor2D;
class vtkPolyData;
class vtkTransform;

class Mesh;
class Camera;
class Calibration;
class Volume;

class Project
{
public:
	Project(vtkSmartPointer<vtkRenderer> renderer, vtkSmartPointer<vtkRenderer> renderer360, wxTreeListCtrl* tree, const std::string& filePath);
	~Project();

	std::vector<Mesh*> meshes;
	std::vector<Camera*> cameras;
	std::vector<Volume*> volumes;

	std::string GetName() const;
	std::string GetPath() const;

	void SetModified(bool modified);
	bool GetModified() const;

	bool GetIsLoaded() const { return isLoaded; };
	bool GetIs360() const { return is360; };

	void SetVisibility(bool visibility);
	bool GetVisibility() const { return visibility; };

	void AddMesh(Mesh* mesh);
	void AddMesh(std::string& filename, vtkSmartPointer<vtkPolyData> polyData);
	void ImportMesh(const std::string& filePath);
	void DeleteMesh(int meshIndex);
	void Transform(vtkSmartPointer<vtkTransform> transform);

	void Save(const std::string& newFilePath = "");

	void SetRepresentation(int representation);

	// Calibration
	void SetCalibration(Calibration* calibration);
	Calibration* GetCalibration() { return calibration; };

	// Cameras
	void DestructCameras();
	void DeleteCamera(int cameraIndex);
	void SetCamerasVisibility(bool visibility);
	bool GetCamerasVisibility() const;

	// Volume
	void AddVolume(vtkSmartPointer<vtkActor> actor, vtkSmartPointer<vtkCaptionActor2D> textActor,
		vtkSmartPointer<vtkPolyData> holePolyData = nullptr);
	void SetVolumesVisibility(bool visible);
	bool GetVolumesVisibility() const;
	void DestructVolumes();
	void DeleteVolume(int volumeIndex);

	//360 mode
	void Update360ClickPoints(Camera* newCam);

	// Tree
	void SetTreeItem(const wxTreeListItem& listItem) { treeItem = listItem; };
	wxTreeListItem GetTreeItem() const { return treeItem; };
	void SetTreeItemCameras(const wxTreeListItem& listItem) { treeItemCameras = listItem; };
	wxTreeListItem GetTreeItemCameras() const { return treeItemCameras; };
	void SetTreeItemVolumes(const wxTreeListItem& listItem) { treeItemVolumes = listItem; };
	wxTreeListItem GetTreeItemVolumes() const { return treeItemVolumes; };

private:
	vtkSmartPointer<vtkRenderer> renderer;
	// Renderer used to control the 360 click points
	vtkSmartPointer<vtkRenderer> renderer360;
	wxTreeListCtrl* tree;
	bool isLoaded;
	bool is360;
	std::string projectPath;
	std::string projectFolder;
	Calibration* calibration;
	bool visibility;
	// Used to know when we need to Save the project
	bool modified;
	// Loads
	void LoadMeshes();
	void LoadCameras();
	// Saves
	void SaveMeshes(bool pathModified);
	void SaveCameras(bool pathModified);

	// Keep track of the used mesh names
	std::map<std::string, unsigned int> meshNames;

	// Volume
	// Just to track how many volumes already passed through this mesh
	unsigned int qtdVolumes = 0;

	//Tree
	wxTreeListItem treeItem;
	wxTreeListItem treeItemCameras;
	wxTreeListItem treeItemVolumes;
	wxTreeListItem treeItemSurfaces;
};