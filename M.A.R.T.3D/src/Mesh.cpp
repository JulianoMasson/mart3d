#include "Mesh.h"

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkCellArray.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkMapper.h>
#include <vtkAppendPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkRenderer.h>
#include <vtkAbstractPicker.h>
#include <vtkTexture.h>

#include "Calibration.h"
#include "MeshIO.h"
#include "Draw.h"
#include "Utils.h"
#include "Camera.h"

void Mesh::CreateMesh()
{
	//Update internal poly data
	GetPolyData();
	//Test to see if there is point color
	if (polyData->GetPointData())
	{
		if (polyData->GetPointData()->GetScalars())
		{
			textureVisibility = true;
			hasColor = true;
		}
	}
	//Test to see if there is texture
	if (actors.front()->GetTexture())
	{
		//Texture names already loaded inside MeshIO::getActorsFromFile
		textureVisibility = true;
		hasTexture = true;
		for (auto actor : actors)
		{
			textures.push_back(actor->GetTexture());
			actor->GetProperty()->SetAmbient(false);
			actor->GetProperty()->SetLighting(false);
		}
	}
	//Test to see if it is a point cloud
	if (polyData->GetVerts())
	{
		if (polyData->GetVerts()->GetNumberOfCells() == 1 && textures.size() == 0)
		{
			isPointCloud = true;
		}
	}
	loaded = true;
	for (const auto& actor : actors)
	{
		renderer->AddActor(actor);
	}
	visible = true;
}

Mesh::Mesh(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree, const std::string& filePath) :
	renderer(renderer), tree(tree), treeItem(nullptr), treeItemTexture(nullptr), polyData(nullptr), filename(Utils::getFileName(filePath, false)),
	loaded(false), isPointCloud(false), hasColor(false), hasTexture(false), visible(false), textureVisibility(false), modified(false), onDisk(true)
{
	if (!MeshIO::getActorsFromFile(filePath, actors, textureNames))
	{
		actors.clear();
		return;
	}
	if (actors.size() > 0)
	{
		CreateMesh();
	}
}

Mesh::Mesh(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree, const std::string& filename, vtkSmartPointer<vtkPolyData> polyData) :
	renderer(renderer), tree(tree), treeItem(nullptr), treeItemTexture(nullptr), polyData(nullptr), filename(Utils::getFileName(filename, false)),
	loaded(false), isPointCloud(false), hasColor(false), hasTexture(false), visible(false), textureVisibility(false), modified(true), onDisk(false)
{
	auto actor = Draw::CreatePolyData(polyData);
	if (actor)
	{
		actors.emplace_back(actor);
		CreateMesh();
	}
}

Mesh::~Mesh()
{
	if (renderer)
	{
		for (auto actor : actors)
		{
			renderer->RemoveActor(actor);
		}
	}
	DestructTextures();
	if (treeItem && tree)
	{
		tree->DeleteItem(treeItem);
		treeItem = nullptr;
	}
	polyData = nullptr;
	renderer = nullptr;
	tree = nullptr;
	actors.clear();
	textureNames.clear();
	textures.clear();
}

void Mesh::DestructTextures()
{
	textures.clear();
	textureNames.clear();
	if (treeItemTexture && tree)
	{
		tree->DeleteItem(treeItemTexture);
		treeItemTexture = nullptr;
	}
}

void Mesh::SetFilename(const std::string & filename)
{
	this->filename = filename;
}

void Mesh::SetModified(bool modified)
{
	this->modified = modified;
	if (tree)
	{
		if (modified)
		{
			tree->SetItemText(GetTreeItem(), GetFilename() + "*");
		}
		else
		{
			tree->SetItemText(GetTreeItem(), GetFilename());
		}
	}
}

bool Mesh::Export(const std::string& filePath)
{
	return MeshIO::exportMesh(filePath, this);
}

bool Mesh::Save(const std::string& dirPath, bool forceSaving)
{
	if (forceSaving || modified)
	{
		SetModified(false);
		onDisk = true;
		if (hasTexture)
		{
			return Export(dirPath + "\\" + this->filename + ".obj");
		}
		else
		{
			return Export(dirPath + "\\" + this->filename + ".ply");
		}
	}
	return false;
}

void Mesh::CreatePickList(vtkSmartPointer<vtkAbstractPicker> picker)
{
	if (picker)
	{
		for (const auto &actor : actors)
		{
			picker->AddPickList(actor);
		}
		picker->PickFromListOn();
	}
}

void Mesh::SetVisibility(bool visibility)
{
	for (auto actor : actors)
	{
		actor->SetVisibility(visibility);
	}
	visible = visibility;
}

void Mesh::SetTextureVisibility(bool visible)
{
	//PointCloud/Mesh without texture
	if (textures.size() == 0)
	{
		for (auto actor : actors)
		{
			actor->GetMapper()->SetScalarVisibility(visible);
		}
		textureVisibility = visible;
		return;
	}
	//Mesh
	if (textures.size() != actors.size())
	{
		return;
	}
	if (visible)
	{
		for (int i = 0; i < actors.size(); i++)
		{
			actors.at(i)->SetTexture(textures.at(i));
			actors.at(i)->GetProperty()->SetInterpolationToPhong();
			actors.at(i)->GetProperty()->SetLighting(false);
		}
	}
	else
	{
		for (auto actor : actors)
		{
			actor->SetTexture(nullptr);
			actor->GetProperty()->SetInterpolationToFlat();
			actor->GetProperty()->SetAmbient(0);
			actor->GetProperty()->SetLighting(true);
		}
	}
	textureVisibility = visible;
}

vtkSmartPointer<vtkPolyData> Mesh::GetPolyData()
{
	if (actors.size() == 0)
	{
		return nullptr;
	}
	if (polyData)
	{
		return polyData;
	}
	if (actors.size() == 1)
	{
		polyData = vtkPolyData::SafeDownCast(actors[0]->GetMapper()->GetInput());
	}
	else//We need to put all the actors in the same PolyData
	{
		vtkNew<vtkAppendPolyData> appendFilter;
		for (const auto &actor : actors)
		{
			appendFilter->AddInputData(vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput()));
		}
		appendFilter->Update();
		polyData = appendFilter->GetOutput();
		if (!polyData)
		{
			return nullptr;
		}
		vtkNew<vtkCleanPolyData> cleaner;
		cleaner->SetInputData(polyData);
		cleaner->Update();
		polyData = cleaner->GetOutput();
	}
	vtkNew<vtkTransformFilter> transformFilter;
	vtkNew<vtkTransform> T;
	vtkSmartPointer<vtkActor> firstActor = actors[0];
	if (firstActor->GetUserMatrix())
	{
		T->SetMatrix(firstActor->GetUserMatrix());
	}
	else if (firstActor->GetMatrix())
	{
		T->SetMatrix(firstActor->GetMatrix());
	}
	else
	{
		return polyData;
	}
	transformFilter->SetTransform(T);
	transformFilter->SetInputData(polyData);
	transformFilter->Update();
	polyData = transformFilter->GetPolyDataOutput();
	return polyData;
}

void Mesh::Transform(vtkSmartPointer<vtkTransform> T)
{
	SetModified(true);
	polyData = nullptr;
	vtkNew<vtkTransform> T3;
	for (auto actor : actors)
	{
		vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
		vtkNew<vtkTransformFilter> transformFilter;
		vtkSmartPointer<vtkMatrix4x4> lasT;
		if (actor->GetUserMatrix())
		{
			lasT = actors.at(0)->GetUserMatrix();
		}
		else if (actor->GetMatrix())
		{
			lasT = actor->GetMatrix();
		}
		vtkNew<vtkMatrix4x4> resultT;
		vtkMatrix4x4::Multiply4x4(lasT, T->GetMatrix(), resultT);

		T3->SetMatrix(resultT);

		transformFilter->SetTransform(T3);
		transformFilter->SetInputData(polydata);
		transformFilter->Update();

		actor->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
		actor->GetMapper()->Modified();
	}
}

void Mesh::UpdateData(vtkDataObject * data)
{
	UpdateData(data, 0);
}

void Mesh::UpdateData(vtkDataObject * data, unsigned int actorIndex)
{
	if (!data)
	{
		return;
	}
	if (actorIndex >= actors.size())
	{
		return;
	}
	SetModified(true);
	polyData = nullptr;
	auto actor = actors[actorIndex];

	vtkNew<vtkTransformFilter> transformFilter;
	vtkNew<vtkTransform> T;
	if (actor->GetUserMatrix())
	{
		T->SetMatrix(actor->GetUserMatrix());
	}
	else if (actor->GetMatrix())
	{
		T->SetMatrix(actor->GetMatrix());
	}
	T->Inverse();
	transformFilter->SetTransform(T);
	transformFilter->SetInputData(data);
	transformFilter->Update();

	actor->GetMapper()->SetInputDataObject(transformFilter->GetOutput());
	actor->GetMapper()->Modified();
}