#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

class vtkPolyData;
class vtkElevationFilter;
class vtkActor;
class vtkSliderWidget;
class vtkPropPicker;
class vtkPoints;
class vtkXYPlotActor;
class wxTreeListCtrl;
class vtkCaptionActor2D;
class LineWidget;
class Calibration;
class Mesh;

class ElevationTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static ElevationTool *New();

	vtkTypeMacro(ElevationTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}

	//@{
	/**
	* Set the mesh that is going to be used
	*/
	void setMesh(Mesh* mesh);
	//@}

	//@{
	/**
	* Set the calibration that is going to be used
	*/
	void setCalibration(Calibration* calibration) { this->calibration = calibration; };
	//@}

	//@{
	/**
	* Set the mesh tree
	*/
	void setMeshTree(wxTreeListCtrl* treeMesh) { this->treeMesh = treeMesh; };
	//@}

protected:
	ElevationTool();
	~ElevationTool();

	//Delete what is necessary to properly disable/kill this class
	void destruct();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	// ProcessEvents() dispatches to these methods.
	void OnMiddleButtonDown();
	void OnKeyPressed();

	//Actors
	vtkSmartPointer<vtkCaptionActor2D> textActor = nullptr;

	//ElevationMap
	vtkSmartPointer<vtkPolyData> outputElevationFilter = nullptr;
	vtkSmartPointer<vtkElevationFilter> elevationFilter = nullptr;
	vtkSmartPointer<vtkActor> elevationMapActor = nullptr;

	//Slider
	vtkSmartPointer<vtkSliderWidget> sliderWidget = nullptr;
	void createSlider();

	void updateElevationMap();
	double getPointElevation(double point[3]);


	// Do the picking
	vtkSmartPointer<vtkPropPicker> propPicker = nullptr;
	Mesh* mesh = nullptr;
	Calibration* calibration = nullptr;

	//Set the zero by clicking in the mesh
	bool setZeroByMouse = false;

	double bounds[6];

	int getMousePosition(double point[3]);

	//ElevationProfile
	vtkSmartPointer<LineWidget> lineWidget = nullptr;
	vtkSmartPointer<vtkActor> elevationProfileActor = nullptr;
	void createElevationProfileLine();
	//Plot
	vtkSmartPointer<vtkXYPlotActor> plotElevation = nullptr;
	void createPlot(vtkSmartPointer<vtkPoints> points, double point0Line[3], double point1Line[3]);

	//Avoid updating the elevation map if there was no change in the slider
	double lastSliderValue = VTK_DOUBLE_MAX;

	wxTreeListCtrl * treeMesh = nullptr;

private:
	ElevationTool(const ElevationTool&) VTK_DELETE_FUNCTION;
	void operator=(const ElevationTool&) VTK_DELETE_FUNCTION;
};

