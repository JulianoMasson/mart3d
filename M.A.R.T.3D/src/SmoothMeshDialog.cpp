#include "SmoothMeshDialog.h"

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkPolyData.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkActor.h>
#include <vtkMapper.h>
#include <vtkPointData.h>

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>
#include <wx/button.h>
#include <wx/checkbox.h>

#include "Mesh.h"
#include "Draw.h"

wxBEGIN_EVENT_TABLE(SmoothMeshDialog, wxDialog)
	EVT_CHECKBOX(idUpdate, SmoothMeshDialog::OnCheckBoxUpdate)
	EVT_SPIN(idUpdate, SmoothMeshDialog::OnSpinCtrlUpdate)
	EVT_BUTTON(wxID_OK, SmoothMeshDialog::OnOK)
	EVT_BUTTON(wxID_CANCEL, SmoothMeshDialog::OnCancel)
	EVT_CLOSE(SmoothMeshDialog::OnClose)
wxEND_EVENT_TABLE()

SmoothMeshDialog::SmoothMeshDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos,
	const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxBoxSizer* bSizer = new wxBoxSizer(wxVERTICAL);

	vtkNew<vtkWindowedSincPolyDataFilter> polyDataFilter;

	wxFlexGridSizer* fgSizer = new wxFlexGridSizer(10, 2, 0, 0);
	fgSizer->SetFlexibleDirection(wxBOTH);
	fgSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Maximum number of iterations"), 0, wxALL, 5);
	spinMaxNumberOfIterations = new wxSpinCtrl(this, idUpdate, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, polyDataFilter->GetNumberOfIterationsMinValue(),
		polyDataFilter->GetNumberOfIterationsMaxValue(), polyDataFilter->GetNumberOfIterations());
	fgSizer->Add(spinMaxNumberOfIterations, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Pass band"), 0, wxALL, 5);
	spinPassBand = new wxSpinCtrlDouble(this, idUpdate, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, polyDataFilter->GetPassBandMinValue(), 
		polyDataFilter->GetPassBandMaxValue(), polyDataFilter->GetPassBand(), 0.1);
	fgSizer->Add(spinPassBand, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Feature angle"), 0, wxALL, 5);
	spinFeatureAngle = new wxSpinCtrlDouble(this, idUpdate, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, polyDataFilter->GetFeatureAngleMinValue(),
		polyDataFilter->GetFeatureAngleMaxValue(), polyDataFilter->GetFeatureAngle());
	fgSizer->Add(spinFeatureAngle, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Edge angle"), 0, wxALL, 5);
	spinEdgeAngle = new wxSpinCtrlDouble(this, idUpdate, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, polyDataFilter->GetEdgeAngleMinValue(),
		polyDataFilter->GetEdgeAngleMaxValue(), polyDataFilter->GetEdgeAngle());
	fgSizer->Add(spinEdgeAngle, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Feature edge smoothing"), 0, wxALL, 5);
	ckFeatureEdgeSmoothing = new wxCheckBox(this, idUpdate, wxEmptyString);
	ckFeatureEdgeSmoothing->SetValue(polyDataFilter->GetFeatureEdgeSmoothing());
	fgSizer->Add(ckFeatureEdgeSmoothing, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Boundary smoothing"), 0, wxALL, 5);
	ckBoundarySmoothing = new wxCheckBox(this, idUpdate, wxEmptyString);
	ckBoundarySmoothing->SetValue(polyDataFilter->GetBoundarySmoothing());
	fgSizer->Add(ckBoundarySmoothing, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Non-manifold smoothing"), 0, wxALL, 5);
	ckNonManifoldSmoothing = new wxCheckBox(this, idUpdate, wxEmptyString);
	ckNonManifoldSmoothing->SetValue(polyDataFilter->GetNonManifoldSmoothing());
	fgSizer->Add(ckNonManifoldSmoothing, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Show errors"), 0, wxALL, 5);
	ckShowErrors = new wxCheckBox(this, idUpdate, wxEmptyString);
	ckShowErrors->SetValue(polyDataFilter->GetGenerateErrorScalars());
	fgSizer->Add(ckShowErrors, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Preview"), 0, wxALL, 5);
	ckPreview = new wxCheckBox(this, idUpdate, wxEmptyString);
	ckPreview->SetValue(false);
	fgSizer->Add(ckPreview, 0, wxALL, 5);
	
	bSizer->Add(fgSizer, 0, wxEXPAND, 5);

	auto bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL, 5);

	bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL, 5);

	bSizer->Add(bSizerBts, 0, wxALIGN_RIGHT, 5);

	this->SetSizer(bSizer);
	this->Layout();

	this->Centre(wxBOTH);

	spinMaxNumberOfIterations->Connect(wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler(SmoothMeshDialog::OnSpinCtrlUpdate), NULL, this);
	spinPassBand->Connect(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinEventHandler(SmoothMeshDialog::OnSpinCtrlUpdate), NULL, this);
	spinFeatureAngle->Connect(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinEventHandler(SmoothMeshDialog::OnSpinCtrlUpdate), NULL, this);
	spinEdgeAngle->Connect(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinEventHandler(SmoothMeshDialog::OnSpinCtrlUpdate), NULL, this);
}

void SmoothMeshDialog::update()
{
	if (ckPreview->IsChecked())
	{
		if (actor)
		{
			renderer->RemoveActor(actor);
		}
		actor = Draw::createPolyData(renderer, smoothMesh());
		renderer->GetRenderWindow()->Render();
	}
	else
	{
		if (actor)
		{
			renderer->RemoveActor(actor);
			actor = nullptr;
			renderer->GetRenderWindow()->Render();
		}
	}
}

vtkSmartPointer<vtkPolyData> SmoothMeshDialog::smoothMesh()
{
	wxBeginBusyCursor();
	vtkNew<vtkWindowedSincPolyDataFilter> smoothPolyData;
	smoothPolyData->NormalizeCoordinatesOn();
	//Input
	smoothPolyData->SetInputData(mesh->GetPolyData());
	//Parameters
	smoothPolyData->SetNumberOfIterations(
		spinMaxNumberOfIterations->GetValue());
	smoothPolyData->SetPassBand(
		spinPassBand->GetValue());
	smoothPolyData->SetFeatureAngle(
		spinFeatureAngle->GetValue());
	smoothPolyData->SetEdgeAngle(
		spinEdgeAngle->GetValue());
	smoothPolyData->SetFeatureEdgeSmoothing(
		ckFeatureEdgeSmoothing->GetValue());
	smoothPolyData->SetBoundarySmoothing(
		ckBoundarySmoothing->GetValue());
	smoothPolyData->SetNonManifoldSmoothing(
		ckNonManifoldSmoothing->GetValue());
	smoothPolyData->SetGenerateErrorScalars(
		ckShowErrors->GetValue());
	//Output
	smoothPolyData->Update();
	wxEndBusyCursor();
	return smoothPolyData->GetOutput();
}

void SmoothMeshDialog::OnOK(wxCommandEvent & WXUNUSED)
{
	ckShowErrors->SetValue(false);
	//Generate normals
	vtkNew<vtkPolyDataNormals> normalGenerator;
	normalGenerator->SetInputData(smoothMesh());
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->ComputeCellNormalsOn();
	normalGenerator->Update();
	mesh->UpdateData(normalGenerator->GetOutput());
	this->OnClose((wxCloseEvent)NULL);
}

void SmoothMeshDialog::OnCancel(wxCommandEvent & WXUNUSED)
{
	this->OnClose((wxCloseEvent)NULL);
}

void SmoothMeshDialog::OnClose(wxCloseEvent & WXUNUSED)
{
	mesh = nullptr;
	if (actor)
	{
		renderer->RemoveActor(actor);
		actor = nullptr;
		renderer->GetRenderWindow()->Render();
	}
	ckPreview->SetValue(false);
	this->Hide();
}

void SmoothMeshDialog::OnCheckBoxUpdate(wxCommandEvent & WXUNUSED)
{
	update();
}

void SmoothMeshDialog::OnSpinCtrlUpdate(wxSpinEvent & WXUNUSED)
{
	update();
}