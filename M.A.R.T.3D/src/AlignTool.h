#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

class Project;
class Mesh;
class LineWidget;
class vtkPoints;
class vtkTransform;
class wxTreeListCtrl;

class AlignTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static AlignTool *New();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	vtkTypeMacro(AlignTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}

	//@{
	/**
	* Set the data that is going to be used as source
	*/
	void SetSourceData(Project* project, Mesh* mesh)
	{
		projectSource = project;
		meshSource = mesh;
	};
	//@}

	//@{
	/**
	* Set the data that is going to be used as target
	*/
	void SetTargetData(Project* project, Mesh* mesh)
	{
		projectTarget = project;
		meshTarget = mesh;
	};
	//@}

	void SetTree(wxTreeListCtrl* treeMesh) { this->treeMesh = treeMesh; };

	void EnterKeyPressed();

	bool HasFinished() const { return (transform != nullptr); };

	vtkSmartPointer<vtkTransform> GetTransform() const { return transform; };

protected:
	AlignTool();
	~AlignTool();

	// set wich mesh will be used to do the picking
	void SetLineWidgetMesh(Mesh* mesh);

	vtkSmartPointer<LineWidget> lineWidget = nullptr;

	Mesh* meshSource = nullptr;
	Mesh* meshTarget = nullptr;
	Project* projectSource = nullptr;
	Project* projectTarget = nullptr;
	wxTreeListCtrl* treeMesh = nullptr;

	vtkSmartPointer<vtkPoints> pointsSource = nullptr;
	vtkSmartPointer<vtkPoints> pointsTarget = nullptr;
	vtkSmartPointer<vtkTransform> transform = nullptr;

private:
	AlignTool(const AlignTool&) VTK_DELETE_FUNCTION;
	void operator=(const AlignTool&) VTK_DELETE_FUNCTION;
};
