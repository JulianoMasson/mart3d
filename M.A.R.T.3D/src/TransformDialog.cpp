#include "TransformDialog.h"

#include <vtkTransform.h>

#include <wx\sizer.h>
#include <wx\stattext.h>
#include <wx\spinctrl.h>
#include <wx\button.h>
#include <wx\msgdlg.h>
#include <wx\filedlg.h>

#include "Utils.h"

wxBEGIN_EVENT_TABLE(TransformDialog, wxDialog)
EVT_BUTTON(wxID_OK, TransformDialog::OnOK)
EVT_BUTTON(idAlignWithTool, TransformDialog::OnAlignWithTool)
EVT_BUTTON(idLoadTxt, TransformDialog::OnLoadTxt)
EVT_HOTKEY(WXK_RETURN, TransformDialog::OnEnter)
wxEND_EVENT_TABLE()

TransformDialog::TransformDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos, const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	auto bSizer = new wxBoxSizer(wxVERTICAL);

	bSizer->Add(new wxStaticText(this, wxID_ANY, "Rotation (degrees)"), 0, wxALL, 5);

	wxFlexGridSizer* flexGridRotation = new wxFlexGridSizer(1, 6, 0, 0);

	wxSize sizeSpin(60, wxDefaultSize.y);

	flexGridRotation->Add(new wxStaticText(this, wxID_ANY, "X"), 0, wxALL, 5);
	spinRotationX = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridRotation->Add(spinRotationX, 0, wxALL, 5);

	flexGridRotation->Add(new wxStaticText(this, wxID_ANY, "Y"), 0, wxALL, 5);
	spinRotationY = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridRotation->Add(spinRotationY, 0, wxALL, 5);

	flexGridRotation->Add(new wxStaticText(this, wxID_ANY, "Z"), 0, wxALL, 5);
	spinRotationZ = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridRotation->Add(spinRotationZ, 0, wxALL, 5);

	bSizer->Add(flexGridRotation, 0, wxALL, 5);


	bSizer->Add(new wxStaticText(this, wxID_ANY, "Translation"), 0, wxALL, 5);

	wxFlexGridSizer* flexGridTranslation = new wxFlexGridSizer(1, 6, 0, 0);

	flexGridTranslation->Add(new wxStaticText(this, wxID_ANY, "X"), 0, wxALL, 5);
	spinTranslationX = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridTranslation->Add(spinTranslationX, 0, wxALL, 5);

	flexGridTranslation->Add(new wxStaticText(this, wxID_ANY, "Y"), 0, wxALL, 5);
	spinTranslationY = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridTranslation->Add(spinTranslationY, 0, wxALL, 5);

	flexGridTranslation->Add(new wxStaticText(this, wxID_ANY, "Z"), 0, wxALL, 5);
	spinTranslationZ = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, sizeSpin, wxSP_ARROW_KEYS, -1000, 1000);
	flexGridTranslation->Add(spinTranslationZ, 0, wxALL, 5);

	bSizer->Add(flexGridTranslation, 0, wxALL, 5);

	auto bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxButton(this, idLoadTxt, "Load matrix"), 0, wxALL, 5);
	bSizerBts->Add(new wxButton(this, idAlignWithTool, "Align Z with tool"), 0, wxALL, 5);

	auto bSizerBts2 = new wxBoxSizer(wxHORIZONTAL);

	btOK = new wxButton(this, wxID_OK, "OK");
	bSizerBts2->Add(btOK, 0, wxALL, 5);
	bSizerBts2->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL, 5);

	bSizer->Add(bSizerBts, 0, wxALIGN_LEFT, 5);
	bSizer->Add(bSizerBts2, 0, wxALIGN_LEFT, 5);

	this->SetSizer(bSizer);
	this->Layout();

	this->Centre(wxBOTH);
	this->RegisterHotKey(WXK_RETURN, wxMOD_NONE, WXK_RETURN);
}

TransformDialog::~TransformDialog()
{
}

void TransformDialog::OnOK(wxCommandEvent & WXUNUSED)
{
	transform = vtkSmartPointer<vtkTransform>::New();
	transform->RotateX(spinRotationX->GetValue());
	transform->RotateY(spinRotationY->GetValue());
	transform->RotateZ(spinRotationZ->GetValue());
	transform->Translate(spinTranslationX->GetValue(), spinTranslationY->GetValue(), spinTranslationZ->GetValue());
	EndModal(wxID_OK);
}

void TransformDialog::OnEnter(wxKeyEvent & event)
{
	btOK->SetFocus();
	OnOK((wxCommandEvent)NULL);
}

void TransformDialog::OnAlignWithTool(wxCommandEvent & WXUNUSED)
{
	EndModal(idAlignWithTool);
}

void TransformDialog::OnLoadTxt(wxCommandEvent & WXUNUSED)
{
	std::string filename = wxLoadFileSelector("Transformation matrix", "TXT files (*.txt)|*.txt");
	if (filename != "")
	{
		vtkNew<vtkMatrix4x4> matrix;
		if (!Utils::loadMatrix4x4(filename, matrix))
		{
			wxMessageBox("Could not load the matrix", "Error", wxICON_ERROR);
			return;
		}
		transform = vtkSmartPointer<vtkTransform>::New();
		transform->SetMatrix(matrix);
		EndModal(wxID_OK);
	}
}
