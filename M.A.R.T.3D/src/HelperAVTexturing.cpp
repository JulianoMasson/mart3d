#include "HelperAVTexturing.h"

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>
#include <vtkExtractEnclosedPoints.h>
#include <vtkMatrix4x4.h>
#include <vtkIdFilter.h>
#include <vtkDataSetSurfaceFilter.h>

#include <wx/log.h>

#include <claudette/collision_model_3d.h>
#include <claudette/ray_collision_test.h>

#include "Camera.h"
#include "ImageIO.h"
#include "Utils.h"
#include "PLYReader.h"

bool HelperAVTexturing::computeAVTexturization(const std::string & meshPath, const std::string & camerasPath, const std::string & outputPath)
{
	AVTexturing::MeshAux mesh;
	vtkSmartPointer<vtkPolyData> meshPolyData = loadMeshToTexturize(meshPath, mesh);
	if (!meshPolyData)
	{
		wxLogError(wxString("Error loading " + Utils::getFileName(meshPath)));
		return false;
	}
	std::vector<Camera*> cameras;
	if (!ImageIO::loadCameraParameters(camerasPath, cameras))
	{
		wxLogError(wxString("Error loading " + Utils::getFileName(camerasPath)));
		return false;
	}
	AVTexturing::CameraParameters cameraParameters;
	createPointVisibilityData(mesh, meshPolyData, cameras);
	for (size_t i = 0; i < cameras.size(); i++)
	{
		//View
		AVTexturing::ViewAux view;
		view.viewId = i;
		view.poseId = i;
		view.intrinsicId = i;
		view.path = cameras[i]->filePath;
		view.width = cameras[i]->getWidth();
		view.height = cameras[i]->getHeight();
		cameraParameters.views.emplace(view.viewId, view);
		//Intrinsic
		AVTexturing::IntrinsicAux intrinsic;
		intrinsic.intrinsicId = i;
		intrinsic.width = cameras[i]->getWidth();
		intrinsic.height = cameras[i]->getHeight();
		intrinsic.pxFocalLength = cameras[i]->getFocalX();
		intrinsic.principalPoint[0] = cameras[i]->getPrincipalPointX();
		intrinsic.principalPoint[1] = cameras[i]->getPrincipalPointY();
		intrinsic.distortionParams.push_back(0);
		intrinsic.distortionParams.push_back(0);
		intrinsic.distortionParams.push_back(0);
		cameraParameters.instrinsics.emplace(intrinsic.intrinsicId, intrinsic);
		//Pose
		AVTexturing::PoseAux pose;
		pose.poseId = i;
		vtkSmartPointer<vtkMatrix4x4> matrixRt = cameras[i]->getMatrixRt();
		for (size_t j = 0; j < 3; j++)
		{
			for (size_t k = 0; k < 3; k++)
			{
				pose.rotation[j * 3 + k] = matrixRt->GetElement(j, k);
			}
			pose.center[j] = cameras[i]->cameraPoints[0][j];
		}
		cameraParameters.poses.emplace(pose.poseId, pose);
	}
	AVTexturing AVText;
	AVTexturing::Options options;
	AVText.applyTexture(mesh, cameraParameters, options, outputPath);
	if (!Utils::exists(outputPath))
	{
		return 0;
	}
	return 1;
}

vtkSmartPointer<vtkPolyData> HelperAVTexturing::loadMeshToTexturize(const std::string& meshPath, AVTexturing::MeshAux& outMesh)
{
	PLYReader plyReader;
	vtkSmartPointer<vtkPolyData> outPolyData = plyReader.read(meshPath);
	if (!outPolyData)
	{
		return nullptr;
	}
	auto points = outPolyData->GetPoints();
	auto normals = vtkFloatArray::SafeDownCast(outPolyData->GetPointData()->GetNormals());
	const auto numberOfPoints = points->GetNumberOfPoints();
	outMesh.points.resize(numberOfPoints);
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		AVTexturing::VertexAux v;
		double point[3], normal[3];
		points->GetPoint(i, point);
		for (size_t j = 0; j < 3; j++)
		{
			v.p[j] = point[j];
		}
		if (normals)
		{
			normals->GetTuple(i, normal);
			for (size_t j = 0; j < 3; j++)
			{
				v.normal[j] = normal[j];
			}
		}
		outMesh.points[i] = v;
	}
	const auto numberOfCells = outPolyData->GetPolys()->GetNumberOfCells();
	if (numberOfCells == 0)
	{
		return nullptr;
	}
	auto offsetArray = outPolyData->GetPolys()->GetOffsetsArray();
	auto idsArray = outPolyData->GetPolys()->GetConnectivityArray();
	outMesh.polygons.resize(numberOfCells);
#pragma omp parallel for
	for (int i = 0; i < numberOfCells; i++)
	{
		double initialOffset[1], finalOffset[1], vertexIndex[1];
		offsetArray->GetTuple(i, initialOffset);
		offsetArray->GetTuple(i + 1, finalOffset);
		const auto numberOfVertices = finalOffset[0] - initialOffset[0];
		AVTexturing::FaceAux face;
		face.vertices.reserve(numberOfVertices);
		for (size_t j = 0; j < numberOfVertices; j++)
		{
			idsArray->GetTuple(initialOffset[0] + j, vertexIndex);
			face.vertices.emplace_back(vertexIndex[0]);
		}
		outMesh.polygons[i] = face;
	}
	return outPolyData;
}

void HelperAVTexturing::createPointVisibilityData(AVTexturing::MeshAux& mesh, vtkSmartPointer<vtkPolyData> meshPolyData, std::vector<Camera*> cameras)
{
	//Create the polydata with the original id as atributte
	vtkNew<vtkIdFilter> idFilter;
	idFilter->SetInputData(meshPolyData);
	idFilter->SetPointIdsArrayName("OriginalIds");
	idFilter->Update();
	// This is needed to convert the ouput of vtkIdFilter (vtkDataSet) back to vtkPolyData
	vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
	surfaceFilter->SetInputConnection(idFilter->GetOutputPort());
	surfaceFilter->Update();
	vtkSmartPointer<vtkPolyData> meshWithPointIds = surfaceFilter->GetOutput();
	//Creating collision model
	Claudette::CollisionModel3D* collisionModel = new Claudette::CollisionModel3D();
	for (const auto& face : mesh.polygons)
	{
		collisionModel->addTriangle(mesh.points[face.vertices[0]].p, mesh.points[face.vertices[1]].p, mesh.points[face.vertices[2]].p);
	}
	collisionModel->finalize();
	const auto numberOfPoints = mesh.points.size();
	//Create a vtkPolyData to select the points using the frustum
	double bounds[6];
	meshPolyData->GetBounds(bounds);
	auto cameraIndex = 0;
	for (const auto& cam : cameras)
	{
		double* cameraOrigin = cam->cameraPoints[0];
		auto maxDistance = -1.0;
		for (size_t i = 4; i < 6; i++)//Z
		{
			for (size_t j = 2; j < 4; j++)//Y
			{
				for (size_t k = 0; k < 2; k++)//X
				{
					double boundPoint[3] = { bounds[k], bounds[j], bounds[i] };
					double dist = vtkMath::Distance2BetweenPoints(cameraOrigin, boundPoint);
					if (maxDistance < dist)
					{
						maxDistance = dist;
					}
				}
			}
		}
		vtkNew<vtkExtractEnclosedPoints> extractEnclosedPoints;
		extractEnclosedPoints->SetSurfaceData(cam->getClosedFrustum(std::sqrt(maxDistance)));
		extractEnclosedPoints->SetInputData(meshWithPointIds);
		extractEnclosedPoints->SetTolerance(.00000001);
		extractEnclosedPoints->Update();
		auto insideArray = vtkIdTypeArray::SafeDownCast(
			extractEnclosedPoints->GetOutput()->GetPointData()->GetArray("OriginalIds"));
		if (insideArray)
		{
			const auto numberOfInsidePoints = insideArray->GetNumberOfTuples();
#pragma omp parallel for
			for (int j = 0; j < numberOfInsidePoints; j++)
			{
				double pointId[1];
				insideArray->GetTuple(j, pointId);
				auto testPoint = mesh.points[pointId[0]].p;
				Claudette::RayCollisionTest rayTest;
				rayTest.setRayOrigin(cameraOrigin[0], cameraOrigin[1], cameraOrigin[2]);
				rayTest.setRaySearch(Claudette::RayCollisionTest::SearchClosestTriangle);
				float direction[3];
				vtkMath::Subtract(testPoint, rayTest.rayOrigin(), direction);
				vtkMath::Normalize(direction);
				rayTest.setRayDirection(direction[0], direction[1], direction[2]);
				if (collisionModel->rayCollision(&rayTest))
				{
					if (vtkMath::Distance2BetweenPoints(rayTest.point(), testPoint) < 0.01)//0.1^2
					{
						mesh.points[pointId[0]].observations.emplace_back(cameraIndex);
					}
				}
			}
		}
		cameraIndex++;
	}
}
