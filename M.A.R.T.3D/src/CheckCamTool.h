#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>
#include <vector>

class vtkPropPicker;
class vtkPolygon;
class wxTreeListCtrl;
class LineWidget;
class Project;
class Mesh;
class Camera;

class CheckCamTool : public vtk3DWidget
{
public:
	/**
	* Instantiate the object.
	*/
	static CheckCamTool *New();

	vtkTypeMacro(CheckCamTool, vtk3DWidget);

	//@{
	/**
	* Methods that satisfy the superclass' API.
	*/
	virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
	//@}

	//@{
	/**
	* Set the project that is going to be used
	*/
	void setProject(Project* project) { this->project = project; };
	//@}

	void setMesh(Mesh* mesh) { this->mesh = mesh; };

	//@{
	/**
	* Set the mesh tree
	*/
	void setTreeMesh(wxTreeListCtrl* treeMesh) { this->treeMesh = treeMesh; };
	//@}

protected:
	CheckCamTool();
	~CheckCamTool();

	//handles the events
	static void ProcessEvents(vtkObject* object, unsigned long event,
		void* clientdata, void* calldata);

	vtkSmartPointer<LineWidget> lineWidget = nullptr;
	vtkSmartPointer<vtkPropPicker> propPicker = nullptr;

	void checkVisibility();
	void destruct();
	void clearVisibleCameras();
	// line -> point0-point1
	bool intersectFrustumWithLine(const double point0[3], const double point1[3], vtkSmartPointer<vtkPolygon> imagePolygon);

	Project* project = nullptr;
	Mesh* mesh = nullptr;
	wxTreeListCtrl* treeMesh = nullptr;
	std::vector<Camera*> visibleCameras;

private:
	CheckCamTool(const CheckCamTool&) VTK_DELETE_FUNCTION;
	void operator=(const CheckCamTool&) VTK_DELETE_FUNCTION;
};
