#pragma once
#include <vtkSmartPointer.h>

#include <string>
#include <vector>

class vtkPolyData;

class PLYWriter
{
public:
	PLYWriter() {};
	~PLYWriter() {};

	static bool Write(const std::string& filename, vtkSmartPointer<vtkPolyData> polyData, bool hasFaces);

private:
	static std::string Get_type(unsigned int type);
	static void write_value(std::ofstream& out, int type, double value);
	static void Create_header(std::ofstream& outFile, std::vector<unsigned int>& pointDataPropertyIndexes, vtkSmartPointer<vtkPolyData> polyData, bool hasFaces);

};