#include "CheckCamTool.h"

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkPropPicker.h>
#include <vtkPoints.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPlane.h>
#include <vtkPolygon.h>

#include "LineWidget.h"
#include "Draw.h"
#include "Utils.h"
#include "Camera.h"
#include "Mesh.h"
#include "Project.h"

vtkStandardNewMacro(CheckCamTool);

//----------------------------------------------------------------------------
CheckCamTool::CheckCamTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(CheckCamTool::ProcessEvents);
}

//----------------------------------------------------------------------------
CheckCamTool::~CheckCamTool()
{
	destruct();
	treeMesh = nullptr;
}

//----------------------------------------------------------------------------
void CheckCamTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !project || !treeMesh || !mesh) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		// Create the picker
		if (!propPicker)
		{
			propPicker = vtkSmartPointer<vtkPropPicker>::New();
		}
		propPicker->InitializePickList();
		mesh->CreatePickList(propPicker);

		// listen for the following events
		if (!lineWidget)
		{
			lineWidget = vtkSmartPointer<LineWidget>::New();
			lineWidget->SetInteractor(this->Interactor);
			lineWidget->setCloseLoopOnFirstNode(true);
			lineWidget->CreateRepresentationFromMesh(mesh);
		}
		lineWidget->EnabledOn();
		lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		// turn off the various actors
		destruct();

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void CheckCamTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	CheckCamTool* self =
		reinterpret_cast<CheckCamTool *>(clientdata);

	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::EndInteractionEvent:
		self->checkVisibility();
		break;
	}
}

//----------------------------------------------------------------------------
void CheckCamTool::checkVisibility()
{
	if (!lineWidget)
	{
		return;
	}
	vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
	if (!rep->isLoopClosed())
	{
		return;
	}
	clearVisibleCameras();
	wxBeginBusyCursor();
	vtkSmartPointer<vtkPoints> testPoints = rep->getPoints();
	const auto numberOfPoints = testPoints->GetNumberOfPoints() - 1;
	//Store the initial camera
	vtkNew<vtkCamera> initialCamera;
	initialCamera->DeepCopy(CurrentRenderer->GetActiveCamera());
	//Start testing cameras
	bool intersections = true;
	bool pointsVisibles = true;
	for (auto cam : project->cameras)
	{
		//Create the polygon
		vtkNew<vtkPolygon> imagePolygon;
		for (size_t i = 1; i < 5; i++)
		{
			imagePolygon->GetPoints()->InsertNextPoint(cam->cameraPoints[i]);
		}
		//To avoid wasting time and processing, we first do a simple test to see if the point is in the camera FOV.
		intersections = true;
		for (vtkIdType i = 0; i < numberOfPoints; i++)
		{
			if (!intersectFrustumWithLine(cam->cameraPoints[0], testPoints->GetPoint(i), imagePolygon))
			{
				intersections = false;
				break;
			}
		}
		//If every point is seen by the camera we check if there is anything obstructing the vision
		if (intersections)
		{
			Utils::updateCamera(CurrentRenderer, cam);
			pointsVisibles = true;
			for (vtkIdType i = 0; i < numberOfPoints; i++)
			{
				double pointCheckCamDisplay[3];
				Utils::getDisplayPosition(CurrentRenderer, testPoints->GetPoint(i), pointCheckCamDisplay);
				double clickPos[3];
				Utils::pickPosition(CurrentRenderer, propPicker, pointCheckCamDisplay, clickPos);
				if (vtkMath::Distance2BetweenPoints(clickPos, testPoints->GetPoint(i)) > 0.003)
				{
					pointsVisibles = false;
					break;
				}
			}
			if (pointsVisibles)
			{
				visibleCameras.push_back(cam);
			}
		}
	}
	//Return to the initial camera
	CurrentRenderer->SetActiveCamera(initialCamera);
	//Update the tree with the visible cameras
	for (auto visibleCamera : visibleCameras)
	{
		treeMesh->SetItemImage(visibleCamera->getListItemCamera(), 0);
		visibleCamera->actorFrustum->GetProperty()->SetColor(0, 255, 0);
	}
	treeMesh->Expand(project->GetTreeItemCameras());
	CurrentRenderer->GetRenderWindow()->Render();
	wxEndBusyCursor();
}

void CheckCamTool::destruct()
{
	clearVisibleCameras();
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
		lineWidget = nullptr;
	}
	project = nullptr;
	propPicker = nullptr;
}

void CheckCamTool::clearVisibleCameras()
{
	//clear the OK icon of the tree and the color of the camera actors
	for (auto camera : visibleCameras)
	{
		treeMesh->SetItemImage(camera->getListItemCamera(), -1);
		camera->actorFrustum->GetProperty()->SetColor(255, 255, 255);
	}
	visibleCameras.clear();
}

bool CheckCamTool::intersectFrustumWithLine(const double point0[3], const double point1[3], vtkSmartPointer<vtkPolygon> imagePolygon)
{
	double n[3];
	imagePolygon->ComputeNormal(imagePolygon->GetPoints()->GetNumberOfPoints(),
		static_cast<double*>(imagePolygon->GetPoints()->GetData()->GetVoidPointer(0)), n);
	double bounds[6];
	imagePolygon->GetPoints()->GetBounds(bounds);
	double t;
	double intersection[3];
	if (vtkPlane::IntersectWithLine(point0, point1, n,
		imagePolygon->GetPoints()->GetPoint(0), t, intersection))
	{
		if (imagePolygon->PointInPolygon(intersection,
			imagePolygon->GetPoints()->GetNumberOfPoints(),
			static_cast<double*>(imagePolygon->GetPoints()->GetData()->GetVoidPointer(0)), bounds, n))
		{
			return true;
		}
	}
	return false;
}