#pragma once

#include <vtkSmartPointer.h>
#include <vector>

class vtkActor;
class vtkPolyData;
class Mesh;

class MeshIO
{
public:
	
	//Get actors from a file: .ply, .obj, .obj + .mtl and .wrl
	static bool getActorsFromFile(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>> &actors, std::vector<std::string> &textureNames);

	//Export mesh
	static bool exportMesh(const std::string& filePath, Mesh* mesh);

private:

	//Input
	static bool getActorsFromPLY(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>> &actors);

	//PLY and OBJ without texture
	static bool getActorsFromOBJ(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>> &actors);

	//OBJ with texture
	static bool getActorsFromTexturedOBJ(const std::string& filePathOBJ, const std::string& filePathMTL, std::vector<vtkSmartPointer<vtkActor>> &actors);
	static std::string getMTLFilenameFromOBJ(const std::string& filename);
	static std::vector<std::string> getTextureNamesFromMTL(const std::string& filename);

	//WRL
	static bool getActorsFromWRL(const std::string& filePath, std::vector<vtkSmartPointer<vtkActor>> &actors);

	//Output

	//PLY
	static bool savePLY(const std::string& filename, Mesh* mesh);

	//OBJ
	static bool saveOBJ(const std::string& filename, Mesh* mesh);
	// Remove the complete path for the textures inside the MTL, to avoid problems if the project is moved
	static void AdjustTextureNamesMTL(const std::string& mtlFilename);

};