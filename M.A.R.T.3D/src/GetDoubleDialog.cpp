#include "GetDoubleDialog.h"

#include <wx\sizer.h>
#include <wx\stattext.h>
#include <wx\spinctrl.h>
#include <wx\button.h>

wxBEGIN_EVENT_TABLE(GetDoubleDialog, wxDialog)
EVT_BUTTON(wxID_OK, GetDoubleDialog::OnOK)
wxEND_EVENT_TABLE()

GetDoubleDialog::GetDoubleDialog(const wxString & message, const wxString & prompt, const wxString & caption,
	const double value, const double minValue, const double maxValue,
	wxWindow * parent, const wxPoint & pos, const wxSize & size, long style) :
	wxDialog(parent, wxID_ANY, caption, pos, size, style)
{
	auto bSizer = new wxBoxSizer(wxVERTICAL);

	bSizer->Add(new wxStaticText(this, wxID_ANY, message), 0, wxALL, 5);

	auto fgSizer = new wxFlexGridSizer(0, 2, wxSize(10, 10));
	fgSizer->AddGrowableCol(1);
	fgSizer->SetFlexibleDirection(wxBOTH);
	fgSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, prompt), 0, wxALL, 5);
	spinDouble = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition,
		wxDefaultSize, 16384L, minValue, maxValue, value);
	fgSizer->Add(spinDouble, 0, wxALL, 5);

	bSizer->Add(fgSizer, 0, wxALL, 5);

	auto bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL, 5);
	bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL, 5);

	bSizer->Add(bSizerBts, 0, wxALL, 5);

	this->SetSizer(bSizer);
	this->Layout();
	this->Centre(wxBOTH);
	this->Fit();
}

double GetDoubleDialog::GetValue()
{
	return spinDouble->GetValue();
}

void GetDoubleDialog::OnOK(wxCommandEvent & WXUNUSED)
{
	EndModal(wxOK);
}
