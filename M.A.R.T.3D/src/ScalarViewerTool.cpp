#include "ScalarViewerTool.h"

#include <sstream>

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSliderWidget.h>
#include <vtkPointPicker.h>
#include <vtkActor.h>
#include <vtkDataArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkLookupTable.h>
#include <vtkScalarBarActor.h>
#include <vtkCaptionActor2D.h>

#include "SliderRepresentation2D.h"
#include "Draw.h"
#include "Utils.h"
#include "Mesh.h"

vtkStandardNewMacro(ScalarViewerTool);

//----------------------------------------------------------------------------
ScalarViewerTool::ScalarViewerTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(ScalarViewerTool::ProcessEvents);
}

//----------------------------------------------------------------------------
ScalarViewerTool::~ScalarViewerTool()
{
	Destruct();
}

void ScalarViewerTool::Destruct()
{
	if (textActor)
	{
		this->CurrentRenderer->RemoveActor2D(textActor);
		textActor = nullptr;
	}
	if (scalarBarActor)
	{
		this->CurrentRenderer->RemoveActor2D(scalarBarActor);
		scalarBarActor = nullptr;
	}
	if (scalarActor)
	{
		this->CurrentRenderer->RemoveActor(scalarActor);
		scalarActor = nullptr;
	}
	if (sliderWidget)
	{
		sliderWidget->EnabledOff();
		sliderWidget = nullptr;
	}
	pointPicker = nullptr;
	if (mesh)
	{
		mesh->SetVisibility(treeMesh->GetCheckedState(mesh->GetTreeItem()));
	}
	scalars = nullptr;
	mesh = nullptr;
	calibration = nullptr;
	historyOfSliderLimits.clear();
}

//----------------------------------------------------------------------------
void ScalarViewerTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh || !treeMesh || !calibration) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		//Actors
		if (!CreateActors())
		{
			return;
		}

		// listen for the following events
		vtkRenderWindowInteractor *i = this->Interactor;
		i->AddObserver(vtkCommand::MiddleButtonPressEvent,
			this->EventCallbackCommand, this->Priority);
		i->AddObserver(vtkCommand::KeyPressEvent,
			this->EventCallbackCommand, this->Priority);

		//Picking stuff
		if (!pointPicker)
		{
			pointPicker = vtkSmartPointer<vtkPointPicker>::New();
			pointPicker->AddPickList(scalarActor);
			pointPicker->PickFromListOn();
		}
		CreateSlider();

		sliderWidget->AddObserver(vtkCommand::InteractionEvent,
			this->EventCallbackCommand, this->Priority);
		sliderWidget->AddObserver(vtkCommand::EndInteractionEvent,
			this->EventCallbackCommand, this->Priority);

		mesh->SetVisibility(false);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);
		sliderWidget->RemoveObserver(this->EventCallbackCommand);

		Destruct();

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void ScalarViewerTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	ScalarViewerTool* self =
		reinterpret_cast<ScalarViewerTool *>(clientdata);

	if (event == vtkCommand::MiddleButtonPressEvent)
	{
		self->OnMiddleButtonDown();
	}
	else if (event == vtkCommand::KeyPressEvent)
	{
		self->OnKeyPress();
	}
	else if (event == vtkCommand::EndInteractionEvent || event == vtkCommand::InteractionEvent)
	{
		self->OnSliderChanged();
	}
}

//----------------------------------------------------------------------------
void ScalarViewerTool::OnMiddleButtonDown()
{
	double* pickedPosition = new double[3];
	if (GetMousePosition(pickedPosition))
	{
		if (textActor)
		{
			this->CurrentRenderer->RemoveActor2D(textActor);
		}
		const auto numberOfComponents = scalars->GetNumberOfComponents();
		double* tuple = new double[numberOfComponents];
		scalars->GetTuple(pointPicker->GetPointId(), tuple);
		std::stringstream s;
		for (size_t i = 0; i < numberOfComponents; i++)
		{
			s << std::fixed << std::setprecision(3) << tuple[i] << " ";
		}
		delete tuple;
		//Text
		textActor = Draw::createNumericIndicator(this->CurrentRenderer, pickedPosition,
			s.str(), 24, 1.0, 1.0, 1.0);
	}
	else
	{
		delete pickedPosition;
		return;
	}
	this->Interactor->Render();
}

void ScalarViewerTool::OnKeyPress()
{
	if (this->Interactor->GetControlKey())
	{
		if (this->Interactor->GetKeyCode() == 'Z')
		{
			if (historyOfSliderLimits.size() != 1)
			{
				historyOfSliderLimits.pop_back();
				UpdateSliderLimits(historyOfSliderLimits.back().first,
					historyOfSliderLimits.back().second);
				this->Interactor->Render();
			}
		}
	}
	if (this->Interactor->GetKeyCode() == 'U')
	{
		vtkSmartPointer<SliderRepresentation2D> sliderRep =
			static_cast<SliderRepresentation2D *>(sliderWidget->GetRepresentation());
		auto minValue = sliderRep->getSliderMinValue();
		auto maxValue = sliderRep->getSliderMaxValue();
		// Avoid adding the same interval in the vector
		if (minValue == historyOfSliderLimits.back().first &&
			maxValue == historyOfSliderLimits.back().second)
		{
			return;
		}
		UpdateSliderLimits(minValue, maxValue);
		historyOfSliderLimits.emplace_back(std::make_pair(minValue, maxValue));
		this->Interactor->Render();
	}
}

bool ScalarViewerTool::CreateActors()
{
	if (!scalars)
	{
		return 0;
	}
	vtkNew<vtkPolyData> polyData;
	polyData->DeepCopy(mesh->GetPolyData());
	polyData->GetPointData()->SetScalars(scalars);

	vtkNew<vtkLookupTable> lookUpTable;

	if (!scalarBarActor)
	{
		scalarBarActor = vtkSmartPointer<vtkScalarBarActor>::New();
	}
	scalarBarActor->SetLookupTable(lookUpTable);
	scalarBarActor->SetNumberOfLabels(4);
	this->CurrentRenderer->AddActor2D(scalarBarActor);

	vtkNew<vtkPolyDataMapper> mapper;
	mapper->SetInputData(polyData);
	mapper->SetScalarRange(polyData->GetScalarRange());
	mapper->SetLookupTable(lookUpTable);
	if (!scalarActor)
	{
		scalarActor = vtkSmartPointer<vtkActor>::New();
	}
	scalarActor->SetMapper(mapper);
	this->CurrentRenderer->AddActor(scalarActor);
	return 1;
}

void ScalarViewerTool::CreateSlider()
{
	double scalarRange[2];
	scalars->GetRange(scalarRange);
	vtkSmartPointer<SliderRepresentation2D> sliderRepresentation = vtkSmartPointer<SliderRepresentation2D>::New();
	sliderRepresentation->SetMinimumValue(scalarRange[0]);
	sliderRepresentation->SetMaximumValue(scalarRange[1]);
	char labelMin[256];
	char labelMax[256];
	snprintf(labelMin, sizeof(labelMin), "%.3f", scalarRange[0]);
	snprintf(labelMax, sizeof(labelMax), "%.3f", scalarRange[1]);
	sliderRepresentation->SetMinLimitText(labelMin);
	sliderRepresentation->SetMaxLimitText(labelMax);
	sliderRepresentation->SetValue(sliderRepresentation->GetMinimumValue());
	sliderRepresentation->SetTitleText(scalars->GetName());
	sliderRepresentation->SetLabelFormat("%.3f");
	sliderRepresentation->setCalibration(calibration);
	sliderRepresentation->setUseMaxSlider(true);

	sliderWidget = vtkSmartPointer<vtkSliderWidget>::New();
	sliderWidget->SetInteractor(this->Interactor);
	sliderWidget->SetRepresentation(sliderRepresentation);
	sliderWidget->SetAnimationModeToJump();
	sliderWidget->EnabledOn();
	historyOfSliderLimits.emplace_back( std::make_pair(scalarRange[0], scalarRange[1]));
}

void ScalarViewerTool::UpdateSliderLimits(double minValue, double maxValue)
{
	vtkSmartPointer<SliderRepresentation2D> sliderRep =
		static_cast<SliderRepresentation2D *>(sliderWidget->GetRepresentation());
	sliderRep->SetMinimumValue(minValue);
	sliderRep->SetMaximumValue(maxValue);
	char labelMin[256];
	char labelMax[256];
	snprintf(labelMin, sizeof(labelMin), "%.3f", minValue);
	snprintf(labelMax, sizeof(labelMax), "%.3f", maxValue);
	sliderRep->SetMinLimitText(labelMin);
	sliderRep->SetMaxLimitText(labelMax);
}

void ScalarViewerTool::OnSliderChanged()
{
	if (sliderWidget)
	{
		vtkSmartPointer<SliderRepresentation2D> slider = static_cast<SliderRepresentation2D *>(sliderWidget->GetRepresentation());
		scalarActor->GetMapper()->SetScalarRange(slider->getSliderMinValue(), slider->getSliderMaxValue());
	}
}

int ScalarViewerTool::GetMousePosition(double * point)
{
	if (pointPicker->Pick(this->Interactor->GetEventPosition()[0], this->Interactor->GetEventPosition()[1], 0, this->CurrentRenderer))
	{
		pointPicker->GetPickPosition(point);
		return 1;
	}
	return 0;
}
