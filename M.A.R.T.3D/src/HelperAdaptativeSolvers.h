#pragma once
#include <vtkSmartPointer.h>

#include "PoissonRecon.h"
#include "SSDRecon.h"
#include "SurfaceTrimmer.h"

class vtkPolyData;

class HelperAdaptativeSolvers
{
public:
	HelperAdaptativeSolvers() {};
	~HelperAdaptativeSolvers() {};

	static vtkSmartPointer<vtkPolyData> computePoisson(vtkSmartPointer<vtkPolyData> polyData, const PoissonRecon::Options& options);
	static vtkSmartPointer<vtkPolyData> computeSSD(vtkSmartPointer<vtkPolyData> polyData, const SSDRecon::Options& options);
	// Only works with triangle mesh, do not use options.polygonMesh
	static vtkSmartPointer<vtkPolyData> computeSurfaceTrimmer(vtkSmartPointer<vtkPolyData> polyData, const SurfaceTrimmer::Options& options);

private:
	template <typename T>
	static AdaptativeSolvers::Mesh<T> polyData2AdaptativeSolvers(vtkSmartPointer<vtkPolyData> polyData, const bool& getFaces = false);

	template <typename T>
	static vtkSmartPointer<vtkPolyData> adaptativeSolvers2polyData(const AdaptativeSolvers::Mesh<T>& mesh);
};


