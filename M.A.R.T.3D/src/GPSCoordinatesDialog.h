#pragma once
#include <wx/dialog.h>

class wxSpinCtrlDouble;

class GPSCoordinatesDialog : public wxDialog
{
public:
	GPSCoordinatesDialog(wxWindow* parent,
		wxWindowID id = wxID_ANY, const wxString& title = "GPS Coordinates", const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxSize(250, 170), long style = wxDEFAULT_DIALOG_STYLE);
	~GPSCoordinatesDialog();

	double GetLatitude();
	double GetLongitude();
	double GetAltitude();

private:
	wxSpinCtrlDouble* spinLatitude;
	wxSpinCtrlDouble* spinLongitude;
	wxSpinCtrlDouble* spinAltitude;
};