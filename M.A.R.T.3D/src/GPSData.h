#pragma once

class GPSData {
private:
	double latitude;
	double longitude;
	double altitude;
public:
	GPSData() : latitude(-1), longitude(-1), altitude(-1) {};
	GPSData(double latitude, double longitude, double altitude) :
		latitude(latitude), longitude(longitude), altitude(altitude) {};
	~GPSData() {};

	void setLongitude(double longitude) { this->longitude = longitude; };
	double getLongitude() const { return longitude; };

	void setLatitude(double latitude) { this->latitude = latitude; };
	double getLatitude() const { return latitude; };

	void setAltitude(double altitude) { this->altitude = altitude; };
	double getAltitude() const { return altitude; };

  /*
  Compute the distance bewtween two GPSData, it uses the latitude, longitude and altitude
  */
  double getDistanceBetweenGPSData(GPSData* gpsData);
};