#include "ReconstructionDialog.h"

#include <gl\GL.h>

#include <vtkMatrix4x4.h>
#include <vtkPLYReader.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>
#include <vtksys/SystemTools.hxx>

#include <wx/sizer.h>
#include <wx/checkbox.h>
#include <wx/statbox.h>
#include <wx/stattext.h>
#include <wx/bmpbuttn.h>
#include <wx/filepicker.h>
#include <wx/dir.h>
#include <wx/sizer.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/progdlg.h>

#include "HelperCOLMAP.h"
#include "HelperMeshRecon.h"
#include "HelperTexRecon.h"
#include "HelperAVTexturing.h"
#include "ReconstructionLog.h"
#include "ConfigurationDialog.h"
#include "Utils.h"
#include "ImageIO.h"
#include "Camera.h"

#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

wxBEGIN_EVENT_TABLE(ReconstructionDialog, wxDialog)
EVT_BUTTON(wxID_OK, ReconstructionDialog::OnOK)
EVT_BUTTON(idBtConfig, ReconstructionDialog::OnBtConfig)
EVT_BUTTON(idBtBatchAdd, ReconstructionDialog::OnBtBatchAdd)
EVT_BUTTON(idBtBatchStart, ReconstructionDialog::OnBtBatchStart)
EVT_CHECKBOX(idCBMesh, ReconstructionDialog::OnCheckBoxs)
EVT_CHECKBOX(idCBTexture, ReconstructionDialog::OnCheckBoxs)
EVT_DIRPICKER_CHANGED(idDirPickerImages, ReconstructionDialog::OnDirPickerImages)
EVT_FILEPICKER_CHANGED(idDirPickerMesh, ReconstructionDialog::OnDirPickerMesh)
EVT_FILEPICKER_CHANGED(idDirPickerCameras, ReconstructionDialog::OnDirPickerCameras)
EVT_FILEPICKER_CHANGED(idDirPickerOutput, ReconstructionDialog::OnDirPickerOutput)
wxEND_EVENT_TABLE()


ReconstructionDialog::ReconstructionDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos, const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	auto bSizer = new wxBoxSizer(wxVERTICAL);

	cbGenerateMesh = new wxCheckBox(this, idCBMesh, "Generate mesh");
	bSizer->Add(cbGenerateMesh, 0, wxALL, 5);
	cbTexturize = new wxCheckBox(this, idCBTexture, "Generate texture");
	bSizer->Add(cbTexturize, 0, wxALL, 5);

	//Mesh
	wxStaticBoxSizer* sbSizerMesh;
	sbSizerMesh = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "Mesh"), wxVERTICAL);

	textImages = new wxStaticText(sbSizerMesh->GetStaticBox(), wxID_ANY, "Select the image folder:");
	sbSizerMesh->Add(textImages, 0, wxALL, 5);
	pickerImages = new wxDirPickerCtrl(sbSizerMesh->GetStaticBox(), idDirPickerImages, wxEmptyString, "Select the image folder");
	sbSizerMesh->Add(pickerImages, 0, wxALL, 5);

	textOutputMesh = new wxStaticText(sbSizerMesh->GetStaticBox(), wxID_ANY, "Select the output file:");
	sbSizerMesh->Add(textOutputMesh, 0, wxALL, 5);
	pickerOutputMesh = new wxFilePickerCtrl(sbSizerMesh->GetStaticBox(), idDirPickerOutput, wxEmptyString, "Select the output file", "PLY files(*.ply) | *.ply", wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
	sbSizerMesh->Add(pickerOutputMesh, 0, wxALL, 5);

	bSizer->Add(sbSizerMesh, 0, wxALL, 5);

	//Texture
	wxStaticBoxSizer* sbSizerTexture;
	sbSizerTexture = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "Texture"), wxVERTICAL);

	textMesh = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the mesh to be texturized:");
	sbSizerTexture->Add(textMesh, 0, wxALL, 5);
	pickerMesh = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), idDirPickerMesh, wxEmptyString, "Select the mesh to be texturized", "PLY files (*.ply)|*.ply");
	sbSizerTexture->Add(pickerMesh, 0, wxALL, 5);

	textCameras = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the cameras file:");
	sbSizerTexture->Add(textCameras, 0, wxALL, 5);
	pickerCameras = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), idDirPickerCameras, wxEmptyString,
		"Select the NVM/SFM file", "NVM and SFM files (*.nvm;*.sfm)|*.nvm;*.sfm");
	sbSizerTexture->Add(pickerCameras, 0, wxALL, 5);

	textOutputTexture = new wxStaticText(sbSizerTexture->GetStaticBox(), wxID_ANY, "Select the output file:");
	sbSizerTexture->Add(textOutputTexture, 0, wxALL, 5);
	pickerOutputTexture = new wxFilePickerCtrl(sbSizerTexture->GetStaticBox(), idDirPickerOutput, wxEmptyString,
		"Select the output file", "OBJ files(*.obj) | *.obj", wxDefaultPosition, wxDefaultSize, wxFLP_USE_TEXTCTRL | wxFLP_SAVE | wxFLP_OVERWRITE_PROMPT);
	sbSizerTexture->Add(pickerOutputTexture, 0, wxALL, 5);

	bSizer->Add(sbSizerTexture, 0, wxALL, 5);

	//Batch reconstruction
	wxStaticBoxSizer* sbSizerBatch = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, "Batch reconstruction"), wxVERTICAL);

	sbSizerBatch->Add(new wxStaticText(sbSizerBatch->GetStaticBox(), wxID_ANY, "Add a reconstruction in the queue:"), 0, wxALL, 5);

	wxBoxSizer* bSizerBtsBatch = new wxBoxSizer(wxHORIZONTAL);
	bSizerBtsBatch->Add(new wxButton(sbSizerBatch->GetStaticBox(), idBtBatchAdd, "Add"), 0, wxALL | wxALIGN_CENTER, 5);
	bSizerBtsBatch->Add(new wxButton(sbSizerBatch->GetStaticBox(), idBtBatchStart, "Start"), 0, wxALL | wxALIGN_CENTER, 5);
	sbSizerBatch->Add(bSizerBtsBatch, 0, wxALL | wxALIGN_CENTER, 5);

	textQueueSize = new wxStaticText(sbSizerBatch->GetStaticBox(), wxID_ANY, "Queue size: 0");
	sbSizerBatch->Add(textQueueSize, 0, wxALL, 5);

	bSizer->Add(sbSizerBatch, 0, wxALL, 5);

	//Bts
	wxBoxSizer* bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxBitmapButton(this, idBtConfig, wxICON(ICON_CONFIG)), 0, wxALL | wxALIGN_CENTER, 5);

	bSizerBts->Add(new wxButton(this, wxID_OK, "OK"), 0, wxALL | wxALIGN_CENTER, 5);

	bSizerBts->Add(new wxButton(this, wxID_CANCEL, "Cancel"), 0, wxALL | wxALIGN_CENTER, 5);

	bSizer->Add(bSizerBts, 0, wxALIGN_RIGHT, 5);

	OnCheckBoxs((wxCommandEvent)NULL);

	this->SetSizer(bSizer);
	this->Layout();
	this->Fit();
	this->Centre(wxBOTH);
}

ReconstructionDialog::~ReconstructionDialog()
{
}

wxString ReconstructionDialog::getOutputPath()
{
	wxString path;
	if (cbTexturize->IsChecked())
	{
		return pickerOutputTexture->GetPath();
	}
	path = pickerOutputMesh->GetPath();
	std::replace(path.begin(), path.end(), '\\', '/');
	return path.substr(0, path.find_last_of('.')) + "_refine.ply";
}

void ReconstructionDialog::texturizeMesh(std::string meshPath, std::string nvmPath, std::string outputPath)
{
	cbTexturize->SetValue(true);
	pickerMesh->SetPath(meshPath);
	pickerCameras->SetPath(nvmPath);
	pickerOutputTexture->SetPath(outputPath);
	OnOK((wxCommandEvent)NULL);
}

void ReconstructionDialog::OnOK(wxCommandEvent & WXUNUSED)
{
	bool generateMesh = false, generateTexture = false;
	std::string outputPath = checkFields(generateMesh, generateTexture);
	if (outputPath == "")
	{
		return;
	}
	//Log
	ReconstructionLog log(outputPath.substr(0, outputPath.size() - 4) + "_log.txt");
	auto bool2String = [](bool flag) { if (flag) { return "Yes"; } return "No"; };
	log.write("Generate mesh - " + ((std::string)bool2String(generateMesh)));
	log.write("Generate texture - " + ((std::string)bool2String(generateTexture)));
	log.addSeparator();
	//Start reconstruction
	if (generateMesh)
	{
		//Input
		std::string imagesPath = pickerImages->GetPath();
		//SFM
		std::string nvmPath = outputPath.substr(0, outputPath.find_last_of('.')) + ".nvm";
		if (!runSFM(imagesPath, nvmPath, log))
		{
			wxLogError("Error during SFM");
			return;
		}
		//Meshing
		if (!runMeshing(imagesPath, nvmPath, log))
		{
			wxLogError("Error during meshing");
			return;
		}
		if (generateTexture)
		{
			std::string meshPath = nvmPath.substr(0, nvmPath.size() - 4) + "_refine.ply";
			if (!runTexturization(meshPath, nvmPath, outputPath, log))
			{
				wxLogError("Error during texturization");
				return;
			}
		}
	}
	else if (cbTexturize->IsChecked())
	{
		std::string camerasPath = pickerCameras->GetPath();
		std::string meshPath = pickerMesh->GetPath();
		if (!runTexturization(meshPath, camerasPath, outputPath, log))
		{
			wxLogError("Error during texturization");
			return;
		}
	}
	if (!disableEndModal)
	{
		EndModal(wxID_OK);
	}
}

void ReconstructionDialog::OnBtConfig(wxCommandEvent & WXUNUSED)
{
	ConfigurationDialog* config = new ConfigurationDialog(this);
	config->ShowModal();
	delete config;
}

void ReconstructionDialog::OnBtBatchAdd(wxCommandEvent & WXUNUSED)
{
	if (cbGenerateMesh->IsChecked())
	{
		if (!cbTexturize->IsChecked())
		{
			if (pickerImages->GetPath() == "" || pickerOutputMesh->GetPath() == "")
			{
				wxLogError("Please fulfill all the fields correctly.");
				return;
			}
		}
		else
		{
			if (pickerImages->GetPath() == "" || pickerOutputTexture->GetPath() == "")
			{
				wxLogError("Please fulfill all the fields correctly.");
				return;
			}
		}
	}
	else if (cbTexturize->IsChecked())
	{
		if (pickerCameras->GetPath() == "" || pickerMesh->GetPath() == "" || pickerOutputTexture->GetPath() == "")
		{
			wxLogError("Please fulfill all the fields correctly.");
			return;
		}
	}
	else
	{
		wxLogError("Please select at least one checkbox.");
		return;
	}
	BatchItem b(cbGenerateMesh->GetValue(), cbTexturize->GetValue(),
		pickerImages->GetPath(), pickerMesh->GetPath(), pickerCameras->GetPath());
	if (b.generateTexture)
	{
		b.outputPath = pickerOutputTexture->GetPath();
	}
	else
	{
		b.outputPath = pickerOutputMesh->GetPath();
	}
	batchItems.push_back(b);
	textQueueSize->SetLabelText("Queue size: " + std::to_string(batchItems.size()));
	cbGenerateMesh->SetValue(false);
	cbTexturize->SetValue(false);
	pickerImages->SetPath("");
	pickerMesh->SetPath("");
	pickerCameras->SetPath("");
	pickerOutputTexture->SetPath("");
	pickerOutputMesh->SetPath("");
	OnCheckBoxs((wxCommandEvent)NULL);
}

void ReconstructionDialog::OnBtBatchStart(wxCommandEvent & WXUNUSED)
{
	disableEndModal = true;
	wxProgressDialog* progDialog = new wxProgressDialog("Reconstructions", "Reconstruction 0", batchItems.size(), this, wxPD_SMOOTH | wxPD_AUTO_HIDE | wxPD_APP_MODAL);
	int i = 0;
	wxBeginBusyCursor();
	for (auto b : batchItems)
	{
		progDialog->Update(i, "Reconstruction " + std::to_string(i));
		i++;
		cbGenerateMesh->SetValue(b.generateMesh);
		cbTexturize->SetValue(b.generateTexture);
		pickerImages->SetPath(b.imagePath);
		if (b.generateTexture)
		{
			pickerMesh->SetPath(b.meshPath);
			pickerCameras->SetPath(b.camerasPath);
			pickerOutputTexture->SetPath(b.outputPath);
		}
		else
		{
			pickerOutputMesh->SetPath(b.outputPath);
		}
		OnOK((wxCommandEvent)NULL);
	}
	//Clear the fields
	cbGenerateMesh->SetValue(false);
	cbTexturize->SetValue(false);
	pickerImages->SetPath("");
	pickerMesh->SetPath("");
	pickerCameras->SetPath("");
	pickerOutputTexture->SetPath("");
	pickerOutputMesh->SetPath("");
	OnCheckBoxs((wxCommandEvent)NULL);
	wxEndBusyCursor();
	delete progDialog;
	disableEndModal = false;
}

void ReconstructionDialog::OnCheckBoxs(wxCommandEvent & WXUNUSED)
{
	if (cbGenerateMesh->IsChecked() && cbTexturize->IsChecked())
	{
		textImages->Enable();
		pickerImages->Enable();
		textOutputMesh->Enable(false);
		pickerOutputMesh->Enable(false);

		textMesh->Enable(false);
		pickerMesh->Enable(false);
		textCameras->Enable(false);
		pickerCameras->Enable(false);
		textOutputTexture->Enable();
		pickerOutputTexture->Enable();
	}
	else
	{
		textImages->Enable(cbGenerateMesh->IsChecked());
		pickerImages->Enable(cbGenerateMesh->IsChecked());
		textOutputMesh->Enable(cbGenerateMesh->IsChecked());
		pickerOutputMesh->Enable(cbGenerateMesh->IsChecked());

		textMesh->Enable(cbTexturize->IsChecked());
		pickerMesh->Enable(cbTexturize->IsChecked());
		textCameras->Enable(cbTexturize->IsChecked());
		pickerCameras->Enable(cbTexturize->IsChecked());
		textOutputTexture->Enable(cbTexturize->IsChecked());
		pickerOutputTexture->Enable(cbTexturize->IsChecked());
	}
}

void ReconstructionDialog::OnDirPickerImages(wxFileDirPickerEvent & WXUNUSED)
{
	wxBeginBusyCursor();
	if (!testImageFolder(pickerImages->GetPath()))
	{
		wxEndBusyCursor();
		pickerImages->SetPath("");
		return;
	}
	wxEndBusyCursor();
	//We need to go back a folder to avoid contaminating the image folder
	wxString reconsPath = pickerImages->GetPath();
	reconsPath = reconsPath.substr(0, reconsPath.find_last_of("\\")) + "\\Reconstruction";
	if (pickerOutputTexture->IsEnabled() && pickerOutputTexture->GetPath() == "" || pickerOutputMesh->IsEnabled() && pickerOutputMesh->GetPath() == "")
	{
		wxString reconsPathTemp = reconsPath;
		int folderNum = 0;
		while (_mkdir(reconsPathTemp))
		{
			folderNum++;
			reconsPathTemp = reconsPath;
			reconsPathTemp << "_" << folderNum;
			if (folderNum == 1000)//If something go really wrong
			{
				return;
			}
		}
		if (pickerOutputTexture->IsEnabled())
		{
			pickerOutputTexture->SetPath(reconsPathTemp + "\\mesh.obj");
		}
		else if (pickerOutputMesh->IsEnabled())
		{
			pickerOutputMesh->SetPath(reconsPathTemp + "\\mesh.ply");
		}
	}
}

void ReconstructionDialog::OnDirPickerMesh(wxFileDirPickerEvent & WXUNUSED)
{
	wxString meshPath = pickerMesh->GetPath();
	meshPath = meshPath.substr(0, meshPath.find_last_of("\\")) + "\\Texturization";
	if (pickerOutputTexture->IsEnabled() && pickerOutputTexture->GetPath() == "")
	{
		wxString meshPathTemp = meshPath;
		int folderNum = 0;
		while (_mkdir(meshPathTemp))
		{
			folderNum++;
			meshPathTemp = meshPath;
			meshPathTemp << "_" << folderNum;
			if (folderNum == 1000)//If something go really wrong
			{
				return;
			}
		}
		pickerOutputTexture->SetPath(meshPathTemp + "\\" + Utils::getFileName(pickerMesh->GetPath().ToStdString(), false) + ".obj");
	}
}

void ReconstructionDialog::OnDirPickerCameras(wxFileDirPickerEvent & WXUNUSED)
{
	wxBeginBusyCursor();
	std::string nvmPath = pickerCameras->GetPath().ToStdString();
	std::vector<std::string> imagePaths;
	if (ImageIO::getCamerasFileImagePaths(nvmPath, imagePaths))
	{
		if (!ImageIO::getImagePathsExist(imagePaths))
		{
			std::string newImageDir;
			wxMessageBox("Please select the correct image folder", "Error", wxICON_ERROR);
			wxDirDialog* imageDialog = new wxDirDialog(this, "Choose the image folder", "", wxDD_DEFAULT_STYLE);
			//Just stop when all the images are in the selected folder
			bool insist = 0;
			do
			{
				if (insist)
				{
					wxMessageBox("The folder still wrong, please select the correct image folder", "Warning", wxICON_WARNING);
				}
				insist = 1;
				if (imageDialog->ShowModal() == wxID_OK)
				{
					newImageDir = imageDialog->GetPath();
				}
				else
				{
					wxLogError("Cameras fule with wrong image paths");
					pickerCameras->SetPath("");
					delete imageDialog;
					return;
				}

			} while (!ImageIO::getImagePathsExist(imagePaths, newImageDir));
			delete imageDialog;
			ImageIO::replaceCamerasFileImageDir(nvmPath, newImageDir);
		}
	}
	else
	{
		wxLogError("Error while loading the cameras file");
		pickerCameras->SetPath("");
	}
	wxEndBusyCursor();
}

void ReconstructionDialog::OnDirPickerOutput(wxFileDirPickerEvent & WXUNUSED)
{
	if (cbTexturize->IsChecked())
	{
		if (pickerOutputTexture->GetPath().find(' ', 0) != wxNOT_FOUND)
		{
			wxLogError("Do not select a folder/filename with spaces in the name");
			pickerOutputTexture->SetPath("");
		}
	}
	else
	{
		if (pickerOutputMesh->GetPath().find(' ', 0) != wxNOT_FOUND)
		{
			wxLogError("Do not select a folder/filename with spaces in the name");
			pickerOutputMesh->SetPath("");
		}
	}

}

bool ReconstructionDialog::testImageFolder(wxString imagePath)
{
	//COLMAP does not accept spaces or special characters in the folder path
	if (imagePath.find(' ', 0) != wxNOT_FOUND)
	{
		wxLogError("Do not select a folder with spaces in the name");
		return false;
	}
	if (imagePath.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_\\/:-()") != std::string::npos)
	{
		wxLogError("Do not select a folder with special characters in the name");
		return false;
	}
	wxDir* directory = new wxDir(imagePath);
	if (!directory->IsOpened())
	{
		wxLogError("Could not open the folder");
		return false;
	}
	wxArrayString files;
	directory->GetAllFiles(imagePath, &files, wxEmptyString, wxDIR_FILES);
	if (files.size() == 0)
	{
		wxLogError("There is no image in the folder");
		return false;
	}
	std::string extension;
	int qtdImages = 0;
	unsigned int width, height;
	for (const auto & file : files)
	{
		extension = Utils::toUpper(Utils::getFileExtension(file.ToStdString()));
		if (extension == "JPG" || extension == "JPEG")
		{
			if (ImageIO::getImageSize(file.ToStdString(), width, height))
			{
				if (qtdImages == 0)
				{
					heightDefault = height;
					widthDefault = width;
				}
				if (height != heightDefault || width != widthDefault)
				{
					wxLogError("The images must have the same resolution");
					return false;
				}
				qtdImages++;
			}
			else
			{
				wxLogError("Error loading the image");
				return false;
			}
		}
	}
	if (qtdImages <= 1)
	{
		wxLogError("You need at least two .jpg images");
		return false;
	}
	return true;
}

//Reconstruction
std::string ReconstructionDialog::checkFields(bool & generateMesh, bool & generateTexture)
{
	if (cbGenerateMesh->IsChecked())
	{
		generateMesh = true;
		if (!cbTexturize->IsChecked())
		{
			if (pickerImages->GetPath() == "" || pickerOutputMesh->GetPath() == "")
			{
				wxLogError("Please fulfill all the fields correctly.");
				return "";
			}
			return pickerOutputMesh->GetPath().ToStdString();
		}
		else
		{
			generateTexture = true;
			if (pickerImages->GetPath() == "" || pickerOutputTexture->GetPath() == "")
			{
				wxLogError("Please fulfill all the fields correctly.");
				return "";
			}
			return pickerOutputTexture->GetPath().ToStdString();
		}
	}
	else if (cbTexturize->IsChecked())
	{
		generateTexture = true;
		if (pickerCameras->GetPath() == "" || pickerMesh->GetPath() == "" || pickerOutputTexture->GetPath() == "")
		{
			wxLogError("Please fulfill all the fields correctly.");
			return "";
		}
		return pickerOutputTexture->GetPath().ToStdString();
	}
	wxLogError("Please select at least one checkbox.");
	return "";
}
bool ReconstructionDialog::runSFM(const std::string & imagesPath, const std::string & nvmPath, ReconstructionLog & log)
{
	log.write("Started SFM", true, true);
	if (!HelperCOLMAP::executeSparse(imagesPath, nvmPath))
	{
		log.write("Error during SFM", true, true);
		return 0;
	}
	log.write("Finished SFM", true, true);
	log.addSeparator();
	return 1;
}
bool ReconstructionDialog::useMeshRecon(const std::string& nvmPath, ReconstructionLog & log)
{
	if (ConfigurationDialog::getForceDenseCOLMAP())
	{
		return false;
	}
	const auto numberOfcameras = ImageIO::GetNumberOfCameras(nvmPath);
	if (numberOfcameras > 200)
	{
		wxLogWarning("Too many cameras to MeshRecon, using COLMAP", "Warning");
		log.write("Too many cameras to MeshRecon, using COLMAP");
		return false;
	}
	else //Memory check
	{
		GLint nCurAvailMemoryInKB = 0;
		glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &nCurAvailMemoryInKB);
		double memoryNeeded = widthDefault * heightDefault * numberOfcameras / 1000;
		int levelOfDetailsAllowed = -1;
		std::string levelOfDetailsAllowedString = "";
		if (nCurAvailMemoryInKB / 1024 > memoryNeeded * 0.025)
		{
			levelOfDetailsAllowed = 0;
			levelOfDetailsAllowedString = "high";
		}
		else if (nCurAvailMemoryInKB / 1024 > memoryNeeded * 0.008)
		{
			levelOfDetailsAllowed = 1;
			levelOfDetailsAllowedString = "medium";
		}
		else if (nCurAvailMemoryInKB / 1024 > memoryNeeded * 0.003)
		{
			levelOfDetailsAllowed = 2;
			levelOfDetailsAllowedString = "low";
		}
		if (std::atoi(ConfigurationDialog::getLevelOfDetails().c_str()) < levelOfDetailsAllowed && !ConfigurationDialog::getForceLevelOfDetails())
		{
			if (levelOfDetailsAllowed >= 0)
			{
				wxLogWarning("Not enough RAM to use this level of details, lowering the settings to: %s", levelOfDetailsAllowedString);
				log.write("Not enough RAM to use this level of details, lowering the settings to: " + levelOfDetailsAllowedString);
				ConfigurationDialog::setLevelOfDetails(levelOfDetailsAllowed);
				return true;
			}
			else
			{
				wxLogWarning("Not enough RAM to use MeshRecon, using COLMAP", "Warning");
				log.write("Not enough RAM to use MeshRecon, using COLMAP");
				return false;
			}
		}
	}
	return true;
}

bool ReconstructionDialog::runMeshing(const std::string & imagesPath, const std::string & nvmPath, ReconstructionLog & log)
{
	//MeshRecon
	if (useMeshRecon(nvmPath, log))
	{
		log.write("Started NVM2SFM", true, true);
		std::string sfmPath = nvmPath.substr(0, nvmPath.size() - 4) + ".sfm";
		if (!HelperMeshRecon::executeNVM2SFM(nvmPath, sfmPath, ConfigurationDialog::getBoundingBox()))
		{
			log.write("Error during NVM2SFM", true, true);
			return 0;
		}
		log.write("Finished NVM2SFM", true, true);
		log.addSeparator();
		log.write("Started MeshRecon", true, true);
		if (!HelperMeshRecon::executeMeshRecon(wxPathOnly(sfmPath).ToStdString(), sfmPath, sfmPath, ConfigurationDialog::getLevelOfDetails()))
		{
			log.write("Error during MeshRecon", true, true);
			return 0;
		}
		log.write("Finished MeshRecon", true, true);
		log.addSeparator();
	}
	else //COLMAP
	{
		log.write("Started COLMAP dense reconstruction", true, true);
		if (!HelperCOLMAP::executeDense(imagesPath, nvmPath))
		{
			log.write("Error during COLMAP dense reconstruction", true, true);
			return 0;
		}
		log.write("Finished COLMAP dense reconstruction", true, true);
		log.addSeparator();
		//Move the file!
		std::string originalPLY = vtksys::SystemTools::GetFilenamePath(nvmPath) + "/dense/0/meshed-" + ConfigurationDialog::getMesher() + ".ply";
		std::wstring stemp = Utils::s2ws(originalPLY);
		LPCWSTR original = stemp.c_str();

		std::string newPLY = nvmPath.substr(0, nvmPath.size() - 4) + "_refine.ply";
		std::wstring stemp2 = Utils::s2ws(newPLY);
		LPCWSTR newPath = stemp2.c_str();

		if (!MoveFile(original, newPath))
		{
			wxLogError("MoveFile failed (%d)", GetLastError());
			log.write("MoveFile failed", true, true);
			return 0;
		}
	}
	return 1;
}

//Texturization
bool ReconstructionDialog::runTexturization(const std::string & meshPath, const std::string & camerasPath, const std::string & outputPath, ReconstructionLog & log)
{
	//TexRecon
	if (ConfigurationDialog::getForceTexReconTexture())
	{
		log.write("Started TexRecon", true, true);
		if (!HelperTexRecon::executeTexRecon(camerasPath, meshPath, outputPath, ConfigurationDialog::getTexReconOptions()))
		{
			log.write("Error during TexRecon", true, true);
			return 0;
		}
		log.write("Finished TexRecon", true, true);
	}
	else//AVTexturing
	{
		wxBeginBusyCursor();
		log.write("Started AVTexturing", true, true);
		if (!HelperAVTexturing::computeAVTexturization(meshPath, camerasPath, outputPath))
		{
			log.write("Error during AVTexturing", true, true);
			wxEndBusyCursor();
			return 0;
		}
		log.write("Finished AVTexturing", true, true);
		wxEndBusyCursor();
	}
	log.addSeparator();
	return 1;
}