#pragma once
#include <vtkSmartPointer.h>
#include <wx/treelist.h>

class vtkActor;
class vtkRenderer;
class vtkTransform;
class vtkCaptionActor2D;
class vtkPolyData;

class Volume
{
private:
	vtkSmartPointer<vtkActor> actor = nullptr;
	vtkSmartPointer<vtkCaptionActor2D> textActor = nullptr;
	wxTreeListItem listItem = nullptr;
	bool visible;
	//Index of the volume in the tree(Just to identify it, not the index of the vector)
	unsigned int index;
	double volume;
	vtkSmartPointer<vtkPolyData> holePolyData = nullptr;

	/*
	Compute the volume of a polyData
	*/
	double computeVolume(vtkSmartPointer<vtkPolyData> polyData, double scalarFactor);
	

public:
	Volume(vtkSmartPointer<vtkActor> actor, vtkSmartPointer<vtkCaptionActor2D> textActor,
		unsigned int index, double scaleFactor, const std::string& measureUnit, vtkSmartPointer<vtkPolyData> holePolyData = nullptr);
	~Volume() {};

	/*
	Should be called before the delete, it remove the actors from the scene and the listItem from the tree
	*/
	void destruct(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree);

	void setVisibility(bool visibility);
	bool getVisibility() const { return visible; };

	void setListItem(const wxTreeListItem& item) { listItem = item; };
	wxTreeListItem getListItem() const { return listItem; };

	/*
	Update the measure unit
	*/
	void updateText(const std::string&  measureUnit);

	/*
	Re-calculate the volume and update the measure unit
	*/
	void updateCalibration(double scalarFactor, const std::string& measureUnit);

	/*
	Tranform the mesh using T
	*/
	void transform(vtkSmartPointer<vtkTransform> T);

	double* getActorBounds();
};