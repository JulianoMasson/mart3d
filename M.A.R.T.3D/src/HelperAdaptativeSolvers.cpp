#include "HelperAdaptativeSolvers.h"
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkIdTypeArray.h>

vtkSmartPointer<vtkPolyData> HelperAdaptativeSolvers::computePoisson(vtkSmartPointer<vtkPolyData> polyData, const PoissonRecon::Options & options)
{
	PoissonRecon poisson;
	AdaptativeSolvers::Mesh<float> mesh = polyData2AdaptativeSolvers<float>(polyData);
	if (!poisson.Compute(mesh, options))
	{
		return adaptativeSolvers2polyData(mesh);
	}
	return nullptr;
}

vtkSmartPointer<vtkPolyData> HelperAdaptativeSolvers::computeSSD(vtkSmartPointer<vtkPolyData> polyData, const SSDRecon::Options & options)
{
	SSDRecon ssdRecon;
	AdaptativeSolvers::Mesh<float> mesh = polyData2AdaptativeSolvers<float>(polyData);
	if (!ssdRecon.Compute(mesh, options))
	{
		return adaptativeSolvers2polyData(mesh);
	}
	return nullptr;
}

vtkSmartPointer<vtkPolyData> HelperAdaptativeSolvers::computeSurfaceTrimmer(vtkSmartPointer<vtkPolyData> polyData, const SurfaceTrimmer::Options & options)
{
	if (polyData->GetPointData())
	{
		if (!polyData->GetPointData()->GetArray("Values"))
		{
			return nullptr;
		}
	}
	SurfaceTrimmer surfaceTrimmer;
	//Surface trimmer does not support double
	AdaptativeSolvers::Mesh<float> mesh = polyData2AdaptativeSolvers<float>(polyData, true);
	surfaceTrimmer.Compute(mesh, options);
	return adaptativeSolvers2polyData(mesh);
}

template<typename T>
AdaptativeSolvers::Mesh<T> HelperAdaptativeSolvers::polyData2AdaptativeSolvers(vtkSmartPointer<vtkPolyData> polyData, const bool& getFaces)
{
	const auto points = polyData->GetPoints();
	const auto colors = polyData->GetPointData()->GetScalars();
	auto getNumberOfArrayComponents = [](vtkSmartPointer<vtkDataArray> dataArray)
	{
		if (dataArray)
		{
			return dataArray->GetNumberOfComponents();
		}
		return 1;
	};
	const auto numberOfColorChannels = getNumberOfArrayComponents(colors);
	const auto normals = polyData->GetPointData()->GetNormals();
	const auto values = polyData->GetPointData()->GetArray("Values");
	const auto numberOfPoints = polyData->GetPoints()->GetNumberOfPoints();
	AdaptativeSolvers::Mesh<T> mesh;
	mesh.points.resize(numberOfPoints);
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		double x[3], normal[3], value[1];
		double* color = new double[numberOfColorChannels];
		points->GetPoint(i, x);
		if (colors)
		{
			colors->GetTuple(i, color);
		}
		if (normals)
		{
			normals->GetTuple(i, normal);
		}
		AdaptativeSolvers::Point<T> point;
		if (values)
		{
			values->GetTuple(i, value);
			point.value = value[0];
		}
		for (size_t j = 0; j < 3; j++)
		{
			point.xyz[j] = x[j];
			if (colors)
			{
				point.color[j] = color[j];
			}
			if (normals)
			{
				point.normal[j] = normal[j];
			}
		}
		delete[] color;
		mesh.points[i] = point;
	}
	if (getFaces)
	{
		auto offsetArray = polyData->GetPolys()->GetOffsetsArray();
		auto idsArray = polyData->GetPolys()->GetConnectivityArray();
		const auto numberOfCells = polyData->GetPolys()->GetNumberOfCells();
		mesh.faces.resize(numberOfCells);
#pragma omp parallel for
		for (int i = 0; i < numberOfCells; i++)
		{
			double initialOffset[1], vertexIndex[1];
			offsetArray->GetTuple(i, initialOffset);
			AdaptativeSolvers::Face face;
			face.point_indices.reserve(3);
			for (size_t j = 0; j < 3; j++)
			{
				idsArray->GetTuple(initialOffset[0] + j, vertexIndex);
				face.point_indices.emplace_back(vertexIndex[0]);
			}
			mesh.faces[i] = face;
		}
	}
	mesh.has_normal = (normals != nullptr);
	mesh.has_color = (colors != nullptr);
	mesh.has_value = (values != nullptr);
	return mesh;
}

template<typename T>
inline vtkSmartPointer<vtkPolyData> HelperAdaptativeSolvers::adaptativeSolvers2polyData(const AdaptativeSolvers::Mesh<T>& mesh)
{
	const auto numberOfPoints = mesh.points.size();
	vtkNew<vtkPoints> points;
	if (std::is_same<T, double>::value)
	{
		points->SetDataTypeToDouble();
	}
	points->SetNumberOfPoints(numberOfPoints);
	vtkSmartPointer<vtkDataArray> normals = nullptr;
	if (mesh.has_normal)
	{
		if (std::is_same<T, double>::value)
		{
			normals = vtkDataArray::CreateDataArray(VTK_DOUBLE);
		}
		else
		{
			normals = vtkDataArray::CreateDataArray(VTK_FLOAT);
		}
		normals->SetNumberOfComponents(3);
		normals->SetNumberOfTuples(numberOfPoints);
		normals->SetName("Normals");
	}
	vtkNew<vtkUnsignedCharArray> colors;
	if (mesh.has_color)
	{
		colors->SetNumberOfComponents(3);
		colors->SetNumberOfTuples(numberOfPoints);
		colors->SetName("RGB");
	}
	vtkNew<vtkFloatArray> values;
	if (mesh.has_value)
	{
		values->SetNumberOfComponents(1);
		values->SetNumberOfTuples(numberOfPoints);
		values->SetName("Values");
	}
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		const AdaptativeSolvers::Point<T> point = mesh.points[i];
		points->InsertPoint(i, point.xyz);
		if (mesh.has_normal)
		{
			normals->InsertTuple3(i, point.normal[0], point.normal[1], point.normal[2]);
		}
		if (mesh.has_color)
		{
			colors->InsertTuple3(i, point.color[0], point.color[1], point.color[2]);
		}
		if (mesh.has_value)
		{
			values->InsertTuple1(i, point.value);
		}
	}
	//Write faces
	vtkNew<vtkIdTypeArray> newOffsets;
	vtkNew<vtkIdTypeArray> newConnectivity;
	const auto newNumberOfTriangles = mesh.faces.size();
	if (newNumberOfTriangles != 0)
	{
		newOffsets->SetNumberOfComponents(1);
		// The last position should be the lenght of the connectivity array
		newOffsets->SetNumberOfTuples(newNumberOfTriangles + 1);
		newConnectivity->SetNumberOfComponents(1);
		newConnectivity->SetNumberOfTuples(newNumberOfTriangles * 3);
#pragma omp parallel for
		for (int i = 0; i < newNumberOfTriangles; i++)
		{
			const auto face = mesh.faces[i];
			newOffsets->InsertTuple1(i, i * 3);
			for (int j = 0; j < 3; j++)
			{
				newConnectivity->InsertTuple1((i * 3) + j, face.point_indices[j]);
			}
		}
		newOffsets->InsertTuple1(newNumberOfTriangles, newConnectivity->GetNumberOfTuples());
	}
	// Create the resulting mesh
	vtkNew<vtkPolyData> polyData;
	polyData->SetPoints(points);
	if (mesh.has_normal)
	{
		polyData->GetPointData()->SetNormals(normals);
	}
	if (mesh.has_color)
	{
		polyData->GetPointData()->SetScalars(colors);
	}
	if (mesh.has_value)
	{
		polyData->GetPointData()->AddArray(values);
	}
	if (newNumberOfTriangles != 0)
	{
		vtkNew<vtkCellArray> triangles;
		triangles->SetData(newOffsets, newConnectivity);
		polyData->SetPolys(triangles);
	}
	return polyData;
}