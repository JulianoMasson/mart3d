#include "ElevationTool.h"

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkXYPlotActor.h>
#include <vtkSliderWidget.h>
#include <vtkSliderRepresentation.h>
#include <vtkPropPicker.h>
#include <vtkActor.h>
#include <vtkMapper.h>
#include <vtkCellLocator.h>
#include <vtkElevationFilter.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkCutter.h>
#include <vtkStripper.h>
#include <vtkSortDataArray.h>
#include <vtkAxisActor2D.h>
#include <vtkPlane.h>
#include <vtkPlanes.h>
#include <vtkClipPolyData.h>
#include <vtkFloatArray.h>
#include <vtkCaptionActor2D.h>

#include <wx/msgdlg.h>

#include "LineWidget.h"
#include "SliderRepresentation2D.h"
#include "Draw.h"
#include "Utils.h"
#include "Calibration.h"
#include "Mesh.h"

vtkStandardNewMacro(ElevationTool);

//----------------------------------------------------------------------------
ElevationTool::ElevationTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(ElevationTool::ProcessEvents);
}

//----------------------------------------------------------------------------
ElevationTool::~ElevationTool()
{
	destruct();
}

void ElevationTool::destruct()
{
	if (textActor)
	{
		this->CurrentRenderer->RemoveActor2D(textActor);
		textActor = nullptr;
	}
	if (elevationMapActor)
	{
		this->CurrentRenderer->RemoveActor(elevationMapActor);
		elevationMapActor = nullptr;
	}
	if (plotElevation)
	{
		this->CurrentRenderer->RemoveActor2D(plotElevation);
		plotElevation = nullptr;
	}
	if (sliderWidget)
	{
		sliderWidget->EnabledOff();
		sliderWidget = nullptr;
	}
	propPicker = nullptr;
	outputElevationFilter = nullptr;
	elevationFilter = nullptr;
	setZeroByMouse = false;
	//ElevationProfile
	lineWidget = nullptr;
	if (elevationProfileActor)
	{
		this->CurrentRenderer->RemoveActor(elevationProfileActor);
		elevationProfileActor = nullptr;
	}
	if (mesh)
	{
		mesh->SetVisibility(treeMesh->GetCheckedState(mesh->GetTreeItem()));
		mesh = nullptr;
	}
	calibration = nullptr;
}

void ElevationTool::setMesh(Mesh * mesh)
{
	if (!mesh)
	{
		return;
	}
	this->mesh = mesh;
	vtkSmartPointer<vtkPolyData> polyMesh = mesh->GetPolyData();
	polyMesh->GetBounds(bounds);
	elevationFilter = vtkSmartPointer<vtkElevationFilter>::New();
	elevationFilter->SetInputData(polyMesh);
	//To avoid the bad vector problem with the elevation filter
	if (bounds[5] < 0)
	{
		elevationFilter->SetLowPoint(0.0, 0.0, bounds[5] * 0.99999);
	}
	else
	{
		elevationFilter->SetLowPoint(0.0, 0.0, bounds[5] * 1.00001);
	}
	elevationFilter->SetHighPoint(0.0, 0.0, bounds[4]);
	elevationFilter->Update();
	outputElevationFilter = vtkPolyData::SafeDownCast(elevationFilter->GetOutput());
	updateElevationMap();
}

//----------------------------------------------------------------------------
void ElevationTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh || !treeMesh || !calibration) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		// listen for the following events
		vtkRenderWindowInteractor *i = this->Interactor;
		i->AddObserver(vtkCommand::MiddleButtonPressEvent,
			this->EventCallbackCommand, this->Priority);
		i->AddObserver(vtkCommand::KeyPressEvent,
			this->EventCallbackCommand, this->Priority);

		//Actors
		if (outputElevationFilter)
		{
			elevationMapActor = Draw::createPolyData(this->CurrentRenderer, outputElevationFilter);
		}
		//Picking stuff
		if (!propPicker)
		{
			this->propPicker = vtkSmartPointer<vtkPropPicker>::New();
			propPicker->AddPickList(elevationMapActor);
			propPicker->PickFromListOn();
		}
		createSlider();

		// The profile line only works with meshes
		if (!mesh->GetIsPointCloud())
		{
			if (!lineWidget)
			{
				lineWidget = vtkSmartPointer<LineWidget>::New();
				lineWidget->SetInteractor(this->Interactor);
				lineWidget->setMaxNumberOfNodes(2);
				mesh->SetVisibility(false);
				vtkNew<LineWidgetRepresentation> rep;
				rep->addProp(elevationMapActor);
				lineWidget->SetRepresentation(rep);
			}
			lineWidget->EnabledOn();
			lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
		}

		mesh->SetVisibility(false);

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);
		sliderWidget->RemoveObserver(this->EventCallbackCommand);

		//ElevationProfile
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
			if (elevationProfileActor)
			{
				this->CurrentRenderer->RemoveActor(elevationProfileActor);
				elevationProfileActor = nullptr;
			}
			if (plotElevation)
			{
				this->CurrentRenderer->RemoveActor2D(plotElevation);
				plotElevation = nullptr;
			}
		}
		destruct();

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void ElevationTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	ElevationTool* self =
		reinterpret_cast<ElevationTool *>(clientdata);


	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::MiddleButtonPressEvent:
		self->OnMiddleButtonDown();
		break;
	case vtkCommand::KeyPressEvent:
		self->OnKeyPressed();
		break;
	case vtkCommand::EndInteractionEvent:
		self->updateElevationMap();
		self->createElevationProfileLine();
		break;
	default:
		break;
	}
}

void ElevationTool::updateElevationMap()
{
	if (!sliderWidget)
	{
		return;
	}
	auto slider = static_cast<vtkSliderRepresentation *>(sliderWidget->GetRepresentation());
	// The slider was not changed, we dont need to update
	if (lastSliderValue == slider->GetValue())
	{
		return;
	}
	lastSliderValue = slider->GetValue();
	wxBeginBusyCursor();
	elevationFilter->SetHighPoint(0.0, 0.0, slider->GetValue() + bounds[4]);
	elevationFilter->Update();
	if (textActor)
	{
		textActor->SetCaption(
			calibration->getCalibratedText(getPointElevation(textActor->GetAttachmentPoint())).c_str());
	}
	wxEndBusyCursor();
}

//----------------------------------------------------------------------------
void ElevationTool::OnMiddleButtonDown()
{
	double pickedPosition[3];
	if (!getMousePosition(pickedPosition))
	{
		return;
	}
	if (textActor)
	{
		CurrentRenderer->RemoveActor2D(textActor);
		textActor = nullptr;
	}
	if (setZeroByMouse)
	{
		auto rep = (static_cast<vtkSliderRepresentation *>(sliderWidget->GetRepresentation()));
		rep->SetValue(pickedPosition[2] - bounds[4]);
		setZeroByMouse = false;
		updateElevationMap();
		createElevationProfileLine();
	}
	else
	{
		//Text
		textActor = Draw::createNumericIndicator(
			CurrentRenderer, pickedPosition,
			calibration->getCalibratedText(getPointElevation(pickedPosition)),
			24, 1.0, 1.0, 1.0);
	}
	this->Interactor->Render();
}

void ElevationTool::OnKeyPressed()
{
	char key = this->Interactor->GetKeyCode();
	if (this->Interactor->GetControlKey() && key == 'Y')
	{
		setZeroByMouse = !setZeroByMouse;
	}
}

void ElevationTool::createSlider()
{
	if (!outputElevationFilter)
	{
		return;
	}
	vtkNew<SliderRepresentation2D> sliderRepresentation;
	sliderRepresentation->SetMinimumValue(0);
	sliderRepresentation->SetMaximumValue(bounds[5] - bounds[4]);
	sliderRepresentation->SetMinLimitText(calibration->getCalibratedText(sliderRepresentation->GetMinimumValue()).c_str());
	sliderRepresentation->SetMaxLimitText(calibration->getCalibratedText(sliderRepresentation->GetMaximumValue()).c_str());
	sliderRepresentation->SetValue(sliderRepresentation->GetMinimumValue());
	sliderRepresentation->SetTitleText("Elevation");
	sliderRepresentation->SetLabelFormat(("%.3f" + calibration->getMeasureUnit()).c_str());
	sliderRepresentation->setCalibration(calibration);

	sliderWidget = vtkSmartPointer<vtkSliderWidget>::New();
	sliderWidget->SetInteractor(this->Interactor);
	sliderWidget->SetRepresentation(sliderRepresentation);
	sliderWidget->SetAnimationModeToJump();
	sliderWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
	sliderWidget->EnabledOn();
	lastSliderValue = sliderRepresentation->GetValue();
}

int ElevationTool::getMousePosition(double point[3])
{
	if (propPicker->Pick(Interactor->GetEventPosition()[0], Interactor->GetEventPosition()[1], 0, CurrentRenderer))
	{
		propPicker->GetPickPosition(point);
		return 1;
	}
	return 0;
}

void ElevationTool::createElevationProfileLine()
{
	if (!lineWidget || lineWidget->getWidgetState() != LineWidget::Finished)
	{
		return;
	}
	if (plotElevation)
	{
		this->CurrentRenderer->RemoveActor2D(plotElevation);
		plotElevation = nullptr;
	}
	if (elevationProfileActor)
	{
		this->CurrentRenderer->RemoveActor(elevationProfileActor);
		elevationProfileActor = nullptr;
	}
	auto pointsLine = lineWidget->GetRepresentation()->getPoints();
	if (!pointsLine)
	{
		return;
	}
	wxBeginBusyCursor();
	double point0[3];
	double point1[3];
	pointsLine->GetPoint(0, point0);
	pointsLine->GetPoint(1, point1);
	vtkNew<vtkPlane> plane;
	// Find the normal
	double n[3];
	double pointUp[3] = { point0[0], point0[1], point0[2] + 1.0 };
	Utils::getNormal(point0, point1, pointUp, n);
	plane->SetOrigin(point0);
	plane->SetNormal(n);
	// Cut the surface
	vtkNew<vtkCutter> cutter;
	cutter->SetCutFunction(plane);
	cutter->SetInputData(vtkPolyData::SafeDownCast(elevationMapActor->GetMapper()->GetInput()));
	cutter->Update();
	// Create the planes used to cut the line
	vtkNew<vtkPlanes> planes;
	vtkNew<vtkPoints> planePoints;
	vtkNew<vtkFloatArray> planeNormals;
	planeNormals->SetNumberOfComponents(3);
	vtkMath::Subtract(point0, point1, n);
	vtkMath::Normalize(n);
	for (size_t i = 0; i < 2; i++)
	{
		if (i == 1)
		{
			vtkMath::MultiplyScalar(n, -1);
		}
		planePoints->InsertNextPoint(pointsLine->GetPoint(i));
		planeNormals->InsertNextTuple(n);
	}
	planes->SetPoints(planePoints);
	planes->SetNormals(planeNormals);
	//DEBUG
	//for (int i = 0; i < planes->GetNumberOfPlanes(); i++)
	//{
	//	Draw::createPlane(CurrentRenderer, planes->GetPlane(i)->GetOrigin(),
	//		planes->GetPlane(i)->GetNormal(), 255, 0, 0);
	//}
	// Cut the line
	vtkNew<vtkClipPolyData> clipPolyData;
	clipPolyData->SetClipFunction(planes);
	clipPolyData->InsideOutOn();
	clipPolyData->SetInputData(cutter->GetOutput());
	clipPolyData->Update();

	vtkNew<vtkCellLocator> cellLocator;
	cellLocator->SetDataSet(clipPolyData->GetOutput());
	cellLocator->BuildLocator();

	double direction[3];
	vtkMath::Subtract(point1, point0, direction);
	vtkMath::Normalize(direction);

	const auto lineLength = std::sqrt(vtkMath::Distance2BetweenPoints(point0, point1));
	const int numberOfSamples = 1000;
	vtkMath::MultiplyScalar(direction, lineLength / numberOfSamples);
	vtkNew<vtkPoints> finalPoints;
	for (int i = 2; i < numberOfSamples-2; i++)
	{
		double pointLine0[3] = { point0[0] + i * direction[0],
			point0[1] + i * direction[1],
			bounds[5] };
		double pointLine1[3] = { pointLine0[0], pointLine0[1], bounds[4] };
		double t;
		int subId;
		double x[3], pcoord[3];
		if (cellLocator->IntersectWithLine(pointLine0, pointLine1, 1e-4, t, x, pcoord, subId))
		{
			finalPoints->InsertNextPoint(x);
		}
		else
		{
			finalPoints->InsertNextPoint(pointLine1);
		}
	}
	createPlot(finalPoints, point0, point1);

	// Create plane actor
	elevationProfileActor = Draw::create3DLine(this->CurrentRenderer, finalPoints);
	elevationProfileActor->GetMapper()->SetRelativeCoincidentTopologyLineOffsetParameters(0, -66000);
	elevationProfileActor->GetMapper()->Update();
	this->CurrentRenderer->Render();
	wxEndBusyCursor();
}

void ElevationTool::createPlot(vtkSmartPointer<vtkPoints> points, double point0Line[3], double point1Line[3])
{
	const auto numberOfPoints = points->GetNumberOfPoints();
	vtkNew<vtkFloatArray> xAxis;
	xAxis->SetNumberOfComponents(1);
	xAxis->SetNumberOfTuples(numberOfPoints);
	vtkNew<vtkFloatArray> yAxis;
	yAxis->SetNumberOfComponents(1);
	yAxis->SetNumberOfTuples(numberOfPoints);
	double* initialPoint;
	if (point0Line[0] < point1Line[0])
	{
		initialPoint = point0Line;
	}
	else
	{
		initialPoint = point1Line;
	}
#pragma omp parallel for
	for (vtkIdType i = 0; i < numberOfPoints; i++)
	{
		double point[3];
		points->GetPoint(i, point);
		xAxis->SetTuple1(i,
			calibration->getCalibratedDistance(Utils::distanceBetween2DisplayPoints(initialPoint, point)));
		yAxis->SetTuple1(i,
			calibration->getCalibratedDistance(getPointElevation(point)));
	}

	vtkNew<vtkFieldData> fieldData;
	fieldData->AllocateArrays(2);
	fieldData->AddArray(xAxis);
	fieldData->AddArray(yAxis);

	vtkNew<vtkDataObject> dataObject;
	dataObject->SetFieldData(fieldData);

	plotElevation = vtkSmartPointer<vtkXYPlotActor>::New();
	plotElevation->AddDataObjectInput(dataObject);
	plotElevation->SetTitle("");
	plotElevation->SetXTitle("");
	plotElevation->SetYTitle("");
	plotElevation->SetXValuesToValue();
	plotElevation->SetGlyphSize(0.05);
	plotElevation->SetWidth(0.4);
	plotElevation->SetHeight(0.4);
	plotElevation->SetPosition(0.01, 0.58);
	plotElevation->PickableOff();
	plotElevation->PlotCurvePointsOn();
	plotElevation->PlotCurveLinesOn();
	plotElevation->SetDataObjectXComponent(0, 0);
	plotElevation->SetDataObjectYComponent(0, 1);
	plotElevation->SetPlotColor(0, 1.0, 0.0, 0.0);
	plotElevation->GetXAxisActor2D()->SetLabelFormat("%.2f");
	plotElevation->GetXAxisActor2D()->SetFontFactor(0.8);
	plotElevation->GetYAxisActor2D()->SetFontFactor(0.8);
	this->CurrentRenderer->AddActor2D(plotElevation);
}

double ElevationTool::getPointElevation(double point[3])
{
	if (!sliderWidget)
	{
		return -1;
	}
	auto rep = (static_cast<vtkSliderRepresentation *>(sliderWidget->GetRepresentation()));
	return (point[2] - bounds[4]) - rep->GetValue();
}