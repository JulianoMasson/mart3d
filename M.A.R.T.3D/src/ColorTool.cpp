#include "ColorTool.h"

#include <utility>

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkNew.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkInformation.h>
#include <vtkPointData.h>
#include <vtkPolygon.h>
#include <vtkFloatArray.h>
#include <vtkCenterOfMass.h>
#include <vtkPlanes.h>
#include <vtkIdFilter.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkExtractSelectedIds.h>
#include <vtkActor.h>
#include <vtkIdTypeArray.h>
#include <vtkSliderWidget.h>

#include <wx/msgdlg.h>

#include "LineWidget.h"
#include "SliderRepresentation2D.h"
#include "Mesh.h"
#include "Utils.h"
#include "Draw.h"

vtkStandardNewMacro(ColorTool);

//----------------------------------------------------------------------------
ColorTool::ColorTool() : vtk3DWidget()
{
	this->EventCallbackCommand->SetCallback(ColorTool::ProcessEvents);
}

//----------------------------------------------------------------------------
ColorTool::~ColorTool()
{
	if (lineWidget)
	{
		lineWidget->EnabledOff();
		lineWidget->RemoveObserver(this->EventCallbackCommand);
		lineWidget = nullptr;
	}
	if (selectionActor)
	{
		CurrentRenderer->RemoveActor(selectionActor);
	}
	if (sliderAngle)
	{
		sliderAngle->EnabledOff();
		sliderAngle->RemoveObserver(this->EventCallbackCommand);
		sliderAngle = nullptr;
	}
	if (sliderDistance)
	{
		sliderDistance->EnabledOff();
		sliderDistance->RemoveObserver(this->EventCallbackCommand);
		sliderDistance = nullptr;
	}
	selection = nullptr;
	mesh = nullptr;
}

//----------------------------------------------------------------------------
void ColorTool::SetEnabled(int enabling)
{
	if (!this->Interactor)
	{
		return;
	}

	if (enabling) //------------------------------------------------------------
	{
		if (this->Enabled || !mesh) //already enabled, just return
		{
			return;
		}
		if (!this->CurrentRenderer)
		{
			this->SetCurrentRenderer(this->Interactor->FindPokedRenderer(
				this->Interactor->GetLastEventPosition()[0],
				this->Interactor->GetLastEventPosition()[1]));
			if (!this->CurrentRenderer)
			{
				return;
			}
		}
		this->Enabled = 1;

		// listen for the following events
		vtkRenderWindowInteractor *i = this->Interactor;
		i->AddObserver(vtkCommand::LeftButtonPressEvent,
			this->EventCallbackCommand, this->Priority);

		if (!lineWidget)
		{
			lineWidget = vtkSmartPointer<LineWidget>::New();
			lineWidget->SetInteractor(this->Interactor);
			lineWidget->setCloseLoopOnFirstNode(true);
			vtkNew<LineWidgetRepresentation> rep;
			rep->set2DRepresentation(true);
			lineWidget->SetRepresentation(rep);
		}
		lineWidget->EnabledOn();
		lineWidget->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);

		CreateSliders();

		this->InvokeEvent(vtkCommand::EnableEvent, nullptr);
	}

	else //disabling----------------------------------------------------------
	{
		if (!this->Enabled) //already disabled, just return
		{
			return;
		}
		this->Enabled = 0;

		// don't listen for events any more
		this->Interactor->RemoveObserver(this->EventCallbackCommand);

		// turn off the various actors
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
		}
		if(selectionActor)
		{
			CurrentRenderer->RemoveActor(selectionActor);
			selectionActor = nullptr;
		}
		sliderAngle->EnabledOff();
		sliderAngle->RemoveObserver(this->EventCallbackCommand);
		sliderAngle = nullptr;
		sliderDistance->EnabledOff();
		sliderDistance->RemoveObserver(this->EventCallbackCommand);
		sliderDistance = nullptr;
		selection = nullptr;
		mesh = nullptr;

		this->InvokeEvent(vtkCommand::DisableEvent, nullptr);
		this->SetCurrentRenderer(nullptr);
	}

	this->Interactor->Render();
}

//----------------------------------------------------------------------------
void ColorTool::ProcessEvents(vtkObject* vtkNotUsed(object),
	unsigned long event,
	void* clientdata,
	void* vtkNotUsed(calldata))
{
	ColorTool* self = reinterpret_cast<ColorTool*>(clientdata);

	//okay, let's do the right thing
	switch (event)
	{
	case vtkCommand::EndInteractionEvent:
		self->UpdateRepresentation();
		break;
	case vtkCommand::LeftButtonPressEvent://Do not let the user rotate the camera
	{
		if (self->lineWidget)
		{
			if (self->lineWidget->GetRepresentation()->getPoints())
			{
				self->EventCallbackCommand->SetAbortFlag(1);
			}
		}
		break;
	}
	}
}

vtkSmartPointer<vtkPlanes> ColorTool::getSelectionFrustum()
{
	vtkSmartPointer<LineWidgetRepresentation> rep = lineWidget->GetRepresentation();
	if (!rep->isLoopClosed())
	{
		return nullptr;
	}
	//Get points
	vtkSmartPointer<vtkPoints> pointsLineWidget = rep->getPoints();
	size_t numberOfPoints = pointsLineWidget->GetNumberOfPoints() - 1;//The last point is equal to the first
	//Test to see if it is convex
	vtkNew<vtkIdTypeArray> ids;
	ids->SetNumberOfComponents(1);
	for (size_t i = 0; i < numberOfPoints; i++)
	{
		ids->InsertNextTuple1(i);
	}
	if (!vtkPolygon::IsConvex(ids, pointsLineWidget))
	{
		wxMessageBox("The selection must be convex", "Error", wxICON_ERROR);
		rep->reset();
		lineWidget->EnabledOff();
		lineWidget->EnabledOn();
		this->Interactor->Render();
		return nullptr;
	}
	vtkNew<vtkPoints> pointsFrustum;
	for (size_t i = 0; i < 2; i++)
	{
		for (size_t j = 0; j < numberOfPoints; j++)
		{
			double display[3];
			Utils::getDisplayPosition(CurrentRenderer, pointsLineWidget->GetPoint(j), display);
			display[2] = i;
			double point[4];
			CurrentRenderer->SetDisplayPoint(display);
			CurrentRenderer->DisplayToWorld();
			CurrentRenderer->GetWorldPoint(point);
			pointsFrustum->InsertNextPoint(point[0], point[1], point[2]);
		}
	}
	//Get the centroid
	vtkNew<vtkCenterOfMass> centerOfMass;
	vtkNew<vtkPolyData> tempPolyData;
	tempPolyData->SetPoints(pointsFrustum);
	centerOfMass->SetInputData(tempPolyData);
	centerOfMass->Update();
	double frustumCentroid[3] = { 0, 0, 0 };
	centerOfMass->GetCenter(frustumCentroid);
	//Create the implict function
	vtkNew<vtkPoints> planesPoints;
	vtkNew<vtkFloatArray> normalsPoints;
	normalsPoints->SetNumberOfComponents(3);
	for (int i = 0; i < 2; i++)//The top and bottom planes
	{
		planesPoints->InsertNextPoint(pointsFrustum->GetPoint(i + i * numberOfPoints));
		double n[3], pointA[3], pointB[3], pointC[3];
		pointsFrustum->GetPoint(numberOfPoints - 1 + (i * numberOfPoints), pointA);
		pointsFrustum->GetPoint(i + (i * numberOfPoints), pointB);
		pointsFrustum->GetPoint(i + 1 + (i * numberOfPoints), pointC);
		Utils::getNormal(pointA, pointB, pointC, frustumCentroid, n);
		vtkMath::MultiplyScalar(n, -1);
		normalsPoints->InsertNextTuple3(n[0], n[1], n[2]);
	}
	for (int i = 0; i < numberOfPoints; i++)
	{
		planesPoints->InsertNextPoint(pointsFrustum->GetPoint(i));
		double n[3], pointA[3], pointB[3], pointC[3];
		pointsFrustum->GetPoint(i + numberOfPoints, pointA);
		if ((i + 1) == numberOfPoints)//Last plane
		{
			pointsFrustum->GetPoint(0, pointB);
			pointsFrustum->GetPoint(i, pointC);
		}
		else//Planes in the side
		{
			pointsFrustum->GetPoint(i, pointB);
			pointsFrustum->GetPoint(i + 1, pointC);
		}
		Utils::getNormal(pointA, pointB, pointC, frustumCentroid, n);
		vtkMath::MultiplyScalar(n, -1);
		normalsPoints->InsertNextTuple3(n[0], n[1], n[2]);
	}
	vtkNew<vtkPlanes> planes;
	planes->SetPoints(planesPoints);
	planes->SetNormals(normalsPoints);
	return planes;
}

//----------------------------------------------------------------------------
void ColorTool::UpdateRepresentation()
{
	if (lineWidget)
	{
		vtkSmartPointer<vtkPlanes> planes = getSelectionFrustum();
		if (!planes)
		{
			return;
		}
		wxBeginBusyCursor();
		vtkMath::MultiplyScalar(colorMean, 0.0);
		//Get Mesh points
		const auto polyData = mesh->GetPolyData();
		vtkNew<vtkIdFilter> idFilter;
		idFilter->SetInputData(polyData);
		idFilter->SetPointIdsArrayName("OriginalIds");
		idFilter->Update();
		// This is needed to convert the ouput of vtkIdFilter (vtkDataSet) back to vtkPolyData
		vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
		surfaceFilter->SetInputConnection(idFilter->GetOutputPort());
		surfaceFilter->Update();
		vtkNew<vtkExtractPolyDataGeometry> extractGeometry;
		extractGeometry->SetImplicitFunction(planes);
		extractGeometry->SetInputConnection(surfaceFilter->GetOutputPort());
		extractGeometry->Update();
		//List of points inside the polygon
		vtkSmartPointer<vtkIdTypeArray> insideArray =
			vtkIdTypeArray::SafeDownCast(extractGeometry->GetOutput()->GetPointData()->GetArray("OriginalIds"));
		if (insideArray)
		{
			const auto numberOfSelectedPoints = insideArray->GetNumberOfTuples();
			const auto pointCloudColors = polyData->GetPointData()->GetScalars();
			for (vtkIdType i = 0; i < numberOfSelectedPoints; i++)
			{
				double color[4];
				pointCloudColors->GetTuple(insideArray->GetTuple1(i), color);
				vtkMath::Add(colorMean, color, colorMean);
			}
			vtkMath::MultiplyScalar(colorMean, 1 / (float)numberOfSelectedPoints);
		}
		else
		{
			wxMessageBox("Nothing was selected", "Warning", wxICON_WARNING);
			lineWidget->GetRepresentation()->reset();
			lineWidget->EnabledOff();
			lineWidget->EnabledOn();
		}
		UpdateSelection();
		CreateSelectionActor();
		// Remove line widget
		if (lineWidget)
		{
			lineWidget->EnabledOff();
			lineWidget->RemoveObserver(this->EventCallbackCommand);
			lineWidget = nullptr;
		}
		this->Interactor->Render();
		wxEndBusyCursor();
	}
	else
	{
		wxBeginBusyCursor();
		UpdateSelection();
		CreateSelectionActor();
		this->Interactor->Render();
		wxEndBusyCursor();
	}
}

void ColorTool::CreateSelectionActor()
{
	if(selectionActor)
	{
		CurrentRenderer->RemoveActor(selectionActor);
		selectionActor = nullptr;
	}
	selectionActor = Draw::createPointCloud(CurrentRenderer, mesh->GetPolyData(), selection, 255, 0, 0);
}

void ColorTool::UpdateSelection()
{
	double normalizedColorMean[3] = { colorMean[0], colorMean[1], colorMean[2] };
	vtkMath::Normalize(normalizedColorMean);
	const auto polyData = mesh->GetPolyData();
	const auto colors = polyData->GetPointData()->GetScalars();
	const auto numberOfPoints = polyData->GetNumberOfPoints();
	vtkNew<vtkIdTypeArray> insideArray;
	insideArray->SetNumberOfComponents(1);
	const auto angleThreshold = vtkMath::RadiansFromDegrees(
		static_cast<vtkSliderRepresentation *>(sliderAngle->GetRepresentation())->GetValue());
	const auto distanceThreshold = static_cast<vtkSliderRepresentation *>(sliderDistance->GetRepresentation())->GetValue();
#pragma omp parallel for
	for (int i = 0; i < numberOfPoints; i++)
	{
		double color[4];
		colors->GetTuple(i, color);
		double difference[3];
		vtkMath::Subtract(color, colorMean, difference);
		double distance = vtkMath::Norm(difference);
		vtkMath::Normalize(color);
		if (std::acos(vtkMath::Dot(normalizedColorMean, color)) <= angleThreshold &&
			distance <= distanceThreshold)
		{
#pragma omp critical
			insideArray->InsertNextTuple1(i);
		}
	}
	selection = vtkSmartPointer<vtkSelection>::New();
	vtkNew<vtkSelectionNode> selectionNode;
	selectionNode->SetFieldType(vtkSelectionNode::POINT);
	selectionNode->SetContentType(vtkSelectionNode::INDICES);
	selectionNode->SetSelectionList(insideArray);
	selection->AddNode(selectionNode);
}

void ColorTool::CreateSliders()
{
	vtkNew<SliderRepresentation2D> sliderRepresentationAngle;
	sliderRepresentationAngle->SetMinimumValue(0);
	sliderRepresentationAngle->SetMaximumValue(180);
	sliderRepresentationAngle->SetMinLimitText(wxString("0\u00B0").ToUTF8());
	sliderRepresentationAngle->SetMaxLimitText(wxString("180\u00B0").ToUTF8());
	sliderRepresentationAngle->SetValue(10);
	sliderRepresentationAngle->SetTitleText("Threshold angle");
	sliderRepresentationAngle->SetLabelFormat(wxString("%.2f \u00B0").ToUTF8());

	sliderAngle = vtkSmartPointer<vtkSliderWidget>::New();
	sliderAngle->SetInteractor(this->Interactor);
	sliderAngle->SetRepresentation(sliderRepresentationAngle);
	sliderAngle->SetAnimationModeToJump();
	sliderAngle->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
	sliderAngle->EnabledOn();

	vtkNew<SliderRepresentation2D> sliderRepresentationDistance;
	sliderRepresentationDistance->SetMinimumValue(0);
	sliderRepresentationDistance->SetMaximumValue(450);
	sliderRepresentationDistance->SetMinLimitText("0");
	sliderRepresentationDistance->SetMaxLimitText("450");
	sliderRepresentationDistance->SetValue(25);
	sliderRepresentationDistance->SetTitleText("Threshold distance");
	sliderRepresentationDistance->SetLabelFormat("%.f");
	sliderRepresentationDistance->Set2DPositionOffsets(200.0, 0.0);

	sliderDistance = vtkSmartPointer<vtkSliderWidget>::New();
	sliderDistance->SetInteractor(this->Interactor);
	sliderDistance->SetRepresentation(sliderRepresentationDistance);
	sliderDistance->SetAnimationModeToJump();
	sliderDistance->AddObserver(vtkCommand::EndInteractionEvent, this->EventCallbackCommand, this->Priority);
	sliderDistance->EnabledOn();
}

bool ColorTool::enterKeyPressed()
{
	if (!selection)
	{
		return false;
	}
	wxBeginBusyCursor();
	auto poly = mesh->GetPolyData();
	if (poly->GetNumberOfCells() != selection->GetNode(0)->GetSelectionList()->GetNumberOfValues())
	{
		selection->GetNode(0)->GetProperties()->Set(vtkSelectionNode::INVERSE(),
			!selection->GetNode(0)->GetProperties()->Get(vtkSelectionNode::INVERSE()));
		vtkNew<vtkExtractSelectedIds> extractSelectedIds;
		extractSelectedIds->SetInputData(0, poly);
		extractSelectedIds->SetInputData(1, selection);
		extractSelectedIds->Update();

		vtkNew<vtkDataSetSurfaceFilter> surfaceFilter;
		surfaceFilter->SetInputConnection(extractSelectedIds->GetOutputPort());
		surfaceFilter->Update();

		poly = surfaceFilter->GetOutput();
	}
	else
	{
		//Delete all mesh
		poly = vtkSmartPointer<vtkPolyData>::New();
	}
	mesh->UpdateData(poly);
	this->SetEnabled(false);
	wxEndBusyCursor();
	return true;
}
