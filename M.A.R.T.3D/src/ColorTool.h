#pragma once
#include <vtkInteractionWidgetsModule.h> // For export macro
#include <vtk3DWidget.h>
#include <vtkSmartPointer.h>

#include <vector>

class vtkPlanes;
class vtkActor;
class vtkPolyData;
class vtkSelection;
class vtkSliderWidget;
class LineWidget;
class Mesh;

class ColorTool : public vtk3DWidget
{
public:
    /**
    * Instantiate the object.
    */
    static ColorTool *New();

    vtkTypeMacro(ColorTool, vtk3DWidget);

    //@{
    /**
    * Methods that satisfy the superclass' API.
    */
    virtual void SetEnabled(int);
	virtual void PlaceWidget(double bounds[6]) {};
    //@}

    //@{
    /**
    * Set the mesh that is going to be used
    */
	void setMesh(Mesh* mesh) { this->mesh = mesh; };
    //@}

    bool enterKeyPressed();

protected:
	ColorTool();
    ~ColorTool();

    //handles the events
    static void ProcessEvents(vtkObject* object, unsigned long event,
        void* clientdata, void* calldata);

    vtkSmartPointer<LineWidget> lineWidget = nullptr;

	//Selection
	vtkSmartPointer<vtkPlanes> getSelectionFrustum();

    // Controlling ivars
    void UpdateRepresentation();
    void CreateSelectionActor();
	void UpdateSelection();
	double colorMean[3];

    Mesh* mesh = nullptr;

	vtkSmartPointer<vtkActor> selectionActor;
	vtkSmartPointer<vtkSelection> selection;

	//Slider
	vtkSmartPointer<vtkSliderWidget> sliderDistance = nullptr;
	vtkSmartPointer<vtkSliderWidget> sliderAngle = nullptr;
	void CreateSliders();

private:
	ColorTool(const ColorTool&) VTK_DELETE_FUNCTION;
    void operator=(const ColorTool&) VTK_DELETE_FUNCTION;
};
