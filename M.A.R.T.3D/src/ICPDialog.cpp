#include "ICPDialog.h"

#include <sstream>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkTransform.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/msgdlg.h>

#include "ICP.h"
#include "Project.h"
#include "Mesh.h"
#include "Utils.h"

wxBEGIN_EVENT_TABLE(ICPDialog, wxDialog)
	EVT_CHECKBOX(idCkUseMeanDistance, ICPDialog::OnCkUseMeanDistance)
	EVT_BUTTON(idBtApplyTransform, ICPDialog::OnApplyTransform)
	EVT_BUTTON(idBtRevertTransform, ICPDialog::OnRevertTransform)
	EVT_BUTTON(idBtStartICP, ICPDialog::OnBtStartICP)
	EVT_BUTTON(idBtChangeMeshOrder, ICPDialog::OnBtChangeMeshOrder)
	EVT_CLOSE(ICPDialog::OnClose)
wxEND_EVENT_TABLE()

ICPDialog::ICPDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos,
	const wxSize & size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	//Used to get the default options
	ICP icp;

	wxBoxSizer* bSizer = new wxBoxSizer(wxVERTICAL);

	wxFlexGridSizer* fgSizer = new wxFlexGridSizer(7, 2, 0, 0);
	fgSizer->SetFlexibleDirection(wxBOTH);
	fgSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Maximum number of iterations"), 0, wxALL, 5);
	spinMaxNumberOfIterations = new wxSpinCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 16384L, 1, 1000000, icp.getMaxNumberOfIterations());
	fgSizer->Add(spinMaxNumberOfIterations, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Maximum number of landmarks"), 0, wxALL, 5);
	spinMaxNumberOfLandmarks = new wxSpinCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 16384L, 4, 1000000, icp.getMaxNumberOfLandmarks());
	fgSizer->Add(spinMaxNumberOfLandmarks, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Maximum mean distance"), 0, wxALL, 5);
	spinMaxMeanDistance = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 16384L, 0.0000001, 100.0, icp.getMaxMeanDistance());
	spinMaxMeanDistance->Enable(icp.getUseMeanDistance());
	fgSizer->Add(spinMaxMeanDistance, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Maximum closest point distance"), 0, wxALL, 5);
	spinClosestPointMaxMeanDistance = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 16384L, 0.0000001, 100.0, icp.getClosestPointMaxDistance());
	fgSizer->Add(spinClosestPointMaxMeanDistance, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Use mean distance"), 0, wxALL, 5);
	ckUseMeanDistance = new wxCheckBox(this, idCkUseMeanDistance, wxEmptyString);
	ckUseMeanDistance->SetValue(icp.getUseMeanDistance());
	fgSizer->Add(ckUseMeanDistance, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Start matching centroids"), 0, wxALL, 5);
	ckStartMatchingCentroids = new wxCheckBox(this, wxID_ANY, wxEmptyString);
	ckStartMatchingCentroids->SetValue(icp.getStartByMatchingCentroids());
	fgSizer->Add(ckStartMatchingCentroids, 0, wxALL, 5);

	fgSizer->Add(new wxStaticText(this, wxID_ANY, "Delimit by minimum bouding box"), 0, wxALL, 5);
	ckDelimitByMinimumBoundingBox = new wxCheckBox(this, wxID_ANY, wxEmptyString);
	fgSizer->Add(ckDelimitByMinimumBoundingBox, 0, wxALL, 5);
	
	bSizer->Add(fgSizer, 0, wxEXPAND, 5);

	auto bSizerBts = new wxBoxSizer(wxHORIZONTAL);

	bSizerBts->Add(new wxButton(this, idBtStartICP, "Start ICP"), 0, wxALL, 5);

	bSizerBts->Add(new wxButton(this, idBtChangeMeshOrder, "Invert Source/Target"), 0, wxALL, 5);

	auto bSizerBts2 = new wxBoxSizer(wxHORIZONTAL);

	btApplyTransform = new wxButton(this, idBtApplyTransform, "Apply transform");
	btApplyTransform->Disable();
	bSizerBts2->Add(btApplyTransform, 0, wxALL, 5);

	btRevertTransform = new wxButton(this, idBtRevertTransform, "Revert transform");
	btRevertTransform->Disable();
	bSizerBts2->Add(btRevertTransform, 0, wxALL, 5);

	bSizer->Add(bSizerBts, 0, wxALIGN_LEFT, 5);
	bSizer->Add(bSizerBts2, 0, wxALIGN_LEFT, 5);
	
	this->SetSizer(bSizer);
	this->Layout();

	this->Centre(wxBOTH);
}

ICPDialog::~ICPDialog()
{
	meshSource = nullptr;
	meshTarget = nullptr;
	projectSource = nullptr;
	projectTarget = nullptr;
}

void ICPDialog::OnCkUseMeanDistance(wxCommandEvent & WXUNUSED)
{
	spinMaxMeanDistance->Enable(ckUseMeanDistance->GetValue());
}

void ICPDialog::OnApplyTransform(wxCommandEvent & WXUNUSED)
{
	if (!T)
	{
		wxMessageBox("You need to start the ICP first.", "Error", wxICON_ERROR);
		return;
	}
	if (renderer)
	{
		//In case the user already reverted the transform
		if (T->GetInverseFlag())
		{
			T->Inverse();
		}
		projectSource->Transform(T);
		// We need to update the text again, since transform will add the * in the tree item
		treeMesh->SetItemText(meshSource->GetTreeItem(), "Source - ICP tool");
		renderer->GetRenderWindow()->Render();
		btApplyTransform->Disable();
		btRevertTransform->Enable();
	}
}

void ICPDialog::OnRevertTransform(wxCommandEvent & WXUNUSED)
{
	if (!T)
	{
		wxMessageBox("You need to start the ICP first.", "Error", wxICON_ERROR);
		return;
	}
	if (renderer)
	{
		T->Inverse();
		projectSource->Transform(T);
		// We need to update the text again, since transform will add the * in the tree item
		treeMesh->SetItemText(meshSource->GetTreeItem(), "Source - ICP tool");
		renderer->GetRenderWindow()->Render();
		btApplyTransform->Enable();
		btRevertTransform->Disable();
	}
}

void ICPDialog::OnBtStartICP(wxCommandEvent & WXUNUSED)
{
	btApplyTransform->Disable();
	btRevertTransform->Disable();
	ICP icp;
	//Parameters
	icp.setMaxNumberOfIterations(spinMaxNumberOfIterations->GetValue());
	icp.setMaxNumberOfLandmarks(spinMaxNumberOfLandmarks->GetValue());
	icp.setMaxMeanDistance(spinMaxMeanDistance->GetValue());
	icp.setClosestPointMaxDistance(spinClosestPointMaxMeanDistance->GetValue());
	icp.setStartByMatchingCentroids(ckStartMatchingCentroids->GetValue());
	icp.setUseMeanDistance(ckUseMeanDistance->GetValue());
	//Inputs
	icp.setSourcePoints(meshSource->GetPolyData()->GetPoints());
	icp.setTargetPoints(meshTarget->GetPolyData()->GetPoints());
	//Process
	if (ckDelimitByMinimumBoundingBox->GetValue())
	{
		icp.delimitByMinimumBoudingBox();
	}
	if (!icp.executeICP())
	{
		return;
	}
	if (!T)
	{
		T = vtkSmartPointer<vtkTransform>::New();
	}
	T = icp.getTransform();
	if (ckUseMeanDistance->GetValue())
	{
		std::stringstream stringStream;
		stringStream << "ICP finished, mean distance: " << std::setprecision(10) << icp.getMeanDistance() << ".";
		wxMessageBox(stringStream.str(), "Information", wxICON_INFORMATION);
	}
	Utils::dialogSaveTransform(T);
	btApplyTransform->Enable();
}

void ICPDialog::OnBtChangeMeshOrder(wxCommandEvent & WXUNUSED)
{
	auto tempMesh = meshSource;
	meshSource = meshTarget;
	meshTarget = tempMesh;
	auto tempProject = projectSource;
	projectSource = projectTarget;
	projectTarget = tempProject;
	treeMesh->SetItemText(meshSource->GetTreeItem(), "Source - ICP tool");
	treeMesh->SetItemText(meshTarget->GetTreeItem(), "Target - ICP tool");
	btApplyTransform->Disable();
	btRevertTransform->Disable();
	T = nullptr;
}

void ICPDialog::OnClose(wxCloseEvent & WXUNUSED)
{
	btApplyTransform->Disable();
	btRevertTransform->Disable();
	treeMesh->SetItemText(meshSource->GetTreeItem(), Utils::getFileName(meshSource->GetFilename()));
	treeMesh->SetItemText(meshTarget->GetTreeItem(), Utils::getFileName(meshTarget->GetFilename()));
	T = nullptr;
	meshSource = nullptr;
	meshTarget = nullptr;
	this->Hide();
}
