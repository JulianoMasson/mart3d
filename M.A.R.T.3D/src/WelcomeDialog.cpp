#include "WelcomeDialog.h"

#include <wx/sizer.h>
#include <wx/dataview.h>
#include <wx/button.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/log.h>
#include <wx/progdlg.h>
#include <wx/stdpaths.h>
#include <wx/dir.h>

#include "ProjectNameWizardPage.h"
#include "ProjectImagesWizardPage.h"
#include "ProjectTemplateWizardPage.h"
#include "ReconstructionLog.h"
#include "Reconstruction.h"
#include "Calibration.h"
#include "Utils.h"

wxBEGIN_EVENT_TABLE(WelcomeDialog, wxDialog)
EVT_BUTTON(idCreateNewProject, WelcomeDialog::OnCreateNewProject)
EVT_BUTTON(idLoadProject, WelcomeDialog::OnLoadProject)
EVT_BUTTON(idImportProject, WelcomeDialog::OnImportProject)
EVT_DATAVIEW_ITEM_ACTIVATED(idDataView, WelcomeDialog::OnDataViewItemActivated)
wxEND_EVENT_TABLE()

WelcomeDialog::WelcomeDialog(wxWindow * parent, wxWindowID id, const wxString & title, const wxPoint & pos, const wxSize & size, long style)
	: wxDialog(parent, id, title, pos, size, style), projectPath("")
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	auto bSizer = new wxBoxSizer(wxHORIZONTAL);

	dataView = new wxDataViewListCtrl(this, idDataView, wxDefaultPosition, wxSize(500, 150));
	dataView->AppendTextColumn("Name", wxDATAVIEW_CELL_INERT, 100);
	//dataView->AppendTextColumn("Creation data", wxDATAVIEW_CELL_INERT, 90, wxALIGN_CENTER);
	dataView->AppendTextColumn("Point cloud", wxDATAVIEW_CELL_INERT, 80, wxALIGN_CENTER);
	dataView->AppendTextColumn("Surface", wxDATAVIEW_CELL_INERT, 55, wxALIGN_CENTER);
	dataView->AppendTextColumn("Textured Surface", wxDATAVIEW_CELL_INERT, 100, wxALIGN_CENTER);
	//dataView->AppendTextColumn("GPS", wxDATAVIEW_CELL_INERT, 20, wxALIGN_CENTER);

	// Load projects
	wxStandardPaths stdPaths = wxStandardPaths::Get();
	const auto smartPath = stdPaths.GetDocumentsDir() + "\\" + "SMART3D";
	wxDir dir(smartPath);
	dir.GetAllFiles(smartPath, &projectList, "*.project", wxDIR_DIRS | wxDIR_FILES);
	for (const auto& dirName : projectList)
	{
		std::string dirNameStr = dirName;
		std::string dirNamePathOnly = wxPathOnly(dirNameStr) + "\\3DData\\";
		wxVector<wxVariant> data;
		data.push_back(wxVariant(Utils::getFileName(dirNameStr, false)));
		if (Utils::exists(dirNamePathOnly + "PointCloud.ply"))
		{
			data.push_back(wxVariant("Yes"));
		}
		else
		{
			data.push_back(wxVariant("No"));
		}
		if (Utils::exists(dirNamePathOnly + "Surface.ply"))
		{
			data.push_back(wxVariant("Yes"));
		}
		else
		{
			data.push_back(wxVariant("No"));
		}
		if (Utils::exists(dirNamePathOnly + "TexturedSurface\\TexturedSurface.obj"))
		{
			data.push_back(wxVariant("Yes"));
		}
		else
		{
			data.push_back(wxVariant("No"));
		}
		dataView->AppendItem(data);
	}
	bSizer->Add(dataView, 0, wxALL, 5);

	auto bSizerBts = new wxBoxSizer(wxVERTICAL);

	bSizerBts->AddSpacer(25);
	bSizerBts->Add(new wxButton(this, idCreateNewProject, "Create new project..."), 0, wxALL | wxALIGN_CENTER, 5);
	bSizerBts->Add(new wxButton(this, idImportProject, "Import project..."), 0, wxALL | wxALIGN_CENTER, 5);
	bSizerBts->Add(new wxButton(this, idLoadProject, "Load project"), 0, wxALL | wxALIGN_CENTER, 5);

	bSizer->Add(bSizerBts);

	this->SetSizer(bSizer);
	this->Layout(); 
	this->CenterOnParent();
}

void WelcomeDialog::ShowNewProjectWizard()
{
	OnCreateNewProject((wxCommandEvent)NULL);
}

void WelcomeDialog::OnCreateNewProject(wxCommandEvent & WXUNUSED)
{
	auto wizard = new wxWizard(this, wxID_ANY, "New Project");
	wizard->SetPageSize(wxSize(700, 400));
	std::vector<wxWizardPageSimple*> wizardPages;
	wizardPages.emplace_back(new ProjectNameWizardPage(wizard));
	wizardPages.emplace_back(new ProjectImagesWizardPage(wizard));
	wizardPages.emplace_back(new ProjectTemplateWizardPage(wizard));
	for (size_t i = 1; i < wizardPages.size(); i++)
	{
		wizardPages[i]->SetPrev(wizardPages[i - 1]);
		wizardPages[i - 1]->SetNext(wizardPages[i]);
	}
	if (wizard->RunWizard(wizardPages[0]))
	{
		//Name
		const auto namePage = dynamic_cast<ProjectNameWizardPage*>(wizardPages[0]);
		const auto projectFolder = namePage->GetProjectPath();
		if (!Utils::CreateDir(projectFolder))
		{
			return;
		}
		//Write the project file
		std::ofstream projectFile(projectFolder + "\\" + Utils::GetLastDirName(projectFolder) + ".project");
		std::string line;
		if (projectFile.is_open())
		{
			projectFile << "is360=0\n";
			projectFile.close();
		}
		else
		{
			wxLogError("Unable to create the project file");
			return;
		}
		//Images
		const auto imagesPage = dynamic_cast<ProjectImagesWizardPage*>(wizardPages[1]);
		const auto imagesPaths = imagesPage->GetImagesPath();
		const auto imagesFolder = projectFolder + "\\images";
		if (!Utils::CreateDir(imagesFolder))
		{
			return;
		}
		wxProgressDialog* progressDialog;
		if (imagesPage->GetMoveImagesToProjectDir())
		{
			progressDialog = new wxProgressDialog("Moving images", "Moving images", imagesPaths.size(), this);
		}
		else
		{
			progressDialog = new wxProgressDialog("Copying images", "Copying images", imagesPaths.size(), this);
		}
		unsigned int imageCount = 0;
		for (const auto& path : imagesPaths)
		{
			const auto newPath = imagesFolder + "\\" + Utils::getFileName(path.ToStdString());
			if (!wxCopyFile(path, newPath))
			{
				wxLogError("Error coping file " + path);
			}
			else if (imagesPage->GetMoveImagesToProjectDir())
			{
				if (!wxRemoveFile(path))
				{
					wxLogError("Error removing file " + path);
				}
			}
			progressDialog->Update(imageCount);
			imageCount++;
		}
		delete progressDialog;
		//Project options
		const auto generateTexture = dynamic_cast<ProjectTemplateWizardPage*>(wizardPages[2])->GetGenerateTexture();
		//Start processing
		Reconstruction::Reconstruct(projectFolder, generateTexture);
		//Add the calibration default file
		Calibration calib;
		calib.save(projectFolder + "\\calibration.calib");
		projectPath = projectFolder + "\\" + Utils::GetLastDirName(projectFolder) + ".project";
		EndModal(wxOK);
	}
}

void WelcomeDialog::OnLoadProject(wxCommandEvent & WXUNUSED)
{
	const auto selectedRow = dataView->GetSelectedRow();
	if (selectedRow != -1)
	{
		projectPath = projectList.Item(selectedRow);
		EndModal(wxOK);
	}
	else
	{
		wxMessageBox("Select some project in the table", "Information", wxICON_INFORMATION);
	}
}

void WelcomeDialog::OnImportProject(wxCommandEvent & WXUNUSED)
{
	wxFileDialog openDialog(this, "Find some cool project", "", "",
		"PROJECT file (*.project)|*.project", wxFD_FILE_MUST_EXIST);
	if (openDialog.ShowModal() == wxID_OK)
	{
		projectPath = openDialog.GetPath();
		EndModal(wxOK);
	}
}

void WelcomeDialog::OnDataViewItemActivated(wxDataViewEvent & WXUNUSED)
{
	OnLoadProject((wxCommandEvent) NULL);
}
