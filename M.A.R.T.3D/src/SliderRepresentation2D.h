#pragma once
#include <vtkSliderRepresentation.h>
#include <vtkSmartPointer.h>

class vtkTextActor;
class vtkTextProperty;
class vtkActor2D;
class vtkTransformFilter;
class Calibration;

class SliderRepresentation2D : public vtkSliderRepresentation
{
public:
	/**
	* Instantiate the class.
	*/
	static SliderRepresentation2D *New();

	//@{
	/**
	* Standard methods for the class.
	*/
	vtkTypeMacro(SliderRepresentation2D, vtkSliderRepresentation);
	//@}

	//@{
	/**
	* Specify the label text for this widget. If the value is not set, or set
	* to the empty string "", then the label text is not displayed.
	*/
	void SetTitleText(const char*) VTK_OVERRIDE;
	const char* GetTitleText() VTK_OVERRIDE;
	//@}

	//@{
	/**
	* Set the text Limits.
	*/
	void SetMaxLimitText(const char* text);
	void SetMinLimitText(const char* text);
	//@}

	//@{
	/**
	* Methods to interface with the vtkSliderWidget. The PlaceWidget() method
	* assumes that the parameter bounds[6] specifies the location in display space
	* where the widget should be placed.
	*/
	void PlaceWidget(double bounds[6]) VTK_OVERRIDE;
	void BuildRepresentation() VTK_OVERRIDE;
	void StartWidgetInteraction(double eventPos[2]) VTK_OVERRIDE;
	void WidgetInteraction(double newEventPos[2]) VTK_OVERRIDE;
	void Highlight(int) VTK_OVERRIDE;
	//@}

	//@{
	/**
	* Methods supporting the rendering process.
	*/
	void GetActors2D(vtkPropCollection*) VTK_OVERRIDE;
	void ReleaseGraphicsResources(vtkWindow*) VTK_OVERRIDE;
	int RenderOverlay(vtkViewport*) VTK_OVERRIDE;
	int RenderOpaqueGeometry(vtkViewport*) VTK_OVERRIDE;
	//@}

	void setCalibration(Calibration* calib) { this->calib = calib; };

	void setUseMaxSlider(bool useMaxSlider) 
	{ 
		this->useMaxSlider = useMaxSlider;
		this->sliderMaxValue = this->GetMaximumValue();
	};

	double getSliderMinValue() const { return this->Value; };
	double getSliderMaxValue() const 
	{ 
		if (useMaxSlider)
		{
			return this->sliderMaxValue;
		}
		return this->Value;
	};

	void Set2DPositionOffsets(double offsetX, double offsetY) 
	{ 
		this->xCenterOffset = offsetX; 
		this->yCenterOffset = offsetY; 
	};

protected:
	SliderRepresentation2D();
	~SliderRepresentation2D() VTK_OVERRIDE;

	// Determine the parameter t along the slider
	virtual double ComputePickPosition(double eventPos[2]);


	//Texts
	vtkSmartPointer<vtkTextProperty> textProperty = nullptr;
	vtkSmartPointer<vtkTextActor> titleTextActor = nullptr;
	vtkSmartPointer<vtkTextActor> minTextActor = nullptr;
	vtkSmartPointer<vtkTextActor> maxTextActor = nullptr;
	vtkSmartPointer<vtkTextActor> sliderMinValueTextActor = nullptr;
	//Optional
	vtkSmartPointer<vtkTextActor> sliderMaxValueTextActor = nullptr;
	//Slider
	//Used to adjust multiple sliders in teh screen
	double xCenterOffset = 0;
	double yCenterOffset = 0;
	double xLenghtTube = 80;
	double yLenghtTube = 2;
	double sliderRadius = 8;
	double xMinTube = -1;
	double xMaxTube = -1;
	double yMinTube = -1;
	double yMaxTube = -1;
	double xMinSliderMin = -1;
	double xMaxSliderMin = -1;
	double yMinSliderMin = -1;
	double yMaxSliderMin = -1;
	double xMinSliderMax = -1;
	double xMaxSliderMax = -1;
	double yMinSliderMax = -1;
	double yMaxSliderMax = -1;
	vtkSmartPointer<vtkActor2D> sliderTubeActor = nullptr;
	vtkSmartPointer<vtkActor2D> sliderMinActor = nullptr;
	//Optional
	vtkSmartPointer<vtkActor2D> sliderMaxActor = nullptr;

	vtkSmartPointer<vtkTransformFilter> sliderTubeTransFilter;
	vtkSmartPointer<vtkTransformFilter> sliderTransFilter;

	// internal variables used for computation
	double X;
	//0 min slider, 1 max slider
	int selectedSlider = -1;

	bool useMaxSlider = false;
	double sliderMaxValue;

	Calibration* calib = nullptr;

private:
	SliderRepresentation2D(const SliderRepresentation2D&) VTK_DELETE_FUNCTION;
	void operator=(const SliderRepresentation2D&) VTK_DELETE_FUNCTION;
};