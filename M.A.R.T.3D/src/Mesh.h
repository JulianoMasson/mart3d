#pragma once

#include <vtkSmartPointer.h>
#include <wx/treelist.h>

class vtkPolyData;
class vtkRenderer;
class vtkTexture;
class vtkAbstractPicker;
class vtkTransform;
class vtkPoints;
class vtkUnsignedCharArray;
class vtkFloatArray;
class vtkDataObject;
class vtkActor;
class vtkCaptionActor2D;

class Mesh 
{
private:
	vtkSmartPointer<vtkRenderer> renderer;
	wxTreeListCtrl* tree;
	// Tree
	wxTreeListItem treeItem;
	wxTreeListItem treeItemTexture;
	// Data
	vtkSmartPointer<vtkPolyData> polyData;
	// Name used in the tree and to Save the file
	std::string filename;
	// Tests
	bool loaded;
	bool isPointCloud;
	bool hasColor;
	bool hasTexture;
	bool visible;
	bool textureVisibility;
	// Used to know if we need to Save this mesh
	bool modified;
	// Used to know if this file was already saved on disk
	bool onDisk;

	void CreateMesh();

public:
	// Create a mesh from a file
	Mesh(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree, const std::string& filePath);
	// Create a mesh from a poly data
	Mesh(vtkSmartPointer<vtkRenderer> renderer, wxTreeListCtrl* tree, const std::string& filename, vtkSmartPointer<vtkPolyData> polyData);
	Mesh() {};
	~Mesh();

	void DestructTextures();

	std::vector<vtkSmartPointer<vtkActor>> actors;
	std::vector<std::string> textureNames;
	std::vector<vtkSmartPointer<vtkTexture>> textures;

	void SetFilename(const std::string& filename);
	std::string GetFilename() const { return filename; };

	bool GetIsLoaded() const { return loaded; };
	bool GetIsPointCloud() const { return isPointCloud; };
	bool GetHasColor() { return hasColor; };
	bool GetHasTexture() { return hasTexture; };
	void SetModified(bool modified);
	bool GetModified() const { return modified; };
	bool GetOnDisk() const { return onDisk; };

	// Export the mesh without checking the modifed variable
	bool Export(const std::string& filePath);
	// Save the mesh only if (modifed == true or forceSaving == true)
	bool Save(const std::string& dirPath, bool forceSaving = false);

	//Create a pickList from the actors, and set the picker to pick from list
	void CreatePickList(vtkSmartPointer<vtkAbstractPicker> picker);

	void SetVisibility(bool visible);
	bool GetVisibility() const { return visible; };

	//Enable/disable the textures
	void SetTextureVisibility(bool visible);
	bool GetTextureVisibility() const { return textureVisibility; };

	//Get the PolyData of the mesh
	vtkSmartPointer<vtkPolyData> GetPolyData();

	//Tranform the mesh using T
	void Transform(vtkSmartPointer<vtkTransform> T);

	//Update the mesh data
	void UpdateData(vtkDataObject* data);
	//Update the data of an specific actor
	void UpdateData(vtkDataObject* data, unsigned int actorIndex);

	//Tree
	//Set the wxTreeItem that represents this mesh on the tree
	void SetTreeItem(const wxTreeListItem& listItem) { treeItem = listItem; };
	wxTreeListItem GetTreeItem() const { return treeItem; };

	//Set the wxTreeItem that represents the texture of this mesh on the tree
	void SetTreeItemTexture(const wxTreeListItem& listItem) { treeItemTexture = listItem; };
	wxTreeListItem GetTreeItemTexture() const { return treeItemTexture; };
};
